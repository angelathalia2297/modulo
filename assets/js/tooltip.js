$(function () {
  $(document).tooltip({
    position: {
      my: "center top",
      at: "center bottom+5",
    },
    show: {
      duration: "fast"
    },
    hide: {
      effect: "hide"
    }
  });
});