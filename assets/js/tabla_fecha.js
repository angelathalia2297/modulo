$(document).ready(function() {
	minDateFilter = "";
	maxDateFilter = "";
	$.fn.dataTableExt.afnFiltering.push(
	function(oSettings, aData, iDataIndex) {
		if (typeof aData._date == 'undefined') {
		aData._date = new Date(aData[0]).getTime();
		}

		if (minDateFilter && !isNaN(minDateFilter)) {
		if (aData._date < minDateFilter) {
			return false;
		}
		}

		if (maxDateFilter && !isNaN(maxDateFilter)) {
		if (aData._date > maxDateFilter) {
			return false;
		}
		}

		return true;
	}
	);
	$(document).ready(function() {
	$("#Date_search").val("");
	

}); 
	var table = $('#vigilancia').DataTable( {
        "paging":   true,
		"ordering": true,
		
        columnDefs: [
   		{ 
		   orderable: false, 
		   targets:  "no-sort"  }
		],
		"order": [[0, 'desc']], 
        "info":     true,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50,"Todo"]],
        language: {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "searchPlaceholder": "Ingrese un Dato",
            "info": "_START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "Dato no encontrado",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
     
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
	}
	);

	$("#Date_search").daterangepicker({
	"locale": {
		"format": "YYYY-MM-DD",
		"separator": "  al  ",
		"applyLabel": "Aceptar",
		"cancelLabel": "Cancelar",
		"fromLabel": "From",
		"toLabel": "To",
		"customRangeLabel": "Custom",
		"weekLabel": "W",
		"daysOfWeek": [
		"Do",
		"Lu",
		"Ma",
		"Mi",
		"Ju",
		"Vi",
		"Sa"
		],
		"monthNames": [
		"Enero",
		"Febrero",
		"Marzo",
		"April",
		"Mayo",
		"Junio",
		"Julio",
		"Agosto",
		"Septiembre",
		"Octubre",
		"Noviembre",
		"Diciembre"
		],
		"firstDay": 1
	},
	"opens": "center",
	}, function(start, end, label) {
	maxDateFilter = end;
	minDateFilter = start;
	table.draw();  
    });
    
  } );
