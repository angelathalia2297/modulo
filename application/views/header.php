<!DOCTYPE html>
<html>

<head>
	<title>Vigilancia AV</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=0, minimal-ui">
    <!-- LOS "script" CONECTAN AL JQUERY 3.3.1  y las funciones -->
	<script src="<?php echo base_url('assets/plugins/jQuery/jquery-3.3.1.slim.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/plugins/jQuery/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/funciones.js'); ?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/funciones.css'); ?>">
	<script src="<?php echo base_url('assets/js/mayus.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/change.js'); ?>"></script>
	<script src="<?php echo base_url('assets/plugins/jQuery/jquery-3.3.1.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/plugins/boostrap_4_0/js/bootstrap.min.js'); ?>"></script>
	<link rel="icon" type="icon/ico" href="<?php echo base_url('assets/imgs/logo_cnsr.png'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/boostrap_4_0/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/_variables.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/styles.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/dx-header.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/dx-footer.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/fonts/feather/style.min.css'); ?>">
	
 <!-- <script src="<?php echo base_url('assets/js/tooltip.js'); ?>"></script>  
	 -->

    <!-- script de comobo complicaciones-->	
    <!-- <script src="<?php echo base_url('assets/plugins/select2/select2.min.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/select2/select2.min.css'); ?>">
 <script>
                    $(document).ready(function() {
                        $('.js-example-basic-multiple').select2();
                        $('.js-example-basic-single').select2();
                      
                    });
                    </script> -->
  
</head>
 
<body>

	<div class="dx-header">
		<nav class="navbar navbar-expand-md bg-dx-primary justify-content-between">
			<div class="navbar-collapse collapse dual-nav w-100"></div>
			
			<?php  if(($Cenasicod == 401)) {?>
		<a  class="navbar-brand mx-auto d-block text-center w-100 dx-title">
			<img src="<?php echo base_url('assets/imgs/logo_cnsr.png') ?>" class="dx-essalud" /> Centro Nacional de Salud Renal</a>
			<?php }?>

			<?php  if(($Cenasicod == 001)) {?>
				<a  class="navbar-brand mx-auto d-block text-center w-100 dx-title">
					<img src="<?php echo base_url('assets/imgs/essalud.jpg') ?>" class="dx-essalud" /> H.N. EDGARDO REBAGLIATI MARTINS</a>
			<?php }?>

		<?php  if(($Cenasicod == 002)) {?>
				<a  class="navbar-brand mx-auto d-block text-center w-100 dx-title">
					<img src="<?php echo base_url('assets/imgs/essalud.jpg') ?>" class="dx-essalud" /> H.N. GUILLERMO ALMENARA IRIGOYEN</a>
			<?php }?>
			<?php  if(($Cenasicod == 005)) {?>
				<a  class="navbar-brand mx-auto d-block text-center w-100 dx-title">
					<img src="<?php echo base_url('assets/imgs/essalud.jpg') ?>" class="dx-essalud" /> H.IV ALBERTO SABOGAL SOLOGUREN</a>
			<?php }?>

			<div class="navbar-collapse collapse dual-nav w-100">
				<ul class="nav navbar-nav ml-auto">
					<li class="nav-item dropdown">
					<meta charset='utf-8'> 
					<a id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle mr-3 mr-lg-0">
					  <?php 
						ini_set('date.timezone','America/Lima'); 
						//echo date("g:i A"); 
						$date= date("Y-m-d");
						//$time=date("g:i A");
						$datetime=$date;
						7//echo date_format($date, 'd/m/Y H:i:s');		//2184 juan carlos aguilar 
					?>
				 <?php echo 'Fecha:  '. $datetime; ?>
					  <i class="ft-user"></i> <strong>  <?php echo($Usuario);?> </strong>
							<span class="caret"></span>
						</a>
						
						<div aria-labelledby="navbarDropdownMenuLink" class="dropdown-menu dropdown-menu-right">
							 <a data-toggle="modal" data-target="#exampleModal"class="dropdown-item"><i class="ft-log-out"> </i>Perfil</a>
							<a  data-toggle="modal" data-target="#exampleModalCenter"class="dropdown-item"><i class="ft-log-out"> </i> Cerrar Sesión</a>
						</div>
					
					</li>
				</ul>
			</div>
		</nav>
		 
		<!-- Modal -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">DATOS DEL USUARIO</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				
			<div class="row">
        <div class="col-md-11 mx-auto">
          <div class="card rounded-0">
            <div class="card-header">
            <div class="text-center">
</div>
            <center> <h2 class="page-header"><p class="text-primary font-weight-light">
                    <?php echo $Nombre?> 
			</h2>	
                        
            </div>
            <div class="card-body">
 <center>
       <p class="card-text"> <h5 > 
         </p>
        
     
<div class="row">
  <div class="col-sm-6">
      <div class="card border-secondary  mb-3" style="max-width: 18rem;">
              <div class="card-body text-secondary ">
                <h4 class="card-title"> Nivel Usuario :</h5>
                <p class="lead"> </p>

                <?php 
                  if ($NivelUsuario== 1) {
                      echo('SUPER-ADMINISTRADOR');

                    } 
                    elseif ($NivelUsuario == 2) 
                      {
                        echo('ADMINISTRADOR');
                      }
                      elseif ($NivelUsuario == 3) 
                        {
                          echo('NEFROLOGO');
                        }
                        else
                          {
                            echo('SUPERVISOR - EVALUADOR');
                  }
               
                ?>
              </div>
    </div>

  </div>
  <div class="col-sm-6">
      <div class="card border-secondary  mb-3" style="max-width: 18rem;">
                          <div class="card-body text-secondary ">
                            <h4 class="card-title"> Estado :</h5>
                            <p class="lead"><?php echo('ACTIVO');?> </p>
                       </div>
                        
            </div>
  </div>

  <div class="col-sm-6">
  <div class="card border-secondary mb-3" style="max-width: 18rem;">
                          <div class="card-body text-secondary ">
                            <h4 class="card-title"> Fecha de Alta :</h5>
                            <p class="lead"><?php  echo($FechaAlta);?> 
                            </p>
                       </div>
                        
            </div>

  </div>
  <div class="col-sm-6">
        <div class="card border-secondary  mb-3" style="max-width: 18rem;">
                          <div class="card-body text-secondary ">
                            <h4 class="card-title">  Ultimo Acceso :</h5>
                            <p class="lead"><?php  echo($UltimoAcceso);?> </p>

                       </div>
                        
            </div>
  </div>
  </div>
          </div>
        </div>
      </div>
</div>



			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
				
			</div>
			</div>
		</div>
		</div>

		<!-- Modal -->
			<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
				<div class="modal-header">

				<div class="form-group col-md-9">
							
							<span class="ft-alert-triangle"></span> 
							<strong><span>  Aviso </span>
							</strong>
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"> <center>
				<p class="text-black font-weight-light"> ¿Seguro que desea salir?  </p>
				
				</div>
				<div class="modal-footer">
				<a class="btn btn-danger" href="<?php echo site_url('usuario/logout'); ?>">Si, Salir</a>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				</div>
				</div>
			</div>
			</div>
	<!-- Modal -->
	</div>
	<nav class="dx-header dx-header-2 navbar navbar-expand-md navbar-light bg-light navbar-shadow sticky-top">
		<button type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false"
			aria-label="Toggle navigation" class="navbar-toggler">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div id="navbarsExample08" class="collapse navbar-collapse justify-content-md-center">
			<ul class="navbar-nav">
			
			<?php if($NivelUsuario == 2 || ($NivelUsuario == 1) || ($NivelUsuario == 4) ){?>
 
				<li class="nav-item dropdown">
					<a href="" data-toggle="dropdown" class="nav-link dropdown-toggle"aria-haspopup="true" aria-expanded="false">
						<i class="icons ft-user d-none d-md-block"></i>
					<strong>	<span> PACIENTE </span> </strong>
					</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="<?php echo site_url('paciente'); ?>">Administrar Paciente </a>
						<a class="dropdown-item" href="<?php echo site_url('paciente/crud'); ?>">Nuevo Paciente</a>
                        
					</div>
				</li>

				<li class="nav-item dropdown">
					<a href="" data-toggle="dropdown" class="nav-link dropdown-toggle"aria-haspopup="true" aria-expanded="false">
						<i class="icons ft-award d-none d-md-block"></i>
						<strong> <span>PROFESIONAL</span> </strong>	
					</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="<?php echo site_url('profesional'); ?>">Administrar Profesional</a>
						<a class="dropdown-item" href="<?php echo site_url('profesional/crud'); ?>">Nuevo Profesional</a>
					</div>
				</li> 
				<li class="nav-item dropdown">
					
					<a href="" data-toggle="dropdown" class="nav-link dropdown-toggle"aria-haspopup="true" aria-expanded="false">
					<i class="icons ft-cpu d-none d-md-block"></i>
					<strong>   <span>ACCESO VASCULAR</span> </strong>	
					</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="<?php echo site_url('acceso_vascular'); ?>">Administrar AV</a>
						<a class="dropdown-item" href="<?php echo site_url('acceso_vascular/mov_av'); ?>">Movimiento del AV</a>
						<a class="dropdown-item" href="<?php echo site_url('acceso_vascular/crud'); ?>">Nuevo AV</a>
					</div>

				</li>	
				
				<li class="nav-item dropdown">
							<a href="<?php echo site_url('vigilancia'); ?>" class="nav-link "aria-haspopup="true" aria-expanded="false">
									<i class="icons ft-clipboard d-none d-md-block"></i>
									<strong>  	<span>VIGILANCIA PRIMARIA</span> </strong>
								</a>
								
				</li>	
				<?php } ?>
				<!-- SUPER ADMIN -->
				<?php   if(($NivelUsuario == 2) || ($NivelUsuario == 3) ||($NivelUsuario == 1) ) {?>
							<li class="nav-item dropdown">
						

                            <a href="" data-toggle="dropdown" class="nav-link dropdown-toggle"aria-haspopup="true" aria-expanded="false">
									<i class="icons ft-pie-chart d-none d-md-block"></i>
									<strong><span>REPORTES</span> </strong>
								</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="<?php echo site_url('vigilancia/reportevigilancia'); ?>">Evaluaciones del AV</a>
                                    <a class="dropdown-item" href="<?php echo site_url('vigilancia/reportetest'); ?>">Monitorización CLínica de FAV</a>
                                    <a class="dropdown-item" href="<?php echo site_url('vigilancia/complicaciones'); ?>">Complicaciones Identificadas</a>
                                  
                                
                                </div>
                      
							</li>

				<?php } ?>

				<!-- SUPER ADMIN -->
				<?php   if(($NivelUsuario == 1) || ($NivelUsuario == 2) ) {?>
				<li class="nav-item dropdown">
				<a href="" data-toggle="dropdown" class="nav-link dropdown-toggle"aria-haspopup="true" aria-expanded="false">
						<i class="icons ft-lock d-none d-md-block"></i>
						<strong> <span> SEGURIDAD</span> </strong>	
					</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="<?php echo site_url('usuario'); ?>">Administrar Usuarios</a>
						<a class="dropdown-item" href="<?php echo site_url('usuario/modificar_usuario'); ?>">Nuevo Usuario</a>
					</div>
				</li>
				<?php } ?>
				<?php   if($NivelUsuario == 1 ) {?>
				<li class="nav-item dropdown">
				<a href="" data-toggle="dropdown" class="nav-link dropdown-toggle"aria-haspopup="true" aria-expanded="false">
						<i class="icons ft-settings d-none d-md-block"></i>
						<strong>  	<span> MANTENIMIENTO</span> </strong>
					</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="<?php echo site_url('grupos'); ?>">Administrar Grupos</a>
						<a class="dropdown-item" href="<?php echo site_url('centro_asistencia/index'); ?>">Centros Asistenciales</a>	
						<a class="dropdown-item" href="<?php echo site_url('estructura/index'); ?>">Clínicas Externas</a>
						<a class="dropdown-item" href="<?php echo site_url('maestro/index'); ?>">Tabla Maestro</a>
					</div>
				</li>
				<?php } ?>


			</ul>
		</div>
	</nav>



 
<style>


</style>


