
<section class="content">
	<div class="row">
		<div class="col-lg-12">
		<div class="alert alert-primary  " role="alert">
			 <h4 align="center"><p class="text-info    font-weight-light">
			 <strong>CENTROS ASISTENCIALES </strong> </p></h4>
		</div>
<div class="card">
	<div class="card-header">
			<div class="form-row">
					<div class="col-8">
					<strong>
							<span class="ft-clipboard"> </span>
							<span> REGISTROS CENTROS ASISTENCIALES</span>
							</strong>
					</div>
					<div class="col">
					</div>
					<div class="col">
						<a href="<?php echo site_url('centro_asistencia/crud'); ?>" class="btn btn-sm btn-primary">
								<i class="ft-plus-circle"></i>
								<b> Nuevo Centro</b>
						</a>
					</div>
			</div> 
  	</div>	
    <div class="card-body">
	<div class="table-responsive">
        <table  id="centros" class=" table table-bordered" >
		 <thead class="thead-dark">
            <tr>
              <th scope="col"><center>CODIGO DE ORIGEN</th>
              <th scope="col"><center>CODIGO </th>          
              <th scope="col"><center>DESCRIPCION LARGA</th>
              <th scope="col"><center>DESCRIPCION CORTA</th>
              <th class="no-sort"scope="col"><center>ACCIONES</th>
            </tr>
          </thead>
            <tbody>
                <?php foreach($model as $m): ?>
                    <tr> 
                        <td scope="row"> <center> <strong> <?php echo $m->ORICENASICOD?> </td>
                        <td> <?php echo $m->CENASICOD?></td>
                        <td> <?php echo $m->CENASIDES?></td>
                        <td> <?php echo $m->CENASIDESCOR?></td>
                        <td><center>
                        <div class="btn-group" >
                                        				
                                                <a class="btn btn-sm btn-danger" href="<?php echo site_url('centro_asistencia/eliminar/' . $m->CENASICOD); ?>" onclick="return confirm('¿Esta seguro de eliminar permanentemente este Centro Asistecial?');"title=" Eliminar">
                                                    <i class="ft-trash-2"></i>
                                                </a>	
                            </div>       
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
  
    </div>
    </div>
</div>
</html>
</form>
