
<!DOCTYPE html>
<html>
<section class="content">
<br>
<br>
		<div class="col-lg-12">
	<div class="row">
        <div class="col-md-10 mx-auto">
          <div class="card rounded-0">
            <div class="card-header">
		<!-- -->	<strong>
                          <?php
                          $titulo = 'Nuevo Centro Asistencial';
                          $esNuevo = true;
                          if(is_object($model)){
                            $titulo = $model->CENASIDESCOR . ' '. $model->ORICENASICOD;
                            $esNuevo = false;
                          }
                          ?>
                          <h2 class="page-header"><p class="text-primary font-weight-light">
                            <?php echo $titulo?>
                          </h2>		
                  <!--    -->	
              </div>
            <div class="card-body">
              <?php echo form_open('centro_asistencia/guardar', ['enctype' => 'multipart/form-data']); ?>
                
                    <form> 
                    <div class="form-row ">
                            <div class="form-group">
                                <label for="validationDefault02">Código de Centro</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i  <i class="ft-grid"></i></div>
                                </div>
                                    <!-- <input  name="<?php echo $esNuevo? 'CENASICOD_NEW': 'CENASICOD'?>" type="text" class="form-control"  value="<?php echo $esNuevo? '': $model->CENASICOD?>"required> -->
                                    <input  name="CENASICOD" type="text" class="form-control"  value="<?php echo $esNuevo? '': $model->CENASICOD?>"required>
                                </div>
                            </div>
                        </div >
                        <div class="form-group">
                            <label for="validationDefault02">Nombre</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i  <i class="ft-home"></i></div>
                                </div>
                                <input onkeyup="mayus(this);" name="CENASIDES" type="text" class="form-control"  value="<?php echo $esNuevo? '': $model->CENASIDES?>"required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="validationDefault02">Abreviatura</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i  <i class="ft-compass"></i></div>
                                </div>
                                <input onkeyup="mayus(this);" name="CENASIDESCOR" type="text" class="form-control"  value="<?php echo $esNuevo? '': $model->CENASIDESCOR?>"required>
                            </div>
                        </div>
                       
                          <a class="btn btn-secondary" href="<?php echo site_url('centro_asistencia'); ?>" >Cancelar</a>
                            <button class="btn btn-primary" type="submit">
                           <?php echo $esNuevo? 'Agregar': 'Actualizar' ?>
                      </button>
          <?php echo form_close(); ?>
          </div>
        </div>
      </div>


</html>