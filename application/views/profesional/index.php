

<section class="content">
	<div class="row">
		<div class="col-lg-12">
		<div class="alert alert-primary  " role="alert">
			 <h4 align="center"><p class="text-info    font-weight-light">
			 <strong>PROFESIONALES</strong> </p></h4>

		</div>

<div class="card">
	<div class="card-header">
			<div class="form-row">
					<div class="col-8">
					<strong>
							<span class="ft-clipboard"> </span>
							<span> REGISTROS</span>
							</strong>
					</div>
					<div class="col">
					</div>
					<div class="col">
						<a href="<?php echo site_url('profesional/crud/'); ?>" class="btn btn-sm btn-primary">
								<i class="ft-plus-circle"></i>
								<b> Agregar Profesional </b>
						</a>
					</div>
			</div> 
  	</div>	

    <div class="card-body">
		<div class="table-responsive table-hover table ">   
			<table id="profesional"  class="table table-bordered" >
					<thead class="thead-dark">
					<tr>
						<th scope="col"><center> NOMBRES COMPLETOS</center> </th>
						<th class="no-sort"scope="col"> <center> TIPO DOCUMENTO </center></th>
						<th class="no-sort"scope="col"> <center> NRO. DOCUMENTO </center></th>
						<th class="no-sort"scope="col"><center> GRUPO OCUPACIONAL</center> </th>
						<th class="no-sort"scope="col"><center>COD. PLANILLA</th>
						<th class="no-sort"scope="col"><center>ACCIONES</th>
					</tr>
					</thead>
					<tbody> 
						<?php foreach($model as $m): ?>
							<tr>
								<td> <?php echo $m->APELLIDO_PAT?> <?php echo $m->APELLIDO_MAT?>, <?php echo $m->NOMBRES?></td>
								<td> <center> <?php echo $m->DES_LARGA?></td>
                				<th> <center> <?php echo $m->NUM_DOC?></td>
								<td> <center> <?php echo $m->GRUPO?></td>
								<td> <center> <?php echo $m->COD_PLANILLA?></td>
								<td> <center> 
									<div class="btn-group">
										<a class="btn btn-sm btn-primary" href="<?php echo site_url('profesional/crud/' . $m->COD_PROFESIONAL); ?>" title="Editar">
											<i class="ft-edit"></i>
										</a>
										<a class="btn btn-sm btn-danger" href="<?php echo site_url('profesional/eliminar/' . $m->COD_PROFESIONAL); ?>" onclick="return confirm('¿Esta seguro de eliminar a este profesional?');" title="Eliminar">
											<i class="ft-trash-2"></i>
										</a>
									</div>
								</td>       
							</tr>
						<?php endforeach; ?>
					</tbody>

			</table>
		</div>
	</div>

