<div class="container py-5">
<div class="card">
    <div class="card-header">
			<div class="form-row">
					<div class="col-8">
                  
						<?php
							$titulo = 'Nuevo Profesional';
							$esNuevo = true;
							if(is_object($model)){
								$titulo =  '🎓 '. $model->NOMBRES . ' '. $model->APELLIDO_PAT . ' '. $model->APELLIDO_MAT.' - NRO. DOC: '. $model->NUM_DOC;
								$esNuevo = false;
							}
							?>
							<h2 class="page-header"><p class="text-primary font-weight-light">
								<?php echo $titulo?>
                            </h2>
					</div>
			</div> 
  	</div>
    
    <div class="card-body"> 
    <?php echo form_open('profesional/guardar', ['enctype' => 'multipart/form-data']);
     ?>
    
        <input type="hidden" name="COD_PROFESIONAL" value="<?php echo $esNuevo? '': $model->COD_PROFESIONAL ?>" />
	    <form>
        <div class="form-row"> 	
 <div class="col-md-4 mb-3">
 <label for="inputState">Tipo de Documento</label>

                <select class="form-control  " name="TIP_DOC"  required>
                        <option value="">
                          </option>
                            <?php foreach($tip_docs as $m): ?>
                                <option <?php echo (!$esNuevo && $m->COD_INT === $model->TIP_DOC)? 'selected': '' ?> value="<?php echo $m->COD_INT?>"><?php echo $m->DES_LARGA?></option><?php endforeach; ?>
                    </select>   
                </div>
                
                <div class="col-md-3 mb-3">
                      <label for="validationDefault04">  <strong> N° Documento  </strong> </label>
                    <input name="NUM_DOC" type="text" class="form-control "id="validationTooltip03" type="text" value="<?php echo $esNuevo? '': $model->NUM_DOC?>"required>
                 
                        </div>
                    <div class="col-md-3 mb-3">
                         <label for="validationDefault03"> <strong> Grupo Ocupacional  </strong> </label>
                    <select name="GRUPO" class="form-control " id="inputState"required>
                        <option value="">
                         </option>
                        <option <?php echo (!$esNuevo && $model->GRUPO === 'MÉDICO')? 'selected': '' ?> value="MÉDICO">MÉDICO</option>
                        <option <?php echo (!$esNuevo && $model->GRUPO === 'ENFERMERA')? 'selected': '' ?> value="ENFERMERA">ENFERMERA</option>
                    </select>
                     </div>
                    <div class="col-md-3 mb-3">
                    </div>
                </div>
                
                <div class="form-row">
                    <div class="col-md-4 mb-3">
                    <label for="validationDefault01"> <strong> Apellido Paterno </strong></label>
                    <input onkeyup="mayus(this);" name="APELLIDO_PAT" type="text" class="form-control "id="validationTooltip03" type="text" value="<?php echo $esNuevo? '': $model->APELLIDO_PAT?>"required>
                    </div>
                    <div class="col-md-4 mb-3">
                    <label for="validationDefault02"><strong> Apellido Materno </strong></label>
                    <input onkeyup="mayus(this);"  name="APELLIDO_MAT" type="text" class="form-control " id="validationTooltip03" type="text" value="<?php echo $esNuevo? '': $model->APELLIDO_MAT?>"required>
                    </div>
                    <div class="col-md-4 mb-3">
                    <label for="validationDefaultUsername"><strong> Nombres</label>
                       <input  onkeyup="mayus(this);" name="NOMBRES" type="text" class="form-control "id="validationTooltip03" type="text" value="<?php echo $esNuevo? '': $model->NOMBRES?>"required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                    <label for="validationDefault03">Código Planilla</label>
                    <input name="COD_PLANILLA"type="text" class="form-control "id="validationTooltip03" type="text" value="<?php echo $esNuevo? '': $model->COD_PLANILLA?>">
                    </div>
                </div>
                <div class="form-row">    
                    <div class="col-md-4 mb-5">
                    <label for="validationDefault04">Código de grupo ocupacional (CEP)/(CMP)</label>
                    <input name="CEEP" type="text" class="form-control "id="validationTooltip03" type="text" value="<?php echo $esNuevo? '': $model->CEEP?>"required>
                    </div>
                    <div class="col-md-4 mb-5">
                    <label for="validationDefault05">Registro de especialidad (REE)</label>
                    <input name="REE" type="text" class="form-control "id="validationTooltip03" type="text" value="<?php echo $esNuevo? '': $model->REE?>">
                    </div>
                    
                    <!-- <div class="col-md-4 mb-3">
                    <label for="validationDefault03"> Centro Asistencial </label>
                    <select name="CENASICOD" id="inputState" class="form-control ">
                          
                                <?php foreach($cenasicods as $m): ?>
                                <option <?php echo (!$esNuevo && $m->CENASICOD === $model->CENASICOD)? 'selected': '' ?> value="<?php echo $m->CENASICOD?>">
                                    <?php echo $m->CENASIDES ?>
                            </option>
                                <?php endforeach; ?>
                    </select>
                     </div> -->
                </div>
                <a class="btn btn-secondary" href="<?php echo site_url('profesional'); ?>" title="Cancelar">Cancelar</a>
                <button class="btn btn-primary" type="submit">
                     <?php echo $esNuevo? 'Agregar': 'Actualizar' ?>
                </button>
        </form>
    <?php echo form_close(); ?>
            
        </div>
</div>
<style>
.ui-tooltip {
        border: 9px  white;
        background: rgba(34, 9, 9, 1);
        color: white;
}
  .custom-combobox {
    position: relative;
    display: inline-block;
  }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
    *top: 0.1em;   
    border: 18px black;
  }
  .custom-combobox-input {
	margin: 2;
    height: 1.7em;
    width: 15.1em;
    padding: 0.3em;
    background: white;
    color: black;
  }

  </style>
