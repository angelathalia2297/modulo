<!DOCTYPE html>
<html>
<section class="content">

		<div class="col-lg-12">
	<div class="row">
        <div class="col-md-10 mx-auto">
          <div class="card rounded-0">
            <div class="card-header">

                <!-- -->	 <strong>
                              <?php
                              $titulo = 'Nuevo Usuario';
                              $esNuevo = true;
                              if(is_object($model)){
                                $titulo = $model->Nombre . ' - '. $model->Usuario;
                                $esNuevo = false;
                              }
                              ?>
                              <h2 class="page-header"><p class="text-primary font-weight-light">
                              <?php echo $titulo?>
                        </h2>	
              </div>
            <div class="card-body">
              <?php echo form_open('usuario/guardar', ['enctype' => 'multipart/form-data']); ?>

                <form >
                
                  <input type="hidden" name="idUsuario" value="<?php echo $esNuevo? '': $model->idUsuario ?>" />
      
      
                    <div class="form-group">
                      <label for="inputAddress">Apellidos y Nombres</label>
                      <input onkeyup="mayus(this);"  type="text" class="form-control"  placeholder="APELLIDOS, NOMBRES"  name="Nombre" value="<?php echo $esNuevo? '': $model->Nombre ?>" required/>
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="inputEmail4">Usuario</label>
                        <input  onkeyup="mayus(this);" type="text"  placeholder="ANGELAPA"  class="form-control" class="noAutoComplete"  name="Usuario" value="<?php echo $esNuevo? '': $model->Usuario ?>" required />
                      </div>

                      <?php if(($NivelUsuario == 1 ) ||($NivelUsuario == 2 )){?>
                        <div class="form-group col-md-6">
                          <label for="inputPassword4">Contraseña</label>
                          <input type="password"  placeholder="INGRESE LETRAS Y NÚMEROS" class="form-control" id="inputPassword4" name="Clave"  required />
                        </div>
                      <?php } ?>
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <label for="inputState">Rol de Usuario</label>
                  			<select name="NivelUsuario" class="form-control " id="inputState" required>
                          <option value="">
                            << Seleccione>></option>
                          <?php foreach($grupos as $m): ?>
                          <option <?php echo (!$esNuevo && $m->idGrupo === $model->NivelUsuario)? 'selected': '' ?> value="<?php echo $m->idGrupo?>">
                            <?php echo $m->NombreGrupo ?>
                          </option>
                          <?php endforeach; ?>
                        </select>           
                      </div>
                     
                      <!-- <?php if($NivelUsuario == 1 ){?>
                       <div class="form-group col-md-6">
                        <label for="inputState">Centro Asistencial</label>
                  			<select name="Cenasicod" class="form-control " id="inputState">
                        
                          <?php foreach($cenasicods as $m): ?>
                          <option <?php echo (!$esNuevo && $m->CENASICOD === $model->Cenasicod)? 'selected': '' ?> value="<?php echo $m->CENASICOD?>">
                            <?php echo $m->CENASIDES ?>
                          </option>
                          <?php endforeach; ?>
                        </select>
                      </div> 
                      <?php } ?> -->

                      <?php if(!$esNuevo){?>
                        <div class="col-md-4 mb-3">
                                    <label for="validationDefault04">  Estado </label>
                                    <select name="Estado" id="inputTipo" class="form-control " required>
                                      <option value="">
                                      << Seleccione >></option>
                                      <option <?php echo (!$esNuevo && $model->Estado === '1')? 'selected': '' ?> value="1">Activo</option>
                                      <option <?php echo (!$esNuevo && $model->Estado === '0')? 'selected': '' ?> value="0">Inactivo</option>
                                    </select>
                        </div>
                      <?php } ?>
                    </div>

                            <a class="btn btn-secondary" href="<?php echo site_url('usuario'); ?>" title="Cancelar">Cancelar</a>
                            <button class="btn btn-primary" type="submit">
                              <?php echo $esNuevo? 'Agregar': 'Actualizar' ?>
                            </button>
                            <?php echo form_close(); ?>
                  </form>
                    
                 

              
            </div>
              <?php echo form_close(); ?>
          </div>
        </div>
      </div>
      
</html>
<style>
	  .ui-tooltip {
        border: 9px  white;
        background: rgba(34, 9, 9, 1);
        color: white;
      }

	</style>