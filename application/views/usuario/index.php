<!doctype html>
<html>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
		<div class="alert alert-primary  " role="alert">
			 <h4 align="center"><p class="text-info    font-weight-light">
			 <strong> ADMINISTRACIÓN USUARIOS  </strong> </p></h4>

		</div>
	  
<div class="card">
	<div class="card-header">
			<div class="form-row">
					<div class="col-8">
					<strong>
							<span class="ft-users"> </span>
							<span> REGISTRO USUARIOS</span>
							</strong>
					</div>
					<div class="col">
					</div>
					<div class="col">
						<a href="<?php echo site_url('usuario/modificar_usuario'); ?>" class="btn btn-sm btn-primary">
								<i class="ft-plus-circle"></i>
								<b> Agregar Usuario </b>
						</a>
					</div>
			</div> 
  	</div>
	<div class="card-body">
	<div class="table-responsive">
        <table  id="" class=" display table table-bordered" >
					<thead class="thead-dark">
            <tr>
              <!-- <th scope="col"><center>Id</th> -->
              <th scope="col"><center>NOMBRE COMPLETO</th> 
							<th scope="col"><center>USUARIO</th>          
              <th class="no-sort"scope="col"><center>NIVEL</th>       
              <th class="no-sort"scope="col"><center>ULTIMO ACCESO</th>
              <th class="no-sort"scope="col"><center>ESTADO</th>
              <th class="no-sort"scope="col"><center>ACCIONES</th>
            </tr>
          </thead>
      <tbody>
      <?php foreach($model as $m): ?>
        <tr>
          <!-- <th scope="row"><center><?php echo $m->idUsuario?></th> -->
          <td> <?php echo $m->Nombre?></td>
					<td >	<?php echo $m->Usuario?>	</td>  
          <td> <?php echo $grupos[array_search($m->NivelUsuario, array_column($grupos, 'idGrupo'))]->NombreGrupo ?></td>
          <td> <center><?php echo $m->UltimoAcceso?>  </td>
          <td> <center> <span class="badge badge-<?php echo $m->Estado === '1'? 'success': 'warning' ?>"><?php echo $m->Estado === '0'? 'Inactivo': 'Activo' ?></span>  </td>
          <td>
					<div class="btn-group">
							<a class="btn btn-sm btn-primary" href="<?php echo site_url('usuario/modificar_usuario/' . $m->idUsuario); ?>" title="Editar">
							<i class="ft-edit"></i>
							</a>
							<a class="btn btn-sm btn-danger" href="<?php echo site_url('usuario/eliminar/' . $m->idUsuario); ?>"onclick="return confirm('¿Esta seguro de eliminar a este Usuario?');"  title="Eliminar">
								<i class="ft-trash-2"></i>
							</a>
						</div>
          </td>
        </tr>  
      <?php endforeach; ?>
      </tbody>
    </table>
</html>
