<!DOCTYPE html>
<html>
    <div class="container py-5">
      <div class="row">
        <div class="col-md-7 mx-auto">
          <div class="card rounded-0">
            <div class="card-header">
                <strong>
                      <?php
                      $titulo = 'Nuevo Grupo';
                      $esNuevo = true;
                      if(is_object($model)){
                        $titulo = $model->NombreGrupo . ' - '. $model->idGrupo;
                        $esNuevo = false;
                      }
                      ?>
                      <h2 class="page-header"><p class="text-primary font-weight-light">
                      <?php echo $titulo?>
							</h2>	

              </div>
            <div class="card-body">
              <?php echo form_open('grupos/guardar', ['enctype' => 'multipart/form-data']); ?>
                  <form >
                    <input type="hidden" name="idGrupo" value="<?php echo $esNuevo? '': $model->idGrupo ?>" />
                
                        
                        <div class="form-group">
                            <label for="validationDefault02">Nombre del Grupo</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i  <i class="ft-codepen"></i></div>
                                </div>
                                <input onkeyup="mayus(this);"  name="NombreGrupo" type="text" class="form-control"  value="<?php echo $esNuevo? '': $model->NombreGrupo?>"required>
                            </div>
                        </div>
                        <?php if(!$esNuevo){?>
                        <div class="form-group">
                          <label for="validationDefault04">Estado</label>
                            <div class="input-group mb-2 mr-sm-2">
                              <div class="input-group-prepend">
                                <div class="input-group-text"><i  <i class="ft-info"></i></div>
                              </div>
                                <select name="EstadoGrupo" class="form-control form-control-m" required>
                                <option value="">
                                  << Seleccione>></option>
                                  <option <?php echo (!$esNuevo && $model->EstadoGrupo === '1')? 'selected': '' ?> value="1">Activo</option>
                                  <option <?php echo (!$esNuevo && $model->EstadoGrupo === '0')? 'selected': '' ?> value="0">Inactivo</option>
                              </select>
                            </div>  
                        </div>
                          <?php } ?>
                        <a class="btn btn-secondary" href="<?php echo site_url('grupos'); ?>" title="Cancelar">Cancelar</a>
                        <button class="btn btn-primary" type="submit">
                          <?php echo $esNuevo? 'Agregar': 'Actualizar' ?>
                        </button>
                        <?php echo form_close(); ?>
                  </form>
                    
        
              
            </div>
              <?php echo form_close(); ?>
          </div>
        </div>
      </div>
      
</html>
<style>
	  .ui-tooltip {
        border: 9px  white;
        background: rgba(34, 9, 9, 1);
        color: white;
      }

	</style>