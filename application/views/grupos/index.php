<!DOCTYPE html>
<html>
<form class="needs-validation" novalidate>
<section class="content">

			<div class="alert alert-primary  " role="alert">
			 <h4 align="center"><p class="text-info    font-weight-light">
			 <strong>PERFILES DE USUARIO </strong> </p></h4>

		</div>
		<br>

    <div class="card ">
      <div class="card-header ">
       
      <div class="form-right">

      </div>

        <div class="form-row">
            <div class="form-group col-md-9">
                <strong>
                  <span class="ft-grid"></span>
                  <span> LISTA</span>
                </strong>
            </div>
            <div class="input-group-append">
            <div class="form-group col-md-7">
            </div>
              <div class="form-group col-md-8">
              <!-- <a href="<?php echo site_url('grupos/crud'); ?>" class="btn btn-sm btn-primary">
			        	<i class="ft-plus-circle"></i>
				        <b> Agregar Grupo</b>
			                </a> -->
              </div>
        </div>
    </div>
    </div>
      <div class="card-body ">
      
      <div class="table-responsive">
        <table class="table">
        <table id="" class=" display table table-bordered" >
					<thead class="thead-dark">
              <tr >
               <th class="no-sort"scope="col"><center>Nivel </th>
                <th scope="col"><center>Nombre </th>
                <th class="no-sort"scope="col"><center>Estado</th>
                <th class="no-sort"scope="col"><center>Acciones</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach($model as $m): ?>
              <tr>
                <th scope="row"><center><?php echo $m->idGrupo?></th>
                <td> <center><?php echo $m->NombreGrupo?> </td>
                <td> <center> 	<span class="badge badge-<?php echo $m->EstadoGrupo === '0'? 'warning': 'success' ?>"><?php echo $m->EstadoGrupo === '1'? 'Activo': 'InActivo' ?></span>  
                </td>
                <td>  <center> 
                <div class="btn-group">
                  <a class="btn btn-sm btn-primary" href="<?php echo site_url('grupos/crud/'. $m->idGrupo ); ?>" title="Editar">
                    <i class="ft-edit"></i>
                  </a>					
                  <a class="btn btn-sm btn-danger" href="<?php echo site_url('grupos/eliminar/'. $m->idGrupo ); ?>" onclick="return confirm('¿Esta seguro de eliminar a este Grupo?');" title=" Eliminar">
                    <i class="ft-trash-2"></i>
                  </a>	
						    </div>
                </td>      
              </tr>
          
            <?php endforeach; ?>
          </tbody>
        </table>

        </table> 	<colspan="12">
				<?php echo $this->pagination->create_links(); ?>
      </div>
      
     


      </div>
    </div>

  
    </div>
</form>
</html>
<style>
	  .ui-tooltip {
        border: 9px  white;
        background: rgba(34, 9, 9, 1);
        color: white;
      }

	</style>

  