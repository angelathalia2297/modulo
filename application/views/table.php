<script src="<?php echo base_url('assets/plugins/tablas/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/tablas/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/tablas/dataTables.buttons.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/tablas/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/tablas/jszip.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/tablas/0.1.36/pdfmake.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/tablas/vfs_fonts.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/tablas/buttons.html5.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/tablas/buttons.flash.min.js'); ?>"></script>

<link rel="stylesheet" href="<?php echo base_url('assets/plugins/tablas/dataTables.bootstrap4.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/tablas/buttons.bootstrap4.min.css'); ?>">
<script src="<?php echo base_url('assets/js/filtrodate.js'); ?>"></script>
	<script src="<?php echo base_url('assets/plugins/tablas/date/moment.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/plugins/tablas/date/daterangepicker.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/tabla_fecha.js'); ?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/tablas/date/daterangepicker.css'); ?>">
<script>
$(document).ready(function() {
//TABLA DE MOVIMIENTO PACIENTE = DIAS Y PAR
$('#mov_total').dataTable({
        dom: 'flrtBip',
        responsive: true,
        buttons: [ { 
            text:   'Exportar <i class="ft ft-printer"></i>',
            extend: 'excelHtml5', 
            title: "PAR/CNSR",
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6,7,8],
                    
                format: {
                    body: function(data, row, column, node) {
                        return data.replace(',', '.');
                    }
 
                },
                
                },
            footer: true  
            },
        ],
        "ordering": true,
        columnDefs: [
   		{ 
		   orderable: false, 
		   targets:  "no-sort"  
        }
		],
        "lengthMenu": [[15, 35, 50, -1], [15, 35, 50,"Todo"]],
        "pageLength": 15,
        language: {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "searchPlaceholder": "Ingrese un Dato",
            "info": "_START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "Dato no encontrado",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            } 
        },
    	"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            var intValD = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
         
            }; 
            // Total over all pages
                total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Total over all pages
            partotal = api
                .column( 8 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Total over this page
            pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // computing column Total the complete result 
			 var thuTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
				
			 var friTotal = api
                .column( 8, { 
                    page: 'current' 
                } )
                .data()
                .reduce( function (a, b) {
                    return intValD(a) + intValD(b);
                }, 0 );
            // Update footer by showing the total with the reference of the column index 
            $( api.column( 4 ).footer() ).html(''+thuTotal +' de ('+ total +' Dias de exposición)');
            $( api.column( 8 ).footer() ).html(''+(friTotal).toFixed(2)+' de ('+ (partotal).toFixed(2) +' PAR Total)');
        },
} );

//TABLA SEGURIDAD - USUARIO & GRUPOS 
$('table.display').DataTable( {
        dom: 'flrtBip',
        buttons: [ {
                extend: 'excelHtml5',
                text:   'Exportar <i class="ft ft-printer"></i>',
                title: "Seguridad-EsSalud/CNSR",
                exportOptions: {
                    columns: [ 0, 1, 2, 3]
                }
            }
        ],
        "ordering": true,
        columnDefs: [
   		{ 
		   orderable: false, 
		   targets:  "no-sort"  
        }
		],
		"order": [[0, 'asc']], 
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50,"Todo"]],
        language: {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "searchPlaceholder": "Ingrese un Dato",
            "info": "_START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "Dato no encontrado",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
        

});

//TABLA MANTEMIENTO CENTROS
$('#centros').DataTable( {
        dom: 'flrtBip',
        buttons: [ {
                extend: 'excelHtml5',
                text:   'Exportar <i class="ft ft-printer"></i>',
                title: "Centros-EsSalud/CNSR",
                exportOptions: {
                    columns: [ 0, 1, 2, 3]
                }
            }
        ],
        "ordering": true,
        columnDefs: [
   		{ 
		   orderable: false, 
		   targets:  "no-sort"  
        }
		],
		"order": [[0, 'asc']], 
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50,"Todo"]],
        language: {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "searchPlaceholder": "Ingrese un Dato",
            "info": "_START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "Dato no encontrado",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
});

//TABLA MANTEMIENTO MAESTRO (TIPOS)
$('#maestro').DataTable( {
        dom: 'flrtBip',
        buttons: [ {
                extend: 'excelHtml5',
                text:   'Exportar <i class="ft ft-printer"></i>',
                title:  "Tabla Maestro-EsSalud/CNSR",
                exportOptions: {
                    columns: [ 0, 1, 2, 3,4,5,6]
                }
            }
        ],
        "ordering": true,
        columnDefs: [
   		{ 
		   orderable: false, 
		   targets:  "no-sort"  
        }
		],
		"order": [[0, 'asc']], 
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50,"Todo"]],
        language: {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "searchPlaceholder": "Ingrese un Dato",
            "info": "_START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "Dato no encontrado",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
});

//TABLA PACIENTE
$('#pacientes').DataTable( {
    dom: 'flrtBip',
        buttons: [ {
                extend: 'excelHtml5',
                text:   'Exportar <i class="ft ft-printer"></i>',
                title: "Pacientes-EsSalud/CNSR",
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4,5,6,7]
                }
            }
        ],
        "ordering": true,
        columnDefs: [
   		{ 
		   orderable: false, 
		   targets:  "no-sort"  
        }
		],
		"order": [[1, 'asc']], 
        "lengthMenu": [[30, 50, 70, -1], [30, 50, 70, "Todo"]],
        "pageLength": 30,
        language: {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "searchPlaceholder": "Ingrese un Dato",
            "info": "_START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "Dato no encontrado",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
});

//TABLA PROFESIONAL
$('#profesional').DataTable( {
    dom: 'flrtBip',
        buttons: [ {
                extend: 'excelHtml5',
                text:   'Exportar <i class="ft ft-printer"></i>',
                title: "Profesionales-EsSalud/CNSR",
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4]
                }
            }
        ],
        "ordering": true,
        columnDefs: [
   		{ 
		   orderable: false, 
		   targets:  "no-sort"  
        }
		],
		"order": [[0, 'asc']], 
        "lengthMenu": [[25, 35, 50, -1], [25, 35, 50,"Todo"]],
        "pageLength": 25,
        language: {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "searchPlaceholder": "Ingrese un Dato",
            "info": "_START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "Dato no encontrado",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
});

//TABLA ACCESO VASCULAR
$('#av').DataTable( {
    dom: 'flrtBip',
        buttons: [ {
                extend: 'excelHtml5',
                text:   'Exportar <i class="ft ft-printer"></i>',
                title: "Accesos Vasculares-EsSalud/CNSR",
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4,5,6,7]
                }
            }
        ],
        "ordering": true,
        columnDefs: [
   		{ 
		   orderable: false, 
		   targets:  "no-sort"  
        }
		],
		"order": [[0, 'asc']], 
        "lengthMenu": [[30, 50, 70, -1], [30, 50, 70, "Todo"]],
        "pageLength": 30,
        language: {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "searchPlaceholder": "Ingrese un Dato",
            "info": "_START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "Dato no encontrado",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
});

//TABLA DE REPORTE VIGILANCIA
$('#repor_vigilancia').DataTable( {
        dom: 'Bflrtip',
        buttons: [
            {
                text:   'Exportar <i class="ft ft-file-text"></i>',
                extend: 'excelHtml5',
                title: "Evaluaciones del Acceso Vascular",
            
            },  
        ],
        "ordering": true,
        columnDefs: [
   		{ 
		   orderable: false, 
		   targets:  "no-sort"  
        }
		],
        "order": [[0, 'desc'],[2, 'asc']],
        "lengthMenu": [[30, 50, 70, -1], [30, 50, 70, "Todo"]],
        "pageLength": 30,
    language: {
        "lengthMenu": "Mostrar _MENU_ registros por pagina",
        "zeroRecords": "No se encontraron resultados",
        "searchPlaceholder": "Ingrese un Dato",
        "info": "_START_ al _END_ de un total de  _TOTAL_ registros",
        "infoEmpty": "Dato no encontrado",
        "infoFiltered": "(filtrado de un total de _MAX_ registros)",
        "search": "Buscar:",
        "paginate": {
            "first": "Primero",
            "last": "Último",
            "next": "Siguiente",
            "previous": "Anterior"
        },
    }
});

//TABLA DE REPORTE TEST
$('#f').DataTable( {
        dom: 'Bflrtip',
        buttons: [
            {
                text:   'Exportar <i class="ft ft-file-text"></i>',
                extend: 'excelHtml5',
                title: "Evaluaciones del Acceso Vascular",
            
            },  
        ],
        "ordering": true,
        columnDefs: [
   		{ 
		   orderable: false, 
		   targets:  "no-sort"  
        }
		],
        "order": [[0, 'desc'],[2, 'asc']],
        "lengthMenu": [[30, 50, 70, -1], [30, 50, 70, "Todo"]],
        "pageLength": 30,
    language: {
        "lengthMenu": "Mostrar _MENU_ registros por pagina",
        "zeroRecords": "No se encontraron resultados",
        "searchPlaceholder": "Ingrese un Dato",
        "info": "_START_ al _END_ de un total de  _TOTAL_ registros",
        "infoEmpty": "Dato no encontrado",
        "infoFiltered": "(filtrado de un total de _MAX_ registros)",
        "search": "Buscar:",
        "paginate": {
            "first": "Primero",
            "last": "Último",
            "next": "Siguiente",
            "previous": "Anterior"
        },
    }
});

//TABLA DE REPORTE VIGILANCIA
$('#repor_compli').DataTable( {
        dom: 'Bflrtip',
        buttons: [
            {
                text:   'Exportar <i class="ft ft-file-text"></i>',
                extend: 'excelHtml5',
                title: "Complicaciones del AV",
            
            },  
        ],
        "ordering": true,
        columnDefs: [
   		{ 
		   orderable: false, 
		   targets:  "no-sort"  
        }
		],
        "order": [[0, 'desc'],[2, 'asc']],
        "lengthMenu": [[30, 50, 70, -1], [30, 50, 70, "Todo"]],
        "pageLength": 30,
    language: {
        "lengthMenu": "Mostrar _MENU_ registros por pagina",
        "zeroRecords": "No se encontraron resultados",
        "searchPlaceholder": "Ingrese un Dato",
        "info": "_START_ al _END_ de un total de  _TOTAL_ registros",
        "infoEmpty": "Dato no encontrado",
        "infoFiltered": "(filtrado de un total de _MAX_ registros)",
        "search": "Buscar:",
        "paginate": {
            "first": "Primero",
            "last": "Último",
            "next": "Siguiente",
            "previous": "Anterior"
        },
    }
});

$('#vWWW').DataTable( {
        "paging":   true,
        "ordering": false,
        "info":     true,
    //    "ordering": false,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todo"]],

        
        language: {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "searchPlaceholder": "Ingrese un Dato",
            "info": "_START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "Dato no encontrado",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
     
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
});

$('#testr').DataTable( {
    dom: 'Bfrtip',
        buttons: [
            {
            extend: 'excelHtml5',
            title: "Test",

            customize: function(xlsx) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 
                // seleccionar en la columna RA `H`
                $('row c[r^="H"]', sheet).each( function () {
                    // Get the value
                    if ( $('is t', this).text() == "23" ) {
                        $(this).attr( 's', '20' );
                    }
                });
            }
        }, 
    ],
            language: {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "searchPlaceholder": "Ingrese un Dato",
            "info": "_START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "Dato no encontrado",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            
        },
    }
});

$('#compli').DataTable( {
  dom: 'Bfrtip',
        buttons: [
            {
                text:   'Imprimir Excel',
                extend: 'excelHtml5',
                title: "Registro de Vigilancia",
                exportOptions: {
                    columns: [ 0, 1,2, 3, 4,5,6,7,8,9 ]
                }
            },
         
        ],

    language: {
        "lengthMenu": "Mostrar _MENU_ registros por pagina",
        "zeroRecords": "No se encontraron resultados",
        "searchPlaceholder": "Ingrese un Dato",
        "info": "_START_ al _END_ de un total de  _TOTAL_ registros",
        "infoEmpty": "Dato no encontrado",
        "infoFiltered": "(filtrado de un total de _MAX_ registros)",
        "search": "Buscar:",
        "paginate": {
            "first": "Primero",
            "last": "Último",
            "next": "Siguiente",
            "previous": "Anterior"
        },
    }
});

} );
</script>

<script>
    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var min = parseInt( $('#min').val(), 10 );
            var max = parseInt( $('#max').val(), 10 );
            var age = parseFloat( data[4] ) || 0; // use data for the age column
    
            if ( ( isNaN( min ) && isNaN( max ) ) ||
                ( isNaN( min ) && age <= max ) ||
                ( min <= age   && isNaN( max ) ) ||
                ( min <= age   && age <= max ) )
            {
                return true;
            }
            return false;
        }
    );

    $(document).ready(function() {
        var table = $('#repor_test').DataTable(
            {
            dom: 'Bflrtip',
            buttons: [
                {
                    text:   'Exportar <i class="ft ft-file-text"></i>',
                    extend: 'excelHtml5',
                    title: "Reporte Test",
                
                },  
            ],
            "ordering": true,
            columnDefs: [
            { 
            orderable: false, 
            targets:  "no-sort"  
            }
            ],
            "order": [[0, 'desc'],[1, 'desc']],
            "lengthMenu": [[30, 50, 70, -1], [30, 50, 70, "Todo"]],
            "pageLength": 30,
        language: {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "searchPlaceholder": "Ingrese un Dato",
            "info": "_START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "Dato no encontrado",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    }


        );
        
        // Event listener to the two range filtering inputs to redraw on input
        $('#min, #max').keyup( function() {
        
            table.draw();
        } );
    } );
</script>

<style>
    .ui-tooltip {
            border: 9px  white;
            background: rgba(34, 9, 9, 1);
            color: white;
    }
    .content {
        min-height: 250px;
        padding: 25px;
        margin-right: auto;
        margin-left: auto;
        padding-left: 25px;
        padding-right: 25px;
    }

    .box-body {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
        padding: 25px 20px 10px;
    }
    td.highlight {
            font-weight: bold;
            color: red;
        }
</style>
