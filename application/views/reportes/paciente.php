
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.1/bootstrap-table.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.1/bootstrap-table.min.js"></script> 


<!DOCTYPE html>
<html>
<form>
    <div class="container py-5">
          <div class="form-row  form-control-sm" >
            <div class="col-md-4 mb-3">
              <label for="validationDefault03">Tipo de Acceso</label>
              <select class="form-control form-control-sm">
                <option value=""><< Seleccione >></option>
                <option value="1">Todas</option>
                <option value="2">Supervisor</option>
                <div class="valid-tooltip"></div>
              </select>
            </div>
            <div class="col-md-4 mb-3">
              <label for="validationDefault04">Complicaciones</label>
              <select class="form-control form-control-sm">
                <option value=""><< Seleccione >></option>
                <option value="1">Todas</option>
                <option value="2">Inactivo</option>
                <div class="valid-tooltip"></div>
              </select>
            </div>
            <div class="col-md-4 mb-3">
            <label for="validationDefault04">Fecha deseada</label>
            <input type="date" name="fecha" class="form-control form-control-sm">
            </div>
    </div>

<?php //var_dump($model); ?>
<h1 class="page-header">
    LISTA PACIENTES
</h1>

<ol class="breadcrumb">
  <li class="active">Pacientes</li>
</ol>

	<div class="table-responsive">
        <table class="table">
        <table class="table table-hover table-bordered ">
       <thead>
        
            <th >Apellidos </th>
            <th> Nombres </th>
            <th> Tipo de Doc. </th>
            <th> Nro. Doc </th>
            <th> Sexo </th>
            <th> Fecha Nac.</th>
            
        </tr>
    </thead>
    <tbody>
        <?php foreach($model as $m): ?>
        <tr>
            
            <td> <?php echo $m->APELLIDO_PATERNO; ?> <?php echo $m->APELLIDO_MATERNO; ?></td>
            <td> <?php echo $m->NOMBRES; ?></td>
            <td> <?php echo $m->DES_LARGA; ?></td>
            <td> <?php echo $m->NRO_DOC; ?></td>
            <td> <?php echo $m->SEXO; ?></td>
            <td> <?php echo $m->FECHA_NAC; ?></td>
            
        </tr>
        <?php endforeach; ?>
    </tbody>
</table></table>
</div>

<script>
function runningFormatter(value, row, index) {
    return index;
}
</script>