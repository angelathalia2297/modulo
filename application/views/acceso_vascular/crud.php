<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>  
     
	<script>
        (function( $ ) {
            $.widget( "custom.combobox", {
            _create: function() {
                this.wrapper = $( "<span>" )
                .addClass( "custom-combobox" )
                .insertAfter( this.element );
        
                this.element.hide();
                this._createAutocomplete();
                this._createShowAllButton();
            },
        
            _createAutocomplete: function() {
                var selected = this.element.children( ":selected" ),
                value = selected.val() ? selected.text() : ""; 
                this.input = $( "<input>" )
                .appendTo( this.wrapper )
                .val( value )
                .attr( "title", "" )
                .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
                .autocomplete({
                    delay: 0,
                    minLength: 0,
                    source: $.proxy( this, "_source" )
                })
                .tooltip({
                    tooltipClass: "ui-state-highlight"
                }); 
                this._on( this.input, {
                autocompleteselect: function( event, ui ) {
                    ui.item.option.selected = true;
                    this._trigger( "select", event, {
                    item: ui.item.option
                    });
                }, 
                autocompletechange: "_removeIfInvalid"
                });
            }, 
            _source: function( request, response ) {
                var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
                response( this.element.children( "option" ).map(function() {
                var text = $( this ).text();
                if ( this.value && ( !request.term || matcher.test(text) ) )
                    return {
                    label: text,
                    value: text,
                    option: this
                    };
                }) );
            }, 
            _removeIfInvalid: function( event, ui ) { 
                if ( ui.item ) {
                return;
                } 
                var value = this.input.val(),
                valueLowerCase = value.toLowerCase(),
                valid = false;
                this.element.children( "option" ).each(function() {
                if ( $( this ).text().toLowerCase() === valueLowerCase ) {
                    this.selected = valid = true;
                    return false;
                }
                });
         
                if ( valid ) {
                return;
                } 
                this.input
                .val( "" )
                .attr( "title", value + " No existe" )
                .tooltip( "open" );
                this.element.val( "" );
                this._delay(function() {
                this.input.tooltip( "close" ).attr( "title", "" );
                }, 2500 );
                this.input.data( "ui-autocomplete" ).term = "";
            },
        
            _destroy: function() {
                this.wrapper.remove();
                this.element.show();
            }
            });
        })( jQuery );
        
        $(function() {
            $( ".combobox" ).combobox(); 
            $( "#toggle" ).click(function() {
            $( ".combobox" ).toggle();
            });
        });
  </script>
<div class="container py-5">
<div class="card">
    <div class="card-header">
			<div class="form-row">
					<div class="col-8">
                    <?php
							$titulo = 'Nuevo Acceso Vascular';
							$esNuevo = true;
							if(is_object($model)){
                                $titulo = 'Tipo de A.V: '.$model->COD_TIPO_ACCESO.' -   Ubicación: '.$model->UBICACION;
								$esNuevo = false;
							}
							?>
							<h2 class="page-header"><p class="text-primary font-weight-light">
					<?php echo $titulo?>
                            </h2>
					</div>
			</div> 
  	</div>

<div class="card-body"> 
    <?php echo form_open('acceso_vascular/guardar', ['enctype' => 'multipart/form-data']);
     ?>
       <input type="hidden" name="PK_ACCESO_V" value="<?php echo $esNuevo? '': $model->PK_ACCESO_V ?>" />
	 
<form>
<div class="form-row">
                    <div class="form-group col-md-9">
                        <strong>
                        <span class="ft-user"></span>
                        <span> DATOS DEL PACIENTE</span>
                        </strong>
                    </div>
                </div>
  <div class="form-row">
    <div class="form-group col-md-3">
      <label for="inputEmail4">Apellidos y Nombres</label>
      <?php if( $esNuevo){?>
        <select  name="COD_PACIENTE" class="form-control combobox" required><option value=""></option><?php foreach($cod_pacientes as $m): ?><option <?php echo (!$esNuevo && $m->COD_PACIENTE === $model->COD_PACIENTE)? 'selected': '' ?> value="<?php echo $m->COD_PACIENTE?>"><?php echo $m->APELLIDO_PATERNO ?> <?php echo $m->APELLIDO_MATERNO?>, <?php echo $m->NOMBRES?></option><?php endforeach; ?></select>       
      <?php } ?> 
    
      <?php if( !$esNuevo){?>
        
 
      <?php } ?>                  

    </div>  
  </div>
  <?php if(!$esNuevo){?>
                       <div class="form-row">    
                           <div class="col-md-2 mb-3">
                            <label for="validationDefault04">  <strong> Nro. Documento  </strong> </label>
                            <select   name="NRO_DOC"  class="form-control " disabled>
                               <option > </option>
                                    <?php foreach($cod_pacientes as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->COD_PACIENTE === $model->COD_PACIENTE)? 'selected': '' ?> value="<?php echo $m->COD_PACIENTE?>" >
                                    <?php echo $m->NRO_DOC?></option>
                                    <?php endforeach; ?> </select>
                            
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationDefault04">  <strong> Fecha de Nacimiento </strong> </label>
                             <select   name="FECHA_NAC"  class="form-control " disabled >
                            <?php foreach($cod_pacientes as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->COD_PACIENTE === $model->COD_PACIENTE)? 'selected': '' ?> value="<?php echo $m->COD_PACIENTE?>" >
                                        <?php echo $m->FECHA_NAC?></option><?php endforeach; ?> </select >
                        </div>
                </div>
  <?php } ?> 
  <div class="form-row">
                    <div class="form-group col-md-9">
                        <strong>
                        <span class="ft-grid"></span>
                        <span> DATOS DEL ACCESO VASCULAR</span>
                        </strong>
                    </div>
                </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label for="inputCity">Tipo Acceso Vascular</label>
      <select class="form-control  " name="COD_TIPO_ACCESO" class="form-control "  required>
                                <option value=""></option>
                                <?php foreach($tipo_acceso as $m): ?><option <?php echo (!$esNuevo && $m->DES_CORTA === $model->COD_TIPO_ACCESO)? 'selected': '' ?> value="<?php echo $m->DES_CORTA?>"><?php echo $m->DES_LARGA?></option>
                            <?php endforeach; ?>
                            </select>
    </div>
    <div class="form-group col-md-4">
      <label for="inputState">Ubicación</label>
      <select class="form-control" name="UBICACION" class="form-control "required>
                            <option value=""> </option>
                                <optgroup label="FISTULA ARTERIOVENOSA">
                                    <?php foreach($cod_tipo_ubicaciones_fav as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->DES_CORTA === $model->UBICACION)? 'selected': '' ?>  value="<?php echo $m->DES_CORTA?>"><?php echo $m->DES_LARGA?></option>
                                    <?php endforeach; ?>
                                </optgroup>
                                <optgroup label="INJERTO">
                                <?php foreach($cod_tipo_ubicaciones_inj as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->DES_CORTA === $model->UBICACION)? 'selected': '' ?>  value="<?php echo $m->DES_CORTA?>"><?php echo $m->DES_LARGA?></option>
                                    <?php endforeach; ?>
                                </optgroup>
                                <optgroup label="CATóTER VENOSO CENTRAL TEMPORAL">
                                <?php foreach($cod_tipo_ubicaciones_cvt as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->DES_CORTA === $model->UBICACION)? 'selected': '' ?>  value="<?php echo $m->DES_CORTA?>"><?php echo $m->DES_LARGA?></option>
                                    <?php endforeach; ?>
                                </optgroup>
                                <optgroup label="CATÉTER VENOSO CENTRAL PERMANENTE">
                                <?php foreach($cod_tipo_ubicaciones_cvp as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->DES_CORTA === $model->UBICACION)? 'selected': '' ?>  value="<?php echo $m->DES_CORTA?>"><?php echo $m->DES_LARGA?></option>
                                    <?php endforeach; ?>
                                </optgroup>
                            </select>
    </div>
    <div class="form-group col-md-4">
      <label for="inputZip">Cirujano</label>
      <input onkeyup="mayus(this);" type="text" class="form-control "  name="CIRUJANO_CV" placeholder="APELLIDOS, NOMBRES"  value="<?php echo $esNuevo? '': $model->CIRUJANO_CV?>" required>
  </div>


  </div>
  <div class="form-row">
    <div class="form-group col-md-5">
      <label for="inputCity">Fecha de Creación</label>
      <input type="date" class="form-control " id="validationDefault04" placeholder="" name="FECHA_CREACION_AV"placeholder="" value="<?php echo $esNuevo? '': $model->FECHA_CREACION_AV?>" required>
   
    </div>
    <div class="form-group col-md-7">
      <label for="inputState">Clinica</label>
      <select class="form-control  " name="COD_CENCLI" id="inputState" class="form-control " required>
                            <option value=""> 
                                    </option>
                                <?php foreach($cod_cenclin as $m): ?><option <?php echo (!$esNuevo && $m->COD_CENCLI === $model->COD_CENCLI)? 'selected': '' ?> value="<?php echo $m->COD_CENCLI?>"><?php echo $m->DES_LARGA ?></option>
                                <?php endforeach; ?>
            </select>
    </div>
  </div>
   
                <!-- <div class="form-row">    
                <div class="col-md-2 mb-4">
                    <label for="inputCity">Nro. Doc</label>
                        <div class="input-group input-group-sm mb-3">
                            <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-sm">	<i  class="ft-credit-card"></i></span>
                            </div>

                            <input id="select_docp" name="NRO_DOC" type="text" class="form-control" value="<?php echo $esNuevo || !$esNuevo? '': $m->NRO_DOC?>"disabled>
                           
                        </div>
                    </div>
                    <div class="col-md-2 mb-4">
                    <label for="inputCity">Edad</label>
                        <div class="input-group input-group-sm mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-sm" >	<i class="ft-trending-up"></i></span>
                            </div>
                            <input id="select_edad" name="EDAD" type="text" class="form-control" aria-label="Small" placeholder="Edad" aria-describedby="inputGroup-sizing-sm" value="<?php echo $esNuevo || !$esNuevo? '': $m->UBICACION?>" disabled>
                        </div>
                    </div>
                    <div class="col-md-2 mb-4">
                    <label for="inputCity">Fecha Nac.</label>
                        <div class="input-group input-group-sm mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-sm">	<i class="ft-calendar"></i></span>
                            </div>
                            <input  id="select_fnac" name= "FECHA_NAC" type="date" class="form-control form-control-sm" id="validationDefault04" placeholder="" value="<?php echo  $esNuevo || !$esNuevo? '': $m->FECHA_NAC?>" disabled>
                            </div>
                        </div>
                </div> -->
 
                <div class="form-row">
                    <div class="form-group col-md-9">
                        <strong>
                        <span class="ft-grid"></span>
                        <span> FECHAS DEL MOVIMIENTO</span>
                        </strong>
                    </div>
                </div>
  <div class="form-row">

    <div class="form-group col-md-5">
      <label for="mov_start">Fecha de Inicio</label>
      <input type="date" class="form-control"  name="FECHA_I"  value="<?php echo $esNuevo? '': $model->FECHA_I?>"required>
    </div>
    <div class="form-group col-md-7">
      <label for="inputState">Condición de Inicio</label>
      <select id="mov_start"  class="form-control"  name="MOTIVO_I"  required>
                        <option value=""> </option>
                                <optgroup label="AV NUEVOS EN CONTINUADORES">
                                    <?php foreach($ingresoavnc as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->COD_INT === $model->MOTIVO_I)? 'selected': '' ?> value="<?php echo $m->COD_INT?>">
                                        <?php echo $m->DES_LARGA?></option>
                                    <?php endforeach; ?>
                                </optgroup>
                                <optgroup label="REINGRESO">
                                <?php foreach($ingresoavreingreso as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->COD_INT === $model->MOTIVO_I)? 'selected': '' ?> value="<?php echo $m->COD_INT?>">
                                        <?php echo $m->DES_LARGA?></option>
                                    <?php endforeach; ?>
                                </optgroup>
                                <optgroup label="NUEVO">
                                <?php foreach($ingresoavnuevo as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->COD_INT === $model->MOTIVO_I)? 'selected': '' ?> value="<?php echo $m->COD_INT?>">
                                        <?php echo $m->DES_LARGA?></option>
                                    <?php endforeach; ?>
                                </optgroup>
        </select>
    </div>
  </div>

      <div class="form-row">
    <div class="form-group col-md-5">
      <label for="mov_end">Fecha de Fin</label>
      <input type="date" class="form-control" name="FECHA_E"  value="<?php echo $esNuevo? '': $model->FECHA_E?>">
        
    </div>
    <div class="form-group col-md-7">
      <label for="inputState">Condición de Fin</label>
      <select   id="mov_end" class="form-control  "  name="MOTIVO_E" >
                        <option value="0"></option>
                                <optgroup label="FAV">
                                <?php foreach($egresoavfav as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->COD_INT === $model->MOTIVO_E)? 'selected': '' ?> value="<?php echo $m->COD_INT?>">
                                        <?php echo $m->DES_CORTA?></option>
                                    <?php endforeach; ?>
                                </optgroup>

                                 <optgroup label="CVC">
                                 <?php foreach($egresoavcvc as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->COD_INT === $model->MOTIVO_E)? 'selected': '' ?> value="<?php echo $m->COD_INT?>">
                                        <?php echo $m->DES_CORTA?></option>
                                    <?php endforeach; ?>
                                 </optgroup>
                                 <optgroup label="OTRAS CAUSAS">
                                <?php foreach($egresoavotras as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->COD_INT === $model->MOTIVO_E)? 'selected': '' ?> value="<?php echo $m->COD_INT?>">
                                        <?php echo $m->DES_CORTA?></option>
                                    <?php endforeach; ?>
                                </optgroup>
        </select>
    </div>
    <fieldset >
    <div class="chip">
        Estado del Acceso Vascular:  
                <div class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input" type="radio" name="ACTIVO" id="mov_active" <?php echo (!$esNuevo && $model->ACTIVO === '1')?'checked':'' ?> value="1">
                    <label class="custom-control-label"for="mov_active"><strong>ACTIVO </strong></label>
                </div> 
                <div class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input" type="radio" name="ACTIVO" id="mov_inactive" <?php echo (!$esNuevo && $model->ACTIVO === '0')? 'checked':'' ?> value="0"  >
                    <label class="custom-control-label"for="mov_inactive"><strong>INACTIVO </strong></label>
                </div> 
     </div>
</div>                      
 <br>
 </fieldset>
   <a class="btn btn-secondary" href="<?php echo site_url('acceso_vascular'); ?>" >Cancelar</a>
                <button class="btn btn-primary" type="submit">
                     <?php echo $esNuevo? 'Agregar': 'Actualizar' ?>
                </button>
</form>
    <?php echo form_close(); ?>

</div>


 
<style>
 
    .chip {
        display: inline-block;
        padding: 0 25px;
        height: 50px;
        font-size: 16px;
        line-height: 50px;
        border-radius: 25px;
        background-color: #f1f1f1;
    }

    .chip img {
        float: left;
        margin: 0 10px 0 -25px;
        height: 50px;
        width: 50px;
        border-radius: 50%;
    }
</style>


<script>
    $('#mov_start').change(function  (valor) {
    console.log('Fecha de Fin', valor.target.value)   
    if (valor.target.value != undefined || valor.target.value ) {
    $('#mov_active').removeAttr("disabled")
    $('#mov_inactive').attr("disabled", "disabled")
    $('#mov_active').prop('checked',true)
    }
    
    });
    $('#mov_end').change(function  (valor) {
    console.log('Fecha de Fin', valor.target.value)   
    if (valor.target.value != undefined || valor.target.value ) {
    $('#mov_inactive').removeAttr("disabled")
    $('#mov_active').attr("disabled", "disabled")
    $('#mov_inactive').prop('checked',true)
    }
    });
</script>



<script> 
    function onChange_Paciente () { 
    var e= document.getElementById("select_paciente");
    var strUser= e.options[e.selectedIndex].value;
    console.log(strUser)

    var  pact=<?php echo(json_encode($cod_pacientes));?>;
    console.log(pact)

    pact = pact || [];
    var pac = pact.find(pac => pac.COD_PACIENTE === strUser);
    console.log(pac)

    document.getElementById("select_docp").value = pac.NRO_DOC

    document.getElementById("select_fnac").value = pac.FECHA_NAC
    //document.getElementById("select_ree").value = prof.REE  --edad .----
    }
</script>

<style>
    .custom-combobox {
        position: relative;
        display: inline-block;
    }
    .custom-combobox-toggle {
        position: absolute;
        top: 0;
        bottom: 0;
        margin-left: -1px;
        padding: 0;
        *top: 0.1em;   
        border: 18px black;
    }
    .custom-combobox-input {
        margin: 8;
        height: 2em;
        width: 38.1em;
        padding: 0.3em;
        background: white;
        color: #7C7C7D;
        font-family : inherit;
  font-size   : 100%;
    }

</style>


<style>
    .chip {
        display: inline-block;
        padding: 0 25px;
        height: 50px;
        font-size: 16px;
        line-height: 50px;
        border-radius: 25px;
        background-color: #f1f1f1;
    }

    .chip img {
        float: left;
        margin: 0 10px 0 -25px;
        height: 50px;
        width: 50px;
        border-radius: 50%;
    }
</style>
</head>
<body>

