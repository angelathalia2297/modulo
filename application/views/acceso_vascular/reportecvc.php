<script src="<?php echo base_url('assets/plugins/tablas/date/moment.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/tablas/date/daterangepicker.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tabla_fecha_mov.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/tablas/date/daterangepicker.css'); ?>">

<section class="content">
	<div class="row">
		<div class="col-lg-12">

		<div class="alert alert-info  " role="alert">
			 <h4 align="center"><p class="text-info    font-weight-light">
			 <strong>MOVIMIENTO DE PACIENTE SEGÚN CVCP</strong> </p></h4>

		</div>
			<div class="content-wrapper" style="min-height: 771px;">


<div class="card ">
	<div class="card-header">
			<div class="form-row">
					<div class="col-8">
					<strong>
							<span class="ft-clipboard"> </span>
							<span> REGISTROS</span>
							</strong>
					</div>
					<div class="col">
					</div>
					<div class="col">

<!-- incluir Modal -->

<a href="<?php echo site_url('movimiento/modal_mov'); ?>" class="btn btn-sm btn-primary">
								<i class="ft-plus-circle"></i>
								<b> Agregar Movimiento</b>
</a>
      	</div>
			</div> 
  	</div>	


    <div class="card-body">
		<div class="table-responsive">   

<!-- <form class="form-inline">
<div class="form-group mb-3">
    <input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="Búsqueda por fecha:">
  </div>
	   <div class="input-group mx-sm-1 mb-3">
			<input class="form-control form-control-sm" autocomplete="off" id="Date_search2" type="text" placeholder="Ingrese rango de fecha " />
			<div class="input-group-append ">
				<button class="btn btn-sm btn-danger" type="submit">x</button>
			</div>
		</div>


</form> -->
<table id="mov_total" class="table table-bordered" >
					<thead class="thead-dark">
					<tr>
						<th scope="col"><center> NOMBRES COMPLETOS</center> </th>
						<th class="no-sort"scope="col"><center>CONDICIÓN DE INICIO</th>
						<th class="no-sort"scope="col"><center>CONDICIÓN DE FIN</th>
                        <th class="no-sort"scope="col"><center>FECHA DE INICIO</th>
                        <th class="no-sort"scope="col"><center>DIAS</th>
                        <th class="no-sort"scope="col"><center>FECHA DE FIN</th>
                        <th class="no-sort"scope="col"><center>TIPO ACCESO VASCULAR</th>
						<th class="no-sort"scope="col"><center>UBICACIÓN</th>
						<th class="no-sort"scope="col"><center>PAR</th>
                        <th class="no-sort"scope="col"><center>ACCIONES</th>
					</tr>
					</thead>
					<tbody> 
          					<?php foreach($model as $m): ?>
							<td> 
							<?php echo $cod_pacientes[array_search($m->COD_PACIENTE, array_column($cod_pacientes, 'COD_PACIENTE'))]->APELLIDO_PATERNO ?>   
						<?php echo $cod_pacientes[array_search($m->COD_PACIENTE, array_column($cod_pacientes, 'COD_PACIENTE'))]->APELLIDO_MATERNO ?>, 
						<?php echo $cod_pacientes[array_search($m->COD_PACIENTE, array_column($cod_pacientes, 'COD_PACIENTE'))]->NOMBRES ?> 
					
							</td>
							
							<td> <?php echo $ingreso[array_search($m->MOTIVO_I, array_column($ingreso, 'COD_INT'))]->DES_LARGA ?></td> 
							
							<td> 
								<?php if($m->MOTIVO_E == '') {?>
									<?php 
										 echo "";
									?> 

								<?php } else {
									echo $egreso[array_search($m->MOTIVO_E, 
								 array_column($egreso, 'COD_INT'))]->DES_LARGA;
								} ?>
							</td> 
							<td>  <?php echo $m->FECHA_I?> </td>
							<td class='text-right dias'>
								<?php if($m->FECHA_E == '0000-00-00') {?>
									<?php 
										//CUANDO FECHA_E ES NULL SE CALCULA LOS DIAS CON LA FECHA ACTUAL
										ini_set('date.timezone','America/Lima'); 
										//echo date("g:i A"); 
										$date= date("Y-m-d");
										//$time=date("g:i A");
										$datetime=$date;
										7//echo date_format($date, 'd/m/Y H:i:s');		
									?> 
										
									<?php 
										$date1 = new DateTime("now");
										$date2 = new DateTime($m->FECHA_I); 
										$diff = $date1->diff($date2);
										// will output 2 days
										echo $diff->days;
										
									?>
								<?php } else { 
										$date1 = new DateTime($m->FECHA_E);
										$date2 = new DateTime($m->FECHA_I); 
										$diff = $date1->diff($date2);
										// will output 2 days
										echo $diff->days;

										// ($diff->days)/365;

								}?>

								</td>
                                <td> 
								 
								<?php if($m->FECHA_E ==  '0000-00-00') {?>
									<?php 
									//CUANDO FECHA_E ES NULL MUESTRA LA FECHA ACTUAL SINO SEMUESTRA LA FECHA REGISTRADA
									ini_set('date.timezone','America/Lima'); 
									//echo date("g:i A"); 
									$date= date("Y-m-d");
									//$time=date("g:i A");
									echo $date;
									//echo date_format($date, 'd/m/Y H:i:s');	
								?>  	
								<?php } else { 
									echo $m->FECHA_E;
								}
								?>
		
							
							</td>
							<td> <?php echo $m->COD_TIPO_ACCESO?> </td>
							<td><?php echo $m->UBICACION?>  </td>
							
							<td class='text-right subtotal'> 
									
								<?php if($m->FECHA_E == '0000-00-00') {?>
									<?php 
										//CUANDO FECHA_E ES NULL SE CALCULA LOS DIAS CON LA FECHA ACTUAL
										ini_set('date.timezone','America/Lima'); 
										//echo date("g:i A"); 
										$date= date("Y-m-d");
										//$time=date("g:i A");
										$datetime=$date;
										7//echo date_format($date, 'd/m/Y H:i:s');		
									?> 
										
									<?php 
										$date1 = new DateTime("now");
										$date2 = new DateTime($m->FECHA_I); 
										$diff = $date1->diff($date2);
										// will output 2 days
										$diff->days;

										$par= ($diff->days)/365;
										echo number_format($par,2,",",".");
									?>

								<?php } else { 
										//CUANDO TIENEN FECHA DE EGRESO EN LA BD
										$date1 = new DateTime($m->FECHA_E);
										$date2 = new DateTime($m->FECHA_I); 
										$diff = $date1->diff($date2);
										// will output 2 days
										 $diff->days;

										 $par= ($diff->days)/365;
										 echo number_format($par,2,",",".");

								}?>
							</td>
								<td>  <center>
								<div class="btn-group">
								<a class="btn btn-sm btn-primary" href="<?php echo site_url('acceso_vascular/crud/' . $m->PK_ACCESO_V); ?>" title="Editar">
									<i class="ft-edit"></i>
								</a>
							
								<a class="btn btn-sm btn-danger" href="<?php echo site_url('acceso_vascular/eliminar/' . $m->PK_ACCESO_V); ?> "onclick="return confirm('¿Esta seguro de eliminar  este Acceso Vascular?');"  title="Eliminar">
									<i class="ft-trash-2"></i>
								</a>	
								 
								</div>
								</td>       
							</tr>
              <?php endforeach; ?>
					</tbody>
					<tfoot>

		<tr>
		<th> </th>
		<th> </th>
		<th> </th>
		<th> </th>
		<th>
		</th> 
		<th> </th>
		<th> </th>	  
		<th> </th>
		<th>    
		</th>


	<th> </th>
		</tr>

	</tfoot>
</table>


  </div>
 </div>
  </div>

<script>
	var sum=0;
	$('.subtotal').each(function() {  
	sum += parseFloat($(this).text().replace(/,/g, ''), 10);  
	}); 
	$('#resultado_total').val(sum.toFixed(2));
</script>

<script>
	var sum=0;
	$('.dias').each(function() {  
	sum += parseFloat($(this).text().replace(/,/g, ''), 10);  
	}); 
	$('#diastotal').val(sum.toFixed());
</script>
