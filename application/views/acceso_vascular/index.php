<section class="content">
	<div class="row">
		<div class="col-lg-12">
		<div class="alert alert-primary  " role="alert">
			 <h4 align="center"><p class="text-info    font-weight-light">
			 <strong>ADMINISTRACIÓN DEL ACCESO VASCULAR</strong> </p></h4>
		</div>
<div class="card">
	<div class="card-header">
			<div class="form-row">
					<div class="col-8">
					<strong>
							<span class="ft-clipboard"> </span>
							<span> REGISTROS</span>
							</strong>
					</div>
					<div class="col">
					</div>
					<div class="col">
						<a href="<?php echo site_url('acceso_vascular/crud'); ?>" class="btn btn-sm btn-primary">
								<i class="ft-plus-circle"></i>
								<b> Agregar</b>
						</a>
						
					</div>
			</div> 
  	</div>	
	  <div class="card-body">
	  <div class="table-responsive table-hover table ">   
			<table id="av" class="table table-bordered" >
					<thead class="thead-dark">
				<tr>
					<th scope="col"> <center>AUTOGENERADO</th>
					<th scope="col"> <center>PACIENTE</th>
					<th scope="col"><center>FECHA DE CREACIÓN</th>
					<th class="no-sort"scope="col"><center>TIPO DE AV.</th>
					<th class="no-sort"scope="col"><center>UBICACIÓN</th>
					<th class="no-sort"scope="col"><center>CIRUJANO</th>
					<th class="no-sort"scope="col"><center>CLÍNICA</th>
					<th scope="col"><center>ESTADO </th>
					<th class="no-sort"scope="col"><center>ACCIONES</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($model as $m): ?>
					<tr>
					<td > <?php echo $m->AUTO?></td>
						<td> 
						
						<?php echo $cod_pacientes[array_search($m->COD_PACIENTE, array_column($cod_pacientes, 'COD_PACIENTE'))]->APELLIDO_PATERNO ?>   
						<?php echo $cod_pacientes[array_search($m->COD_PACIENTE, array_column($cod_pacientes, 'COD_PACIENTE'))]->APELLIDO_MATERNO ?>, 
						<?php echo $cod_pacientes[array_search($m->COD_PACIENTE, array_column($cod_pacientes, 'COD_PACIENTE'))]->NOMBRES ?> 
						</td> 
						<td > <?php echo $m->FECHA_CREACION_AV?></td>
						<td > 	<center>
							 <?php echo  
							 $cod_tipo_accesos[array_search($m->COD_TIPO_ACCESO, 
							 array_column($cod_tipo_accesos, 'DES_CORTA'))]->DES_LARGA ?>
			
						</td>
					    <td> <center>
						<?php echo  $ubicaciones[array_search($m->UBICACION, array_column($ubicaciones, 'DES_CORTA'))]->DES_LARGA ?>
						</td>
						<td> 	<center> <?php echo $m->CIRUJANO_CV?></td>
						<td> 	<center>
						 <?php echo $cod_cenclin[array_search($m->COD_CENCLI, array_column($cod_cenclin, 'COD_CENCLI'))]->DES_LARGA ?> </td>
						<td>
						<center>
						
						<h5>	<span class="badge badge-<?php echo $m->ACTIVO === '1'? 'success': 'warning' ?>"><?php echo $m->ACTIVO === '1'? 'ACTIVO': 'INACTIVO' ?></span></h5>
						</td>

						<td>	<center>
						<div class="btn-group">
								<a class="btn btn-sm btn-primary" href="<?php echo site_url('acceso_vascular/crud/' . $m->PK_ACCESO_V); ?>" title="Editar">
									<i class="ft-edit"></i>
								</a>
							
								<a class="btn btn-sm btn-danger" href="<?php echo site_url('acceso_vascular/eliminar/' . $m->PK_ACCESO_V); ?> "onclick="return confirm('¿Esta seguro de eliminar  este Acceso Vascular?');"  title="Eliminar">
									<i class="ft-trash-2"></i>
								</a>	
							
						</div>
						</td>
					</tr>
				<?php endforeach; ?>
				<tbody>
		</table>
		</div></div>
</section>