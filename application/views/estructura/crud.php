<!DOCTYPE html>
<html>
<section class="content">
<br>
<br>
		<div class="col-lg-12">
	<div class="row">
        <div class="col-md-10 mx-auto">
          <div class="card rounded-0">
            <div class="card-header">
		<!-- -->	<strong>
                          <?php
                          $titulo = 'Nueva Clínica';
                          $esNuevo = true;
                          if(is_object($model)){
                            $titulo = $model->DES_LARGA . ' '. $model->COD_CENCLI;
                            $esNuevo = false;
                          }
                          ?>
                          <h2 class="page-header"><p class="text-primary font-weight-light">
                            <?php echo $titulo?>
                          </h2>	 
              </div>
            <div class="card-body">
              <?php echo form_open('estructura/guardar', ['enctype' => 'multipart/form-data']); ?> 
                    <form> 
                    <div class="form-row ">
                            <div class="form-group">
                                <label for="validationDefault02">Código</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text"><i   class="ft-grid"></i></div>
                                </div>
                                     <input  name="COD_CENCLI" type="text" class="form-control"  required>
                                </div>
                            </div>
                        </div >
                        <div class="form-group">
                            <label for="validationDefault02">Nombre</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i  <i class="ft-home"></i></div>
                                </div>
                                <input onkeyup="mayus(this);" name="DES_LARGA" type="text" class="form-control"  required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="validationDefault02">Abreviatura</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text"><i  <i class="ft-compass"></i></div>
                                </div>
                                <input onkeyup="mayus(this);" name="DES_CORTA" type="text" class="form-control"   required>
                            </div>
                        </div>
     
                            <div class="form-row">
                              <div class="col">
                               <label for="validationDefault04">Origen</label>
                               <select name="ORICENASICOD" class="form-control form-control-m" required>
                                    <option value="">
                                      </option>
                                      <option <?php echo (!$esNuevo && $model->ORICENASICOD === '0')? 'selected': '' ?> value="0">EsSalud</option>
                                      <option <?php echo (!$esNuevo && $model->ORICENASICOD === '1')? 'selected': '' ?> value="1">Otros</option>
                              </select>
                              </div>
                              <div class="col">
                              <label for="validationDefault04">Centro Asistencial</label>
                                <input name="CENASICOD" type="text" class="form-control"  >
                              </div>
                            </div>
<br>

                          <a class="btn btn-secondary" href="<?php echo site_url('estructura'); ?>">Cancelar</a>
                            <button class="btn btn-primary" type="submit">
                           <?php echo $esNuevo? 'Agregar': 'Actualizar' ?>
                      </button>
          <?php echo form_close(); ?>
          </div>
        </div>
      </div>
      
</html>
