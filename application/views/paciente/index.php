<section class="content">
	<div class="row">
		<div class="col-lg-12">
		<div class="alert alert-primary  " role="alert">
			 <h4 align="center"><p class="text-info    font-weight-light">
			 <strong>PACIENTES</strong> </p></h4>
		</div>
		<?php echo form_open('paciente/crud', ['enctype' => 'multipart/form-data']); ?>
	<?php echo form_close(); ?>	

<?php  

if (empty($COD_PACIENTE['alert'])) {
  echo "";
} 

elseif ($_GET['alert'] == 1) {
  echo "<div class='alert alert-success alert-dismissable'>
		  <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
		  <h4>  <i class='icon fa fa-check-circle'></i> Exito!</h4>
		  Datos de medicamentos han sido registrado correctamente.
		</div>";
}
?>

<?php if( 1==NULL) { 
	echo "<div class='alert alert-success alert-dismissable'>
	<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
	<h5> <strong>  <i class='ft ft-check-circle'></i> Exito!</strong></h5>
	Los datos del paciente han sido almacenados correctamente.
	</div>";
}?>
<div class="card">
	<div class="card-header">
			<div class="form-row">
					<div class="col-8">
					<strong>
							<span class="ft-clipboard"> </span>
							<span> REGISTRO </span>
							</strong>
					</div>
					<div class="col">
					</div>
					<div class="col">
						<a href="<?php echo site_url('paciente/crud/'); ?>" class="btn btn-sm btn-primary">
								<i class="ft-plus-circle"></i>
								<b> Agregar Paciente </b>
						</a>
					</div>
			</div> 
  	</div>
		
	  <div class="card-body">
		<div class="table-responsive table-hover table ">   
		<table id="pacientes"class="table table-bordered" >
					<thead class="thead-dark">
				<tr>
					<th scope="col"><center>AUTOGENERADO</th>
					<th scope="col"><center>NOMBRE COMPLETO</th>
					<th class="no-sort"scope="col"><center>TIPO DE DOCUMENTO</th>
					<th class="no-sort"scope="col"><center>NRO. DOCUMENTO</th>
					<th class="no-sort"scope="col"><center>FECHA DE NACIMIENTO</th>
					<th scope="col"><center>EDAD</th>
					<th class="no-sort"scope="col"><center>SEXO</th>
					<th class="no-sort"scope="col"><center>CENTO ASISTENCIAL</th>
					<th scope="col"><center>ESTADO</th>
					<th class="no-sort"scope="col"><center>Acciones</th>
				</tr>
			</thead>
			<tbody> 
				<?php foreach($model as $m): ?>
					<tr>
					
						 <td > <center> <strong> <?php echo $m->AUTO?></td>  
						<td> <?php echo $m->APELLIDO_PATERNO?> <?php echo $m->APELLIDO_MATERNO?>, <?php echo $m->NOMBRES?></td>
						<td> <?php echo $m->DES_LARGA?></td>
						<td> <?php echo $m->NRO_DOC?></td>
						<td> <?php echo $m->FECHA_NAC?></td> 

						<td>
						
						<?php
						// CALCULAR EDAD
							$cumple = new DateTime($m->FECHA_NAC);
							$hoy = new DateTime("now");
							$edad = $hoy->diff($cumple);
							echo $edad->y; ?>
						
						</td>
						
						<td> <?php echo $m->SEXO?></td>
						<td> <?php echo $cenasicods[array_search($m->CENASICOD, array_column($cenasicods, 'CENASICOD'))]->CENASIDES ?></td>
						<td> <center> 
						<?php if($m->ACTIVO === '1') { 
							echo '<span title= "Paciente asiste a las sesiones de hemodiálisis"class="badge badge-success">ACTIVO</span>';
							}	elseif( $m->ACTIVO === '2'){
							
								echo 	'<span title= "Registre la fecha de inicio para activar al paciente" class="badge badge-warning">EN PROCESO</span>';
						
						} else {
						echo 	'<span title="Paciente egresado del centro" class="badge badge-danger">INACTIVO</span>';
						}
						?>

															
						</td>

						<td>	<CENTER> 
						<div class="btn-group"> 
								<a class="btn btn-sm btn-primary" href="<?php echo site_url('paciente/crud/' . $m->COD_PACIENTE); ?>" title="Editar">
									<i class="ft-edit"></i>
								</a>
								<a class="btn btn-sm btn-danger" href="<?php echo site_url('paciente/eliminar/' . $m->COD_PACIENTE); ?>" onclick="return confirm('¿Esta seguro de eliminar a este paciente?');" title="Eliminar">
									<i class="ft-trash-2"></i>
								</a>
						</div>
						</td>
					</tr>
						
				<?php endforeach; ?>



			</tbody>
			
		</table>	
		

    </div>

	</div>
</div>
 

</div>
