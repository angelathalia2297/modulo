

  
<div class="container py-5">
<div class="card">
	<div class="card-header">
			<div class="form-row">
					<div class="col-8">
		<!-- -->	<strong>
							<?php
							$titulo = 'Nuevo Paciente';
							$esNuevo = true;
							if(is_object($model)){
								$titulo = $model->NOMBRES . ' '. $model->APELLIDO_PATERNO . ' '. $model->APELLIDO_MATERNO. ' - NRO. DOC: '. $model->NRO_DOC;
								$esNuevo = false;
							}
							?>
							<h2 class="page-header"><p class="text-primary font-weight-light">
								<?php echo $titulo?>
							</h2>		
			<!--    -->	
					</div>
			</div> 
  	</div>
	<div class="card-body">  
	<?php echo form_open('paciente/guardar', ['enctype' => 'multipart/form-data']); ?>
<form>
 <div class="form-row"> 	
 <div class="col-md-4 mb-3">
 <label for="inputState">Tipo de Documento</label> 
    <select class="form-control " name="TIPO_DOC" required><option value=""></option><?php foreach($tipo_docs as $m): ?><option <?php echo (!$esNuevo && $m->COD_INT === $model->TIPO_DOC)? 'selected': '' ?> value="<?php echo $m->COD_INT?>"><?php echo $m->DES_LARGA?></option><?php endforeach; ?></select>
	</div>
	<input type="hidden" name="COD_PACIENTE" value="<?php echo $esNuevo? '': $model->COD_PACIENTE ?>" />
  			<div class="col-md-4 mb-3">
				<label for="validationTooltipUsername">N° Documento</label>
			<div class="input-group">
				<input name="NRO_DOC" class="form-control " id="" type="text" value="<?php echo $esNuevo? '': $model->NRO_DOC?>"required>
			</div>
			</div>
		<div class="col-md-6 mb-3"> 
		</div>
	</div>
	<div class="form-row">
		<div class="col-md-4 mb-3">
			<label for="validationTooltip01">Apellido Paterno</label>
			<input onkeyup="mayus(this);" name="APELLIDO_PATERNO" class="form-control " id="validationTooltip01" type="text" value="<?php echo $esNuevo? '': $model->APELLIDO_PATERNO?>"required>
			<div class="valid-tooltip"></div>
		</div>
		<div class="col-md-4 mb-3">
			<label for="validationTooltip02">Apellido Materno</label>
			<input onkeyup="mayus(this);" name="APELLIDO_MATERNO" class="form-control " id="validationTooltip02" type="text" value="<?php echo $esNuevo? '': $model->APELLIDO_MATERNO?>"required>
			<div class="valid-tooltip"></div>
		</div>
		<div class="col-md-4 mb-3">
			<label for="validationTooltip04">Nombres</label>
			<input onkeyup="mayus(this);" name="NOMBRES" class="form-control " id="validationTooltip04" type="text" value="<?php echo $esNuevo? '': $model->NOMBRES?>"required>
			
		</div>
	</div>
	<div class="form-row">
		<div class="col-md-4 mb-3">
			<label for="inputState">Sexo</label>
			<select class=" input-large form-control " name="SEXO" class="form-control " id="inputState"required>
				<option value="">
					</option>
				<option <?php echo (!$esNuevo && $model->SEXO === 'M')? 'selected': '' ?> value="M">MASCULINO</option>
				<option <?php echo (!$esNuevo && $model->SEXO === 'F')? 'selected': '' ?> value="F">FEMENINO</option>
			</select>
		</div>

		<div class="col-md-4 mb-3">
			<label for="validationTooltipUsername">Fecha de Nacimiento</label>
	
				<input type="date" name="FECHA_NAC" class="form-control " value="<?php echo $esNuevo? '': $model->FECHA_NAC?>" required >
			
		</div>
	</div>
	<div class="form-row">

		<?php if(!$esNuevo){?>
		<div class="col-md-4 mb-3">
								<label for="validationDefault04">  Estado </label>
								<select name="ACTIVO" class="form-control " required>
									<option value="">
									<< Seleccione >></option>
									<option <?php echo (!$esNuevo && $model->ACTIVO === '1')? 'selected': '' ?> value="1">Activo</option>
									<option <?php echo (!$esNuevo && $model->ACTIVO === '0')? 'selected': '' ?> value="0">Inactivo</option>
								</select>
			</div>
		<?php } ?>
		

		<!-- <div class="col-md-4 mb-3">
			<label for="inputState">Centro Asistencial</label>
			<select name="CENASICOD" class="form-control " id="inputState"required>
				<option value="">
					<< Seleccione>></option>
				<?php foreach($cenasicods as $m): ?>
				<option <?php echo (!$esNuevo && $m->CENASICOD === $model->CENASICOD)? 'selected': '' ?> value="<?php echo $m->CENASICOD?>">
					<?php echo $m->CENASIDES ?>
				</option>
				<?php endforeach; ?>
			</select>
		</div> -->

		
	</div>

	<a class="btn btn-secondary" href="<?php echo site_url('paciente'); ?>" >Cancelar</a>
	<button class="btn btn-primary" type="submit">
		<?php echo $esNuevo? 'Agregar': 'Actualizar' ?>
	</button>
	<?php echo form_close(); ?>
</div>


<style>
.ui-tooltip {
        border: 9px  white;
        background: rgba(34, 9, 9, 1);
        color: white;
}
  .custom-combobox {
    position: relative;
    display: inline-block;
  }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
    *top: 0.1em;   
    border: 18px black;
  }
  .custom-combobox-input {
	margin: 2;
    height: 1.7em;
    width: 15.1em;
    padding: 0.3em;
    background: white;
    color: black;
  }

  </style>
