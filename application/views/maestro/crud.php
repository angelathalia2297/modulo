<!DOCTYPE html>
<html>
<section class="content">
<br>
<br>
      <div class="row">
        <div class="col-md-10 mx-auto">
          <div class="card rounded-0">
            <div class="card-header">
		<!-- -->	<strong>

                <?php
                          $titulo = 'Nuevo Tipo';
                          $esNuevo = true;
                          if(is_object($model)){
                            $titulo = $model->DES_CORTA . ' '. $model->COD_TIPO;
                            $esNuevo = false;
                          }
                          ?>
                          <h2 class="page-header"><p class="text-primary font-weight-light">
                            <?php echo $titulo?>
                          </h2>		
                  <!--    -->	
              </div>
            <div class="card-body">
            <?php echo form_open('maestro/guardar', ['enctype' => 'multipart/form-data']); ?>

              <input type="hidden" name="COD_TIPO" value="<?php echo $esNuevo? '': $model->COD_TIPO ?>" />
      <form> 
         
                    <div class="form-row">
                    <div class="form-group col-md-8">
                        <label for="inputEmail4">Descripción</label>
                        <input onkeyup="mayus(this);" type="text" class="form-control" name="DES_LARGA" value="<?php echo $esNuevo? '': $model->DES_LARGA?>"required>
                        </div>
                        <div class="form-group col-md-8">
                        <label for="inputPassword4">Abreviatura</label>
                        <input onkeyup="mayus(this);" type="text" class="form-control" name="DES_CORTA" id="inputPassword4" value="<?php echo $esNuevo? '': $model->DES_CORTA?>"required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                        <label for="inputAddress">Codigo interno de orden</label>
                        <input type="text" min="0" class="form-control" name="COD_INT"  id="inputAddress" value="<?php echo $esNuevo? '': $model->COD_INT?>">
                        </div>
                        <div class="form-group col-md-6">
                        <label for="inputAddress2">Codigo para relación</label>
                        <input type="number" min="0" class="form-control" name="COD_REL" id="inputAddress2" value="<?php echo $esNuevo? '': $model->COD_REL?>">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputZip">Codigo tipo padre</label>
                            <input type="number"  min="0" class="form-control"name="COD_TIP_PAD" value="<?php echo $esNuevo? '': $model->COD_TIP_PAD?>">
                        </div>
                  
                        <div class="form-group col-md-4">
                            <label for="validationDefault02">Nivel tipo</label>
                            <input name="NIV_TIPO" min="0" type="number" class="form-control"  value="<?php echo $esNuevo? '': $model->NIV_TIPO?>">
                        </div>
                    </div>
                    <?php if(!$esNuevo){?>
                          <div class="col-md-4 mb-3">
                          <label for="validationDefault04">  Estado </label>
                          <select name="ACTIVO" class="form-control form-control-sm" required>
                            <option value="">
                            << Seleccione >></option>
                            <option <?php echo (!$esNuevo && $model->ACTIVO === '1')? 'selected': '' ?> value="1">ACTIVO</option>
                            <option <?php echo (!$esNuevo && $model->ACTIVO === '0')? 'selected': '' ?> value="0">INACTIVO</option>
                          </select>
                          </div>
		                <?php } ?>
		

                          <a class="btn btn-secondary" href="<?php echo site_url('maestro'); ?>">Cancelar</a>
                            <button class="btn btn-primary" type="submit">
                           <?php echo $esNuevo? 'Agregar': 'Actualizar' ?>
                    </form>           
                      </button>
          <?php echo form_close(); ?>
          </div>
        </div>
      </div>
      
</html>
<style>
	  .ui-tooltip {
        border: 9px  white;
        background: rgba(34, 9, 9, 1);
        color: white;
      }

	</style>