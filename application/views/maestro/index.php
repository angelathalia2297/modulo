<section class="content">
    <div class="alert alert-primary  " role="alert">
			 <h4 align="center"><p class="text-info    font-weight-light">
			 <strong> TABLA MAESTRO</strong> </p></h4>

		</div>
		<div class="row">	
<div class="card">
	<div class="card-header">
			<div class="form-row">
					<div class="col-8">
					<strong>
							<span class="ft-clipboard"> </span>
							<span> REGISTROS </span>
							</strong>
					</div>
					<div class="col">
					</div>
					<div class="col">
						<a href="<?php echo site_url('maestro/crud'); ?>" class="btn btn-sm btn-primary">
								<i class="ft-plus-circle"></i>
								<b> Nuevo Tipo</b>
						</a>
					</div>
			</div> 
  	</div>	
    <div class="card-body">
	  <div class="table-responsive table-hover table ">   
			<table id="maestro" class="table table-bordered" >
			<thead class="thead-dark">
            <tr>
              <th scope="col"><center>#</th>
              <th scope="col"><center>DESCRIPCIÓN CORTA</th>          
              <th scope="col"><center>DESCRIPCIÓN LARGA</th>
              <th class="no-sort"scope="col"><center>CODIGO INTERNO</th>
              <th class="no-sort"scope="col"><center>CODIGO RELACION</th>
              <th class="no-sort"scope="col"><center>CODIGO PADRE</th>
              <th class="no-sort"scope="col"><center>NIVEL TIPO</th>
              <th class="no-sort"scope="col"><center>ESTADO</th>
              <th class="no-sort"scope="col"><center>ACCIONES</th>
            </tr>
          </thead>
            <tbody>
            <?php foreach($model as $m): ?>                
                <tr>
                    <td scope="row"> <strong> <?php echo $m->COD_TIPO?></td>
                    <td> <?php echo $m->DES_CORTA?></td>
                    <td><?php echo $m->DES_LARGA?> </td>
                    <td> <?php echo $m->COD_INT?></td>
                    <td><?php echo $m->COD_REL?> </td>
                    <td> <?php echo $m->COD_TIP_PAD?></td>
                    <td><?php echo $m->NIV_TIPO?> </td>
                    <td> <center> 	<span class="badge badge-<?php echo $m->ACTIVO === '1'? 'success': 'warning' ?>"><?php echo $m->ACTIVO === '1'? 'ACTIVO': 'INACTIVO' ?></span>  
					</td>
                    <td> 
                    <div class="btn-group" >
                                    <a class="btn btn-sm btn-primary" href="<?php echo site_url('maestro/crud/'. $m->COD_TIPO); ?>" title="Editar">
                                                <i class="ft-edit"></i>
                                            </a>					
                                            <a class="btn btn-sm btn-danger" href="<?php echo site_url('maestro/eliminar/'. $m->COD_TIPO); ?>"onclick="return confirm('¿Esta seguro de eliminar a este tipo?');" title="Eliminar">
                                                <i class="ft-trash-2"></i>
                                            </a>	

                    </div>       
                    </td>
                </tr>
			<?php endforeach; ?>
            </tbody>
            </table>	
            
            <colspan="12">
				<?php echo $this->pagination->create_links(); ?>
				
		</table>
  
    </div>
    </div>
</div>
</html>
</form>
<style>
	  .ui-tooltip {
        border: 9px  white;
        background: rgba(34, 9, 9, 1);
        color: white;
      }

	</style>