<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="author" content="Kodinger">
    <title>Login |  &mdash; Vigilancia AV  </title>

    <script src="<?php echo base_url('assets/plugins/jQuery/jquery-3.3.1.slim.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/plugins/jQuery/jquery.min.js'); ?>"></script>
	
	<script src="<?php echo base_url('assets/plugins/boostrap_4_0/js/bootstrap.min.js'); ?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/boostrap_4_0/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/boostrap_4_0/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/my-login.css'); ?>">
    <script src="<?php echo base_url('assets/js/mayus.js'); ?>"></script>
    <link rel="icon" type="icon/ico" href="<?php echo base_url('assets/imgs/logo_cnsr.png'); ?>" />
</head>
<body class="my-login-page">
	<section class="h-100">
		<div class="container h-100">
			<div class="row justify-content-md-center h-100">
				<div class="card-wrapper">
                <center>
                <img src="<?php echo base_url('assets/imgs/login_cnsr.png'); ?>" alt="Card image cap" class="rounded mx-auto d-block" border="0" width="500" height="250"/>
                </center>
					<div class="card fat">
                    <?php 
                      $titulo =  '';
                      $esNuevo = true;
                      ?>
                      <?php echo $titulo?>
						<div class="card-body">
							<h4 class="card-title">Iniciar Sesión</h4>
                            <?php echo form_open('auth/autenticar'); ?>
                                <form>
                                <?php if(isset($error)): ?>
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <?php echo $error; ?> 
                                    </div>   
                                    <?php endif; ?>
							 
								<div class="form-group">
									<label for="email">Usuario</label>

									<input   class="form-control" onkeyup="mayus(this);"  name="Usuario" type="text" value="" required >
								</div>

								<div class="form-group pass_show ">
									<label for="password">Contraseña  </label>
									<input    type="password" class="form-control" name="Clave" required  >
								</div>

								<div class="form-group"> 
                                <label for="validationDefault02">CENTRO ASISTENCIAL</label>
                               <select class="form-control" name="Cenasicod" required>
                                <option value="" selected="selected">Ingrese Nombre de CAS</option>
                                <?php foreach($cenasicods as $m): ?>
                                <option <?php echo (!$esNuevo && $m->CENASICOD === $model->Cenasicod)? 'selected': '' ?> value="<?php echo $m->CENASICOD?>"
                                ><?php echo $m->CENASIDES ?></option>
                                <?php endforeach; ?>
                                </select>

								</div>

								<div class="form-group no-margin">
									<button type="submit" class="btn btn-primary btn-block">
										Ingresar
									</button>
								</div>
								 
                            </form>
                            <?php echo form_close(); ?>   
						</div>
					</div>
					<div class="footer">
						EsSalud &copy; CNSR 2018
					</div>
				</div>
			</div>
		</div>
	</section>
 
    <script src="<?php echo base_url('assets/js/my-login.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/jQuery/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/funciones.js'); ?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/funciones.css'); ?>">
        <!-- LOS "script" CONECTAN AL JQUERY 3.3.1  y las funciones -->

