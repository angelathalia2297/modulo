

<!-- <script src="<?php echo base_url('assets/plugins/kendo_UI/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/kendo_UI/kendo.all.min.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/kendo_UI/kendo.common-material.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/kendo_UI/kendo.material.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/plugins/kendo_UI/kendo.material.mobile.min.css'); ?>"> -->

 <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.3.1017/styles/kendo.common-material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.3.1017/styles/kendo.material.min.css" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.3.1017/styles/kendo.material.mobile.min.css" />
<script src="https://kendo.cdn.telerik.com/2018.3.1017/js/jquery.min.js"></script> 
<script src="https://kendo.cdn.telerik.com/2018.3.1017/js/kendo.all.min.js"></script> 

<script> 
    function onChange_Profesional () {

    var e= document.getElementById("select_profesional");

    var strUser= e.options[e.selectedIndex].value;
    console.log(strUser)

    var  profs=<?php echo(json_encode($cod_profesionales));?>;
    console.log(profs)

    profs = profs || [];
    var prof = profs.find(prof => prof.COD_PROFESIONAL === strUser);
    console.log(prof)

    document.getElementById("select_dni").value = prof.NUM_DOC

    document.getElementById("select_ceep").value = prof.CEEP

    document.getElementById("select_ree").value = prof.REE

    }

</script>
<script> 
    function onChange_av() {

    var e= document.getElementById("select_av");

    var strUser= e.options[e.selectedIndex].value;
    console.log(strUser)

    var  avs=<?php echo(json_encode($acceso_vascular));?>;
    console.log(avs)

    avs = avs || [];
    var av = avs.find(av => av.PK_ACCESO_V === strUser);
    console.log(av)
   
    document.getElementById("select_ubi").value = av.UBICACION

    document.getElementById("select_tipo").value = av.COD_TIPO_ACCESO

    document.getElementById("select_fc").value = av.FECHA_CREACION_AV

    document.getElementById("select_cj").value = av.CIRUJANO_CV

    document.getElementById("select_docp").value = av.NRO_DOC

    document.getElementById("select_fnac").value = av.FECHA_NAC

    document.getElementById("select_auto").value = av.AUTO

    }

</script>

 <style>
           #tshirt {
               display: block;
               margin: 1em auto;
           }
           .k-readonly
           {
               color: white;
           }
 </style>

<script>
                $(document).ready(function() {
                    // create ComboBox from input HTML element
                    $(".fabric").kendoComboBox( );
                    // create ComboBox from select HTML element
                    $(".size").kendoComboBox( );

                    var fabric = $(".fabric").data("kendoComboBox");
					var select = $(".size").data("kendoComboBox");

                });
</script>

<section class="content">
	<div class="row">
		<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
                <div class="form-row">
                        <div class="col-8">
                        <?php
							$titulo = 'Nueva Evaluación';
							$esNuevo = true;
							if(is_object($model)){
								$titulo = 'NRO.' .$model->PK_EVALUACION. ' - ' .$model->FECHA_EVAL ;
								$esNuevo = false;
							}
					    ?>
							<h2 class="page-header"><p class="text-primary font-weight-light">
								<?php echo $titulo?>
                            </h2>
                
                        </div>
                </div> 
        </div>

    <div class="card-body"> 
    <?php echo form_open('vigilancia/guardar', ['enctype' => 'multipart/form-data']);?>
          <input type="hidden" name="PK_EVALUACION" value="<?php echo $esNuevo? '': $model->PK_EVALUACION ?>" />
	        <form>
                            <div class="form-row">
                                <div class="form-group col-md-9">
                                <strong>
                                    <span class="ft-bookmark"></span>
                                    <span> DATOS DEL EVALUADOR</span>
                                    </strong>
                                </div>
                            </div>
                    <div class="form-row was-validated"></div>
                    <div class="form-row ">
                   
                        <div class="col-md-4 mb-4">

                         <label for="inputCity">Nombres y Apellidos</label>   
                         <div class="input-group input-group-sm mb-3">
                          <select  id="select_profesional"  onchange="onChange_Profesional()" name="COD_PROFESIONAL"placeholder="Ingrese Nombres" class="fabric" style="width: 100%;" /> 
 <option value="">  </option>
        <?php foreach($cod_profesionales as $m): ?>
      <option <?php echo (!$esNuevo && $m->COD_PROFESIONAL === $model->COD_PROFESIONAL)? 'selected': '' ?> 
      value="<?php echo $m->COD_PROFESIONAL?>">
      <?php echo $m->APELLIDO_PAT ?> <?php echo $m->APELLIDO_MAT?>, <?php echo $m->NOMBRES?></option>
      <?php endforeach; ?>
 </select>
                            </div>
                        </div>
                        <?php if($esNuevo){?>  
                        <div class="col-md-2 mb-4">  
                        <label for="inputCity">Nro. Doc</label>  
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend"> 
                                    <span class="input-group-text" id="inputGroup-sizing-sm">	<i  class="ft-credit-card"></i></span>
                                </div>
                                    <input id="select_dni" name="NUM_DOC" type="text" class="form-control" aria-label="Small"  aria-describedby="inputGroup-sizing-sm" 
                                    value="<?php echo $esNuevo || !$esNuevo? '': $m->NRO_DOC?>" disabled>
                            </div>
                        </div>						
                        <div class="col-md-2 mb-4">
                        <label for="inputCity">Cep</label>  
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend"> 
                                    <span class="input-group-text" id="inputGroup-sizing-sm">	<i  class="ft-home"></i></span>
                                </div>
                                    <input  id="select_ceep" type="text" class="form-control" aria-label="Small" name="CEEP" aria-describedby="inputGroup-sizing-sm" 
                                    value="<?php echo $esNuevo || !$esNuevo? '': $m->CEEP?>"disabled>
                            </div>
                        </div>	
                        <div class="col-md-2 mb-4">
                        <label for="inputCity">Ree</label>  
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend"> 
                                    <span class="input-group-text" id="inputGroup-sizing-sm">	<i   class="ft-briefcase"></i></span>
                                </div>
                                    <input  id="select_ree"  type="text" class="form-control" aria-label="Small" name="REE" aria-describedby="inputGroup-sizing-sm" 
                                    value="<?php echo $esNuevo || !$esNuevo? '': $m->REE?>" disabled>
                            </div>
                        </div>	
                        <div class="col-md-2 mb-4">
                        <label for="inputCity">   </label>  
                            <div class="input-group input-group-sm mb-3">
                            <a href="<?php echo site_url('profesional/crud/'); ?>" class="btn btn-sm btn-primary">
                                    <i class="ft-plus-circle"></i>
                                    <b> Nuevo Evaluador </b>
                                    </a>
                            </div>
                          
                        </div>	
                    <?php } ?>  	
                       <!--  CRUD-->
                    <?php if(!$esNuevo){?>  
                        <div class="col-md-2 mb-4">  
                        <label for="inputCity">Nro. Doc</label>  
                        <select   name="NUM_DOC"  class="form-control form-control-sm" disabled >
                               <option > </option>
                                    <?php foreach($cod_profesionales as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->COD_PROFESIONAL === $model->COD_PROFESIONAL)? 'selected': '' ?> value="<?php echo $m->COD_PROFESIONAL?>" >
                                        <?php echo $m->NUM_DOC?> 
                                </option>
                            <?php endforeach; ?>
                            </select >
                        </div>						
                        <div class="col-md-2 mb-4">
                        <label for="inputCity">Cep</label>  
                            <select   name="CEEP"  class="form-control form-control-sm" disabled >
                               <option > </option>
                                    <?php foreach($cod_profesionales as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->COD_PROFESIONAL === $model->COD_PROFESIONAL)? 'selected': '' ?> value="<?php echo $m->COD_PROFESIONAL?>" >
                                        <?php echo $m->CEEP?> 
                                </option>
                            <?php endforeach; ?>
                            </select >
                        </div>	
                        <div class="col-md-2 mb-4">
                        <label for="inputCity">Ree</label>  
                        <select   name="REE"  class="form-control form-control-sm" disabled >
                               <option > </option>
                                    <?php foreach($cod_profesionales as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->COD_PROFESIONAL === $model->COD_PROFESIONAL)? 'selected': '' ?> value="<?php echo $m->COD_PROFESIONAL?>" >
                                        <?php echo $m->REE?> 
                                </option>
                            <?php endforeach; ?>
                            </select >
                        </div>	
                        <div class="col-md-2 mb-4">
                        <label for="inputCity">   </label>  
                            <div class="input-group input-group-sm mb-3">
                            <a href="<?php echo site_url('profesional/crud/'); ?>" class="btn btn-sm btn-primary">
                                    <i class="ft-plus-circle"></i>
                                    <b> Nuevo Evaluador </b>
                                    </a>
                            </div>
                        </div>	
                    <?php } ?>   
                  
                  </div>
                    <div class="form-row">
                            <div class="form-group col-md-9">
                                 <strong>
                                    <span class="ft-grid"></span>
                                    <span> DATOS DEL PACIENTE</span>
                                    </strong>
                            </div>
                    </div>
                    <br>

                    <div class="form-row ">
                    <div class="col-md-5 mb-4">
                    <label for="inputCity">Nombres y Apellidos</label>
                        <div class="input-group input-group-sm mb-3">
                            <select  id="select_av" onchange="onChange_av()" name="PK_ACCESO_V"   placeholder="Ingrese Nombres" class="size" style="width: 100%;">
                                 <option > </option>
                                     <?php foreach($acceso_vascular as $m ):?>
                                      <option <?php echo (!$esNuevo && $m->PK_ACCESO_V === $model->PK_ACCESO_V)? 'selected': '' ?> 
                                         value="<?php echo $m->PK_ACCESO_V?>">
                                    <?php echo $m->APELLIDO_PATERNO?> <?php echo $m->APELLIDO_MATERNO?>,  
                                    <?php echo $m->NOMBRES?> ▪ <?php echo $m->DES_LARGA?>
                                     </option>
                             <?php endforeach; ?>
                             </select>  


                            </div>
                    </div>
                    <div class="col-md-2 mb-4">
                    <label for="inputCity">Nro. Doc</label>
                        <div class="input-group input-group-sm mb-3">
                            <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-sm">	<i  class="ft-credit-card"></i></span>
                            </div>

                            <input id="select_docp" name="NRO_DOC" type="text" class="form-control" value="<?php echo $esNuevo || !$esNuevo? '': $m->NRO_DOC?>"disabled>
                           
                        </div>
                    </div>
                    <div class="col-md-2 mb-4">
                    <label for="inputCity">Autogenerado</label>
                        <div class="input-group input-group-sm mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-sm" >	<i class="ft-trending-up"></i></span>
                            </div>
                            <input id="select_auto" name="AUTO" type="text" class="form-control" aria-label="Small" placeholder="" aria-describedby="inputGroup-sizing-sm" value="<?php echo $esNuevo || !$esNuevo? '': $m->UBICACION?>" disabled>
                        </div>
                    </div>
                    <div class="col-md-2 mb-4">
                    <label for="inputCity">Fecha Nac.</label>
                        <div class="input-group input-group-sm mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-sm">	<i class="ft-calendar"></i></span>
                            </div>
                            <input  id="select_fnac" name= "FECHA_NAC" type="date" class="form-control form-control-sm" id="validationDefault04" placeholder="" value="<?php echo  $esNuevo || !$esNuevo? '': $m->FECHA_NAC?>" disabled>
                            </div>
                        </div>

                          
                    </div>
                    <center> 
         
                        <!-- <div class="btn-group" role="group" aria-label="Basic example">

                                        <a href="<?php echo site_url('paciente/crud/'); ?>" class="btn btn-sm btn-primary">
                                                <i class="ft-plus-circle"></i>
                                                <b> Nuevo Paciente </b>
                                        </a>
                            
</div> -->
                            </center> 
                    <div class="form-row">
                            <div class="form-group col-md-9">
                                <strong>
                                    <span class="ft-box"></span>
                                    <span> DATOS DEL ACCESO VASCULAR FUNCIONANTE</span>
                                    </strong>
                            </div>
                    </div>
                    <br>
                 
                        
                    <!--  CRUD-->
                    <div class="form-row">
                    <?php if(!$esNuevo){?>
                        <div class="col-md-3 mb-4">
                        <label for="inputCity">Tipo Acceso </label>
                        
                        <select   name="COD_TIPO_ACCESO"  class="form-control form-control-sm" disabled>
                               <option > </option>
                                    <?php foreach($acceso_vascular as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->PK_ACCESO_V === $model->PK_ACCESO_V)? 'selected': '' ?> value="<?php echo $m->PK_ACCESO_V?>">
                                        <?php echo $m->DES_LARGA?> 
                                </option>
                            <?php endforeach; ?>
                            </select >
                            
                        </div>
                        <div class="col-md-2 mb-4">
                        <label for="inputCity">Ubicación</label>
                        <select class="form-control form-control-sm" disabled >
                                <option value="">  </option>
                                    <?php foreach($acceso_vascular as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->PK_ACCESO_V === $model->PK_ACCESO_V)? 'selected': '' ?> value="<?php echo $m->PK_ACCESO_V?>">
                                    <?php echo $m->UBICACION?> 
                                </option>
                            <?php endforeach;?>
                            </select>
                        </div>
                        <div class="col-md-2 mb-4">
                        <label for="inputCity">Fecha de Creación</label>
                        <select name="FECHA_CREACION_AV" class="form-control form-control-sm" disabled >
                                <option value="">  </option>
                                    <?php foreach($acceso_vascular as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->PK_ACCESO_V === $model->PK_ACCESO_V)? 'selected': '' ?> value="<?php echo $m->PK_ACCESO_V?>">
                                    <?php echo $m->FECHA_CREACION_AV?>
                                </option>
                            <?php endforeach; ?>
                            </select>
                            </div>
                            <div class="col-md-3 mb-4">
                            <label for="inputCity">Cirujano</label>
                            <select name="CIRUJANO_CV" class="form-control form-control-sm" disabled >
                                <option value="">  </option>
                                    <?php foreach($acceso_vascular as $m): ?>
                                        <option <?php echo (!$esNuevo && $m->PK_ACCESO_V === $model->PK_ACCESO_V)? 'selected': '' ?> value="<?php echo $m->PK_ACCESO_V?>">
                                    <?php echo $m->CIRUJANO_CV?>
                                </option>
                            <?php endforeach; ?>
                            </select>
                            </div>
                            <div class="col-md-2 mb-4">
                            <label for="inputCity">  </label>
                            <div class="input-group input-group-sm mb-3">
                                <a href="<?php echo site_url('profesional/crud/'); ?>" class="btn btn-sm btn-primary">
                                    <i class="ft-plus-circle"></i>
                                    <b> Nuevo AV </b>
                                </a>
                            </div>
                        </div>	         
                    <?php } ?>
                    <!-- NUEVO ONCLIC-->
                    <?php if($esNuevo){?>
                        <div class="col-md-3 mb-4">
                        <label for="inputCity">Tipo Acceso</label>
                        <input disabled id="select_tipo" name="COD_TIPO_ACCESO" type="text" class="form-control form-control-sm" aria-describedby="inputGroup-sizing-sm" value="<?php echo $esNuevo || !$esNuevo? '': $m->UBICACION?>"required>
                           
                        </div>
                        <div class="col-md-2 mb-4">
                        <label for="inputCity">Ubicación</label>
                        <input disabled id="select_ubi" name="UBICACION" type="text" class="form-control form-control-sm" aria-describedby="inputGroup-sizing-sm" value="<?php echo $esNuevo || !$esNuevo? '': $m->UBICACION?>"required>
                        
                        </div>
                        <div class="col-md-2 mb-4">
                        <label for="inputCity">Fecha de Creación</label>
                        <input disabled id="select_fc" name="FECHA_CREACION_AV" type="text" class="form-control form-control-sm" aria-describedby="inputGroup-sizing-sm" value="<?php echo $esNuevo || !$esNuevo? '': $m->UBICACION?>"required>
                        
                    </div>

                    <div class="col-md-3 mb-4">
                    <label for="inputCity">Cirujano</label>
                    <input disabled id="select_cj" name="NOMBRES" type="text" class="form-control form-control-sm" aria-describedby="inputGroup-sizing-sm" value="<?php echo $esNuevo || !$esNuevo? '': $m->UBICACION?>"required>
                        
                    </div>
                    <div class="col-md-2 mb-4">
                    <label for="inputCity">   </label>
                            <div class="input-group input-group-sm mb-3">
                                <a href="<?php echo site_url('profesional/crud/'); ?>" class="btn btn-sm btn-primary">
                                    <i class="ft-plus-circle"></i>
                                    <b> Nuevo AV </b>
                                </a>
                            </div>
                        </div>	
                   <?php } ?>  
                   </div>

                    <div class="form-row">
                            <div class="form-group col-md-9">
                                <strong>
                                    <span class="ft-clipboard"></span>
                                    <span> SEGUIMIENTO</span>
                                    </strong>
                            </div>
                    </div>
                    <div class="form-row">
                            <div class="form-group col-md-9">
                                <strong> <p class="text-primary"> <span class="ft-chevron-right"></span> Evaluación
                                    </strong></p>
                            </div>
                    </div>      
                    <div class="form-row">
                  
                        <div class="form-group col 5-md-4">
                        <?php if($esNuevo){?>
                            <label for="inputCity">Fecha de Evaluación</label>
                            <input type="date" name="FECHA_EVAL" id="fechaActual"class="form-control form-control-sm"   value="<?php echo $esNuevo? '': $model->FECHA_EVAL?>" required>
                        <?php } ?>  

                         <?php if(!$esNuevo){?>
                            <label for="inputCity">Fecha de Evaluación</label>
                            <input type="date" name="FECHA_EVAL" class="form-control form-control-sm"   value="<?php echo $esNuevo? '': $model->FECHA_EVAL?>" required>
                            <?php } ?> 
                        </div>
               
                        <div class="form-group col 3-md-3">
                        <label for="inputState">Sala</label>

                      
                        <select name="SALA" class="form-control form-control-sm"  required>
                        <option value=""> </option><?php foreach($sala as $m): ?>
                        <option <?php echo (!$esNuevo && $m->COD_INT === $model->SALA)? 'selected': '' ?> value="<?php echo $m->COD_INT?>">
                        <?php echo $m->DES_LARGA?></option>
                        <?php endforeach; ?></select>

                        <!-- <select name="SALA" class="form-control form-control-sm" required>
                            <option value="">  </option>
                            <option <?php echo (!$esNuevo && $model->SALA === 'SALA 1')? 'selected': '' ?> value="SALA 1">SALA 1</option>
                            <option <?php echo (!$esNuevo && $model->SALA === 'SALA 2')? 'selected': '' ?> value="SALA 2">SALA 2</option>
                            <option <?php echo (!$esNuevo && $model->SALA === 'SALA 3')? 'selected': '' ?> value="SALA 3">SALA 3</option>
                            <option <?php echo (!$esNuevo && $model->SALA === 'SALA 4')? 'selected': '' ?> value="SALA 4">SALA 4</option>
                        </select> -->

                        
                        </div>
                        <div class="form-group col 5-md-3">
                            <label for="inputState">Turno</label>
                            <select name="TURNO" class="form-control form-control-sm"  required>
                            <option value=""> </option><?php foreach($turno as $m): ?>
                            <option <?php echo (!$esNuevo && $m->COD_INT === $model->TURNO)? 'selected': '' ?> value="<?php echo $m->COD_INT?>">
                            <?php echo $m->DES_LARGA?></option>
                            <?php endforeach; ?></select>

                            <!-- <select class="form-control form-control-sm" name="TURNO" id="inputState" required>
                                <option value="">  </option>
                                <option <?php echo (!$esNuevo && $model->TURNO === 'T1')? 'selected': '' ?> value="T1">TURNO 1</option>
                                <option <?php echo (!$esNuevo && $model->TURNO === 'T2')? 'selected': '' ?> value="T2">TURNO 2</option>
                                <option <?php echo (!$esNuevo && $model->TURNO === 'T3')? 'selected': '' ?> value="T3">TURNO 3</option>
                                <option <?php echo (!$esNuevo && $model->TURNO === 'T4')? 'selected': '' ?> value="T4">TURNO 4</option>
                            </select> -->
                        </div>
                        <div class="form-group col 5-md-3">
                            <label for="inputState">Frecuencia</label>
                            <!-- <select class="form-control form-control-sm" name= "FRECUENCIA_DIAL" id="inputState"required>
                     
                                <option value="">  </option>
                                <option <?php echo (!$esNuevo && $model->FRECUENCIA_DIAL === 'LUN-MIER-VIER')? 'selected': '' ?> value="LUN-MIER-VIER">LUN-MIER-VIER</option>
                                <option <?php echo (!$esNuevo && $model->FRECUENCIA_DIAL === 'MART-JUEV-SAB')? 'selected': '' ?> value="MART-JUEV-SAB">MART-JUEV-SAB</option>
                      
                            </select> -->

                            <select name="FRECUENCIA_DIAL" class="form-control form-control-sm"  required>
                            
                            <option value=""> </option><?php foreach($frecuencia as $m): ?>
                            <option <?php echo (!$esNuevo && $m->COD_INT === $model->FRECUENCIA_DIAL)? 'selected': '' ?> value="<?php echo $m->COD_INT?>">
                            <?php echo $m->DES_CORTA?></option>
                            <?php endforeach; ?>
                            
                            </select>
                        </div>
                 </div>



                  <div class="form-row">
                            <strong> <p class="text-primary"> <span class="ft-chevron-right">
                            </span> Valoración Física </strong></p> </button>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="limpiar" >
                          <label class="custom-control-label text-danger" for="limpiar"> <strong>Si el Acceso Vascular es de Tipo "FAV" registre la Valoración Física*</strong> </label>
                          <small id="emailHelp" class="form-text text-muted"> Hacer Click para habilitar </small>
                        </div>

    <script type="text/javascript">
    
        $(document).ready(function() { 
         //   $('#limpiar').prop('checked', true);
            $('#limpiar').click(function() {
                $('.l-input').val(function(){

    			if ( document.getElementById("myCheck").disabled){
    				document.getElementById("myCheck").disabled = false 
                    document.getElementById("myCheck2").disabled = false 
                    document.getElementById("myCheck3").disabled = false
                    document.getElementById("myCheck4").disabled = false
                    document.getElementById("myCheck5").disabled = false 
                        }else{
                    document.getElementById("myCheck").disabled = true
                    document.getElementById("myCheck2").disabled = true
                    document.getElementById("myCheck3").disabled = true
                    document.getElementById("myCheck4").disabled = true
                    document.getElementById("myCheck5").disabled = true 
    			    }
    		    }
                );
            });
        });
    </script> 
 <br>
                    <div class="form-row"> </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-body">
                                    <h5 class="card-title"><center>TEST</h5>
                        <div class="form-row">
                            <div class="form-group col 5-md-3">
                                <label for="inputState">Elevación</label>
                            <select class="form-control form-control-sm l-input"  name="TEST_ELEVACION" id="myCheck" disabled >
                     
                                <option value="">  </option>
                                <option <?php echo (!$esNuevo && $model->TEST_ELEVACION === '+')? 'selected': '' ?> value="+">+</option>
                                <option <?php echo (!$esNuevo && $model->TEST_ELEVACION === '-')? 'selected': '' ?> value="-">-</option>
                      
                            </select>
                              
                            </div>
                            <div class="form-group col 5-md-3">
                                <label for="inputState">Aumento de Pulso</label>
                            <select class="form-control form-control-sm l-input"  name="TEST_AUM_PULSO" id="myCheck2" disabled>
                                <option value="">  </option>
                                <option <?php echo (!$esNuevo && $model->TEST_AUM_PULSO === '+')? 'selected': '' ?> value="+">+</option>
                                <option <?php echo (!$esNuevo && $model->TEST_AUM_PULSO === '-')? 'selected': '' ?> value="-">-</option>
                            </select>
                            </div>
                            <div class="form-group col 5-md-3">
                                <label for="inputState">Venas Colaterales</label>
                                    <select class="form-control form-control-sm l-input"  name="TEST_VENAS_COLAT" id="myCheck3" disabled>
                                        <option value="">  </option>
                                        <option <?php echo (!$esNuevo && $model->TEST_VENAS_COLAT === '+')? 'selected': '' ?> value="+">+</option>
                                        <option <?php echo (!$esNuevo && $model->TEST_VENAS_COLAT === '-')? 'selected': '' ?> value="-">-</option>
                            
                                    </select>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-sm-6"> 
                        <div class="card">
                            <div class="card-body">
                            <h5 class="card-title"><center> FACTOR CLINICO(TRILL)</h5>
                            <div class="form-row">
                                <div class="form-group col 5-md-3">
                                <label for="inputState">Distancia</label>
                                <input  min="0" max="50" class="form-control form-control-sm l-input" id="myCheck4" name="FC_DISTANCIA" type="number" value="<?php echo $esNuevo? '': $model->FC_DISTANCIA?>"disabled>
                                </div>
                                <div class="form-group col 5-md-3">
                                <label for="inputState">Carateristica</label>
                                <select class="form-control form-control-sm l-input" name="FC_CARACTERISTICA"  id="myCheck5"disabled> 
                                    <option  value=""> </option> 
                                    <optgroup label="CARACTERISTICAS">
                                        <option <?php echo (!$esNuevo && $model->FC_CARACTERISTICA === 'B')? 'selected': '' ?> value="B">BUENO</option>
                                        <option <?php echo (!$esNuevo && $model->FC_CARACTERISTICA === 'R')? 'selected': '' ?> value="R">REGULAR</option>  
                                        <option <?php echo (!$esNuevo && $model->FC_CARACTERISTICA === 'D')? 'selected': '' ?> value="D">DEFICIENTE</option>
                                       
                                </optgroup>
                                    </select>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
      
                    
                  </div>
                
                    <br>

                    <div class="form-row">
                                <div class="form-group col-md-9">
                                    <strong> <p class="text-primary"> <span class="ft-chevron-right"></span> Valoracion Funcional
                                        </strong></p>
                                </div>
                        </div> 

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="card">
                            <div class="card-body">
                                <h5 class="card-title"> <center> PRESIÓN ARTERIAL INICIAL</h5>
                                <div class="form-row">
                                        <div class="form-group col 3-md-3">
                                        <label for="inputState">Sistólica</label>
                                        <input type="number" min="1" max="300"   required="required"class="form-control form-control-sm" name="PA_INI_SISTOLICA" type="number"  value="<?php echo $esNuevo? '': $model->PA_INI_SISTOLICA?>"  required>
                                        </div> 
                                        <div class="form-group col 3-md-3">
                                        <label for="inputState">Diastólica</label>
                                        <input type="number" min="1" max="200"   required="required" class="form-control form-control-sm" name="PA_INI_DIASTOLICA" id="" type="text" value="<?php echo $esNuevo? '': $model->PA_INI_DIASTOLICA?>" required  >
                                        </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card">
                            <div class="card-body">
                                <h5 class="card-title"><center> PRESIÓN ARTERIAL FINAL</h5>
                                <div class="form-row">
                                        <div class="form-group col 3-md-3">
                                        <label for="inputState">Sistólica</label>
                                        <input  type="number" min="1" max="300"  class="form-control form-control-sm" id="f_sistolica" type="text" name="PA_FINAL_SISTOLICA"  value="<?php echo $esNuevo? '': $model->PA_FINAL_SISTOLICA?>"  >
                                        </div>
                                        <div class="form-group col 3-md-3">
                                        <label for="inputState">Diastólica</label>
                                        <input  type="number" min="1" max="200"   class="form-control form-control-sm" id="" type="text" name="PA_FINAL_DIASTOLICA"  value="<?php echo $esNuevo? '': $model->PA_FINAL_DIASTOLICA?>"  >
                                        </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="card">
                            <div class="card-body">
                                 <h5 class="card-title"><center> PARAMETROS</h5>
                                <div class="form-row">
                                        <div class="form-group col 3-md-3">
                                        <label for="inputState">QB</label>
                                        <input  type="number" min="0" max="500"   required="required" class="form-control form-control-sm" id="" placeholder="200(cc/min)" type="text"  name="QB"  value="<?php echo $esNuevo? '': $model->QB?>" required >
                                        </div>
                                        <div class="form-group col 3-md-3">
                                        <label for="inputState">RV/PVE</label>
                                        <input  type="number" min="0" max="400"   required="required" class="form-control form-control-sm" id="" placeholder="(mmHg)"  type="text"  name="RV_PVE"  value="<?php echo $esNuevo? '': $model->RV_PVE?>" required >
                                        </div>
                                        <div class="form-group col 3-md-3">
                                        <label for="inputState">RA/PAE</label>
                                        <input  type="number" min="-400" max="150"   required="required"class="form-control form-control-sm" id="" placeholder="(mmHg) " type="text"  name="RA_PAE"  value="<?php echo $esNuevo? '': $model->RA_PAE?>" required >
                                        </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="form-row">
                                <div class="form-group col-md-9">
                                    <strong> <p class="text-primary"> <span class="ft-chevron-right"></span> Complicaciones Identificadas
                                        </strong></p>
                                </div>
                    </div> 

            <div class="">
                <ul id="fieldlist">
                    <li>
                        <select id="compli1" name="COMPLI_INI"style="width: 100%;" class=" input-large form-control" required>
                            <option value="" selected="selected"></option> 
                            <?php foreach($complicaciones as $m): ?>
                            <option <?php echo (!$esNuevo && $m->COD_INT === $model->COMPLI_INI)? 'selected': '' ?> value="<?php echo $m->COD_INT?>">
                                    <?php echo $m->DES_LARGA?>
                                    </option>
                            <?php endforeach; ?>
                        </select>
                          
                    </li>
                    <li>
                       
                        <select id="compli2" name="COMPLI_INI2"style="width: 100%;" class=" input-large form-control" >               
                             <option value="" selected="selected"></option> 
                             <?php foreach($complicaciones as $m): ?>
                            <option <?php echo (!$esNuevo && $m->COD_INT === $model->COMPLI_INI2)? 'selected': '' ?> value="<?php echo $m->COD_INT?>">
                            <?php echo $m->DES_LARGA?></option>
                            <?php endforeach; ?>
                        </select>
                   
                    </li>
                    <li>
                         
                        <select id="compli3" name="COMPLI_INI3"style="width: 100%;" class=" input-large form-control" >
                            <option value="" selected="selected"></option>
                            <?php foreach($complicaciones as $m): ?>
                            <option <?php echo (!$esNuevo && $m->COD_INT === $model->COMPLI_INI3)? 'selected': '' ?> value="<?php echo $m->COD_INT?>">
                                    <?php echo $m->DES_LARGA?>
                                    </option>
                            <?php endforeach; ?>
                        </select>
                    </li>
                    <li>
                         
                        <select id="compli4" name="COMPLI_INI4"style="width: 100%;" class=" input-large form-control" >
                         <option value="" selected="selected"></option>
                            <?php foreach($complicaciones as $m): ?>
                            </option> <option <?php echo (!$esNuevo && $m->COD_INT === $model->COMPLI_INI4)? 'selected': '' ?> value="<?php echo $m->COD_INT?>">
                                    <?php echo $m->DES_LARGA?>
                                    </option>
                            <?php endforeach; ?>
                        </select>
                    </li>
                   
                </ul>
            </div>
   
        <div class="chip" >
            El registro esta:  
                <div class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input" type="radio" name="REG_COMPLETO" id="radio_process" <?php echo (!$esNuevo && $model->REG_COMPLETO === '0')? 'checked':'' ?> value="0"  >
                    <label class="custom-control-label"for="radio_process"><strong>EN PROCESO </strong></label>
                </div> 
                <div class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input" type="radio" name="REG_COMPLETO" id="radio_end" <?php echo (!$esNuevo && $model->REG_COMPLETO === '1')?'checked':'' ?> value="1">
                    <label class="custom-control-label"for="radio_end"><strong>FINALIZADO </strong></label>
                </div> 
        </div>
         
    <BR><BR>
         <div class="form-group">
                        <div class="form-row">
                            <div class="form-group col-md-9">
                                <strong> <p class="text-primary"> <span class="ft-chevron-right"></span> Observaciones
                                </strong></p>
                            </div>
                        </div> 
                        <textarea class="form-control" rows="4" name="OBSER"><?php echo $esNuevo? '': $model->OBSER?></textarea>
                    </div>

                 
                <center>    
                <a class="btn btn-secondary" href="<?php echo site_url('vigilancia'); ?>" title="Cancelar">Cancelar</a>
                <button class="btn btn-primary" type="submit">
                     <?php echo $esNuevo? 'Agregar': 'Actualizar' ?>
                </button>
        </form>       
    <?php echo form_close(); ?>
   

                </center>  
            </form>   


    </div> 
    </div> 
</div>
<script> 

    </script>
<style>
    .chip {
        display: inline-block;
        padding: 0 25px;
        height: 50px;
        font-size: 16px;
        line-height: 50px;
        border-radius: 25px;
        background-color: #f1f1f1;
    }

    .chip img {
        float: left;
        margin: 0 10px 0 -25px;
        height: 50px;
        width: 50px;
        border-radius: 50%;
    }
</style> 

    





<script>
    window.onload = function(){
    var fecha = new Date(); //Fecha actual
    var mes = fecha.getMonth()+1; //obteniendo mes
    var dia = fecha.getDate(); //obteniendo dia
    var ano = fecha.getFullYear(); //obteniendo año
 
    if(dia<10)
        dia='0'+dia; //agrega cero si el menor de 10
    if(mes<10)
        mes='0'+mes //agrega cero si el menor de 10
    document.getElementById('fechaActual').value=ano+"-"+mes+"-"+dia;
    }
    $('#f_sistolica').on('keyup', function (e) {
        console.log(e.target.value)
        console.log(typeof e.target.value)
        if (e.target.value) {
            $('#radio_end').removeAttr("disabled");
            $('#radio_process').attr("disabled", "disabled");
            $('#radio_end').prop('checked', true);
        } else {
            $('#radio_process').removeAttr("disabled");
            $('#radio_end').attr("disabled", "disabled");
            $('#radio_process').prop('checked', true);
        }
    })
</script>


            <style>
               #fieldlist {
                   margin: 0;
                   padding: 0;
               }
       
               #fieldlist li {
                   list-style: none;
                   padding-bottom: 1.5em;
                   text-align: left;
               }
       
               #fieldlist label {
                   display: block;
                   padding-bottom: .3em;
                   font-weight: bold;
                   text-transform: uppercase;
                   font-size: 12px;
               }
            </style>

            <script>
                $(document).ready(function() {
                    var compli1 = $("#compli1").kendoComboBox({ 
                        placeholder: "Ingrese Complicación",
                        dataTextField: "compli1",
                        dataValueField: "CategoryID",
                        dataSource: {
                            type: "odata",
                        }
                    });
                    var compli2 = $("#compli2").kendoComboBox({
                        autoBind: false,
                        cascadeFrom: "compli1",
                        placeholder: "Ingrese Complicación",
                        dataTextField: "compli1",
                        dataValueField: "CategoryID",
                        dataSource: {
                            type: "",
                            serverFiltering: true,
                        }
                    });

                    var compli3 = $("#compli3").kendoComboBox({
                        autoBind: false,
                        cascadeFrom: "compli2",
                        placeholder: "Ingrese Complicación",
                        dataTextField: "cpmpli3",
                        dataValueField: "CategoryID",
                        dataSource: {
                            type: "",
                            serverFiltering: true,
                        }
                    });

                    var compli4 = $("#compli4").kendoComboBox({
                        autoBind: false,
                        cascadeFrom: "compli3",
                        
                        placeholder: "Ingrese Complicación",
                        dataTextField: "cpmpli3",
                        dataValueField: "CategoryID",
                        dataSource: {
                            type: "odata",
                            serverFiltering: true,
                           
                        }
                    });
                });
            </script>

