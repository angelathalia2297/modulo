

<section class="content">
	<div class="row">
		<div class="col-lg-12">
		<div class="alert alert-primary  " role="alert">
			 <h4 align="center"><p class="text-info    font-weight-light">
			 <strong> EVALUACIÓN DEL ACCESO VASCULAR</strong> </p></h4>
		</div>
		<div class='alert alert-info alert-dismissable'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
               <strong><i class='ft-user'></i> 
			    "<?php echo($Nombre);?>" </strong>, Bienvenido(a) a la aplicación de Vigilancia del Acceso Vascular
        </div>
	<div class="card">
		<div class="card-header">
				<div class="form-row">
						<div class="col-8">
						<strong>
								<span class="ft-clipboard"> </span>
								<span> REGISTROS</span>
								</strong>
						</div>
						<div class="col">
						</div>
						<div class="col">
							<a href="<?php echo site_url('vigilancia/crud'); ?>" class="btn btn-sm btn-primary">
									<i class="ft-plus-circle"></i>
									<b> Nueva Evaluación</b>
							</a>
						</div>
				</div> 
		</div>	
		<div class="card-body">
			<div class="table-responsive table-hover table ">  
			<form class="form-inline" >
					<div class="form-group mb-3">
							<input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="Búsqueda por fecha:">
						</div>
						<div class="input-group mx-sm-1 mb-3">
						
							<input class="form-control form-control-sm" autocomplete="off" id="Date_search" type="text" placeholder="Ingrese rango de fecha " />
							<div class="input-group-append">
								<button class="btn btn-sm btn-danger" type="submit"> <STRONG> x </STRONG></button>
							</div>
						</div> 
			</form> 
				<table id="vigilancia" class="table table-bordered table-striped" >
						<thead class="thead-dark">
						<tr>
								<th scope="col"><center>FECHA</th> 
								<th scope="col"><center>AUTOGENERADO</th>         
								<th scope="col"><center>PACIENTE</th>   
								<th class="no-sort" scope="col"><center>PROFESIONAL</th>    
								<th class="no-sort" ><center>SALA</th>
								<th class="no-sort"><center>TURNO</th>
								<th class="no-sort" scope="col"><center>FRECUENCIA</th>
								<th class="no-sort" scope="col"><center>TIPO A.V</th>
								<th class="no-sort" scope="col"><center>ESTADO</th>
								<th class="no-sort" scope="col"><center>Acciones</th>
								
						</tr> 
						<!-- <?php foreach($contadorC as $m):?>
						 registros completos <?php echo $m->CT?>
						<?php endforeach; ?> -->

					</thead>
						<tbody>	<?php foreach($model as $m):?>
								<tr>
								<td><center> <?php echo $m->FECHA_EVAL?>
								</td>
								<td > <center>   <?php echo $m->AUTO?></td>  
								<td> <?php echo $m->APELLIDO_PATERNO?> <?php echo $m->APELLIDO_MATERNO?>, <?php echo $m->NOMBRES?></td>
								<td> <?php echo $cod_profesionales[array_search($m->COD_PROFESIONAL, 
								array_column($cod_profesionales, 'COD_PROFESIONAL'))]->APELLIDO_PAT?>
								<?php echo $cod_profesionales[array_search($m->COD_PROFESIONAL, array_column($cod_profesionales, 'COD_PROFESIONAL'))]->APELLIDO_MAT?>, 
								<?php echo $cod_profesionales[array_search($m->COD_PROFESIONAL, array_column($cod_profesionales, 'COD_PROFESIONAL'))]->NOMBRES?></td> 
								<td ><center> <?php echo $m->SALA?> </td>
								<td><center> <?php echo $m->TURNO?> </td>
								<td><center> <?php echo $m->FRECUENCIA_DIAL?></td>
								<td><center> <?php echo $m->DES_LARGA?> </td> 
							<td> <center> 
						<h5>	<span class="badge badge-<?php echo $m->REG_COMPLETO === '1'? 'success': 'warning' ?>"><?php echo $m->REG_COMPLETO === '1'? 'FINALIZADO': 'EN PROCESO' ?></span></h5>
					 
							</td>
								<td><center>
								<div class="btn-group">
									<a class="btn btn-sm btn-primary" href="<?php echo site_url('vigilancia/crud/' . $m->PK_EVALUACION); ?>" title="Editar">
									<i class="ft-edit"></i></a>
									<a class="btn btn-sm btn-danger" href="<?php echo site_url('vigilancia/eliminar/' . $m->PK_EVALUACION); ?> "onclick="return confirm('¿Esta seguro de eliminar  este Registro');"  title="Eliminar">
									<i class="ft-trash-2"></i></a>						
								</div>    
								</td>
								</tr>
						<?php endforeach; ?>
						</tbody>
				</table>
		</div>
		</div>
		</div>

 <!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ESTADISTICAS">
  Estadisticas
</button> -->

<!-- Modal -->
<!-- <div class="modal fade" id="ESTADISTICAS" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">CANTIDAD DE REGISTROS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">COMPLETOS</th>
      <th scope="col">INCOMPLETOS</th>
      <th scope="col">TOTAL</th>
    </tr>
  </thead>
  <tbody>

<?php foreach($contadorC as $m):?>
    <tr>
      <th scope="row">1</th>
      <td><?php echo $m->TOTAL?></td>
      <td> </td>
      <td> </td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td></td>
      <td> </td>
      <td>  </td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td colspan="2"> </td>
      <td> </td>
    </tr>
<?php endforeach; ?>
  </tbody>
</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
       
      </div>
    </div>
  </div>
</div> -->

