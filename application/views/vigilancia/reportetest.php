<script>

	/* Custom filtering function which will search data in column four between two values */
	$.fn.dataTable.ext.search.push(
		function( settings, data, dataIndex ) {
			var min = parseInt( $('#min').val(), 10 );
			var max = parseInt( $('#max').val(), 10 );
			var age = parseFloat( data[4] ) || 0; // use data for the age column
	
			if ( ( isNaN( min ) && isNaN( max ) ) ||
				( isNaN( min ) && age <= max ) ||
				( min <= age   && isNaN( max ) ) ||
				( min <= age   && age <= max ) )
			{
				return true;
			}
			return false;
		}
	);
	
	$(document).ready(function() {
		var table = $('#example').DataTable();
		
		// Event listener to the two range filtering inputs to redraw on input
		$('#min, #max').keyup( function() {
			table.draw();
		} );
	} );
</script>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
		<div class="alert alert-primary" role="alert">
			 <h4 align="center"><p class="text-info font-weight-light">
			 <strong> MONITORIZACIÓN CLÍNICA DE LA FAV </strong> </p></h4>
		</div> 
		<br>
<div class="card">
	<div class="card-header">
			<div class="form-row">
					<div class="col-8">
					<strong>
							<span class="ft-book"> </span>
							<span>REGISTROS</span>
							</strong>
					</div>
					<div class="col">
					</div>
			</div> 
  	</div>	
	  <div class="card-body">
	  <div class="">    
	  <table class="table ">
		<thead >
			<tr  >
			<th scope="col">Mínima RA:</th>
			<th scope="col"><input class="form-control"type="text" id="min" name="min"></th>
			<th scope="col">Máxima RA:</th>
			<th scope="col"><input class="form-control" type="text" id="max" name="max"></th>
			</tr>
		</thead> 
	  </table> 
  	</div>	 
	  <div class="table-responsive table-hover table ">   
	  <table id="repor_test" class="table table-bordered table-striped" >
						<thead class="thead-dark">
				<tr >	   	
							<th scope="col"><center>FECHA</th>
							<th scope="col"><center>AUTOGENERADO</th>
							<th scope="col"><center>PACIENTE</th>   
                            <th class="no-sort" scope="col"><center>UBICACIÓN</th>
                            <th class="no-sort" scope="col"><center>TIPO A.V</th>
                            <th scope="col"><center>RA. PAE</th>
                            <th class="no-sort" scope="col"><center>TEST AUMENTO DE PULSO</th>
                            <th scope="col"><center>RV. PVE</th>
                            <th class="no-sort" scope="col"><center>TEST ELEVACION</th>
                            <th class="no-sort" scope="col"><center>TEST VENAS COLAT.</th>
              	</tr>
				</thead>
					<tbody>	<?php foreach($model as $m):?>
						<tr>
							<td><center>  <?php echo  $m->FECHA_EVAL?>  </td>
							<td><center>  <?php echo  $m->AUTO?>  </td>
							<td> <?php echo $m->APELLIDO_PATERNO?> <?php echo $m->APELLIDO_MATERNO?>, <?php echo $m->NOMBRES?></td>
						 	<td> <center>  <?php echo $m->UBICACION?>  </td>
							<td> <center>  <?php echo $m->COD_TIPO_ACCESO?></td>
                            <td> <center> <?php echo $m->RA_PAE?> </td>
                            <td> <center> <?php echo $m->TEST_AUM_PULSO?> </td>   
                            <td> <center> <?php echo $m->RV_PVE?> </td>
                            <td> <center> <?php echo $m->TEST_ELEVACION?> </td> 
                            <td> <center> <?php echo $m->TEST_VENAS_COLAT?> </td>				
						</tr>
					<?php endforeach; ?>
				<tbody>
		</table>
		</div></div>
</section>

