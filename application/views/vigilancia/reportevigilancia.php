<section class="content">
	<div class="row">
	<div class="col-lg-12">
		<div class="alert alert-primary" role="alert">
			 <h4 align="center"><p class="text-info font-weight-light">
			 <strong>REPORTE DE EVALUACIONES DEL ACCESO VASCULAR</strong> </p></h4>
		</div> 
		 <br>
<div class="card">
	<div class="card-header">
			<div class="form-row">
					<div class="col-8">
					<strong>
							<span class="ft-book"> </span>
							<span> REGISTROS</span>
							</strong>
					</div>
					<div class="col">
					</div>
			</div> 
  	</div>	
	  <div class="card-body">
	  <div class="table-responsive table-hover table ">   
	  <table id="repor_vigilancia" class="table table-bordered table-striped" >
						<thead class="thead-dark">
				<tr>       
						<th rowspan="2" scope="col">FECHA</th>
						<th rowspan="2" scope="col">SALA</th>
						<th colspan="3" scope="col"><center>DATOS DEL PACIENTE</th>
						<th rowspan="2" scope="col">TURNO</th>
						<th rowspan="2" class="no-sort"scope="col"><center>FRECUENCIA</th>
						<th rowspan="2" class="no-sort"scope="col"><center>PROFESIONAL</th>				
						<th colspan="3" scope="col"><center>DATOS DEL AV</th>
						<th colspan="5" scope="col"><center>VALORACIÓN FÍSICA (solo "FAV" y "Catéter" ) </th>
						<th colspan="7" scope="col"><center>VALORACIÓN FUNCIONAL DE LOS AV (Todos)</th>
						<th rowspan="2" class="no-sort"scope="col"><center>OBSERVACIONES</th>
				</tr>
						<tr> 
						<th scope="col"><center>AUTOGENERADO</th>
							<th scope="col"><center>PACIENTE</th>
							<th class="no-sort"scope="col"><center>EDAD</th>
							<th class="no-sort"scope="col"><center>SEXO</th> 
 
							<th class="no-sort"scope="col"><center>CONDICIÓN DE INICIO DEL AV</th>
							<th scope="col"><center>TIPO A.V</th>
							<th class="no-sort"scope="col"><center>UBICACIÓN</th>

  							<th class="no-sort"scope="col"><center>TEST ELEVACION</th>
                            <th class="no-sort"scope="col"><center>TEST AUMENTO DE PULSO</th>
                            <th class="no-sort"scope="col"><center>TEST VENAS COLAT.</th>
                            <th class="no-sort"scope="col"><center>FC.(TRILL) DISTANCIA</th>
							<th class="no-sort"scope="col"><center>FC.(TRILL) CARACTERISTICA</th> 
                            
							<th class="no-sort"scope="col"><center>P. I. SISTOLICA</th>
                            <th class="no-sort"scope="col"><center>P. I. DIASTOLICA</th>
                            <th class="no-sort"scope="col"><center>P. F. SISTOLICA</th>
                            <th class="no-sort"scope="col"><center>P. F. DIASTOLICA</th>
                            <th class="no-sort"scope="col"><center>QB</th>
                            <th class="no-sort"scope="col"><center>RA. PAE</th>
                            <th class="no-sort"scope="col"><center>RV. PVE</th>

                            
					</tr>
				</thead>
				<tbody>	<?php foreach($model as $m):?>
						<tr>
							<td><center> <?php echo $m->FECHA_EVAL?></td>
							<td ><center> <?php echo $m->SALA?></td>
							<td > <?php echo $m->AUTO?></td>
							<td> <?php echo $m->APELLIDO_PATERNO?> <?php echo $m->APELLIDO_MATERNO?>, <?php echo $m->NOMBRES?></td>
							<td> <center>
							<?php
						// CALCULAR EDAD
							$cumple = new DateTime($m->FECHA_NAC);
							$hoy = new DateTime("now");
							$edad = $hoy->diff($cumple);
							echo $edad->y; ?>
							</td>
							
							<td><center> <?php echo $m->SEXO?></td>
							<td><center> <?php echo $m->TURNO?></td>
							<td><center> <?php echo $m->FRECUENCIA_DIAL?></td>
							<td> <center><?php echo $cod_profesionales[array_search($m->COD_PROFESIONAL, 
							array_column($cod_profesionales, 'COD_PROFESIONAL'))]->APELLIDO_PAT?>
							<?php echo $cod_profesionales[array_search($m->COD_PROFESIONAL, array_column($cod_profesionales, 'COD_PROFESIONAL'))]->APELLIDO_MAT?>, 
							<?php echo $cod_profesionales[array_search($m->COD_PROFESIONAL, array_column($cod_profesionales, 'COD_PROFESIONAL'))]->NOMBRES?>
							</td> 

							 
							<td> <center>
						<?php echo  $ingreso[array_search($m->MOTIVO_I, array_column($ingreso, 'COD_INT'))]->DES_CORTA ?>
						</td>

							<td><center> <?php echo  $m->COD_TIPO_ACCESO?> </td> 
							<td><center> <?php echo  $m->UBICACION?>
							<td> <center> <?php echo $m->TEST_ELEVACION?> </td>
                            <td> <center> <?php echo $m->TEST_AUM_PULSO?> </td>
                            <td> <center> <?php echo $m->TEST_VENAS_COLAT?> </td>
                            <td> <center> <?php echo $m->FC_DISTANCIA?> </td>
							<td> <center> <?php echo $m->FC_CARACTERISTICA?> </td>
							<td><center>  <?php echo $m->PA_INI_SISTOLICA?> </td>
                            <td><center>  <?php echo $m->PA_INI_DIASTOLICA?> </td>
                            <td> <center> <?php echo $m->PA_FINAL_SISTOLICA?> </td>
                            <td> <center> <?php echo $m->PA_FINAL_DIASTOLICA?> </td>
                            <td> <center> <?php echo $m->QB?> </td>
                            <td> <center> <?php echo $m->RA_PAE?> </td>
                            <td> <center> <?php echo $m->RV_PVE?> </td>
                            
                            <td> <center> <?php echo $m->OBSER?> </td>
						</tr>
							<?php endforeach; ?>
				<tbody>
		</table>
		</div></div>
</section>

