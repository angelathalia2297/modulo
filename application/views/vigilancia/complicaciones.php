<section class="content">
	<div class="row">
		<div class="col-lg-12">
		<div class="alert alert-primary  " role="alert">
			 <h4 align="center"><p class="text-info    font-weight-light">
			 <strong> COMPLICACIONES</strong> </p></h4>

		</div> 
<div class="card">
	<div class="card-header">
			<div class="form-row">
					<div class="col-8">
					<strong>
							<span class="ft-clipboard"> </span>
							<span> REGISTROS</span>
							</strong>
					</div>
					<div class="col">
					</div>
			</div> 
  	</div>	
	  <div class="card-body">
	  <div class="table-responsive table-hover table ">   
	  <table id="repor_compli" class="table table-bordered table-striped" >
				<thead class="thead-dark">
				<tr>       
						<th rowspan="2" scope="col">FECHA</th>
						<th rowspan="2" scope="col">AUTOGENERADO</th> 
						<th rowspan="2" scope="col">PACIENTE</th> 
						<th rowspan="2" scope="col">SALA</th>
						<th rowspan="2" scope="col">TURNO</th>
						<th rowspan="2" class="no-sort"scope="col"><center>FRECUENCIA</th>
						<th rowspan="2" class="no-sort"scope="col"><center>TIPO A.V</th>				
						<th colspan="2" scope="col"><center>COMPLICACIÓN NRO. 1</th>
						<th colspan="2" scope="col"><center>COMPLICACIÓN NRO. 2</th>
						<th colspan="2" scope="col"><center>COMPLICACIÓN NRO. 3</th>
						<th colspan="2" scope="col"><center>COMPLICACIÓN NRO. 4</th>
				</tr>
					<tr>              
						<th scope="col"><center>Cód.</th>
						<th scope="col"><center>D.Larga</th>
						<th scope="col"><center>Cód.</th>
						<th scope="col"><center>D.Larga</th>
                        <th scope="col"><center>Cód.</th>
						<th scope="col"><center>D.Larga</th>
						<th scope="col"><center>Cód.</th>
						<th scope="col"><center>D.Larga</th>
					</tr>
				</thead>
					<tbody>	<?php foreach($model as $m):?>
					<tr>
							<td><center> <?php echo $m->FECHA_EVAL?></td>
							<td><center>  <?php echo  $m->AUTO?>  </td>
							<td> <?php echo $m->APELLIDO_PATERNO?> <?php echo $m->APELLIDO_MATERNO?>, <?php echo $m->NOMBRES?></td>
							<td ><center> <?php echo $m->SALA?></td>
							<td><center> <?php echo $m->TURNO?></td>
							<td><center> <?php echo $m->FRECUENCIA_DIAL?></td>
							<td ><?php echo  $acceso_vascular[array_search($m->PK_ACCESO_V, 
							array_column($acceso_vascular, 'PK_ACCESO_V'))]->COD_TIPO_ACCESO?></td>
							
							<td>
							<?php if($m->COMPLI_INI == '') {?>
									<?php 
										 echo "";
									?> 

								<?php } else {
									echo $m->COMPLI_INI;
								} ?> 
							</td>
							
							<td> 
							<?php if($m->COMPLI_INI == '') {?>
									<?php 
										 echo "";
									?> 

								<?php } else {
									  echo $complicaciones[array_search($m->COMPLI_INI, array_column($complicaciones, 'COD_INT'))]->DES_LARGA;
								} ?> 
							
						 </td>
							
							<td> <?php echo $m->COMPLI_INI2?> </td>
							
							<td> 
							
							<?php if($m->COMPLI_INI2 == '') {?>
									<?php 
										 echo "";
									?> 

								<?php } else {
									  echo $complicaciones[array_search($m->COMPLI_INI2, array_column($complicaciones, 'COD_INT'))]->DES_LARGA;
								} ?> 
								
							</td>
							
							<td> 
							<?php if($m->COMPLI_INI3 == '') {?>
									<?php 
										 echo "";
									?> 

								<?php } else {
									echo $m->COMPLI_INI3;
								} ?> 
							</td>
							
							<td> 
							<?php if($m->COMPLI_INI3 == '') {?>
									<?php 
										 echo "";
									?> 

								<?php } else {
									  echo $complicaciones[array_search($m->COMPLI_INI3, array_column($complicaciones, 'COD_INT'))]->DES_LARGA;
								} ?> 
							 
							</td>
							
							<td> <?php echo $m->COMPLI_INI4?> </td>
							
							<td> 
							
							<?php if($m->COMPLI_INI4 == '') {?>
									<?php 
										 echo "";
									?> 

								<?php } else {
									  echo $complicaciones[array_search($m->COMPLI_INI4, array_column($complicaciones, 'COD_INT'))]->DES_LARGA;
								} ?> 
							 </td>
					</tr>
					<?php endforeach; ?>
					</tbody>   
			</table>		
		</table>	
	</div>
    </div>
    </div>
</div>
</html>
</form>

</body>
</html>




