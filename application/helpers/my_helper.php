<?php
function paginacion_config($base_url, $total, $limite){
    $config['base_url']   = $base_url;
    $config['total_rows'] = $total;
    $config['per_page']   = $limite;
	
	// Bootstrap 4 Pagination fix
	$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
	$config['full_tag_close']   = '</ul></nav></div>';
	$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
	$config['num_tag_close']    = '</span></li>';
	$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
	$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
	$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
	$config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
	// $config['next_tag_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
	$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
	$config['prev_tag_close']  = '</span></li>';
	$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
	$config['first_tag_close'] = '</span></li>';
	$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
	$config['last_tag_close']  = '</span></li>';
	
	$config['last_link'] = 'Último';
	$config['first_link'] = 'Primero';


	// $config['first_link'] = false;
	
	// $config['last_link'] = false;
	
    return $config;
}

function encode_image_to_base64($file){
    if($file !== null){
        $path = $file["tmp_name"];
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);

        if(getimagesize($path) !== false){
            return 'data:image/' . $type . ';base64,' . base64_encode($data);        
        }
    }
    
    return null;
}

function test_header_data(){
    return [
        'user' => (object)[
            'Nombre' => 'Eduardo',
            'Correo' => 'erodriguez@anexsoft.com'
        ]
    ];
}
