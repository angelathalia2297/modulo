<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maestro extends CI_Controller {
	private $user;
    public function __CONSTRUCT(){
		parent::__CONSTRUCT();
		$this->user = RestApi::getUserData();
		// Valida que exista el usuario obtenido del token, del caso contrario lo regresa a la pagina de inicio que es nuestro controlador auth
		
		if($this->user === null) redirect('');

		$this->load->helper('url');
		$this->load->model('MaestroModel', 'mam');
    }

		public function index($p = 0){

			$this->load->view('header', $this->user);
			$limite = 0;
			$data   = [];
			$total  = 0;
			try{
							//ERROR - CAMBIAR METODO EN MODEL A listar_page
				$result = $this->mam->listar_page($limite, $p);
				
				$total  = $result->total;
				$data   = $result->data;
			} catch(Exception $e){
					var_dump($e);
			}
			//Inicializando paginacion
			$this->pagination->initialize(
				paginacion_config(
						site_url("maestro/index"),
						$total,
						$limite
				)
				);
				$this->load->view('table');
				$this->load->view('maestro/index', [
				'model' => $data,
			]);

			$this->load->view('footer');
			}
    public function crud($id = 0)
	{
		$COD_TIPO = $this->input->post('COD_TIPO');

		if($COD_TIPO){
		
			redirect('maestro/crud/'.$COD_TIPO);
		}else {
			$msg = 'This is the test message for echo';
		}

		$data = null;
		if($id > 0){
			$data = $this->mam->obtener($id);
		}
		$this->load->view('table');
		$this->load->view('header', $this->user);
		$this->load->view('maestro/crud', [
			'model' => $data,
        ]);
		$this->load->view('footer');
	}
	//GUARDAR
	public function guardar(){
		
		$COD_TIPO = $this->input->post('COD_TIPO');

		$data = [
			'DES_CORTA' 			=> $this->input->post('DES_CORTA'),
			'DES_LARGA' 			=> $this->input->post('DES_LARGA'),
			'COD_INT' 			=> $this->input->post('COD_INT'),
			'COD_REL' 			=> $this->input->post('COD_REL'),
			'COD_TIP_PAD' 			=> $this->input->post('COD_TIP_PAD'),
			'NIV_TIPO' 			=> $this->input->post('NIV_TIPO'),
			'EST_REG' 			=> $this->input->post('EST_REG'),
			'ACTIVO' 			=> $this->input->post('ACTIVO'),
		];

		if(empty($COD_TIPO)){
			$data["EST_REG"] = 1; 
			$data["ACTIVO"] = 1; 
			$this->mam->registrar($data);
		}else {
	
			$this->mam->actualizar($data, $COD_TIPO);
		}
		// var_dump($data);
		redirect('maestro');   
	}
	
    public function eliminar($id){
		$data["EST_REG"] = 0; 
		$this->mam->actualizar($data, $id);
		redirect('maestro');
    }

}
