<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paciente extends CI_Controller {
	private $user;
    public function __CONSTRUCT(){
		parent::__CONSTRUCT();
		
			$this->user = RestApi::getUserData();
			// Valida que exista el usuario obtenido del token, del caso contrario lo regresa a la pagina de inicio que es nuestro controlador auth
			
		if($this->user === null) redirect('');
		$this->load->helper('url');
		$this->load->model('PacienteModel', 'pm');
		$this->load->model('CentroAsistenciaModel', 'cam');
		$this->load->model('EstructuraModel', 'esm');
		$this->load->model('MaestroModel', 'mam');
    }

	public function index($p = 0){		
		$this->load->view('header', $this->user);
		$cenasicods = $this->cam->listar();
		// Definimos unas variables para traer la data y mantener la lógica de paginación 
        $limite = 0;
        $data   = [];
		$total  = 0;
	
        try{
            $result = $this->pm->listar_page($limite, $p,$this->user->Cenasicod);
            $total  = $result->total;
			$data   = $result->data;
        } catch(Exception $e){
            var_dump($e);
        }

		//Inicializando paginacion
        $this->pagination->initialize(
            paginacion_config(
                site_url("paciente/index"),
                $total,
                $limite
            )
        );
		$this->load->view('table');
		
        $this->load->view('paciente/index', [
			'model' => $data,
			'cenasicods' => $cenasicods,
        ]);
        
        $this->load->view('footer');
	}

	
	public function crud($id = 0){
		$COD_PACIENTE = $this->input->post('COD_PACIENTE');

		if($COD_PACIENTE){
			redirect('paciente/crud/'.$COD_PACIENTE);
		}else {
			$msg = 'This is the test message for echo';
		}

		$data = null;
		if($id > 0){
			$data = $this->pm->obtener($id);
		}
		$tipo_docs = $this->mam->listar(237);
		$cenasicods = $this->cam->listar();
		$this->load->view('header', $this->user);
		$this->load->view('table');
		$this->load->view('paciente/crud', [
			'model' => $data,
			'tipo_docs' => $tipo_docs,
			'cenasicods' => $cenasicods
        ]);
		$this->load->view('footer');
	}

	public function guardar(){
		$COD_PACIENTE = $this->input->post('COD_PACIENTE');

		$data = [
			'TIPO_DOC' 			=> $this->input->post('TIPO_DOC'),
			'NRO_DOC' 			=> $this->input->post('NRO_DOC'),
			'APELLIDO_PATERNO' 	=> $this->input->post('APELLIDO_PATERNO'),
			'APELLIDO_MATERNO' 	=> $this->input->post('APELLIDO_MATERNO'),
			'NOMBRES' 			=> $this->input->post('NOMBRES'),
			'FECHA_NAC' 		=> $this->input->post('FECHA_NAC'),
			'SEXO' 				=> $this->input->post('SEXO'),
			'FECHA_REG' 		=> $this->input->post('FECHA_REG'),
			//'CENASICOD' 		=> $this->input->post('CENASICOD'),
			'ESTADO' 			=> $this->input->post('ESTADO'),
			'ACTIVO' 			=> $this->input->post('ACTIVO'),
		];
		if(empty($COD_PACIENTE)){
			ini_set('date.timezone','America/Lima'); 
		    $today = date("Y-m-d H:i:s"); 
            $data["FECHA_REG"] = $today;
			$data["ORICENASICOD"]=1;
			$data["ESTADO"] = 1;
			$data["ACTIVO"] = 1; //PACIENTE REGISTRADO PERO AUN NO ACTIVO EN MODULO MOV. " 2= EN PROCESO"
			$data["CENASICOD"] =  $this->user->Cenasicod;
			$data["USUARIO_REG"] = $this->user->Usuario;
			$this->pm->registrar($data);
			//var_dump($r->message);
		}else {
			ini_set('date.timezone','America/Lima'); 
		    $today = date("Y-m-d H:i:s"); 
			$data["FECHA_MOD"] = $today;
			$data["ESTADO"] = 2;
			$data["USUARIO_MOD"] = $this->user->Usuario;
			$this->pm->actualizar($data, $COD_PACIENTE);
		}
		redirect('paciente'); 
		// var_dump($data);
	}

    public function eliminar($id){
		ini_set('date.timezone','America/Lima'); 
		$today = date("Y-m-d H:i:s"); 
        $data["FECHA_BAJA"] = $today;
		$data["USUARIO_BAJA"] = $this->user->Usuario;
		$data["ESTADO"] = 0;
		$data["ACTIVO"] = 0;
		$this->pm->actualizar($data, $id);
			redirect('paciente');
	
		 
	}

}
