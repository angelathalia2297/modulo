<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Vigilancia extends CI_Controller {
	private $user;
    public function __CONSTRUCT(){
		parent::__CONSTRUCT();
		$this->user = RestApi::getUserData();
		// Valida que exista el usuario obtenido del token, del caso contrario lo regresa a la pagina de inicio que es nuestro controlador auth
	
		if($this->user === null) redirect('');
		$this->load->helper('url');
		$this->load->model('GruposModel', 'grm');
		$this->load->model('VigilanciaModel', 'vm');
		$this->load->model('MaestroModel', 'mam');	
		$this->load->model('CentroAsistenciaModel', 'cam');
		$this->load->model('EstructuraModel', 'esm');
		$this->load->model('AccesoVascularModel', 'avm'); 
		$this->load->model('PacienteModel', 'pm');
		$this->load->model('ProfesionalModel', 'prm');
		//añadir el model de las demas tablas complicaciones y test
    }

	public function index($p = 0){
		$this->load->view('header', $this->user); 
		$limite = 0;
		$data   = [];
		$total  = 0;
		try{
			$result = $this->vm->listar_page($limite,$p,$this->user->Cenasicod);
			$contadorC=$this->vm->listar();
			$total  = $result->total;
			$data   = $result->data;
			$cod_profesionales = $this->prm->listar($this->user->Cenasicod);
			$ingreso = $this->mam->listar(270);
		} catch(Exception $e){
				var_dump($e);
		}
		$this->load->view('table');
        $this->load->view('vigilancia/index', [
					'contadorC'=>$contadorC,
					'model' => $data,
					'cod_profesionales' => $cod_profesionales,
					'ingreso' => $ingreso,
		]);
        $this->load->view('footer');
}
	
	public function crud($id=0)	{
			$PK_EVALUACION = $this->input->post('PK_EVALUACION');
			if($PK_EVALUACION){
				redirect('vigilancia/crud/'.$PK_EVALUACION);
			}else {
				$msg = 'This is the test message for echo';
			}
			$data = null;
			if($id > 0){
				$data = $this->vm->obtener($id);
			}
			$cenasicods = $this->cam->listar();
			$cod_tipo_accesos = $this->mam->listar(40);
			//---MAESTROO TIPOO DE ACCESOOO
			$ubicaciones = $this->mam->listar(110);
			//$cod_cenclin = $this->esm->listar();
			$acceso_vascular = $this->avm->listar();
			$cod_pacientes = $this->pm->listar($this->user->Cenasicod);
			$complicaciones = $this->mam->listar(130);
			$cod_profesionales = $this->prm->listar($this->user->Cenasicod);
			
			$turno = $this->mam->listar(253);
			$sala = $this->mam->listar(248);
			$frecuencia = $this->mam->listar(258);

		

			$this->load->view('header', $this->user);
			$this->load->view('table');
			$this->load->view('vigilancia/crud', [
				'model' => $data,
				'cenasicods' => $cenasicods,
				'cod_tipo_accesos' => $cod_tipo_accesos,
				'ubicaciones' => $ubicaciones, 
				'acceso_vascular' => $acceso_vascular,
				'cod_pacientes' => $cod_pacientes,
				'complicaciones' => $complicaciones,
				'cod_profesionales' => $cod_profesionales,

				'turno' => $turno,
				'sala' => $sala,
				'frecuencia' => $frecuencia,

				]);
        $this->load->view('footer');
	}
// METODO GRABAR
	public function guardar(){
		$PK_EVALUACION = $this->input->post('PK_EVALUACION');
		
		$data = [
				'COD_PROFESIONAL' 				=> $this->input->post('COD_PROFESIONAL'),
				'PK_ACCESO_V' 					=> $this->input->post('PK_ACCESO_V'),
				'FECHA_EVAL' 					=> $this->input->post('FECHA_EVAL'),
				'SALA' 							=> $this->input->post('SALA'),
				'TURNO' 						=> $this->input->post('TURNO'),
				'FRECUENCIA_DIAL' 				=> $this->input->post('FRECUENCIA_DIAL'), 
				'PA_INI_SISTOLICA' 				=> $this->input->post('PA_INI_SISTOLICA'),
				'PA_INI_DIASTOLICA' 			=> $this->input->post('PA_INI_DIASTOLICA'),
				'PA_FINAL_SISTOLICA' 			=> $this->input->post('PA_FINAL_SISTOLICA'),
				'PA_FINAL_DIASTOLICA' 			=> $this->input->post('PA_FINAL_DIASTOLICA'),
				'QB' 							=> $this->input->post('QB'),
				'RA_PAE' 						=> $this->input->post('RA_PAE'),
				'RV_PVE' 						=> $this->input->post('RV_PVE'),
				'COMPLI_INI' 					=> $this->input->post('COMPLI_INI'),
				'COMPLI_INI2' 					=> $this->input->post('COMPLI_INI2'),
				'COMPLI_INI3' 					=> $this->input->post('COMPLI_INI3'),
				'COMPLI_INI4' 					=> $this->input->post('COMPLI_INI4'),
				'OBSER' 						=> $this->input->post('OBSER'),
				'TEST_ELEVACION' 				=> $this->input->post('TEST_ELEVACION'),
				'TEST_AUM_PULSO' 				=> $this->input->post('TEST_AUM_PULSO'),
				'TEST_VENAS_COLAT' 				=> $this->input->post('TEST_VENAS_COLAT'),
				'FC_DISTANCIA' 				    => $this->input->post('FC_DISTANCIA'),
				'FC_CARACTERISTICA' 			=> $this->input->post('FC_CARACTERISTICA'),
				'REG_COMPLETO' 					=> $this->input->post('REG_COMPLETO'),
		];

		if(empty($PK_EVALUACION)){
			ini_set('date.timezone','America/Lima'); 
		    $today = date("Y-m-d H:i:s"); 
			$data["FECHA_REG"] = $today;
		  	$data["ORICENASICOD"]=1;	
			$data["ESTADO"] = 1;
			$data["CENASICOD"] =  $this->user->Cenasicod;
			$data["USUARIO_REG"] = $this->user->Usuario;
		//var_dump($data);
			$this->vm->registrar($data);
		}else {
			ini_set('date.timezone','America/Lima'); 
			$today = date("Y-m-d H:i:s"); 
			
			$data["FECHA_MOD"] = $today; 
			$data["USUARIO_MOD"] = $this->user->Usuario;
			$this->vm->actualizar($data, $PK_EVALUACION);
		}
		redirect('vigilancia');   
}
		public function eliminar($id){
			ini_set('date.timezone','America/Lima'); 
			$today = date("Y-m-d H:i:s"); 
			$data["FECHA_BAJA"] = $today;
			$data["USUARIO_BAJA"] = $this->user->Usuario;
			$data["ESTADO"] = 0;
			$this->vm->actualizar($data, $id);
					redirect('vigilancia');
		}

		public function reportevigilancia($p = 0){
				  $this->load->view('header', $this->user);
				  $limite = 0;
				  $data   = [];
				  $total  = 0;
				  try{
					  
			  		  $grupos = $this->grm->listar();
					  $result = $this->vm->listar_page($limite,$p,$this->user->Cenasicod);
					  $cenasicods = $this->cam->listar();
					  $cod_tipo_accesos = $this->mam->listar(40);
					  $ubicaciones = $this->mam->listar(110);
					  $cod_cenclin = $this->esm->listar();
					  $acceso_vascular = $this->avm->listar();
					  $cod_profesionales = $this->prm->listar($this->user->Cenasicod);
					  $complicaciones = $this->mam->listar(130);
					  $ingreso = $this->mam->listar(270);
					  $total  = $result->total;
					  $data   = $result->data;
				  } catch(Exception $e){
						  var_dump($e);
				  }
				  $this->load->view('table');
				  $this->load->view('vigilancia/reportevigilancia', [
								  'model' => $data,
								  'grupos' => $grupos,
								  'cod_tipo_accesos' => $cod_tipo_accesos,
								  'ubicaciones' => $ubicaciones, 
								  'cod_cenclin' => $cod_cenclin,
								  'acceso_vascular' => $acceso_vascular,
								  'cenasicods' => $cenasicods,
								  'ingreso' => $ingreso,
								  'complicaciones' => $complicaciones,
								  'cod_profesionales' => $cod_profesionales,
							
		  
				  ]);
				  
				  $this->load->view('footer');
		}

		  
		  public function reportetest($p = 0){
			  $this->load->view('header', $this->user);
			  $limite = 0;
			  $data   = [];
			  $total  = 0;
			  try{
		  		  $grupos = $this->grm->listar();
				  $result = $this->vm->listar_page($limite,$p,$this->user->Cenasicod);
				  $cenasicods = $this->cam->listar();
				  $cod_tipo_accesos = $this->mam->listar(40);
				  $ubicaciones = $this->mam->listar(110);
				  $cod_cenclin = $this->esm->listar();
				  $acceso_vascular = $this->avm->listar();
				  $cod_pacientes = $this->pm->listar($this->user->Cenasicod);
				  $cod_profesionales = $this->prm->listar($this->user->Cenasicod);
				  $complicaciones = $this->mam->listar(130);
				  
				  $total  = $result->total;
				  $data   = $result->data;
			  } catch(Exception $e){
					  var_dump($e);
			  }
		  
		  
			  $this->load->view('table');
			$this->load->view('vigilancia/reportetest', [
							  'model' => $data,
	  //----------MAESTROO TIPOO DE ACCESOOO
							  'grupos' => $grupos,
							  'cod_tipo_accesos' => $cod_tipo_accesos,
	  //----------MAESTROO TubicacionesO
							  'ubicaciones' => $ubicaciones, 
							  'cod_cenclin' => $cod_cenclin,
							  'acceso_vascular' => $acceso_vascular,
								  'cenasicods' => $cenasicods,
							  'cod_pacientes' => $cod_pacientes,
							  'complicaciones' => $complicaciones,
							  'cod_profesionales' => $cod_profesionales,
	  
	  
			  ]);
		  //footer
			  $this->load->view('footer');
	  }

	  public function complicaciones($p = 0){
		  $this->load->view('header', $this->user);
		  $limite = 0;
		  $data   = [];
		  try{
	  //ERROR - CAMBIAR METODO EN MODEL A listar_page
			  $result = $this->vm->compli($limite,$p,$this->user->Cenasicod);
			  $cenasicods = $this->cam->listar();
		  //-------MAESTROO TIPOO DE ACCESOOO
			  $cod_tipo_accesos = $this->mam->listar(40);
		  //---MAESTROO TIPOO DE ACCESOOO
			  $ubicaciones = $this->mam->listar(110);
			  $cod_cenclin = $this->esm->listar();
			  $acceso_vascular = $this->avm->listar();
			  $cod_profesionales = $this->prm->listar($this->user->Cenasicod);
			  //COMPLI_MAESTRO
			  $complicaciones = $this->mam->listar(130);
			  $data   = $result->data;
		  } catch(Exception $e){
				  var_dump($e);
		  }
	  
	  
		  $this->load->view('table');
		  $this->load->view('vigilancia/complicaciones', [
						  'model' => $data,
						  'cod_tipo_accesos' => $cod_tipo_accesos,
						  'ubicaciones' => $ubicaciones, 
						  'cod_cenclin' => $cod_cenclin,
						  'acceso_vascular' => $acceso_vascular,
						  'cenasicods' => $cenasicods,
						  'complicaciones' => $complicaciones,
						  'cod_profesionales' => $cod_profesionales,
  
		  ]);
		  
		  $this->load->view('footer');
  }
}
