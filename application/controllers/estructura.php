<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estructura extends CI_Controller {
	private $user;
    public function __CONSTRUCT(){
		parent::__CONSTRUCT();
		$this->user = RestApi::getUserData();
		// Valida que exista el usuario obtenido del token, del caso contrario lo regresa a la pagina de inicio que es nuestro controlador auth
		 
		if($this->user === null) redirect('');
		$this->load->helper('url');
		$this->load->model('EstructuraModel', 'esm');
    }

	public function index($p = 0){

		$this->load->view('header', $this->user);
		$limite = 15;
		$data   = [];
		$total  = 0;
		try{
			$result = $this->esm->listar_page($limite, $p);
			$total  = $result->total;
			$data   = $result->data;
		} catch(Exception $e){
				var_dump($e);
		}
		//Inicializando paginacion
		$this->pagination->initialize(
			paginacion_config(
					site_url("estructura/index"),
					$total,
					$limite
			)
	);
	$this->load->view('table');
	$this->load->view('estructura/index', [
		'model' => $data,
		]);
		$this->load->view('footer');
		}
		

    public function crud($id = 0){
		$COD_CENCLI = $this->input->post('COD_CENCLI'); 
		if($COD_CENCLI){
			redirect('estructura/crud/'.$COD_CENCLI);
		}else {
			$msg = 'This is the test message for echo';
		}

		$data = null;
		if($id > 0){
			$data = $this->esm->obtener($id);
		
		} 
		$this->load->view('header', $this->user);
		$this->load->view('estructura/crud', [
			'model' => $data,
		]);
		$this->load->view('footer');
	}
//GUARDAR
	public function guardar(){

	$ORICENASICOD = $this->input->post('ORICENASICOD'); 
		$data = [ 
			'COD_CENCLI' 			=> $this->input->post('COD_CENCLI'),
			'DES_LARGA' 			=> $this->input->post('DES_LARGA'),
			'DES_CORTA' 			=> $this->input->post('DES_CORTA'),
			'CENASICOD' 			=> $this->input->post('DES_CORTA'), 
		];

		if(empty($COD_CENCLI)){ 
			$data["ORICENASICOD"]=1;
 
	
			$this->esm->registrar($data);
		}
		// var_dump($data);
		redirect('estructura');   
	}

    public function eliminar($id){  
		$this->esm->eliminar(  $id);
		redirect('estructura');
	}

}
