<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acceso_Vascular extends CI_Controller {

    private $user;

	public function __CONSTRUCT(){
        parent::__CONSTRUCT();
			$this->user = RestApi::getUserData();
			//var_dump($this->user->Cenasicod);
			// Valida que exista el usuario obtenido del token, del caso contrario lo regresa a la pagina de inicio que es nuestro controlador auth
			
			if($this->user === null) redirect('');
		$this->load->helper('url');

        $this->load->model('AccesoVascularModel', 'avm');        
        $this->load->model('CentroAsistenciaModel', 'cam');
		$this->load->model('EstructuraModel', 'esm');
        $this->load->model('MaestroModel', 'mam');
        $this->load->model('PacienteModel', 'pm');
    }

	public function index($p = 0){
        $this->load->view('header', $this->user);
		$cenasicods = $this->cam->listar();
		// Definimos unas variables para traer la data y mantener la lógica de paginación 
        $limite = 0;
        $data   = [];
        $total  = 0;
        
        try{
            $result = $this->avm->listar_page($limite, $p,$this->user->Cenasicod);
            $cod_tipo_accesos = $this->mam->listar(40);
            $ubicaciones = $this->mam->listar(110);
            $cod_cenclin = $this->esm->listar();
            $cod_pacientes = $this->pm->listar($this->user->Cenasicod);
            $egreso = $this->mam->listar(261);
            $ingreso = $this->mam->listar(270);
            
            $total  = $result->total;
            $data   = $result->data;
        } catch(Exception $e){
            var_dump($e);
        }


		//Inicializando paginacion
        $this->pagination->initialize(
            paginacion_config(
                site_url("acceso_vascular/index"),
                $total,
                $limite
            )
        );
        $this->load->view('table');
        $this->load->view('acceso_vascular/index', [
            'model' => $data,
            'cod_tipo_accesos' => $cod_tipo_accesos,
            'ubicaciones' => $ubicaciones, 
            'cod_cenclin' => $cod_cenclin,
            'cenasicods' => $cenasicods,
            'cod_pacientes' => $cod_pacientes,
            'egreso' => $egreso, 
            'ingreso' => $ingreso
        ]);
        
        $this->load->view('footer');
    }
    
	public function crud($id=0) {
		$PK_ACCESO_V = $this->input->post('PK_ACCESO_V');
		
		if($PK_ACCESO_V){
			redirect('acceso_vascular/crud/'.$PK_ACCESO_V);
		}else {
			$msg = 'This is the test message for echo';
		}

		$data = null;
		if($id > 0){
			$data = $this->avm->obtener($id);
		}

        $cod_tipo_ubicaciones_fav = $this->mam->listar_cod_rela(41);
        $cod_tipo_ubicaciones_inj = $this->mam->listar_cod_rela(43);
        $cod_tipo_ubicaciones_cvt = $this->mam->listar_cod_rela(45);
        $cod_tipo_ubicaciones_cvp = $this->mam->listar_cod_rela(44);
        $tipo_acceso = $this->mam->listar(40);
        $cod_cenclin = $this->esm->listar();
        $cenasicods = $this->cam->listar();
        $cod_pacientes = $this->pm->paciente_av($this->user->Cenasicod);
        $egresoavfav = $this->mam->listar_cod_rela(61);
		$egresoavcvc = $this->mam->listar_cod_rela(62);
		$egresoavotras = $this->mam->listar_cod_rela(67);
		$acceso_vascular = $this->avm->listar();
		$ingresoavnc = $this->mam->listar_cod_rela(63);
		$ingresoavreingreso = $this->mam->listar_cod_rela(64);
		$ingresoavnuevo = $this->mam->listar_cod_rela(65);
        
        $this->load->view('header', $this->user);
		$this->load->view('acceso_vascular/crud', [
			'model' => $data,
            'cod_tipo_ubicaciones_fav' => $cod_tipo_ubicaciones_fav,
            'cod_tipo_ubicaciones_inj' => $cod_tipo_ubicaciones_inj,
            'cod_tipo_ubicaciones_cvt' => $cod_tipo_ubicaciones_cvt,
            'cod_tipo_ubicaciones_cvp' => $cod_tipo_ubicaciones_cvp,
            'tipo_acceso' => $tipo_acceso, 
            'cod_cenclin' => $cod_cenclin,
            'cenasicods' => $cenasicods,
            'cod_pacientes' => $cod_pacientes,
            'egresoavfav' => $egresoavfav,
			'egresoavcvc' => $egresoavcvc,
			'egresoavotras' => $egresoavotras,
			'acceso_vascular' => $acceso_vascular,
			'ingresoavnc' => $ingresoavnc,
			'ingresoavreingreso' => $ingresoavreingreso,
			'ingresoavnuevo' => $ingresoavnuevo
        ]);
		$this->load->view('footer');

	}


    
    public function guardar(){
        $PK_ACCESO_V = $this->input->post('PK_ACCESO_V');
    
        $data = [
            'COD_PACIENTE' 			=> $this->input->post('COD_PACIENTE'),
            'FECHA_CREACION_AV' 		=> $this->input->post('FECHA_CREACION_AV'),
            'COD_TIPO_ACCESO' 			=> $this->input->post('COD_TIPO_ACCESO'),
            'UBICACION' 			=> $this->input->post('UBICACION'),
            'CIRUJANO_CV' 		=> $this->input->post('CIRUJANO_CV'),
            'COD_CENCLI' 		=> $this->input->post('COD_CENCLI'),
          //'CENASICOD' 		=> $this->input->post('CENASICOD'),
            'ESTADO' 			=> $this->input->post('ESTADO'),
            'ACTIVO' 			=> $this->input->post('ACTIVO'),
            'FECHA_I' 		=> $this->input->post('FECHA_I'),
			'MOTIVO_I' 		=> $this->input->post('MOTIVO_I'),
			'FECHA_E' 		=> $this->input->post('FECHA_E'),
			'MOTIVO_E' 		=> $this->input->post('MOTIVO_E'),
        ];
    
        if(empty($PK_ACCESO_V)){
            ini_set('date.timezone','America/Lima'); 
		    $today = date("Y-m-d H:i:s"); 
            $data["FECHA_REG"] = $today;
            $data["ORICENASICOD"]=1;
            $data["ACTIVO"] = 1;
            $data["ESTADO"] = 1;
            $data["CENASICOD"] =  $this->user->Cenasicod;
		    $data["USUARIO_REG"] = $this->user->Usuario;
         //var_dump($data);
            $this->avm->registrar($data);
        
        }else {
            ini_set('date.timezone','America/Lima'); 
		    $today = date("Y-m-d H:i:s"); 
            $data["FECHA_MOD"] = $today;
            $data["USUARIO_MOD"] = $this->user->Usuario;
            $this->avm->actualizar($data, $PK_ACCESO_V);
        }
        redirect('acceso_vascular');   
    }



    public function eliminar($id){
        ini_set('date.timezone','America/Lima'); 
		$today = date("Y-m-d H:i:s"); 
        $data["FECHA_BAJA"] = $today;
        $data["ESTADO"] = 0; 
        $data["USUARIO_BAJA"] = $this->user->Usuario;
	   
		$this->avm->actualizar($data, $id);
        redirect('acceso_vascular');
    }

	public function mov_av($p = 0){ 
		$this->load->view('header', $this->user);
		$this->load->view('frame'); 
		$limite = 0;
		$data   = [];
		$total  = 0;
		try{
            $result = $this->avm->listar_page($limite, $p,$this->user->Cenasicod);
			$total  = $result->total;
			$data   = $result->data;
		//	$acceso_vascular = $this->avm->listar();
			$cod_pacientes = $this->pm->listar($this->user->Cenasicod);
			$egreso = $this->mam->listar(261);
			$ingreso = $this->mam->listar(270);
		} catch(Exception $e){
				var_dump($e);
		}
		//Inicializando paginacion
		$this->pagination->initialize(
			paginacion_config(
					site_url("acceso_vascular/mov_av"),
					$total,
					$limite
			)
        );
        $this->load->view('table'); 
        $this->load->view('acceso_vascular/mov_av', [
            'model' => $data,
        	'cod_pacientes' => $cod_pacientes,
            'egreso' => $egreso, 
            'ingreso' => $ingreso, 
        //	'acceso_vascular' => $acceso_vascular
            ]);
            $this->load->view('footer');
	}

	public function reportecvc($p = 0){

        $this->load->view('header', $this->user);
        $this->load->view('frame'); 
        $limite = 0;
        $data   = [];
        try{
            $result = $this->avm->listarCVC($limite, $p,$this->user->Cenasicod);
            $data   = $result->data;
            $cod_pacientes = $this->pm->listar($this->user->Cenasicod);
        
            $egreso = $this->mam->listar(261);
            $ingreso = $this->mam->listar(270);
        } catch(Exception $e){
                var_dump($e);
        }
        //Inicializando paginacion

    $this->load->view('table'); 
    $this->load->view('acceso_vascular/reportecvc', [
        'model' => $data,
        'cod_pacientes' => $cod_pacientes,
        'egreso' => $egreso, 
        'ingreso' => $ingreso, 
      
        ]);
        $this->load->view('footer');
    }

public function reportefavi($p = 0){
        $this->load->view('header', $this->user);
        $this->load->view('frame'); 
        $limite = 0;
        $data   = [];
        //$total  = 0;
        try{
            $result = $this->avm->listarFAVI($limite, $p,$this->user->Cenasicod);
        //	$total  = $result->total;
            $data   = $result->data;
            $cod_pacientes = $this->pm->listar($this->user->Cenasicod);
          
            $egreso = $this->mam->listar(261);
            $ingreso = $this->mam->listar(270);
        } catch(Exception $e){
                var_dump($e);
        }

    $this->load->view('table'); 
    $this->load->view('acceso_vascular/reportefavi', [
        'model' => $data,
        'cod_pacientes' => $cod_pacientes,
        'egreso' => $egreso, 
        'ingreso' => $ingreso
      
        ]);
        $this->load->view('footer');
}
    
public function reportect($p = 0){

            $this->load->view('header', $this->user);
            $this->load->view('frame'); 
            $limite = 0;
            $data   = [];
        
            try{
                $result = $this->avm->listarCT($limite, $p,$this->user->Cenasicod);
            
                $data   = $result->data;
                $cod_pacientes = $this->pm->listar($this->user->Cenasicod);
               
                $egreso = $this->mam->listar(261);
                $ingreso = $this->mam->listar(270);
            } catch(Exception $e){
                    var_dump($e);
            }

        $this->load->view('table'); 
        $this->load->view('acceso_vascular/reportect', [
            'model' => $data,
            'cod_pacientes' => $cod_pacientes,
            'egreso' => $egreso, 
            'ingreso' => $ingreso
          
            ]);
            $this->load->view('footer');
}

}

