<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller  {
	private $user;
    public function __CONSTRUCT(){
		parent::__CONSTRUCT();

		
		$this->load->model('authmodel', 'am');
		$this->load->model('UsuarioModel', 'usr');
		$this->load->model('CentroAsistenciaModel', 'cam');
		$this->load->model('GruposModel', 'grm');
    }

	public function index()
	{

		$cenasicods = $this->cam->listar();
		$this->load->view('auth/index', [
		
			'cenasicods' => $cenasicods,
			]);
		
	}

	
	public function autenticar(){
		$error = '';
		$r = $this->am->autenticar(
				$this->input->post('Usuario'),
				$this->input->post('Clave'),	
				$this->input->post('Cenasicod')
		);
		if($r->response){
            // Seteamos el token
            RestApi::setToken($r->result);
			// User
			$user = RestApi::getUserData();
			//VALIDACION SI EL USUARIO ESTA ACTIVO y NO ESTA ELIMINADO 
         if(($user->Estado == 1) && ($user->EstadoReg == 1) ) {
			if($this->input->post('Cenasicod') ==  $user->Cenasicod)  {
				$cenasicods = $this->cam->listar();
				redirect('vigilancia');
			} else {
				RestApi::destroyToken();
				$error = 'No pertenece a ese Centro Asistencial';
			}
			} else {
				RestApi::destroyToken();
				$error = 'Su usuario no se encuentra activo';
		
			}		
			
		} else {
				$error = $r->message;
		}	

		$cenasicods = $this->cam->listar();

		$this->load->view('auth/index', [
			'error' => $error,
			'cenasicods' => $cenasicods
		]);

	}



	


}
