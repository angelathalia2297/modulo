<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupos extends CI_Controller {
	private $user;
    public function __CONSTRUCT(){
		parent::__CONSTRUCT();
		$this->user = RestApi::getUserData();
		// Valida que exista el usuario obtenido del token, del caso contrario lo regresa a la pagina de inicio que es nuestro controlador auth
		
		if($this->user === null) redirect('');
		$this->load->helper('url');
		$this->load->model('GruposModel', 'grm');
	}

	public function index($p = 0)
	{
		$this->load->view('header', $this->user);
		
			// Definimos unas variables para traer la data y mantener la lógica de paginación 
			$limite = 15;
			$data   = [];
			$total  = 0;	
					
		try{
			$result = $this->grm->listar_page($limite, $p);
			$total  = $result->total;
			$data   = $result->data;
		} catch(Exception $e){
			var_dump($e);
		}
		//Inicializando paginacion
		$this->pagination->initialize(
			paginacion_config(
					site_url("grupos/index"),
					$total,
					$limite
			)
		);
		$this->load->view('table');
		$this->load->view('grupos/index', [
			'model' => $data,
		
        ]);
	
		      //footer
		$this->load->view('footer');
	 
	
	
	}
	
    public function crud($id = 0)
	{
		$idGrupo = $this->input->post('idGrupo');

		if($idGrupo){
		
			redirect('grupos/crud/'.$idGrupo);
		}else {
			$msg = 'This is the test message for echo';
		}

		$data = null;
		if($id > 0){
			$data = $this->grm->obtener($id);
		}
		$this->load->view('header', $this->user);
		$this->load->view('grupos/crud', [
			'model' => $data,
        ]);
		$this->load->view('footer');

	}

	public function guardar(){
		
		$idGrupo = $this->input->post('idGrupo');

		$data = [
			'idGrupo' 			=> $this->input->post('idGrupo'),
			'NombreGrupo' 			=> $this->input->post('NombreGrupo'),
			'EstadoGrupo' 			=> $this->input->post('EstadoGrupo'),
			'EstadoReg' 			=> $this->input->post('EstadoReg'),
		];

		if(empty($idGrupo)){
			$data["EstadoGrupo"] = 1; 
			$data["EstadoReg"] = 1; 
			$this->grm->registrar($data);
		}else {
	
			$this->grm->actualizar($data, $idGrupo);
		}
		// var_dump($data);
		redirect('grupos');   
	}

    public function eliminar($id){
		$data["EstadoReg"] = 0; 
		$this->grm->actualizar($data, $id);
		redirect('grupos');
    }

}
