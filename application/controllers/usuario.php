<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {
	private $user;
    public function __CONSTRUCT(){
		parent::__CONSTRUCT();
		$this->user = RestApi::getUserData();
			// Valida que exista el usuario obtenido del token, del caso contrario lo regresa a la pagina de inicio que es nuestro controlador auth
			
		if($this->user === null) redirect('');
		$this->load->helper('url');
		$this->load->model('UsuarioModel', 'usr');
		$this->load->model('GruposModel', 'grm');
		$this->load->model('CentroAsistenciaModel', 'cam');
		$this->load->model('MaestroModel', 'mam');
	}
	
	public function index($p = 0){
			$this->load->view('header', $this->user);
			$grupos = $this->grm->listar();
			$cenasicods = $this->cam->listar();
		// Definimos unas variables para traer la data y mantener la lógica de paginación 
		$limite = 15;
		$data   = [];
		$total  = 0;
		
		try{
			
			$result = $this->usr->listar_page($limite, $p,$this->user->Cenasicod);
			$total  = $result->total;
			$data   = $result->data;
		} catch(Exception $e){
			var_dump($e);
		}
		//Inicializando paginacion
        $this->pagination->initialize(
            paginacion_config(
                site_url("usuario/index"),
                $total,
                $limite
            )
		);
		$this->load->view('table');
		$this->load->view('usuario/index', [
			'model' => $data,
			'grupos' => $grupos,
			'cenasicods' => $cenasicods
			]);
        $this->load->view('footer');
	}

    public function modificar_usuario($id = 0)
	{
		$idUsuario = $this->input->post('idUsuario');

		if($idUsuario){
			redirect('usuario/modificar_usuario/'.$idUsuario);
		}else {
			$msg = 'This is the test message for echo';
		}

		$data = null;
		if($id > 0){
			$data = $this->usr->obtener($id);
		}
		$grupos = $this->grm->listar();
		$cenasicods = $this->cam->listar();
		$this->load->view('table');
		$this->load->view('header', $this->user);
		$this->load->view('usuario/modificar_usuario', [
			'model' => $data,
			'grupos' => $grupos,
			'cenasicods' => $cenasicods
        ]);
		$this->load->view('footer');
	}

	public function guardar(){
		$idUsuario = $this->input->post('idUsuario');
		
		$data = [

			'NivelUsuario' 		=> $this->input->post('NivelUsuario'),
			'Nombre' 			=> $this->input->post('Nombre'),
			'Usuario' 			=> $this->input->post('Usuario'),
			'Clave' 			=> $this->input->post('Clave'),
			'Estado' 			=> $this->input->post('Estado'),
			'Cenasicod' 		=> $this->input->post('Cenasicod'),
			
		];

		if(empty($idUsuario)){
			ini_set('date.timezone','America/Lima'); 
			$today = date("Y-m-d H:i:s"); 
			$data["FechaRegistro"] = $today;
			$data["FechaAlta"] = $today;
			$data["Oricenasicod"]=1;
			$data["EstadoReg"] = 1;
			$data["Estado"] = 1;
			$data["Cenasicod"] =  $this->user->Cenasicod;
			$data["UsrRegistro"] = $this->user->Usuario;
			$this->usr->registrar($data);
			
		}else {

			if(empty($data['Clave'])) unset($data['Clave']);
			ini_set('date.timezone','America/Lima'); 
			$today = date("Y-m-d H:i:s"); 
			$data["FechaModifica"] = $today;
			$data["UsrModifica"] = $this->user->Usuario;
			$this->usr->actualizar($data, $idUsuario);
		}
		redirect('usuario');   
	}
	
    public function eliminar($id){
		
		ini_set('date.timezone','America/Lima'); 
		$today = date("Y-m-d H:i:s"); 
		$data["FechaBaja"] = $today;
		$data["EstadoReg"] = 0;
		$data["Estado"] = 0;
		$data["UsrBaja"] = $this->user->Usuario;
		$this->usr->actualizar($data, $id);
		redirect('usuario');

	}
	
    public function modpass($id = 0){
		//validar el usuario que ingreso
		$this->load->view('header', $this->user);
		$this->load->view('usuario/modpass');
		$this->load->view('footer');
	}

    public function perfil()
	{
		$this->load->view('header',$this->user);
		$this->load->view('usuario/perfil');
		$this->load->view('footer');
	}

	public function logout(){
	
		RestApi::destroyToken();
		redirect('');
	}

}
