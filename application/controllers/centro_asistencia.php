<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Centro_Asistencia extends CI_Controller {
	private $user;
    public function __CONSTRUCT(){
		parent::__CONSTRUCT();
		$this->user = RestApi::getUserData();
		// Valida que exista el usuario obtenido del token, del caso contrario lo regresa a la pagina de inicio que es nuestro controlador auth
		
		if($this->user === null) redirect('');
		$this->load->helper('url');
		$this->load->model('CentroAsistenciaModel', 'cam');
    }

	public function index($p = 0){

		$this->load->view('header', $this->user);
		$limite = 10;
		$data   = [];
		$total  = 0;
		try{
			$result = $this->cam->listar_page($limite, $p);
			$total  = $result->total;
			$data   = $result->data;
		} catch(Exception $e){
				var_dump($e);
		}
		//Inicializando paginacion
		$this->pagination->initialize(
			paginacion_config(
					site_url("centro_asistencia/index"),
					$total,
					$limite
			)
	    );
		$this->load->view('table');
	    $this->load->view('centro_asistencia/index', [
		'model' => $data,
		]);

        
		$this->load->view('footer');
		}
		
		public function crud($id = 0){
			
			$CENASICOD = $this->input->post('CENASICOD'); 
			if($CENASICOD){
				redirect('centro_asistencia/crud/'.$CENASICOD);
			}else {
				$msg = 'This is the test message for echo';
			}

			$data = null;
			if($id > 0){
				$data = $this->cam->obtener($id);
			
			}

			$this->load->view('header', $this->user);
			$this->load->view('centro_asistencia/crud', [
				'model' => $data,
			]);
			$this->load->view('footer');
		}


//GUARDAR
	public function guardar(){

	$ORICENASICOD = $this->input->post('ORICENASICOD'); 
		$data = [ 
			'CENASICOD' 			=> $this->input->post('CENASICOD'),
			'CENASIDES' 			=> $this->input->post('CENASIDES'),
			'CENASIDESCOR' 			=> $this->input->post('CENASIDESCOR'), 
			'ACTIVO' 			=> $this->input->post('ACTIVO'),
		];

		if(empty($CENASICOD)){ 
			$data["ORICENASICOD"]=1;
			$data["ACTIVO"] = 1;
	
			$this->cam->registrar($data);
		}
		// var_dump($data);
		redirect('centro_asistencia');   
	}

    public function eliminar($id){  
		$this->cam->eliminar(  $id);
		redirect('centro_asistencia');
		
	}
}
