


<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error404 extends CI_Controller {
    public function __CONSTRUCT(){
		parent::__CONSTRUCT();
		$this->load->helper('url');
    }

	public function index()
	{
		$this->load->helper('url');
		$this->load->view('error404/404');
	}
}




