<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profesional extends CI_Controller {
	private $user;
    public function __CONSTRUCT(){
		parent::__CONSTRUCT();
		$this->user = RestApi::getUserData();
		// Valida que exista el usuario obtenido del token, del caso contrario lo regresa a la pagina de inicio que es nuestro controlador auth
		
		if($this->user === null) redirect('');
		$this->load->helper('url');
		$this->load->model('ProfesionalModel', 'prm');
		$this->load->model('CentroAsistenciaModel', 'cam');
		$this->load->model('EstructuraModel', 'esm');
		$this->load->model('MaestroModel', 'mam');
	}
	
	public function index($p = 0)
	{
			$this->load->view('header', $this->user);

		// Definimos unas variables para traer la data y mantener la lógica de paginación 
		$limite = 0;
		$data   = [];
		$total  = 0;
		$u  = 'op' ;

		try{
			$result = $this->prm->listar_page($limite, $p,$this->user->Cenasicod,$u);
			$total  = $result->total;
			$data   = $result->data;
		} catch(Exception $e){
			var_dump($e);
		}


		$this->load->view('table');
		$this->load->view('profesional/index', [
			'model' => $data,
		
        ]);
        
        $this->load->view('footer');
	}

	public function crud($id=0)
	{
		
		$COD_PROFESIONAL = $this->input->post('COD_PROFESIONAL');
		
		if($COD_PROFESIONAL){
			redirect('profesional/crud/'.$COD_PROFESIONAL);
		}else {
			$msg = 'This is the test message for echo';
		}

		$data = null;
		if($id > 0){
			$data = $this->prm->obtener($id);
		}

		$tip_docs = $this->mam->listar(237);
		$cenasicods = $this->cam->listar();

		$this->load->view('header', $this->user);
		$this->load->view('table');
		$this->load->view('profesional/crud', [
			'model' => $data,
			'tip_docs' => $tip_docs,
			'cenasicods' => $cenasicods
        ]);
		$this->load->view('footer');
	}
	
public function guardar(){
	$COD_PROFESIONAL = $this->input->post('COD_PROFESIONAL');

	$data = [
		'TIP_DOC' 			=> $this->input->post('TIP_DOC'),
		'NUM_DOC' 			=> $this->input->post('NUM_DOC'),
		'APELLIDO_PAT' 		=> $this->input->post('APELLIDO_PAT'),
		'APELLIDO_MAT' 		=> $this->input->post('APELLIDO_MAT'),
		'NOMBRES' 			=> $this->input->post('NOMBRES'),
		'COD_PLANILLA' 		=> $this->input->post('COD_PLANILLA'),
		'CEEP' 				=> $this->input->post('CEEP'),
		'REE' 				=> $this->input->post('REE'),
		'GRUPO' 			=> $this->input->post('GRUPO'),
		//'CENASICOD' 		=> $this->input->post('CENASICOD'),
	];

	if(empty($COD_PROFESIONAL)){
		ini_set('date.timezone','America/Lima'); 
		$today = date("Y-m-d H:i:s"); 
        $data["FECHA_REG"] = $today;
		$data["ORICENASICOD"]=1;	
		$data["ESTADO"] = 1;
		$data["CENASICOD"] =  $this->user->Cenasicod;
		$data["USUARIO_REG"] = $this->user->Usuario;
		$this->prm->registrar($data);
	
	}else {
	
		ini_set('date.timezone','America/Lima'); 
		$today = date("Y-m-d H:i:s"); 
        $data["FECHA_MOD"] = $today;
		$data["USUARIO_MOD"] = $this->user->Usuario;
		$this->prm->actualizar($data, $COD_PROFESIONAL);
	}
	redirect('profesional');   
}
    	public function eliminar($id){
			ini_set('date.timezone','America/Lima'); 
			$today = date("Y-m-d H:i:s"); 
			$data["FECHA_BAJA"] = $today;
			$data["USUARIO_BAJA"] = $this->user->Usuario;
			$data["ESTADO"] = 0;
			$this->prm->actualizar($data, $id);
			redirect('profesional');
    }
}
