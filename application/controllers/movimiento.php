<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Movimiento extends CI_Controller {
	private $user;
    public function __CONSTRUCT(){
		parent::__CONSTRUCT();
		$this->user = RestApi::getUserData();
		// Valida que exista el usuario obtenido del token, del caso contrario lo regresa a la pagina de inicio que es nuestro controlador auth
		
		if($this->user === null) redirect('');
		$this->load->helper('url');
		$this->load->model('MovimientoModel', 'mov');
		$this->load->model('PacienteModel', 'pm');
		$this->load->model('AccesoVascularModel', 'avm'); 
		$this->load->model('MaestroModel', 'mam');
    }

	public function index($p = 0){
		$this->load->view('header', $this->user);
		$this->load->view('frame'); 
		$limite = 0;
		$data   = [];
		$total  = 0;
		try{
            $result = $this->mov->listar_page($limite, $p,$this->user->Cenasicod);
			$total  = $result->total;
			$data   = $result->data;
			$acceso_vascular = $this->avm->listar();
			$cod_pacientes = $this->pm->listar($this->user->Cenasicod);
			$egreso = $this->mam->listar(261);
			$ingreso = $this->mam->listar(270);
		} catch(Exception $e){
				var_dump($e);
		}
		//Inicializando paginacion
		$this->pagination->initialize(
			paginacion_config(
					site_url("movimiento/index"),
					$total,
					$limite
			)
	);
	$this->load->view('table'); 
	$this->load->view('movimiento/index', [
		'model' => $data,
		'cod_pacientes' => $cod_pacientes,
		'egreso' => $egreso, 
		'ingreso' => $ingreso, 
		'acceso_vascular' => $acceso_vascular
		]);
		$this->load->view('footer');
	}

    public function modal_mov($id=0, $cod=0) {
		$ID_MOV = $this->input->post('ID_MOV');
	
		if($ID_MOV){
			redirect('movimiento/modal_mov/'.$ID_MOV);
		}else {
			$msg = 'This is the test message for echo';
		}

		$data = null;
		if($id > 0){
			$data = $this->mov->obtener($id);
		} else {
			$data = $this->pm->obtener($cod);
		}
		$egresoavfav = $this->mam->listar_cod_rela(61);
		$egresoavcvc = $this->mam->listar_cod_rela(62);
		$egresoavotras = $this->mam->listar_cod_rela(67);
		$acceso_vascular = $this->avm->listar();
		$ingresoavnc = $this->mam->listar_cod_rela(63);
		$ingresoavreingreso = $this->mam->listar_cod_rela(64);
		$ingresoavnuevo = $this->mam->listar_cod_rela(65);
	
		$cod_pacientes = $this->pm->listar($this->user->Cenasicod);
		$this->load->view('header', $this->user);
		$this->load->view('table');	
		$this->load->view('movimiento/modal_mov', [
			'model' => $data,
			'cod_pacientes' => $cod_pacientes,
			'egresoavfav' => $egresoavfav,
			'egresoavcvc' => $egresoavcvc,
			'egresoavotras' => $egresoavotras,
			'acceso_vascular' => $acceso_vascular,
			'ingresoavnc' => $ingresoavnc,
			'ingresoavreingreso' => $ingresoavreingreso,
			'ingresoavnuevo' => $ingresoavnuevo,

		]);
		$this->load->view('footer');
	}

	public function guardar(){
		$ID_MOV = $this->input->post('ID_MOV');

		$data = [
			'COD_PACIENTE_AV' 	=> $this->input->post('COD_PACIENTE_AV'),
			'FECHA_I' 		=> $this->input->post('FECHA_I'),
			'MOTIVO_I' 		=> $this->input->post('MOTIVO_I'),
			'FECHA_E' 		=> $this->input->post('FECHA_E'),
			'MOTIVO_E' 		=> $this->input->post('MOTIVO_E'),
		//	'ACTIVO' 		=> $this->input->post('ACTIVO'),
		];

		if(empty($ID_MOV)){
			ini_set('date.timezone','America/Lima'); 
		    $today = date("Y-m-d H:i:s"); 
            $data["FECHA_REG"] = $today;
			$data["ORICENASICOD"]=1;
			$data["ESTADO"] = 1;
	//		$data["ACTIVO"] = 1;
			$data["CENASICOD"] =  $this->user->Cenasicod;
			$data["USUARIO_REG"] = $this->user->Usuario;
		//	var_dump($data);
			$this->mov->registrar($data);
			
		}else {
			ini_set('date.timezone','America/Lima'); 
		    $today = date("Y-m-d H:i:s"); 
			$data["FECHA_MOD"] = $today;
			$data["ESTADO"] = 2;
			$data["USUARIO_MOD"] = $this->user->Usuario;
			
			$this->mov->actualizar($data, $ID_MOV);
		} 
	redirect('movimiento'); 
	}

	public function eliminar($id){
		ini_set('date.timezone','America/Lima'); 
		$today = date("Y-m-d H:i:s"); 
        $data["FECHA_BAJA"] = $today;
		$data["USUARIO_BAJA"] = $this->user->Usuario;
		$data["ESTADO"] = 0;
		$this->mov->actualizar($data, $id);
			redirect('movimiento');		 
	}


	public function reportecvc($p = 0){

			$this->load->view('header', $this->user);
			$this->load->view('frame'); 
			$limite = 0;
			$data   = [];
			try{
				$result = $this->mov->listarCVC($limite, $p,$this->user->Cenasicod);
				$data   = $result->data;
				$cod_pacientes = $this->pm->listar($this->user->Cenasicod);
				$acceso_vascular = $this->avm->listar();
				$egreso = $this->mam->listar(261);
				$ingreso = $this->mam->listar(270);
			} catch(Exception $e){
					var_dump($e);
			}
			//Inicializando paginacion

		$this->load->view('table'); 
		$this->load->view('movimiento/reportecvc', [
			'model' => $data,
			'cod_pacientes' => $cod_pacientes,
			'egreso' => $egreso, 
			'ingreso' => $ingreso, 
			'acceso_vascular' => $acceso_vascular
			]);
			$this->load->view('footer');
	}

	public function reportefavi($p = 0){
			$this->load->view('header', $this->user);
			$this->load->view('frame'); 
			$limite = 0;
			$data   = [];
			//$total  = 0;
			try{
				$result = $this->mov->listarFAVI($limite, $p,$this->user->Cenasicod);
			//	$total  = $result->total;
				$data   = $result->data;
				$cod_pacientes = $this->pm->listar($this->user->Cenasicod);
				$acceso_vascular = $this->avm->listar();
				$egreso = $this->mam->listar(261);
				$ingreso = $this->mam->listar(270);
			} catch(Exception $e){
					var_dump($e);
			}
	
		$this->load->view('table'); 
		$this->load->view('movimiento/reportefavi', [
			'model' => $data,
			'cod_pacientes' => $cod_pacientes,
			'egreso' => $egreso, 
			'ingreso' => $ingreso, 
			'acceso_vascular' => $acceso_vascular
			]);
			$this->load->view('footer');
	}
		
	public function reportect($p = 0){

				$this->load->view('header', $this->user);
				$this->load->view('frame'); 
				$limite = 0;
				$data   = [];
			
				try{
					$result = $this->mov->listarCT($limite, $p,$this->user->Cenasicod);
				
					$data   = $result->data;
					$cod_pacientes = $this->pm->listar($this->user->Cenasicod);
					$acceso_vascular = $this->avm->listar();
					$egreso = $this->mam->listar(261);
					$ingreso = $this->mam->listar(270);
				} catch(Exception $e){
						var_dump($e);
				}
	
			$this->load->view('table'); 
			$this->load->view('movimiento/reportect', [
				'model' => $data,
				'cod_pacientes' => $cod_pacientes,
				'egreso' => $egreso, 
				'ingreso' => $ingreso, 
				'acceso_vascular' => $acceso_vascular
				]);
				$this->load->view('footer');
	}
}
