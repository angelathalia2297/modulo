<?php
class ProfesionalModel extends CI_Model{
    public function listar($n){
        return RestApi::call(
            RestApiMethod::GET,
            "profesional/listar/$n"
        );
    }
    
    public function listar_page($l = 15, $p = 0,$c,$u){
        return RestApi::call(
            RestApiMethod::GET,
            "profesional/listar/$l/$p/$c/$u"
        );
    }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "profesional/obtener/$id"
        );
	}	
    
    public function getProfesionalDate($fechainicio,$fechafin){
        return RestApi::call(
            RestApiMethod::GET,
            "profesional/getProfesionalDate/$fechainicio/$fechafin"
        );
	}	
    
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
			"profesional/registrar",
			$data
        );
	}
	
	public function actualizar($data, $id){
        return RestApi::call(
            RestApiMethod::PUT,
			"profesional/actualizar/$id", $data
                    );
	}

	public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "profesional/eliminar/$id"
        );
	}
}
