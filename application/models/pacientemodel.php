<?php
class PacienteModel extends CI_Model{
    public function listar($n){
        return RestApi::call(
            RestApiMethod::GET,
            "paciente/listar/$n"
        );
    }
    public function paciente_av($n){
        return RestApi::call(
            RestApiMethod::GET,
            "paciente/paciente_av/$n"
        );
    }
    public function listar_page($l = 0, $p = 0,$c){
        return RestApi::call(
            RestApiMethod::GET,
            "paciente/listar/$l/$p/$c"
        );
    }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "paciente/obtener/$id"
        );
	}	
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
			"paciente/registrar",
			$data
        );
	}
	public function actualizar($data, $id){
        return RestApi::call(
            RestApiMethod::PUT,
			"paciente/actualizar/$id",
            $data
                    );
	}
//METODOOO BAJA
	public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "paciente/eliminar/$id"
        );
	}
}
