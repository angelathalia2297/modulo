<?php
class GruposModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "grupo/listar"
        );
    }
    public function listar_page($l = 10, $p = 0){
        return RestApi::call(
            RestApiMethod::GET,
            "grupo/listar/$l/$p"
        );
    }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "grupo/obtener/$id"
        );
	}	
    
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
			"grupo/registrar",
			$data
        );
	}
	
	public function actualizar($data, $id){
        return RestApi::call(
            RestApiMethod::PUT,
			"grupo/actualizar/$id",
            $data
                    );
	}

	public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "grupo/eliminar/$id"
        );
	}
}
