<?php
class MaestroModel extends CI_Model{
    public function listar($tipo){
        return RestApi::call(
            RestApiMethod::GET,
            "maestro/listar/tipo_padre/$tipo"
        );
    }
    public function listar_cod_rela($rel){
        return RestApi::call(
            RestApiMethod::GET,
            "maestro/listar_cod_rela/$rel"
        );
    }
//MESTRO_ROUTE COINCIDIR METODOS
    public function listar_page($l = 10, $p = 0){
        return RestApi::call(
            RestApiMethod::GET,
            "maestro/listar/$l/$p"
        );
    }

    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "maestro/obtener/$id"
        );
    }
    
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
			"maestro/registrar",
			$data
        );
	}
	
	public function actualizar($data, $id){
        return RestApi::call(
            RestApiMethod::PUT,
			"maestro/actualizar/$id",
            $data
                    );
	}

	public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "maestro/eliminar/$id"
        );
	}
}
