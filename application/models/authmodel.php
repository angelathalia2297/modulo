<?php
class AuthModel extends CI_Model{
    public function autenticar($Usuario, $Clave, $Cenasicod){
        return RestApi::call(
            RestApiMethod::POST,
            "auth/autenticar",
            [
                'Usuario' => $Usuario,
                'Clave' => $Clave,
                'Cenasicod' => $Cenasicod,
               
            ]
        );
    }

    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
			"auth/registrar",
            $data
        );
    }
    

}