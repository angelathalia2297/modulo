<?php
class CentroAsistenciaModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "centro_asistencia/listar"
        );
    }
    public function listar_page($l = 15, $p = 0){
        return RestApi::call(
            RestApiMethod::GET,
            "centro_asistencia/listar/$l/$p"
        );
    }

    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "centro_asistencia/obtener/$id"
        );
	}	
  
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
			"centro_asistencia/registrar",
			$data
        );
	}
	
	public function actualizar($data, $id){
        return RestApi::call(
            RestApiMethod::PUT,
			"centro_asistencia/actualizar/$id",
            $data
                    );
    }
    
    //METODOOO BAJA
	public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "centro_asistencia/eliminar/$id"
        );
	}

}
