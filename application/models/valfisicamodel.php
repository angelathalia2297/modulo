<?php
class ValfisicaModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "valfisica/listar"
        );
    }
    public function listar_page($l = 10, $p = 0){
        return RestApi::call(
            RestApiMethod::GET,
            "valfisica/listar/$l/$p"
        );
    }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "valfisica/obtener/$id"
        );
	}	
   
}
