<?php
class VigilanciaModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "vigilancia/listar"
        );
    }
    public function listar_page($l = 15, $p = 0,$c){
        return RestApi::call(
            RestApiMethod::GET,
            "vigilancia/listar/$l/$p/$c"
        );
    }	
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "vigilancia/obtener/$id"
        );
	}	 
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
			"vigilancia/registrar",
			$data
        );
	}
	public function actualizar($data, $id){
        return RestApi::call(
            RestApiMethod::PUT,
			"vigilancia/actualizar/$id",
            $data
                    );
	}
    //METODOOO BAJA
	public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "vigilancia/eliminar/$id"
        );
    }
    public function compli($l = 15, $p = 0,$c){
        return RestApi::call(
            RestApiMethod::GET,
            "vigilancia/compli/$l/$p/$c"
        );
    }
}
