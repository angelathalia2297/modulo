<?php
class EstructuraModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "estructura/listar"
        );
    }
    public function listar_page($l = 10, $p = 0){
        return RestApi::call(
            RestApiMethod::GET,
            "estructura/listar/$l/$p"
        );
    }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "estructura/obtener/$id"
        );
	}	

    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
			"estructura/registrar",
			$data
        );
	}

	public function actualizar($data, $id){
        return RestApi::call(
            RestApiMethod::PUT,
			"estructura/actualizar/$id",
            $data
                    );
	}
//METODOOO BAJA

	public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "estructura/eliminar/$id"
        );
	}
    
}
