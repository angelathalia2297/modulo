<?php
class AccesoVascularModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "vascular/listar"
        );
    } 
    public function listar_page($l = 15, $p = 0,$c){
        return RestApi::call(
            RestApiMethod::GET,
            "vascular/listar/$l/$p/$c"
        );
    }
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "vascular/obtener/$id"
        );
	}	
    
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
			"vascular/registrar",
			$data
        );
	}
	
	public function actualizar($data, $id){
        return RestApi::call(
            RestApiMethod::PUT,
			"vascular/actualizar/$id",
            $data
                    );
	}

	public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "vascular/eliminar/$id"
        );
    }
    
    public function listarFAVI($l = 15, $p = 0,$c){
        return RestApi::call(
            RestApiMethod::GET,
            "vascular/listarFAVI/$l/$p/$c"
        );
    }

    public function listarCVC($l = 15, $p = 0,$c){
        return RestApi::call(
            RestApiMethod::GET,
            "vascular/listarCVC/$l/$p/$c"
        );
    }

    public function listarCT($l = 15, $p = 0,$c){
        return RestApi::call(
            RestApiMethod::GET,
            "vascular/listarCT/$l/$p/$c"
        );
    }
}
