<?php
class UsuarioModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "usuario/listar"
        );
    }

    public function listar_page($l = 15, $p = 0,$c){
        return RestApi::call(
            RestApiMethod::GET,
            "usuario/listar/$l/$p/$c"
        );
    }
	
    public function obtener($id){
        return RestApi::call(
            RestApiMethod::GET,
            "usuario/obtener/$id"
        );
	}	
    
    public function registrar($data){
        return RestApi::call(
            RestApiMethod::POST,
			"usuario/registrar",
            $data
        );
	}
	
	public function actualizar($data, $id){
        return RestApi::call(
            RestApiMethod::PUT,
			"usuario/actualizar/$id",
            $data
                    );
	}

	public function eliminar($id){
        return RestApi::call(
            RestApiMethod::DELETE,
            "usuario/eliminar/$id"
        );
	}
}
