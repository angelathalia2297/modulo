-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-10-2018 a las 22:49:52
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `registro`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`admin`@`10.0.52.65` PROCEDURE `sp_combo` (`_parametro` VARCHAR(20))  BEGIN
	DROP TEMPORARY TABLE IF EXISTS _combo;
	CREATE TEMPORARY TABLE _combo (columna1 VARCHAR(10), columna2 VARCHAR(50) );
	IF (_parametro= 'NIVELUSUARIO') THEN
		INSERT INTO _combo VALUES ('01','ADMIN. SISTEMA');
		INSERT INTO _combo VALUES ('02','USUARIO NORMAL');
	END IF;
	
	IF (_parametro= 'ESTADOCUMENTO') THEN
		INSERT INTO _combo VALUES ('PRELIMINAR','PRELIMINAR');
		INSERT INTO _combo VALUES ('CONFORME','CONFORMIZADO');
		INSERT INTO _combo VALUES ('ANULADO','ANULADO');
	END IF;
	
	IF (_parametro= 'COMPLICACIONES') THEN
		INSERT INTO _combo 
		SELECT COD_TIPO,DES_CORTA FROM CNSR_MAESTRO WHERE COD_TIP_PAD=130;
	END IF;
	
	IF (_parametro= 'TIPACCESO') THEN
		INSERT INTO _combo 
		SELECT COD_TIPO,DES_CORTA FROM CNSR_MAESTRO WHERE COD_TIP_PAD=40;
	END IF;	
	
	IF (_parametro= 'GRPUBICACION') THEN
		INSERT INTO _combo 
		SELECT COD_TIPO,DES_LARGA FROM CNSR_MAESTRO WHERE COD_TIPO IN (41,43,44,45);
	END IF;	
	
	
	SELECT * FROM _combo;
	DROP TEMPORARY TABLE _combo;
END$$

CREATE DEFINER=`admin`@`10.0.52.65` PROCEDURE `sp_datosav_list` (`_vopcion` CHAR(1), `_vcod` INT, `_vfecha` CHAR(10))  BEGIN
       IF (_vopcion = '1') THEN
         SELECT p.FECHA_CREACION_AV,p.COD_TIPO_ACCESO,p.UBICACION,p.CIRUJANO_CV FROM cnsr_datos_av p
         WHERE p.COD_PACIENTE = _vcod AND p.FECHA_CREACION_AV = STR_TO_DATE(_vfecha,'%Y-%m-%d') AND p.ESTADO <> '3';
       END IF;
       IF (_vopcion = '2') THEN
         SELECT p.FECHA_CREACION_AV,p.COD_TIPO_ACCESO,p.UBICACION,p.CIRUJANO_CV FROM cnsr_datos_av p
         WHERE p.COD_PACIENTE = _vcod AND p.FECHA_CREACION_AV = STR_TO_DATE(_vfecha,'%Y-%m-%d') AND p.ESTADO <> '3';
       END IF;
       IF (_vopcion = '3') THEN	-- opcion de busqueda mantenimiento de datos_av
	 SELECT p.COD_PACIENTE,p.FECHA_CREACION_AV,p.COD_TIPO_ACCESO,p.UBICACION,p.CIRUJANO_CV,p.COD_CENCLI FROM cnsr_datos_av p
         WHERE p.COD_PACIENTE = _vcod AND p.FECHA_CREACION_AV = STR_TO_DATE(_vfecha,'%Y-%m-%d') AND p.ESTADO <> '3';
       END IF;
       IF (_vopcion = '4') THEN
         SELECT p.COD_TIPO_ACCESO,p.UBICACION,p.FECHA_CREACION_AV,p.CIRUJANO_CV FROM cnsr_datos_av p
         WHERE p.COD_PACIENTE = _vcod AND p.ACTIVO = '1';
       END IF;
    END$$

CREATE DEFINER=`admin`@`10.0.52.65` PROCEDURE `sp_evaluaciones_list` (`_vopcion` CHAR(1), `_vcod` INT)  BEGIN
       IF (_vopcion = '1') THEN -- opcion de busqueda de evaluaciones para mostrar formulario al terminar el registro, no fistula
          SELECT e.PK_EVALUACION,e.FECHA_EVAL,CONCAT(p.APELLIDO_PATERNO,' ',p.APELLIDO_MATERNO,' ',p.NOMBRES) AS PACIENTE,
		CONCAT(r.APELLIDO_PAT,' ',r.APELLIDO_MAT,' ',r.NOMBRES) AS PROFESIONAL,d.COD_TIPO_ACCESO,d.UBICACION,e.FECHA_CREACION_AV,e.SALA,e.TURNO,e.FRECUENCIA_DIAL,
		e.CONDICION_PAC,e.PA_INI_SISTOLICA,e.PA_INI_DIASTOLICA,e.PA_FINAL_SISTOLICA,e.PA_FINAL_DIASTOLICA,e.QB,e.RA_PAE,
		e.RV_PVE,e.USUARIO_REG,
		(SELECT GROUP_CONCAT(r.descripcion SEPARATOR '|') FROM cnsr_complicaciones_av r LEFT JOIN cnsr_evaluaciones_av e ON r.PK_EVALUACION=e.PK_EVALUACION 
		WHERE e.`PK_EVALUACION`=_vcod AND r.estado <> '3' GROUP BY e.PK_EVALUACION) AS COMPLICACIONES		
		FROM cnsr_evaluaciones_av e 
		INNER JOIN cnsr_profesional r INNER JOIN cnsr_paciente p INNER JOIN cnsr_datos_av d
		ON e.COD_PROFESIONAL=r.COD_PROFESIONAL AND e.COD_PACIENTE=p.COD_PACIENTE
		AND e.`COD_PACIENTE`=d.COD_PACIENTE AND e.FECHA_CREACION_AV=d.FECHA_CREACION_AV
		WHERE e.PK_EVALUACION =_vcod AND e.ESTADO <> '3';
       END IF;
       IF (_vopcion = '2') THEN -- opcion de busqueda de solo evaluaciones de todos los campos para mantenimiento crud
         SELECT e.PK_EVALUACION,e.FECHA_EVAL,e.COD_PACIENTE,e.COD_PROFESIONAL,e.FECHA_CREACION_AV,e.SALA,e.TURNO,e.FRECUENCIA_DIAL,
		e.CONDICION_PAC,e.PA_INI_SISTOLICA,e.PA_INI_DIASTOLICA,e.PA_FINAL_SISTOLICA,e.PA_FINAL_DIASTOLICA,e.QB,e.RA_PAE,
		e.RV_PVE,e.USUARIO_REG FROM cnsr_evaluaciones_av e
         WHERE e.PK_EVALUACION = _vcod AND e.ESTADO <> '3';
       END IF;
       IF (_vopcion = '3') THEN -- opcion de busqueda todo evaluaciones, complicacion y fistula
         SELECT e.PK_EVALUACION,e.FECHA_EVAL,CONCAT(p.APELLIDO_PATERNO,' ',p.APELLIDO_MATERNO,' ',p.NOMBRES) AS PACIENTE,
		CONCAT(r.APELLIDO_PAT,' ',r.APELLIDO_MAT,' ',r.NOMBRES) AS PROFESIONAL,d.COD_TIPO_ACCESO,d.UBICACION,e.FECHA_CREACION_AV,e.SALA,e.TURNO,e.FRECUENCIA_DIAL,
		e.CONDICION_PAC,e.PA_INI_SISTOLICA,e.PA_INI_DIASTOLICA,e.PA_FINAL_SISTOLICA,e.PA_FINAL_DIASTOLICA,e.QB,e.RA_PAE,
		e.RV_PVE,e.USUARIO_REG,
		(SELECT GROUP_CONCAT(r.descripcion SEPARATOR '|') FROM cnsr_complicaciones_av r LEFT JOIN cnsr_evaluaciones_av e ON r.PK_EVALUACION=e.PK_EVALUACION 
		WHERE e.`PK_EVALUACION`=1 AND r.estado <> '3' GROUP BY e.PK_EVALUACION) AS COMPLICACIONES,
		f.`TEST_ELEVACION`,f.`TEST_AUM_PULSO`,f.`TEST_VENAS_COLAT`,f.`FC_DISTANCIA`,f.`FC_CARACTERISTICA`		
		FROM cnsr_evaluaciones_av e 
		INNER JOIN cnsr_profesional r INNER JOIN cnsr_paciente p INNER JOIN cnsr_datos_av d
		ON e.COD_PROFESIONAL=r.COD_PROFESIONAL AND e.COD_PACIENTE=p.COD_PACIENTE
		AND e.`COD_PACIENTE`=d.COD_PACIENTE AND e.FECHA_CREACION_AV=d.FECHA_CREACION_AV
		INNER JOIN cnsr_valfisica_fistula f ON e.`PK_EVALUACION`=f.`PK_EVALUACION`
		WHERE e.PK_EVALUACION =1 AND e.ESTADO <> '3';
       END IF;
END$$

CREATE DEFINER=`admin`@`10.0.52.65` PROCEDURE `sp_login` (IN `_usuario` VARCHAR(20), IN `_clave` VARCHAR(30))  BEGIN
	SELECT u.idUsuario, u.nivelusuario, CASE u.nivelusuario WHEN '01' THEN 'USR. ADMINISTR.' WHEN '02' THEN 'USR.NORMAL' END AS desNivel,
	u.nombre, u.estado, DATE_FORMAT(u.fechaalta,'%d/%m/%Y') as fechaalta, 
	DATE_FORMAT(CURRENT_DATE(), '%d/%m/%Y') AS fechaactual, ifnull(u.TmpSesion,5) as tiempoSession,
	u.Cenasicod, c.cenasides
	FROM Usuario u INNER JOIN cmcas10 c ON u.oricenasicod=c.oricenasicod AND u.cenasicod=c.cenasicod 
	WHERE u.idUsuario = _usuario AND u.clave = md5(_clave);
	
	UPDATE Usuario SET UltimoAcceso = CURRENT_TIMESTAMP() WHERE idUsuario= _usuario AND Clave= _clave;
	
    END$$

CREATE DEFINER=`admin`@`10.0.52.65` PROCEDURE `sp_marcador_list` (`_vopcion` CHAR(1), `_vtexto` VARCHAR(100))  BEGIN
       if (_vopcion = '1') then
         select concat (codMarcador, ' | ' , desMarcador) as descripcion
         from marcador m where m.codMarcador like CONCAT(_vtexto,'%') and  estReg='1';
       end if;
       IF (_vopcion = '2') THEN
         SELECT CONCAT (codMarcador, ' | ' , desMarcador) as descripcion
         FROM marcador m WHERE m.desMarcador LIKE CONCAT('%',_vtexto,'%') AND estReg='1';
       END IF;
       IF (_vopcion = '3') THEN	-- opcion de busqueda bandeja de marcador
	 select codMarcador, desMarcador, desUnd, estReg, indStock
	 from marcador m where m.desMarcador LIKE CONCAT('%',_vtexto,'%');
       end if;
    END$$

CREATE DEFINER=`admin`@`10.0.52.65` PROCEDURE `sp_paciente_list` (`_vopcion` CHAR(1), `_vtexto` VARCHAR(200))  BEGIN
       IF (_vopcion = '1') THEN
         SELECT p.COD_PACIENTE,p.NRO_DOC,CONCAT(p.APELLIDO_PATERNO,' ',p.APELLIDO_MATERNO,' ',p.NOMBRES) AS PACIENTE,p.FECHA_NAC FROM cnsr_paciente p
         WHERE p.nro_doc LIKE CONCAT(_vtexto,'%') AND  p.ESTADO <> '3';
       END IF;
       IF (_vopcion = '2') THEN
         SELECT p.COD_PACIENTE,p.NRO_DOC,CONCAT(p.APELLIDO_PATERNO,' ',p.APELLIDO_MATERNO,' ',p.NOMBRES) AS PACIENTE,p.FECHA_NAC FROM cnsr_paciente p
         WHERE CONCAT(p.APELLIDO_PATERNO,' ',p.APELLIDO_MATERNO,' ',p.NOMBRES) LIKE CONCAT(_vtexto,'%') AND  p.ESTADO <> '3';
       END IF;
       IF (_vopcion = '3') THEN	-- opcion de busqueda mantenimiento de pacientes
	 SELECT p.COD_PACIENTE,p.TIPO_DOC,p.NRO_DOC,p.APELLIDO_PATERNO, p.APELLIDO_MATERNO,p.NOMBRES,p.FECHA_NAC,p.SEXO,p.FECHA_REG,p.USUARIO_REG,p.ORICENASICOD,
		p.CENASICOD,p.FECHA_BAJA,p.ESTADO,p.FECHA_MOD,p.USUARIO_MOD FROM cnsr_paciente p
         WHERE p.cod_paciente = _vtexto AND  p.ESTADO <> '3';
       END IF;
        IF (_vopcion = '4') THEN	-- opcion de busqueda filtro de pacientes en evaluacion
	 SELECT CONCAT(p.COD_PACIENTE,' | ',p.`NRO_DOC`, ' | ' , CONCAT(p.APELLIDO_PATERNO,' ',p.APELLIDO_MATERNO,' ',p.NOMBRES)) AS PACIENTE 
	 FROM cnsr_paciente p
	 WHERE CONCAT(p.APELLIDO_PATERNO,' ',p.APELLIDO_MATERNO,' ',p.NOMBRES) LIKE CONCAT('%',_vtexto,'%') AND  p.ESTADO <> '3';
       END IF;
    END$$

CREATE DEFINER=`admin`@`10.0.52.65` PROCEDURE `sp_profesional_list` (`_vopcion` CHAR(1), `_vtexto` VARCHAR(100))  BEGIN
       IF (_vopcion = '1') THEN
         SELECT p.COD_PROFESIONAL,p.NUM_DOC,CONCAT(p.APELLIDO_PAT,' ',p.APELLIDO_MAT,' ',p.NOMBRES) AS PROFESIONAL,p.CEEP, p.REE FROM cnsr_profesional p
         WHERE p.NUM_DOC LIKE CONCAT(_vtexto,'%') AND  p.ESTADO <> '3';
       END IF;
       IF (_vopcion = '2') THEN
         SELECT p.COD_PROFESIONAL,p.NUM_DOC,CONCAT(p.APELLIDO_PAT,' ',p.APELLIDO_MAT,' ',p.NOMBRES) AS PROFESIONAL,p.CEEP, p.REE FROM cnsr_profesional p
         WHERE CONCAT(p.APELLIDO_PAT,' ',p.APELLIDO_MAT,' ',p.NOMBRES) LIKE CONCAT(_vtexto,'%') AND  p.ESTADO <> '3';
       END IF;
       IF (_vopcion = '3') THEN	-- opcion de busqueda mantenimiento de pacientes
	 SELECT COD_PROFESIONAL,TIP_DOC,NUM_DOC, p.APELLIDO_PAT,p.APELLIDO_MAT,p.NOMBRES,COD_PLANILLA,CEEP,REE,GRUPO
		FROM cnsr_profesional p
         WHERE p.COD_PROFESIONAL = _vtexto AND  p.ESTADO <> '3';
       END IF;
       IF (_vopcion = '4') THEN	-- opcion de busqueda para autocompletar
	 SELECT COD_PROFESIONAL,CONCAT(p.APELLIDO_PAT,' ',p.APELLIDO_MAT,' ',p.NOMBRES) as PROFESIONAL,NUM_DOC,CEEP,REE FROM cnsr_profesional p
         WHERE CONCAT(p.APELLIDO_PAT,' ',p.APELLIDO_MAT,' ',p.NOMBRES) LIKE CONCAT('%',_vtexto,'%') AND  p.ESTADO <> '3';
        END IF;
        IF (_vopcion = '5') THEN	-- otra opcion de busqueda para autocompletar
	 SELECT CONCAT(COD_PROFESIONAL,' | ',`NUM_DOC`,' | ',CONCAT(p.APELLIDO_PAT,' ',p.APELLIDO_MAT,' ',p.NOMBRES)) AS PROFESIONAL FROM cnsr_profesional p
         WHERE CONCAT(p.APELLIDO_PAT,' ',p.APELLIDO_MAT,' ',p.NOMBRES) LIKE CONCAT('%',_vtexto,'%') AND  p.ESTADO <> '3';
        END IF;
    END$$

CREATE DEFINER=`admin`@`10.0.52.65` PROCEDURE `sp_registrodet_list` (`_vOpcion` CHAR(1), `_vNroReg` INT(11))  BEGIN
    if (_vOpcion ='1') then
	SELECT r.NroReg, r.codMarcador, m.desMarcador, m.desUnd, DATE_FORMAT(r.FecRegistro,'%d/%m/%Y') as fecRegistro
	FROM registrodet r inner join marcador m on m.codMarcador = r.codMarcador
	WHERE r.NroReg= _vNroReg order by r.codMarcador;
    end if;
	
    END$$

CREATE DEFINER=`admin`@`10.0.52.65` PROCEDURE `sp_registro_list` (`_vOpcion` CHAR(1), `_vRegAnp` VARCHAR(10), `_vnomPac` VARCHAR(80), `_vfecIni` CHAR(10), `_vfecFin` CHAR(10))  BEGIN
      if (_vOpcion = '1') then
        SELECT r.nroReg, r.regAnp, r.nomPaciente, DATE_FORMAT(r.fecIngMuestra,'%d/%m/%Y') as fecIngMuestra ,
        DATE_FORMAT(r.fecSolIHQ,'%d/%m/%Y') AS fecSolIHQ, IF(r.fecSolIHQ='0000-00-00', '2000-01-01', r.fecSolIHQ) as fecSolIHQDate,
         r.nomPatSolicit, r.estReg, r.RegExComp, r.Comentario,
        (select count(1) from registrodet rd where rd.NroReg=r.NroReg) as totalDet
	FROM registro R
	WHERE r.fecIngMuestra BETWEEN STR_TO_DATE(_vfecIni, '%d/%m/%Y') AND STR_TO_DATE(_vfecFin, '%d/%m/%Y') 
	AND (r.RegANP like concat('%',_vRegAnp,'%')  OR _vRegAnp is null)
	and (r.nomPaciente LIKE CONCAT('%',_vnomPac,'%')  OR _vnomPac IS NULL);
      end if;
    END$$

CREATE DEFINER=`admin`@`10.0.52.65` PROCEDURE `sp_usuario_list` (`_vOpcion` CHAR(1), `_vTexto` VARCHAR(30))  BEGIN
   IF (_vOpcion = 'C') THEN
         SELECT idUsuario, NivelUsuario, Nombre, Clave, Estado, 
         case NivelUsuario when '01' then 'ADMIN.SISTEMA' when '02' then 'USUARIO NORMAL' else 'N/INDICA' end as desNivelUsuario ,
         DATE_FORMAT(FechaAlta,'%d/%m/%Y') as FechaAlta, 
         DATE_FORMAT(FechaBaja,'%d/%m/%Y') AS FechaBaja, ifnull(TmpSesion,5) as TmpSesion
         FROM usuario u WHERE u.idUsuario LIKE CONCAT(_vtexto,'%');
   END IF;
   IF (_vOpcion = 'N') THEN
         SELECT idUsuario, NivelUsuario, Nombre, Clave, Estado, 
         CASE NivelUsuario WHEN '01' THEN 'ADMIN.SISTEMA' WHEN '02' THEN 'USUARIO NORMAL' ELSE 'N/INDICA' END AS desNivelUsuario ,
         DATE_FORMAT(FechaAlta,'%d/%m/%Y') AS FechaAlta, 
         DATE_FORMAT(FechaBaja,'%d/%m/%Y') AS FechaBaja, ifnull(TmpSesion,5) AS TmpSesion
         FROM usuario u WHERE u.Nombre LIKE CONCAT(_vtexto,'%');
   END IF;
END$$

CREATE DEFINER=`admin`@`10.0.52.65` PROCEDURE `sp_valfisica_list` (`_vopcion` CHAR(1), `_vcod` INT)  BEGIN
       IF (_vopcion = '1') THEN
         SELECT p.PK_EVALUACION,p.TEST_ELEVACION,p.TEST_AUM_PULSO,p.TEST_VENAS_COLAT,p.FC_DISTANCIA,p.FC_CARACTERISTICA,e.ESTADO 
	FROM cnsr_valfisica_fistula p INNER JOIN `cnsr_evaluaciones_av` e ON p.`PK_EVALUACION`=e.`PK_EVALUACION`
        WHERE p.PK_EVALUACION =_vcod;
       END IF;
       IF (_vopcion = '2') THEN	-- opcion de busqueda mantenimiento de valoracion fisica fistula
	SELECT p.PK_EVALUACION,p.TEST_ELEVACION,p.TEST_AUM_PULSO,p.TEST_VENAS_COLAT,p.FC_DISTANCIA,p.FC_CARACTERISTICA 
	FROM cnsr_valfisica_fistula p INNER JOIN `cnsr_evaluaciones_av` e ON p.`PK_EVALUACION`=e.`PK_EVALUACION`
        WHERE p.PK_EVALUACION =_vcod AND e.`ESTADO`<> '3';
       END IF;
    END$$

--
-- Funciones
--
CREATE DEFINER=`admin`@`10.0.52.65` FUNCTION `f_complicaciones_mant` (`_vopcion` CHAR(1), `_vpkevaluacion` INT, `_vpkcomplicacion` INT, `_vdescripcion` VARCHAR(100)) RETURNS VARCHAR(100) CHARSET utf8 BEGIN
  DECLARE _maximo NUMERIC(8,0);
  DECLARE _mensaje VARCHAR(100);
  DECLARE _filas SMALLINT;
  SET _mensaje='';
  IF (_vopcion ='I') THEN
     SET _vpkevaluacion = REPLACE(_vpkevaluacion,' ','');
     SELECT COUNT(1) INTO _filas
     FROM cnsr_complicaciones_av c WHERE c.pk_evaluacion=_vpkevaluacion and c.pk_complicacion=_vpkcomplicacion;
     IF ISNULL(_filas) THEN 
        SET _filas =0;
     END IF;
     IF (_filas > 0) THEN
        SET _mensaje = CONCAT('Complicacion [',_vpkevaluacion,' ',_vpkcomplicacion, '] ya se encuentra registrada, verifique.');
     ELSE
	SELECT MAX(PK_COMPLICACION) INTO _maximo FROM cnsr_complicaciones_av where PK_EVALUACION=_vpkevaluacion; 
	IF ISNULL(_maximo) THEN SET _maximo =1;
	ELSE SET _maximo = _maximo + 1;
	END IF;      
     
	INSERT INTO cnsr_complicaciones_av (PK_EVALUACION,PK_COMPLICACION,DESCRIPCION,ESTADO)
	VALUES (_vpkevaluacion,_maximo,_vdescripcion,'1');
	SET _mensaje = 'Complicación creada correctamente.';
     END IF;     
  END IF;
 IF (_vopcion = 'U') THEN
	UPDATE cnsr_complicaciones_av c
	SET 
	  c.estado = '2'
	WHERE c.pk_evaluacion=_vpkevaluacion AND c.pk_complicacion=_vpkcomplicacion;
      SET _mensaje = 'Complicación modificada correctamente';
  END IF;
  IF (_vopcion = 'A') THEN
	UPDATE cnsr_complicaciones_av c
	SET 
	  c.estado = '3'
	WHERE c.pk_evaluacion=_vpkevaluacion AND c.pk_complicacion=_vpkcomplicacion;
      SET _mensaje = 'Complicación eliminada correctamente';
  END IF;
    
  RETURN (_mensaje);
END$$

CREATE DEFINER=`admin`@`10.0.52.65` FUNCTION `f_datosav_mant` (`_vopcion` CHAR(1), `_vcodpaciente` INT, `_vfechcreacion` CHAR(10), `_vcodtipoacceso` INT, `_vubicacion` INT, `_vcirujanocv` VARCHAR(150), `_vcodcencli` VARCHAR(3), `_vusuarioreg` VARCHAR(20)) RETURNS VARCHAR(100) CHARSET utf8 BEGIN
  DECLARE _mensaje VARCHAR(100);
  DECLARE _filas SMALLINT;
  SET _mensaje='';
  IF (_vopcion ='I') THEN
     SET _vcodpaciente = REPLACE(_vcodpaciente,' ','');
     SET _vfechcreacion = REPLACE(_vfechcreacion,' ','');
     SELECT COUNT(1) INTO _filas
     FROM cnsr_datos_av a WHERE a.cod_paciente=_vcodpaciente AND a.activo='1' AND a.fecha_creacion_av=STR_TO_DATE(_vfechcreacion,'%Y-%m-%d') AND a.oricenasicod='1' AND a.cenasicod='401';
     IF ISNULL(_filas) THEN 
        SET _filas =0;
     END IF;
     IF (_filas > 0) THEN
        SET _mensaje = CONCAT('Acceso [',_vcodpaciente,_vactivo,_vfechcreacion, '] ya se encuentra registrado, verifique.');
     ELSE
	UPDATE cnsr_datos_av a
	SET a.activo='0'
	WHERE a.cod_paciente = _vcodpaciente AND a.activo='1' AND a.oricenasicod='1' AND a.cenasicod='401';
	
	INSERT INTO cnsr_datos_av (COD_PACIENTE, FECHA_CREACION_AV,COD_TIPO_ACCESO, UBICACION, CIRUJANO_CV, COD_CENCLI, ACTIVO, FECHA_REG, USUARIO_REG,ORICENASICOD,CENASICOD,FECHA_BAJA, ESTADO,FECHA_MOD,USUARIO_MOD)
	VALUES (_vcodpaciente,STR_TO_DATE(_vfechcreacion,'%Y-%m-%d'),_vcodtipoacceso,_vubicacion,UPPER(_vcirujanocv),_vcodcencli,'1',CURRENT_TIMESTAMP(),_vusuarioreg,'1','401',NULL,'1',NULL,NULL);
	SET _mensaje = 'Acceso creado correctamente.';
     END IF;     
  END IF;
 IF (_vopcion = 'U') THEN
	UPDATE cnsr_datos_av a
	SET 
	  a.cod_tipo_acceso  = _vcodtipoacceso,
	  a.ubicacion   = _vubicacion,
	  a.cirujano_cv  = UPPER(_vcirujanocv),
	  a.cod_cencli  = _vcodcencli,
	  a.estado = '2',
	  a.fecha_mod = CURRENT_TIMESTAMP(),
	  a.usuario_mod = _vusuarioreg
	WHERE a.cod_paciente = _vcodpaciente AND a.fecha_creacion_av=STR_TO_DATE(_vfechcreacion,'%Y-%m-%d') AND a.activo='1' AND a.oricenasicod='1' AND a.cenasicod='401';
      SET _mensaje = 'Acceso modificado correctamente';
  END IF;
  IF (_vopcion = 'A') THEN
	UPDATE cnsr_datos_av a
	   SET 
		a.fecha_baja = CURRENT_TIMESTAMP(),
		a.estado = '3',
		a.activo='0',
		a.usuario_mod = _vusuarioreg
	   WHERE a.cod_paciente = _vcodpaciente AND a.fecha_creacion_av=STR_TO_DATE(_vfechcreacion,'%Y-%m-%d') AND a.oricenasicod='1' AND a.cenasicod='401';
	   SET _mensaje = 'Acceso anulado correctamente';  
  END IF;
  
  RETURN (_mensaje);
END$$

CREATE DEFINER=`admin`@`10.0.52.65` FUNCTION `f_evaluaciones_mant` (`_vopcion` CHAR(1), `_vpkevaluacion` INT, `_vfechaeval` VARCHAR(10), `_vcodpaciente` INT, `_vcodprofesional` INT, `_vfechacreacionav` VARCHAR(10), `_vsala` VARCHAR(20), `_vturno` VARCHAR(20), `_vfrecuenciadial` VARCHAR(20), `_vcondicionpac` VARCHAR(20), `_vpainisistolica` VARCHAR(20), `_vpainidiastolica` VARCHAR(20), `_vpafinalsistolica` VARCHAR(20), `_vpafinaldiastolica` VARCHAR(20), `_vqb` VARCHAR(20), `_vrapae` VARCHAR(20), `_vrvpve` VARCHAR(20), `_vusuarioreg` VARCHAR(20)) RETURNS VARCHAR(100) CHARSET utf8 BEGIN
  DECLARE _maximo NUMERIC(8,0);
  DECLARE _mensaje VARCHAR(100);
  DECLARE _filas SMALLINT;
  SET _mensaje='';
  IF (_vopcion ='I') THEN
     SET _vpkevaluacion = REPLACE(_vpkevaluacion,' ','');
     SELECT COUNT(1) INTO _filas
     FROM cnsr_evaluaciones_av e WHERE e.pk_evaluacion=_vpkevaluacion AND e.oricenasicod='1' AND e.cenasicod='401';
     IF ISNULL(_filas) THEN 
        SET _filas =0;
     END IF;
     IF (_filas > 0) THEN
        SET _mensaje = CONCAT('Evaluacion [',_vpkevaluacion, '] ya se encuentra registrada, verifique.');
     ELSE
     
	SELECT MAX(PK_EVALUACION) INTO _maximo FROM cnsr_evaluaciones_av; 
	IF ISNULL(_maximo) THEN SET _maximo =1;
	ELSE SET _maximo = _maximo + 1;
	END IF;
	   
	INSERT INTO cnsr_evaluaciones_av (PK_EVALUACION, FECHA_EVAL, COD_PACIENTE, COD_PROFESIONAL, FECHA_CREACION_AV,SALA,TURNO,FRECUENCIA_DIAL,CONDICION_PAC,PA_INI_SISTOLICA,PA_INI_DIASTOLICA,PA_FINAL_SISTOLICA,PA_FINAL_DIASTOLICA,QB,RA_PAE,RV_PVE,FECHA_REG,USUARIO_REG,ORICENASICOD,CENASICOD,FECHA_BAJA,ESTADO,FECHA_MOD,USUARIO_MOD)
	VALUES (_maximo,STR_TO_DATE(_vfechaeval,'%Y-%m-%d'),_vcodpaciente,_vcodprofesional,STR_TO_DATE(_vfechacreacionav,'%Y-%m-%d'),_vsala,_vturno,_vfrecuenciadial,_vcondicionpac,_vpainisistolica,_vpainidiastolica,_vpafinalsistolica,_vpafinaldiastolica,_vqb,_vrapae,_vrvpve,CURRENT_TIMESTAMP(),_vusuarioreg,'1','401',NULL,'1',NULL,NULL);
	SET _mensaje = 'Evaluacion creado correctamente.';
     END IF;     
  END IF;
 IF (_vopcion = 'U') THEN
	UPDATE cnsr_evaluaciones_av e
	SET 
	  e.fecha_eval = STR_TO_DATE(_vfechaeval,'%Y-%m-%d'),
	  e.cod_paciente = _vcodpaciente,
	  e.cod_profesional  = _vcodprofesional,
	  e.fecha_creacion_av  = STR_TO_DATE(_vfechacreacionav,'%Y-%m-%d'),
	  e.sala  = _vsala,
	  e.turno  = _vturno,
	  e.frecuencia_dial  = _vfrecuenciadial,
	  e.condicion_pac  = _vcondicionpac,
	  e.pa_ini_sistolica  = _vpainisistolica,
	  e.pa_ini_diastolica  = _vpainidiastolica,
	  e.pa_final_sistolica  = _vpafinalsistolica,
	  e.pa_final_diastolica  = _vpafinaldiastolica,
	  e.qb  = _vqb,
	  e.ra_pae  = _vrapae,
	  e.rv_pve  = _vrvpve,
	  e.estado = '2',
	  e.fecha_mod = CURRENT_TIMESTAMP(),
	  e.usuario_mod = _vusuarioreg
	WHERE e.pk_evaluacion= _vpkevaluacion AND e.oricenasicod='1' AND e.cenasicod='401';
      SET _mensaje = 'Evaluacion modificado correctamente';
  END IF;
  
  IF (_vopcion = 'A') THEN
	UPDATE cnsr_evaluaciones_av e
	   SET 
		e.fecha_baja = CURRENT_TIMESTAMP(),
		e.estado = '3',
		e.usuario_mod = _vusuarioreg
	   WHERE e.pk_evaluacion= _vpkevaluacion AND e.oricenasicod='1' AND e.cenasicod='401';
	   SET _mensaje = 'Evaluacion anulada correctamente';  
  END IF;
  
  RETURN (_mensaje);
END$$

CREATE DEFINER=`admin`@`10.0.52.65` FUNCTION `f_horaactual` () RETURNS CHAR(8) CHARSET utf8 BEGIN
	Return (SELECT CURRENT_TIME() );
    END$$

CREATE DEFINER=`admin`@`10.0.52.65` FUNCTION `f_marcador_mant` (`_vopcion` CHAR(1), `_vcodmarcador` CHAR(9), `_vdesmarcador` VARCHAR(80), `_vdesund` VARCHAR(5), `_vindstock` CHAR(1), `_vestreg` CHAR(1)) RETURNS VARCHAR(100) CHARSET utf8 BEGIN
	DECLARE _mensaje VARCHAR(100);
	SET _mensaje='';
	
	IF (_vopcion = 'I') THEN
	  INSERT INTO marcador(codMarcador, desMarcador, desUnd, estReg, indStock)
	  VALUES (_vcodmarcador, _vdesmarcador, _vdesund, '1','R');
	  SET _mensaje='Se agregó Marcador correctamente';
	END IF;
	IF (_vopcion = 'U') THEN
	  UPDATE marcador 
	  SET
	    desMarcador = _vdesmarcador,
	    desUnd      = _vdesund,
	    estReg      = _vestreg,
	    indStock    = _vindstock
	  WHERE codMarcador = _vcodmarcador;
	  SET _mensaje='Se modificó Marcador correctamente';
	END IF;
	IF (_vopcion = 'C') THEN  /*Actualizar color*/
	  UPDATE marcador 
	  SET
	    indStock    = _vindstock
	  WHERE codMarcador = _vcodmarcador;
	  SET _mensaje='Se asignó indicador correctamente';
	END IF;
	
	RETURN (_mensaje);
    END$$

CREATE DEFINER=`admin`@`10.0.52.65` FUNCTION `f_paciente_mant` (`_vopcion` CHAR(1), `_vcodpaciente` INT, `_vtipodoc` CHAR(1), `_vnrodoc` VARCHAR(20), `_vappaterno` VARCHAR(50), `_vapmaterno` VARCHAR(50), `_vnombres` VARCHAR(150), `_vfechanac` VARCHAR(10), `_vsexo` CHAR(1), `_vusuarioreg` VARCHAR(20)) RETURNS VARCHAR(100) CHARSET utf8 BEGIN
  DECLARE _maximo NUMERIC(8,0);
  DECLARE _mensaje VARCHAR(100);
  DECLARE _filas SMALLINT;
  SET _mensaje='';
  IF (_vopcion ='I') THEN
     SET _vcodpaciente = REPLACE(_vcodpaciente,' ','');
     SELECT COUNT(1) INTO _filas
     FROM cnsr_paciente p WHERE p.cod_paciente=_vcodpaciente AND p.oricenasicod='1' AND p.cenasicod='401';
     IF ISNULL(_filas) THEN 
        SET _filas =0;
     END IF;
     IF (_filas > 0) THEN
        SET _mensaje = CONCAT('Paciente [',_vcodpaciente, '] ya se encuentra registrado, verifique.');
     ELSE     
	 SELECT MAX(COD_PACIENTE) INTO _maximo FROM cnsr_paciente; 
	 IF ISNULL(_maximo) THEN SET _maximo =1;
	 ELSE SET _maximo = _maximo + 1;
	 END IF;
     
	INSERT INTO cnsr_paciente (COD_PACIENTE, TIPO_DOC, NRO_DOC, APELLIDO_PATERNO, APELLIDO_MATERNO, NOMBRES, FECHA_NAC, SEXO, FECHA_REG, USUARIO_REG,ORICENASICOD,CENASICOD,FECHA_BAJA, ESTADO,FECHA_MOD,USUARIO_MOD)
	VALUES (_maximo,_vtipodoc,_vnrodoc,UPPER(_vappaterno),UPPER(_vapmaterno),UPPER(_vnombres),STR_TO_DATE(_vfechanac,'%Y-%m-%d'),_vsexo,CURRENT_TIMESTAMP(),_vusuarioreg,'1','401',NULL,'1',NULL,NULL);
	SET _mensaje = 'Paciente creado correctamente.';
	END IF;     
END IF;
 IF (_vopcion = 'U') THEN
	UPDATE cnsr_paciente p
	SET 
	  p.tipo_doc  = _vtipodoc,
	  p.nro_doc   = _vnrodoc,
	  p.apellido_paterno  = UPPER(_vappaterno),
	  p.apellido_materno  = UPPER(_vapmaterno),
	  p.nombres  = UPPER(_vnombres),
	  p.fecha_nac  = STR_TO_DATE(_vfechanac,'%Y-%m-%d'),
	  p.sexo  = _vsexo,
	  p.estado = '2',
	  p.fecha_mod = CURRENT_TIMESTAMP(),
	  p.usuario_mod = _vusuarioreg
	WHERE p.cod_paciente = _vcodpaciente AND p.oricenasicod='1' AND p.cenasicod='401';
      SET _mensaje = 'Paciente modificado correctamente';
  END IF;
  
  IF (_vopcion = 'A') THEN
	UPDATE cnsr_paciente p
	   SET 
		p.fecha_baja = CURRENT_TIMESTAMP(),
		p.estado = '3',
		p.usuario_mod = _vusuarioreg
	   WHERE p.cod_paciente = _vcodpaciente AND p.oricenasicod='1' AND p.cenasicod='401';
	   SET _mensaje = 'Paciente anulado correctamente';  
  END IF;
  
  RETURN (_mensaje);
END$$

CREATE DEFINER=`admin`@`10.0.52.65` FUNCTION `f_profesional_mant` (`_vopcion` CHAR(1), `_vcodprofesional` INT, `_vtipdoc` INT, `_vnumdoc` VARCHAR(20), `_vapellidopat` VARCHAR(100), `_vapellidomat` VARCHAR(100), `_vnombres` VARCHAR(150), `_vcodplanilla` VARCHAR(20), `_vceep` VARCHAR(20), `_vree` VARCHAR(20), `_vgrupo` INT, `_vusuarioreg` VARCHAR(20)) RETURNS VARCHAR(100) CHARSET utf8 BEGIN
  DECLARE _maximo NUMERIC(8,0);
  DECLARE _mensaje VARCHAR(100);
  DECLARE _filas SMALLINT;
  SET _mensaje='';
  IF (_vopcion ='I') THEN
     SET _vcodprofesional = REPLACE(_vcodprofesional,' ','');
     SELECT COUNT(1) INTO _filas
     FROM cnsr_profesional p WHERE p.cod_profesional=_vcodprofesional AND p.oricenasicod='1' AND p.cenasicod='401';
     IF ISNULL(_filas) THEN 
        SET _filas =0;
     END IF;
     IF (_filas > 0) THEN
        SET _mensaje = CONCAT('Profesional [',_vcodprofesional, '] ya se encuentra registrado, verifique.');
     ELSE
     
	SELECT MAX(COD_PROFESIONAL) INTO _maximo FROM cnsr_profesional; 
	IF ISNULL(_maximo) THEN SET _maximo =1;
	ELSE SET _maximo = _maximo + 1;
	END IF; 
	   
	INSERT INTO cnsr_profesional (COD_PROFESIONAL, TIP_DOC,NUM_DOC, APELLIDO_PAT, APELLIDO_MAT, NOMBRES, COD_PLANILLA,CEEP,REE,GRUPO,FECHA_REG,USUARIO_REG,ORICENASICOD,CENASICOD,FECHA_BAJA, ESTADO,FECHA_MOD,USUARIO_MOD)
	VALUES (_maximo,_vtipdoc,_vnumdoc,UPPER(_vapellidopat),UPPER(_vapellidomat),UPPER(_vnombres),_vcodplanilla,_vceep,_vree,_vgrupo,CURRENT_TIMESTAMP(),_vusuarioreg,'1','401',NULL,'1',NULL,NULL);
	SET _mensaje = 'Profesional creado correctamente.';
     END IF;     
  END IF;
 IF (_vopcion = 'U') THEN
	UPDATE cnsr_profesional p
	SET 
	  p.tip_doc = _vtipdoc,
	  p.num_doc = _vnumdoc,
	  p.apellido_pat  = UPPER(_vapellidopat),
	  p.apellido_mat  = UPPER(_vapellidomat),
	  p.nombres  = UPPER(_vnombres),
	  p.cod_planilla  = _vcodplanilla,
	  p.ceep  = _vceep,
	  p.ree  = _vree,
	  p.grupo  = _vgrupo,
	  p.estado = '2',
	  p.fecha_mod = CURRENT_TIMESTAMP(),
	  p.usuario_mod = _vusuarioreg
	WHERE p.cod_profesional = _vcodprofesional AND p.oricenasicod='1' AND p.cenasicod='401';
      SET _mensaje = 'Profesional modificado correctamente';
  END IF;
  
  IF (_vopcion = 'A') THEN
	UPDATE cnsr_profesional p
	   SET 
		p.fecha_baja = CURRENT_TIMESTAMP(),
		p.estado = '3',
		p.usuario_mod = _vusuarioreg
	   WHERE p.cod_profesional = _vcodprofesional AND p.oricenasicod='1' AND p.cenasicod='401';
	   SET _mensaje = 'Profesional anulado correctamente';  
  END IF;
  
  RETURN (_mensaje);
END$$

CREATE DEFINER=`admin`@`10.0.52.65` FUNCTION `f_registrodet_mant` (`_vopcion` CHAR(1), `_vnroReg` NUMERIC(11,0), `_vcodMarcador` CHAR(9), `_vUsrReg` VARCHAR(10)) RETURNS VARCHAR(100) CHARSET utf8 BEGIN

    DECLARE _fila SMALLINT;

    DECLARE _mensaje VARCHAR(100);

    

    if (_vopcion = 'I') then

        set _fila = 0;

	select count(1) INTO _fila

	from registrodet 

	where NroReg=_vnroReg and codMarcador=_vcodMarcador;

	

	IF ISNULL(_fila) THEN 

	  SET _fila =0;

	end if;

	

	if (_fila = 0) then

	    INSERT INTO registrodet(NroReg, codMarcador, FecRegistro, UsrRegistro)

	    VALUES (_vnroReg, _vcodMarcador, CURRENT_TIMESTAMP(), _vUsrReg);

	    SET _mensaje = 'Marcador registrado';

	  else 

	    SET _mensaje = 'el Marcador ya fue registrado, verifique';

	end if;

    end if;

    

    IF (_vopcion = 'A') THEN

	delete from registrodet

	where NroReg = _vnroReg and codMarcador = _vcodMarcador;

	SET _mensaje = 'Marcador anulado';

    end if;

    

    rETURN (_mensaje);

    END$$

CREATE DEFINER=`admin`@`10.0.52.65` FUNCTION `f_registro_mant` (`_vopcion` CHAR(1), `_vnroReg` NUMERIC(11,0), `_vnroregANP` VARCHAR(10), `_vnomPaciente` VARCHAR(80), `_vfecingmuestra` CHAR(10), `_vfecsolihq` CHAR(10), `_vnroRegExaComp` VARCHAR(10), `_vnomPatSolicit` VARCHAR(80), `_usrRegistro` VARCHAR(10), `_vComentario` VARCHAR(200)) RETURNS VARCHAR(100) CHARSET utf8 BEGIN
	DECLARE _maximo NUMERIC(8,0);
	declare _fila smallint;
	DECLARE _mensaje varchar(100);
	
	set _mensaje='';
	
      IF (_vopcion = 'I') THEN
        
	select count(RegANP) into _fila
	from registro where RegANP = _vnroregANP;
	
	IF ISNULL(_fila) THEN 
	   SET _fila =0;
	end if;
	
	if (_fila > 0) then
	  SET _mensaje = concat('ya existe registro previo para Registro:', _vnroregANP);
	else
	
	
	   SELECT MAX(NroReg) INTO _maximo FROM registro; 
	   iF ISNULL(_maximo) THEN SET _maximo =1;
	   ELSE SET _maximo = _maximo + 1;
	   END IF;
		
	   INSERT INTO registro(NroReg, RegANP, NomPaciente, FecIngMuestra, FecSolIHQ, RegExComp, NomPatSolicit, 
	   estReg, comentario, FecRegistro, UsrRegistro)
	   VALUES ( _maximo, UPPER(_vnroregANP), UPPER(_vnomPaciente), STR_TO_DATE(_vfecingmuestra, '%d/%m/%Y'), 
	   STR_TO_DATE(_vfecsolihq, '%d/%m/%Y'),_vnroRegExaComp, 
		 UPPER(_vnomPatSolicit), '1', _vComentario , CURRENT_TIMESTAMP(), _usrRegistro );
	   SET _mensaje = 'Insercion conforme';
	END IF;
      END IF;
      
      IF (_vopcion = 'U') THEN
	UPDATE registro r
	SET
	r.RegANP        = UPPER(_vnroregANP),
	r.NomPaciente   = UPPER(_vnomPaciente), 
	r.FecIngMuestra = STR_TO_DATE(_vfecingmuestra, '%d/%m/%Y'), 
	r.FecSolIHQ     = STR_TO_DATE(_vfecsolihq, '%d/%m/%Y'),
	r.RegExComp	= upper(_vnroRegExaComp),
	r.NomPatSolicit = UPPER(_vnomPatSolicit),
	r.estReg	= '1',
	r.comentario	= _vComentario,
	r.usrModifica	= _usrRegistro,
	r.fecModifica	= CURRENT_TIMESTAMP()
	WHERE r.nroReg= _vnroReg;
	SET _mensaje = 'Modificacion conforme';
      END IF;
      
      IF (_vopcion = 'A') THEN
        UPDATE registro r
	SET
	r.estReg        = '0',
	r.usrModifica	= _usrRegistro,
	r.fecModifica	= CURRENT_TIMESTAMP()
	WHERE r.nroReg= _vnroReg;
	set _mensaje = 'Anulacion conforme';
      END IF;
      
      if (_vopcion = 'M') THEN  /*Entrega de muestra*/
        SELECT COUNT(codMarcador) INTO _fila
        FROM registrodet rd WHERE rd.NroReg = _vnroReg;
        IF ISNULL(_maximo) THEN 
           SET _fila =0;
        END IF;
        IF (_fila > 0) THEN
	    UPDATE registro r
	    SET
		r.RegExComp	= UPPER(_vnroRegExaComp),
		r.comentario	= UPPER(_vComentario),
		r.usrModifica	= _usrRegistro,
		r.FecSolIHQ     = STR_TO_DATE(_vfecsolihq, '%d/%m/%Y'),
		r.fecModifica	= CURRENT_TIMESTAMP()
	     WHERE r.nroReg= _vnroReg;
	     SET _mensaje = 'Reg.Entrega Muestra conforme';
	  ELSE
	    SET _mensaje= 'El registro aun no tiene marcadores registrados, no se puede entregar';
        END IF;
      end if;
      
      return (_mensaje);
    END$$

CREATE DEFINER=`admin`@`10.0.52.65` FUNCTION `f_usuario_mant` (`_vopcion` CHAR(1), `_vidusuario` VARCHAR(20), `_vnivel` CHAR(2), `_vnombre` VARCHAR(60), `_vclave` VARCHAR(40), `_vestado` CHAR(3), `_vtiempo` TINYINT, `_vusuario` VARCHAR(10)) RETURNS VARCHAR(100) CHARSET utf8 BEGIN
  DECLARE _mensaje VARCHAR(100);
  declare _filas smallint;
  sET _mensaje='';
  if (_vopcion ='I') then
     set _vidusuario = replace(_vidusuario,' ','');
     select count(1) into _filas
     from usuario u where lower(u.idUsuario) = lower(_vidusuario);
     if isnull(_filas) then 
        set _filas =0;
     end if;
	
     if (_filas > 0) then
        set _mensaje = concat('Login de usuario [', _vidusuario, '] ya se encuentra registrado, verifique.');
     else
	INSERT INTO usuario (idUsuario, NivelUsuario, Nombre, Clave, Estado, TmpSesion, FechaAlta, FechaBaja, FechaRegistro, UsrRegistro,Oricenasicod,Cenasicod)
	VALUES ( LOWER(_vidusuario) , _vnivel, UPPER(_vnombre), MD5(_vclave), 'ACT',_vtiempo , CURRENT_TIMESTAMP(), NULL, CURRENT_TIMESTAMP(), _vusuario,'1','401');
	SET _mensaje = 'Usuario creado correctamente.';
     end if;     
  end if;
  
  if (_vopcion = 'U') then
	update usuario u
	set 
	  u.NivelUsuario  = _vnivel,
	  u.nombre        = upper(_vnombre),
	  u.TmpSesion     = _vtiempo,
	  u.Estado        = _vestado
	where u.idUsuario = _vidusuario;
      set _mensaje = 'Usuario modificado correctamente';
  end if;
  
  IF (_vopcion = 'A') THEN
	IF (_vidusuario = 'admin') then
	   SET _mensaje = 'El usuario ADMIN no puede anularse'; 
	else
	   UPDATE usuario u
	   SET 
		u.FechaBaja = CURRENT_TIMESTAMP(),
		u.UsrModifica = _vusuario,
		u.FechaModifica = CURRENT_TIMESTAMP() ,
		u.Estado = 'INA'
	   WHERE u.idUsuario = _vidusuario;
	   SET _mensaje = 'Usuario anulado correctamente';  
	end if;
  end if;
  
  IF (_vopcion = 'C') THEN
  UPDATE usuario u
	SET 
	  u.Clave = md5(_vclave),
	  u.UsrModifica = _vusuario,
	  u.FechaModifica = CURRENT_TIMESTAMP(),
	  u.Estado = 'ACT'
	WHERE u.idUsuario = _vidusuario;
      SET _mensaje = 'Password actualizado correctamente';  
  end if;
  
  return (_mensaje);
END$$

CREATE DEFINER=`admin`@`10.0.52.65` FUNCTION `f_valfisicafistula_mant` (`_vopcion` CHAR(1), `_vpkevaluacion` INT, `_vtestelevacion` VARCHAR(20), `_vtestaumpulso` VARCHAR(20), `_vtestvenascolat` VARCHAR(20), `_vfcdistancia` VARCHAR(20), `_vfccaracteristica` VARCHAR(20)) RETURNS VARCHAR(100) CHARSET utf8 BEGIN
  DECLARE _mensaje VARCHAR(100);
  DECLARE _filas SMALLINT;
  SET _mensaje='';
  IF (_vopcion ='I') THEN
     SET _vpkevaluacion = REPLACE(_vpkevaluacion,' ','');
     SELECT COUNT(1) INTO _filas
     FROM cnsr_valfisica_fistula v WHERE v.pk_evaluacion=_vpkevaluacion;
     IF ISNULL(_filas) THEN 
        SET _filas =0;
     END IF;
     IF (_filas > 0) THEN
        SET _mensaje = CONCAT('Valoracion fisica fistula [',_vpkevaluacion, '] ya se encuentra registrado, verifique.');
     ELSE
	INSERT INTO cnsr_valfisica_fistula (PK_EVALUACION,TEST_ELEVACION,TEST_AUM_PULSO,TEST_VENAS_COLAT,FC_DISTANCIA,FC_CARACTERISTICA)
	VALUES (_vpkevaluacion,_vtestelevacion,_vtestaumpulso,_vtestvenascolat,_vfcdistancia,_vfccaracteristica);
	SET _mensaje = 'Valoracion fisica fistula creada correctamente.';
     END IF;     
  END IF;
 IF (_vopcion = 'U') THEN
	UPDATE cnsr_valfisica_fistula v
	SET 
	  v.test_elevacion = _vtestelevacion,
	  v.test_aum_pulso = _vtestaumpulso,
	  v.test_venas_colat = _vtestvenascolat,
	  v.fc_distancia = _vfcdistancia,
	  v.fc_caracteristica = _vfccaracteristica
	WHERE v.pk_evaluacion= _vpkevaluacion; 
      SET _mensaje = 'Valoracion fisica fistula modificada correctamente';
  END IF;
    
  RETURN (_mensaje);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cmcas10`
--

CREATE TABLE `cmcas10` (
  `ORICENASICOD` char(1) NOT NULL COMMENT 'Origen de codigo',
  `CENASICOD` varchar(3) NOT NULL COMMENT 'Codigo de centro asistencial',
  `CENASIDES` varchar(40) DEFAULT NULL COMMENT 'Descripcion de centro asistencial',
  `CENASIDESCOR` varchar(20) DEFAULT NULL COMMENT 'Descripcion corta de centro asistencial',
  `ACTIVO` char(1) DEFAULT NULL,
  `FECHA_REG` datetime DEFAULT NULL,
  `USUARIO_REG` varchar(20) DEFAULT NULL,
  `FECHA_MOD` datetime DEFAULT NULL,
  `USUARIO_MOD` varchar(20) DEFAULT NULL,
  `FECHA_BAJA` datetime DEFAULT NULL,
  `USUARIO_BAJA` varchar(20) DEFAULT NULL,
  `ESTADO` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cmcas10`
--

INSERT INTO `cmcas10` (`ORICENASICOD`, `CENASICOD`, `CENASIDES`, `CENASIDESCOR`, `ACTIVO`, `FECHA_REG`, `USUARIO_REG`, `FECHA_MOD`, `USUARIO_MOD`, `FECHA_BAJA`, `USUARIO_BAJA`, `ESTADO`) VALUES
('1', '001', 'H.N. EDGARDO REBAGLIATI MARTINS', 'H.N. E. REBAGLIATI', '1', NULL, NULL, NULL, NULL, NULL, NULL, '1'),
('1', '002', 'H.N. GUILLERMO ALMENARA IRIGOYEN', 'H.N. G. ALMENARA', '1', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, '1'),
('1', '005', 'H.IV ALBERTO SABOGAL SOLOGUREN', 'H.IV A. SABOGAL', '1', NULL, NULL, '2018-08-27 11:15:30', NULL, '2018-08-20 00:00:00', NULL, '1'),
('1', '401', 'CENTRO NACIONAL DE SALUD RENAL', 'C.N. SALUD RENAL', '1', NULL, NULL, '2018-08-31 18:00:39', NULL, NULL, NULL, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cnsr_complicaciones_av`
--

CREATE TABLE `cnsr_complicaciones_av` (
  `PK_EVALUACION` int(10) NOT NULL COMMENT 'Id de Evaluacion',
  `PK_COMPLICACION` int(10) NOT NULL COMMENT 'Id de Complicacion que figura en maestra',
  `DESCRIPCION` varchar(100) DEFAULT NULL COMMENT 'Descripcion de complicacion',
  `ESTADO` char(1) DEFAULT NULL COMMENT 'Estado del registro'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cnsr_complicaciones_av`
--

INSERT INTO `cnsr_complicaciones_av` (`PK_EVALUACION`, `PK_COMPLICACION`, `DESCRIPCION`, `ESTADO`) VALUES
(1, 1, 'DESCRIPCION1', '1'),
(1, 2, 'DESCRIPCION2', '1'),
(1, 3, 'DESCRIPCION3', '1'),
(1, 4, 'DESCRIPCION4', '1'),
(1, 5, 'PRUEBAS', '2'),
(2, 1, 'DESCRIPCION5', '1'),
(2, 2, 'DESCRIPCION6', '1'),
(2, 3, 'DESCRIPCION7', '1'),
(2, 4, 'DESCRIPCION1', '1'),
(3, 1, 'DESCRIPCION2', '1'),
(3, 2, 'DESCRIPCION3', '1'),
(3, 3, 'DESCRIPCION7', '1'),
(3, 4, 'DESCRIPCION8', '1'),
(3, 5, 'borrar', '3'),
(1, 1, 'DESCRIPCION1', '1'),
(1, 2, 'DESCRIPCION2', '1'),
(1, 3, 'DESCRIPCION3', '1'),
(1, 4, 'DESCRIPCION4', '1'),
(1, 5, 'PRUEBAS', '2'),
(2, 1, 'DESCRIPCION5', '1'),
(2, 2, 'DESCRIPCION6', '1'),
(2, 3, 'DESCRIPCION7', '1'),
(2, 4, 'DESCRIPCION1', '1'),
(3, 1, 'DESCRIPCION2', '1'),
(3, 2, 'DESCRIPCION3', '1'),
(3, 3, 'DESCRIPCION7', '1'),
(3, 4, 'DESCRIPCION8', '1'),
(3, 5, 'borrar', '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cnsr_datos_av`
--

CREATE TABLE `cnsr_datos_av` (
  `PK_ACCESO_V` int(10) NOT NULL,
  `COD_PACIENTE` int(10) NOT NULL COMMENT 'Cod. Paciente',
  `COD_TIPO_ACCESO` varchar(20) DEFAULT NULL COMMENT 'Codigo de tipo de acceso',
  `COD_CENCLI` int(3) DEFAULT NULL COMMENT 'Codigo de clinica donde se realizo el acceso',
  `FECHA_CREACION_AV` date DEFAULT NULL,
  `UBICACION` varchar(20) DEFAULT NULL COMMENT 'Codigo de Ubicacion del acceso',
  `CIRUJANO_CV` varchar(150) DEFAULT NULL COMMENT 'Cirujano que realizo el acceso',
  `ACTIVO` int(1) DEFAULT NULL COMMENT 'Indicador de activo',
  `FECHA_REG` datetime DEFAULT NULL COMMENT 'Fecha de registro',
  `USUARIO_REG` varchar(20) DEFAULT NULL COMMENT 'Usuario que registra',
  `FECHA_MOD` datetime DEFAULT NULL COMMENT 'Fecha de modificacion del registro',
  `USUARIO_MOD` varchar(20) DEFAULT NULL COMMENT 'Usuario que modifica',
  `FECHA_BAJA` datetime DEFAULT NULL COMMENT 'Fecha de baja registro',
  `USUARIO_BAJA` varchar(20) DEFAULT NULL,
  `ESTADO` char(1) DEFAULT NULL COMMENT 'Estado del registro',
  `CENASICOD` varchar(3) DEFAULT NULL COMMENT 'Centro asistencial',
  `ORICENASICOD` char(1) DEFAULT NULL COMMENT 'Origen de cas'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cnsr_datos_av`
--

INSERT INTO `cnsr_datos_av` (`PK_ACCESO_V`, `COD_PACIENTE`, `COD_TIPO_ACCESO`, `COD_CENCLI`, `FECHA_CREACION_AV`, `UBICACION`, `CIRUJANO_CV`, `ACTIVO`, `FECHA_REG`, `USUARIO_REG`, `FECHA_MOD`, `USUARIO_MOD`, `FECHA_BAJA`, `USUARIO_BAJA`, `ESTADO`, `CENASICOD`, `ORICENASICOD`) VALUES
(1, 823, 'INJERTO', 802, '2018-08-31', 'FRCD', 'JUAN PEREZ', 1, '2018-04-25 00:00:00', 'CJUNCO', '2018-09-21 09:35:39', 'ESSALUD', NULL, NULL, '1', '401', '1'),
(19, 4, 'CVCT', 838, '2018-09-01', 'FBI', 'MARTINEZ LUNA, JUAN', 0, '2018-09-03 00:16:43', 'ESSALUD', '2018-09-13 15:57:46', 'ESSALUD', NULL, NULL, '1', '401', '1'),
(20, 823, 'INJERTO', 846, '2018-09-01', 'IRI', 'THALIA PEREZ ', 1, '2018-09-03 01:32:51', 'ESSALUD', '2018-09-19 10:24:03', 'ESSALUD', NULL, NULL, '1', '401', '1'),
(21, 2065, 'CVCP', 837, '2018-09-02', 'IFD', 'NATALY PORRAS', 1, '2018-09-03 15:15:46', 'PEREZ', '2018-09-06 18:09:08', 'ESSALUD', NULL, NULL, '1', '401', '1'),
(22, 2065, 'CPERITONEAL', 844, '2018-08-26', 'FBI', 'MARTINEZ LUNA, JUAN', 1, '2018-09-03 15:16:08', 'PEREZ', '2018-09-06 18:09:29', 'ESSALUD', NULL, NULL, '1', '401', '1'),
(23, 823, 'INJERTO', 842, '2018-09-13', 'FFD', 'NATALY PORRAS', 1, '2018-09-03 15:30:43', 'ADMIN', '2018-09-06 18:09:21', 'ESSALUD', NULL, NULL, '1', '401', '1'),
(24, 2065, 'FAV', 844, '2018-09-06', 'FBI', 'NATALY PORRAS', 1, '2018-09-06 17:53:46', 'ESSALUD', '2018-09-06 18:09:15', 'ESSALUD', NULL, NULL, '1', '401', '1'),
(25, 2116, 'AUTOINJERTO', 838, '2018-09-06', 'CPF', 'THALIA PEREZ FFFFFFFFFF', 1, '2018-09-06 17:55:05', 'ESSALUD', '2018-09-20 10:13:10', 'ESSALUD', NULL, NULL, '1', '401', '1'),
(26, 2065, 'FAV', 844, '2018-09-14', 'FBD', 'MARTINEZ LUNA, JUAN', 1, '2018-09-13 15:57:33', 'ESSALUD', NULL, NULL, NULL, NULL, '1', '401', '1'),
(27, 823, 'AUTOINJERTO', 838, '2018-09-20', 'CPS', 'TTT', 1, '2018-09-19 15:01:55', 'ESSALUD', NULL, NULL, '2018-09-20 09:59:56', 'ESSALUD', '0', '401', '1'),
(28, 1116, 'FAV', 846, '2018-09-20', 'CPF', 'MARTINEZ LUNA, JUAN', 1, '2018-09-20 10:01:09', 'ESSALUD', '2018-09-20 10:04:35', 'ESSALUD', NULL, NULL, '1', '401', '1'),
(29, 2116, 'FAV', 835, '2018-09-02', 'IRI', 'MARTINEZ LUNA, JUAN', 1, '2018-09-20 10:13:42', 'ESSALUD', NULL, NULL, NULL, NULL, '1', '401', '1'),
(30, 4, 'AUTOINJERTO', 837, '2018-09-21', 'CTF', 'DDDDD', 1, '2018-09-21 09:43:03', 'ESSALUD', NULL, NULL, NULL, NULL, '1', '401', '1'),
(31, 10, 'FAV', 844, '2018-08-27', 'FRCI', 'MARTINEZ LUNA, JUAN', 1, '2018-09-21 11:04:03', 'ESSALUD', NULL, NULL, NULL, NULL, '1', '401', '1'),
(32, 8, 'FAV', 837, '2018-09-25', 'CTS', 'MARTINEZ LUNA, JUAN', 1, '2018-09-25 13:53:23', 'ESSALUD', NULL, NULL, NULL, NULL, '1', '401', '1'),
(33, 8, 'FAV', 835, '2018-09-03', 'CPTL', 'MARTINEZ LUNA, JUAN', 1, '2018-09-25 13:54:11', 'ESSALUD', NULL, NULL, NULL, NULL, '1', '401', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cnsr_estructura`
--

CREATE TABLE `cnsr_estructura` (
  `COD_CENCLI` int(3) NOT NULL COMMENT 'Cod. Clinica',
  `DES_LARGA` varchar(600) DEFAULT NULL COMMENT 'Descripcion Larga',
  `DES_CORTA` varchar(300) DEFAULT NULL COMMENT 'Descripcion Corta',
  `ORICENASICOD` char(1) DEFAULT NULL COMMENT 'Origen de cas',
  `CENASICOD` varchar(3) DEFAULT NULL COMMENT 'Centro asistencial',
  `ORIGEN` char(1) DEFAULT NULL COMMENT 'Centro de Essalud 0 otro 1',
  `ACTIVO` char(2) DEFAULT NULL COMMENT 'Activo | Inactivo ',
  `ESTADO` char(2) DEFAULT NULL COMMENT 'estado del registro 0 | 1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cnsr_estructura`
--

INSERT INTO `cnsr_estructura` (`COD_CENCLI`, `DES_LARGA`, `DES_CORTA`, `ORICENASICOD`, `CENASICOD`, `ORIGEN`, `ACTIVO`, `ESTADO`) VALUES
(802, 'CENTRO NEFROLOGICO S.A.', 'CENESA', '1', '401', NULL, '1', NULL),
(809, 'CENTRO DE DIALISIS SANTA ANA', 'SANTA ANA', '1', '401', NULL, '1', NULL),
(813, 'CENTRO MEDICO ESPECIALIZADO SANTA ENA S.A.', 'SANTA ENA', '1', '401', NULL, '1', NULL),
(816, 'CENTRO DE DIALISIS SAN FERNANDO S.A.', 'SAN FERNANDO', '1', '401', NULL, '1', NULL),
(821, 'CENTRO MEDICO VILLA MARIA SAC', 'VILLA MARIA', '1', '401', NULL, '1', NULL),
(825, 'CENTRO DE DIALISIS CONO NORTE E.R.L.', 'CONO NORTE', '1', '401', NULL, '1', NULL),
(828, 'CENTRO RENAL SAN MARCELO S.A.', 'SAN MARCELO', '1', '401', NULL, '1', NULL),
(829, 'SERVICIOS ESPEC. SAN CAMILO S.A.C.', 'SAN CAMILO', '1', '401', NULL, '1', NULL),
(830, 'CENTRO DE DIALISIS SANTA ROSA S.A.C.', 'SANTA ROSA', '1', '401', NULL, '1', NULL),
(831, 'SERV. DE APOYO DIAG. Y TERAPEU. SAN MIGUEL SA', 'SAN MIGUEL', '1', '401', NULL, '1', NULL),
(835, 'RENEXA S.A.', 'RENEXA', '1', '401', NULL, '1', NULL),
(837, 'CENTRO DE DIALISIS SANTO TOMAS DE AQUINO SAC', 'SANTO TOMAS', '1', '401', NULL, '1', NULL),
(838, 'CIA. DE SERVICIOS MULTIPLES S.A.C.  NORDIAL', 'NORDIAL I', '1', '401', NULL, '1', NULL),
(839, 'CENTRO MEDICO CAMINOS DEL INCA S.A.C.', 'CAMINOS DEL INCA', '1', '401', NULL, '1', NULL),
(842, 'CENTRO DE DIALISIS RENAL CARE S.A.C.', 'RENAL CARE', '1', '401', NULL, '1', NULL),
(844, 'CENTRO MEDICO JESUS MARIA S.A.C.', 'CM JESUS MARIA', '1', '401', NULL, '1', NULL),
(845, 'CENTRO DE DIALISIS JESUS MARIA S.A.C.', 'CD JESUS MARIA', '1', '401', NULL, '1', NULL),
(846, 'CENTRO DE DIALISIS VENTANILLA S.A.C.', 'VENTANILLA', '1', '401', NULL, '1', NULL),
(847, 'CLINICA SAN JUAN MASIAS S.A.C.', 'SAN JUAN MASIAS', '1', '401', NULL, '1', NULL),
(848, 'INSTITUTO DE DIALISIS Y TRASPLANTE S.A.C.', 'I.D.T.', '1', '401', NULL, '1', NULL),
(849, 'CENTRO DE SALUD RENAL S.A.C.', 'CENTRO DE SALU RENAL', '1', '401', NULL, '1', NULL),
(850, 'CENTRO DE DIALISIS OM DIAL SAC', 'OM DIAL SAC', '1', '401', NULL, '1', NULL),
(851, 'CENTRO DE DIALISIS INTERDIAL NORTE S.A.C', 'INTERDIAL', '1', '401', NULL, '1', NULL),
(852, 'INSTITUTO NEFROLOGICO PERUANO AMERICANO SAC', 'PERUANO AMERICANO', '1', '401', NULL, '1', NULL),
(854, 'CLINICA DEL RIÐON S.A.C.', 'DEL RIÐON', '1', '401', NULL, '1', NULL),
(855, 'CLINICA DE ENFERMEDADES RENALES GRAU', 'GRAU', '1', '401', NULL, '1', NULL),
(857, 'CENTRO NEFROLOGICO TINGO MARIA S.A.C.', 'CD. TINGO MARIA', '1', '401', NULL, '1', NULL),
(859, 'CLINICA BENEDICTO XVI SAC.', 'CD. BENEDICTO XVI', '1', '401', NULL, '1', NULL),
(861, 'SERVICIOS ESPECIALIZADOS D HEMODIALISIS S.A.C', 'PRONEFROS S.A.C', '1', '401', NULL, '1', NULL),
(862, 'NEPHRO CARE SAC.', 'NEPHRO CARE', '1', '401', NULL, '1', NULL),
(863, 'SERVIBENDESA SAC.', 'SERVIBENDESA SAC.', '1', '401', NULL, '1', NULL),
(864, 'PLUSVIDA SERVICIOS MEDICOS S.A.C.', 'PLUSVIDA', '1', '401', NULL, '1', NULL),
(865, 'CENTRO DE PREVENCION DE ENFERMERDAD RENAL SAC', 'CENPER SAC', '1', '401', NULL, '1', NULL),
(866, 'INVERSIONES MEDICAS PERUANAS SAC', 'I.M.P. S.A.C', '1', '401', NULL, '1', NULL),
(868, 'RIOBRANCO INVERSIONES MEDICAS SAC', 'RIOBRANCO I.M. SAC', '1', '401', NULL, '1', NULL),
(870, 'DAVITA S.A.', 'DAVITA S.A', '1', '401', NULL, '1', NULL),
(873, 'CENTRO NEFROFOLOGICO CIPRESES', 'C.N. CIPRESES', '1', '401', NULL, '1', NULL),
(875, 'CLINICA SANTA PATRICIA', 'SANTA PATRICIA', '1', '401', NULL, '1', NULL),
(876, 'CENTRO DE HEMODIALISIS COMAS SAC', 'C.D. COMAS SAC', '1', '401', NULL, '1', NULL),
(877, 'CENTRO DE DIALISIS VIDA RENAL', 'C.D. VIDA RENAL EIRL', '1', '401', NULL, '1', NULL),
(879, 'CENTRO ESPECIALIZADO SAN JUDAS TADEO SAC', 'C.E. SAN JUDAS TADEO', '1', '401', NULL, '1', NULL),
(880, 'PLUS VIDA CAÐETE', 'PLUS VIDA CAÐETE', '1', '401', NULL, '1', NULL),
(881, 'CENTRO MEDICO RENAL VILLA SOL', 'VILLA SOL', '1', '401', NULL, '1', NULL),
(882, 'CENTRO DE DIALISIS SAN LUIS S.A.C.', 'SAN LUIS', '1', '401', NULL, '1', NULL),
(883, 'CENTRO DE APOYO MEDICO LOS LAURELES', 'CAM LOS LAURELES', '1', '401', NULL, '1', NULL),
(884, 'CENTRO DE APOYO MEDICO FRANCISCO I', 'CAM FRANCISCO I', '1', '401', NULL, '1', NULL),
(885, 'CLINICAL CARE S.A.C.', 'CLINICAL CARE S.A.C.', '1', '401', NULL, '1', NULL),
(886, 'PB&G WORLD TRADING S.A.C.', 'WORLD TRADING', '1', '401', NULL, '1', NULL),
(887, 'CENTRO DE DIALISIS ENDOSCOPY CENTER E.I.R.L.', 'ENDOSCOPY CENTER', '1', '401', NULL, '1', NULL),
(888, 'ORGANIZACION MEDICA Y DE SERVICIOS NORDIAL', 'NORDIAL II', '1', '401', NULL, '1', NULL),
(889, 'CLINICA DE HEMODIALISIS LA PAZ', 'C.H. LA PAZ', '1', '401', NULL, '1', NULL),
(890, 'HZ ASOCIADOS SAC', 'HZ ASOCIADOS SAC', '1', '401', NULL, '1', NULL),
(891, 'IGSA MEDICAL SERVICE PERU S.A.', 'IGSA MSP S.A', '1', '401', NULL, '1', NULL),
(892, 'SERVICIO MEDICO RENAL CORAZON DE JESUS SAC', 'SMR CORAZON DE JESUS', '1', '401', NULL, '1', NULL),
(893, 'SERVICIOS ESPECIALIZADOS SAN CAMILO SAC', 'SAN CAMILO SAC I', '1', '401', NULL, '1', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cnsr_evaluaciones_av`
--

CREATE TABLE `cnsr_evaluaciones_av` (
  `PK_EVALUACION` int(10) NOT NULL COMMENT 'Indice de evaluacion',
  `PK_ACCESO_V` int(10) DEFAULT NULL,
  `COD_PROFESIONAL` int(10) DEFAULT NULL COMMENT 'Codigo de evaluador',
  `FECHA_EVAL` date NOT NULL COMMENT 'Fecha de evaluacion',
  `SALA` varchar(20) DEFAULT NULL COMMENT 'Sala',
  `TURNO` varchar(20) DEFAULT NULL COMMENT 'Turno',
  `FRECUENCIA_DIAL` varchar(20) DEFAULT NULL COMMENT 'Frecuencia de dialisis',
  `CONDICION_PAC` varchar(20) DEFAULT NULL COMMENT 'Condicion del paciente',
  `CONDICION_EGRESO` varchar(10) DEFAULT NULL,
  `PA_INI_SISTOLICA` int(4) DEFAULT NULL COMMENT 'Presion arterial inicio sistolica',
  `PA_INI_DIASTOLICA` int(4) DEFAULT NULL COMMENT 'Presion arterial inicio diastolica',
  `PA_FINAL_SISTOLICA` int(4) DEFAULT NULL COMMENT 'Presion arterial final sistolica',
  `PA_FINAL_DIASTOLICA` int(4) DEFAULT NULL COMMENT 'Presion arterial final diastolica',
  `QB` int(4) DEFAULT NULL COMMENT 'QB',
  `RA_PAE` int(4) DEFAULT NULL COMMENT 'RA/PAE',
  `RV_PVE` int(4) DEFAULT NULL COMMENT 'RV/PVE',
  `COMPLI_INI` varchar(6) DEFAULT NULL,
  `COMPLI_INI2` varchar(6) DEFAULT NULL,
  `COMPLI_INI3` varchar(6) DEFAULT NULL,
  `COMPLI_INI4` varchar(6) DEFAULT NULL,
  `OBSER` varchar(500) DEFAULT NULL,
  `TEST_ELEVACION` varchar(3) DEFAULT '-',
  `TEST_AUM_PULSO` varchar(3) DEFAULT '-',
  `TEST_VENAS_COLAT` varchar(3) DEFAULT '-',
  `FC_DISTANCIA` int(2) DEFAULT NULL,
  `FC_CARACTERISTICA` varchar(10) DEFAULT '-',
  `FECHA_REG` datetime DEFAULT NULL COMMENT 'Fecha de registro',
  `USUARIO_REG` varchar(20) DEFAULT NULL COMMENT 'Usuario que registra',
  `ORICENASICOD` char(1) DEFAULT NULL COMMENT 'Origen de centro',
  `CENASICOD` varchar(3) DEFAULT NULL COMMENT 'Centro asistencial',
  `USUARIO_BAJA` varchar(20) DEFAULT NULL,
  `FECHA_BAJA` datetime DEFAULT NULL COMMENT 'Fecha de baja del registro',
  `ESTADO` char(1) DEFAULT NULL COMMENT 'Estado del registro',
  `FECHA_MOD` datetime DEFAULT NULL COMMENT 'Fecha de modificacion del registro',
  `USUARIO_MOD` varchar(20) DEFAULT NULL COMMENT 'Usuario que modifica el registro'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cnsr_evaluaciones_av`
--

INSERT INTO `cnsr_evaluaciones_av` (`PK_EVALUACION`, `PK_ACCESO_V`, `COD_PROFESIONAL`, `FECHA_EVAL`, `SALA`, `TURNO`, `FRECUENCIA_DIAL`, `CONDICION_PAC`, `CONDICION_EGRESO`, `PA_INI_SISTOLICA`, `PA_INI_DIASTOLICA`, `PA_FINAL_SISTOLICA`, `PA_FINAL_DIASTOLICA`, `QB`, `RA_PAE`, `RV_PVE`, `COMPLI_INI`, `COMPLI_INI2`, `COMPLI_INI3`, `COMPLI_INI4`, `OBSER`, `TEST_ELEVACION`, `TEST_AUM_PULSO`, `TEST_VENAS_COLAT`, `FC_DISTANCIA`, `FC_CARACTERISTICA`, `FECHA_REG`, `USUARIO_REG`, `ORICENASICOD`, `CENASICOD`, `USUARIO_BAJA`, `FECHA_BAJA`, `ESTADO`, `FECHA_MOD`, `USUARIO_MOD`) VALUES
(1, 1, 2, '2018-04-25', 'SALA 1', 'T1', 'LUN-MIER-VIER', 'N', 'ET', 12, 12, 12, 12, 12, 12, 12, '4AV', '7AV', '7AV', '11AV', '', NULL, NULL, NULL, NULL, NULL, '2018-04-25 00:00:00', 'CJUNCO', '1', '401', 'ESSALUD', '2018-09-06 18:10:40', '1', '2018-09-03 09:56:26', 'ESSALUD'),
(2, 1, 5, '2018-04-25', 'SALA 1', 'T1', 'LUN-MIER-VIER', 'N', 'ET', 12, 12, 12, 12, 12, 12, 12, NULL, NULL, NULL, NULL, 'ASDSADAS', NULL, NULL, NULL, NULL, NULL, '2018-04-25 00:00:00', 'CJUNCO', '1', '401', 'ESSALUD', '2018-09-06 18:10:34', '1', NULL, NULL),
(3, 1, 7, '2018-04-25', 'SALA 1', 'T1', 'LUN-MIER-VIER', 'R', 'ET', 12, 12, 12, 12, 12, 12, 12, '4AV', '5AV', '11AV', '14AV', 'ASDF', NULL, NULL, NULL, NULL, NULL, '2018-04-25 00:00:00', 'CJUNCO', '1', '401', NULL, '2018-08-29 15:20:08', '1', '2018-09-13 16:44:09', 'ESSALUD'),
(4, 1, 7, '2018-04-15', 'SALA 1', 'T1', 'LUN-MIER-VIER', 'C', 'ET', 12, 12, 12, 12, 12, 12, 12, 'G10', NULL, NULL, NULL, 'EL PACIENTE PRESENTO SINTOMAS ... 123 .-,-´´, ()', NULL, NULL, NULL, NULL, NULL, '2018-04-25 00:00:00', 'CJUNCO', '1', '401', NULL, '2018-08-20 17:17:11', '1', NULL, NULL),
(5, 1, 5, '2018-04-25', 'S-2', 'T4', 'LUN-MIER-VIER', '2', '', 123, 123, 123, 123, 123, 123, 123, '11AV', '9AV', '10AV', '11AV', 'EL PACIENTE PRESENTO HOLII', '', '', '', 0, '', '2018-04-30 00:00:00', 'CJUNCO', '1', '401', 'ESSALUD', '2018-09-06 18:10:45', '1', '2018-10-03 15:08:18', 'ESSALUD'),
(20, 19, 3, '2018-09-26', 'S-2', 'T2', 'LUN-MIER-VIER', '3', '2', 7, 7, 7, 7, 7, 7, 7, '3C', '3C', '12AV', '3C', 'QWQWQ', '', '', '', 0, '', '2018-09-03 00:31:57', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-10-03 15:02:53', 'ESSALUD'),
(21, 20, 11, '2018-09-05', 'SALA 2', 'T2', 'MART-JUEV-SAB', 'N', 'EF', 300, 200, 0, 0, 5, 5, 5, '3AV', '15AV', '15AV', '3AV', 'BIEN833', '', '', '', 0, '', '2018-09-03 00:57:32', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-09-28 16:42:10', 'ESSALUD'),
(22, 21, 2, '2018-09-06', 'SALA 2', 'T2', 'LUN-MIER-VIER', 'N', 'EF', 2, 23, 23, 23, 23, 23, 23, '5AV', '5AV', '12AV', '13AV', 'bien4', NULL, NULL, NULL, NULL, NULL, '2018-09-05 20:14:17', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-09-06 00:28:26', 'ESSALUD'),
(23, 22, 11, '2018-09-06', 'SALA 3', 'T3', 'MART-JUEV-SAB', 'N', 'EXT', 99, 99, 99, 99, 99, 99, 99, '10AV', '1C', '15AV', '11AV', '123', NULL, NULL, NULL, NULL, NULL, '2018-09-06 00:30:16', 'ESSALUD', '1', '401', 'ESSALUD', '2018-09-07 09:26:09', '0', NULL, NULL),
(24, 19, 11, '2018-01-22', 'SALA 2', 'T4', 'LUN-MIER-VIER', 'R', 'ET', 9, 9, 99, 9, 400, -400, 400, '11AV', '12AV', '5AV', '8AV', 'BIEN', '', '', '', 0, '', '2018-09-06 08:23:53', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-10-01 11:58:17', 'ESSALUD'),
(25, 21, 2, '2018-09-11', 'SALA 1', 'T3', 'MART-JUEV-SAB', 'N', 'EF', 12, 12, 12, 12, 12, 12, 12, '4AV', '13AV', '1C', '12AV', 'BIEN', '', '', '', 0, '', '2018-09-11 11:41:58', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-09-28 15:59:08', 'ESSALUD'),
(26, 1, 11, '2018-09-11', 'SALA 2', 'T2', 'MART-JUEV-SAB', 'N', 'EF', 1, 1, 1, 1, 1, 1, 1, '14AV', '13AV', '6AV', '7AV', 'BIEN\r\n', NULL, NULL, NULL, NULL, NULL, '2018-09-11 13:30:54', 'ESSALUD', '1', '401', NULL, NULL, '1', NULL, NULL),
(27, 24, 11, '2018-09-11', 'SALA 2', 'T2', 'LUN-MIER-VIER', 'R', 'EXT', 4, 4, 5, 5, 676, 5, 5, '1C', '1C', '13AV', '11AV', 'r', '23', '232', '232', 23, 'R', '2018-09-11 15:50:09', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-09-13 17:17:38', 'ESSALUD'),
(28, 24, 7, '2018-09-11', 'SALA 1', 'T3', 'LUN-MIER-VIER', 'N', 'EF', 4, 5, 3343, 5, 676, 5, 767, '12AV', '9AV', '2C', '2C', '3e', '+', '+', '+', 23, 'R', '2018-09-11 15:50:58', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-09-21 15:53:03', 'ESSALUD'),
(29, 22, 14, '2018-09-06', 'SALA 3', 'T2', 'MART-JUEV-SAB', 'N', 'EXT', 5, 3434, 5, 5, 676, 5, 767, '6AV', '4AV', '14AV', '1C', 'bir', NULL, NULL, NULL, NULL, NULL, '2018-09-12 15:28:56', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-09-12 16:30:38', 'ESSALUD'),
(30, 22, 3, '2018-09-06', 'SALA 4', 'T3', 'LUN-MIER-VIER', 'N', 'EXT', 4, 5, 65, 6, 6, 7, 6, '3AV', '4AV', '2C', '1C', '444444444444', '', '', '', 0, '', '2018-09-12 16:31:36', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-09-28 14:44:59', 'ESSALUD'),
(31, 22, 2, '2018-09-26', 'SALA 1', 'T3', 'MART-JUEV-SAB', 'N', 'EF', 1, 3, 3, 3, 4, 4, 4, '2C', '15AV', '14AV', '14AV', '333', '', '', '', 0, '', '2018-09-12 16:56:43', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-09-26 17:19:22', 'ESSALUD'),
(32, 19, 8, '2018-08-29', 'SALA 1', 'T3', 'MART-JUEV-SAB', 'N', 'EF', 1, 3, 3, 3, 4, 4, 4, '2C', '15AV', '14AV', '14AV', 'tttttttttttttt', '-', '+', '+', 0, 'B', '2018-09-12 16:56:43', 'ESSALUD', '1', '401', NULL, NULL, '1', NULL, NULL),
(33, 25, 3, '2018-09-13', 'SALA 2', 'T2', 'MART-JUEV-SAB', 'R', 'EF', 4, 5, 3343, 5, 676, -200, 150, '3AV', '4AV', '13AV', '12AV', 'TODO BIEN', '', '', '', 0, '', '2018-09-13 17:07:50', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-09-13 17:18:43', 'ESSALUD'),
(34, 26, 2, '2018-09-13', 'SALA 2', 'T2', 'LUN-MIER-VIER', 'N', 'EF', 1, 5, 5, 200, 400, -220, 151, '6AV', '4AV', '1C', '15AV', 'TH', '+', '+', '+', 0, 'B', '2018-09-13 17:12:57', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-09-28 15:59:44', 'ESSALUD'),
(35, 22, 14, '2018-09-18', 'SALA 2', 'T1', 'MART-JUEV-SAB', 'N', 'EXT', 4, 5, 5, 4, 4, 4, 343, '4AV', '15AV', '12AV', '11AV', '433', '', '', '', 0, '', '2018-09-18 16:08:54', 'ESSALUD', '1', '401', NULL, NULL, '1', NULL, NULL),
(36, 29, 14, '2018-09-20', 'SALA 2', 'T2', 'LUN-MIER-VIER', 'N', 'EXT', 3, 3, 3, 3, 3, 3, 3, '5AV', '6AV', '15AV', '1C', '333', '', '', '', 0, '', '2018-09-20 10:16:04', 'ESSALUD', '1', '401', NULL, NULL, '1', NULL, NULL),
(37, 26, 16, '2018-09-21', 'SALA 3', 'T2', 'MART-JUEV-SAB', 'N', 'EXT', 0, 0, 0, 0, 0, 0, 0, '3AV', '15AV', '13AV', '10AV', 'd', '', '', '', 0, '', '2018-09-21 09:43:57', 'ESSALUD', '1', '401', NULL, NULL, '1', NULL, NULL),
(38, 22, 21, '2018-09-21', 'SALA 2', 'T1', 'MART-JUEV-SAB', 'N', '', 4, 5, 0, 0, 0, 0, 0, '1C', '12AV', '7AV', '8AV', 'tha', '', '', '', 0, '', '2018-09-21 09:48:25', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-09-21 09:49:23', 'ESSALUD'),
(39, 24, 2, '2018-09-21', 'SALA 2', 'T1', 'LUN-MIER-VIER', 'N', 'ET', 4, 5, 334, 5, 676, -201, 767, '4AV', '2C', '14AV', '11AV', 'ok', '+', '+', '-', 23, 'B', '2018-09-21 15:54:41', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-09-21 16:45:03', 'ESSALUD'),
(40, 24, 21, '2018-09-25', 'SALA 2', 'T2', 'MART-JUEV-SAB', 'N', 'EF', 34, 34, 34, 34, 34, 34, 34, '5AV', '1C', '12AV', '1C', '3434', '-', '+', '+', 23, 'R', '2018-09-25 13:14:58', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-09-25 13:40:42', 'ESSALUD'),
(41, 26, 16, '2018-09-26', 'SALA 2', 'T3', 'MART-JUEV-SAB', 'N', 'EXT', 4, 5, 334, 5, 676, 5, 767, '6AV', '', '', '', '.,', '+', '+', '+', 23, 'R', '2018-09-26 17:16:58', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-09-26 17:17:21', 'ESSALUD'),
(42, 19, 11, '2018-09-26', 'S-3', 'T2', 'LUN-MIER-VIER', '1', '6', 3, 99, 105, 110, 0, 23, 99, '3AV', '3C', '2AV', '12AV', '5', '', '', '', 0, '', '2018-09-26 17:20:39', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-10-04 15:35:51', 'ESSALUD'),
(43, 22, 2, '2018-09-08', 'SALA 2', 'T2', 'MART-JUEV-SAB', 'N', 'EF', 4, 5, 34, 5, 34, 5, 34, '3AV', '', '', '', 's', '', '', '', 0, '', '2018-09-26 17:35:05', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-09-28 16:53:25', 'ESSALUD'),
(44, 21, 21, '2018-09-28', 'S-3', 'T3', 'MART-JUEV-SAB', '3', '2', 4, 5, 99, 99, 4, 5, 400, '0', '', '', '', '', '', '', '', 0, '', '2018-09-28 10:46:40', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-10-05 14:58:31', 'ESSALUD'),
(45, 24, 2, '2018-09-28', 'SALA 2', 'T3', 'MART-JUEV-SAB', 'N', 'EXT', 4, 4, 0, 0, 3, 3, 3, '', '', '', '', '', '', '', '', 0, '', '2018-09-28 16:07:36', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-09-28 16:09:28', 'ESSALUD'),
(46, 25, 11, '2018-10-01', 'S-2', 'T1', 'LUN-MIER-VIER', 'N', 'EF', 152, 66, 45, 5, 58, -150, 150, '', '', '', '', '', '', '', '', 0, '', '2018-10-01 09:18:17', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-10-02 14:44:57', 'ESSALUD'),
(47, 22, 2, '2018-10-01', 'S-2', 'T3', 'LUN-MIER-VIER', 'N', 'EXT', 4, 5, 52, 5, 212, 22, 87, '', '', '', '', '', '', '', '', 0, '', '2018-10-01 09:23:19', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-10-01 12:00:06', 'ESSALUD'),
(48, 24, 16, '2018-10-01', 'S-2', 'T1', 'MART-JUEV-SAB', 'N', 'EXT', 23, 23, 23, 23, 23, 23, 23, '', '', '', '', '', '', '', '', 0, '', '2018-10-01 11:59:02', 'ESSALUD', '1', '401', NULL, NULL, '1', NULL, NULL),
(49, 28, 3, '2018-10-02', 'S-1', 'T3', 'LUN-MIER-VIER', '1', '2', 300, 200, 300, 200, 400, 85, 400, '', '', '', '', '9', '+', '+', '-', 50, 'R', '2018-10-02 09:06:40', 'ESSALUD', '1', '401', NULL, NULL, '1', '2018-10-02 16:01:19', 'ESSALUD'),
(50, 21, 21, '2018-10-04', 'S-3', 'T3', 'MART-JUEV-SAB', '', '', 3, 33, 43, 44, 4, 23, 4, '', '', '', '', '', '', '', '', 0, '', '2018-10-04 16:52:26', 'ESSALUD', '1', '401', NULL, NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cnsr_maestro`
--

CREATE TABLE `cnsr_maestro` (
  `COD_TIPO` int(10) NOT NULL COMMENT 'Indice',
  `DES_CORTA` varchar(100) NOT NULL COMMENT 'Descripcion corta',
  `DES_LARGA` varchar(150) DEFAULT NULL COMMENT 'Descripcion larga',
  `COD_INT` varchar(15) DEFAULT NULL COMMENT 'Codigo interno de orden',
  `COD_REL` int(10) DEFAULT NULL COMMENT 'Codigo para relacion',
  `COD_TIP_PAD` int(10) DEFAULT NULL COMMENT 'Codigo tipo padre',
  `NIV_TIPO` int(10) DEFAULT NULL COMMENT 'Nivel tipo',
  `EST_REG` char(1) DEFAULT '1' COMMENT 'Estado del registro',
  `ACTIVO` char(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cnsr_maestro`
--

INSERT INTO `cnsr_maestro` (`COD_TIPO`, `DES_CORTA`, `DES_LARGA`, `COD_INT`, `COD_REL`, `COD_TIP_PAD`, `NIV_TIPO`, `EST_REG`, `ACTIVO`) VALUES
(1, 'GRADO DE INSTRUCCIÓN', 'GRADO DE INSTRUCCIÓN', '', 0, 0, 0, '1', '1'),
(2, 'ANALFABETO', 'ANALFABETO', '1', 2, 1, 1, '1', '1'),
(3, 'PRIMARIA', 'PRIMARIA', '2', 0, 1, 1, '1', '1'),
(4, 'SECUNDARIA ', 'SECUNDARIA ', '3', 0, 1, 1, '1', '1'),
(5, 'SUPERIOR', 'SUPERIOR', '4', NULL, 1, 1, '1', '1'),
(6, 'TIPO DE ETIOLOGIAS', 'TIPO DE ETIOLOGIAS', NULL, NULL, NULL, 0, '1', '1'),
(7, 'DIABETES', 'DIABETES', '1', NULL, 6, 1, '1', '1'),
(8, 'GLOMERULONEFRITIS', 'GLOMERULONEFRITIS', '2', NULL, 6, 1, '1', '1'),
(9, 'GLOMERULONEFRITIS SECUNDARIA', 'GLOMERULONEFRITIS SECUNDARIA/VASCULITIS', '3', NULL, 6, 1, '1', '1'),
(10, 'NEFRITIS INTERSTICIAL', 'NEFRITIS INTERSTICIAL/PIELONEFRITIS', '4', NULL, 6, 1, '1', '1'),
(11, 'HIPERTENSION', 'HIPERTENSION/ENFERMEDAD DE VASOS GRANDES', '5', NULL, 6, 1, '1', '1'),
(12, 'ENFERMEDAD QUISTICA', 'ENFERMEDAD QUISTICA/HEREDITARIA CONGENITA', '6', NULL, 6, 1, '1', '1'),
(13, 'NEOPLASIAS', 'NEOPLASIAS/TUMORES', '7', NULL, 6, 1, '1', '1'),
(14, 'OTRAS CONDICIONES', 'OTRAS CONDICIONES', '8', NULL, 6, 1, '1', '1'),
(15, 'MODALIDAD DE DIALISIS', 'MODALIDAD DE DIALISIS', NULL, NULL, NULL, 0, '1', '1'),
(16, 'HD', 'HEMODIALISIS', '1', NULL, 15, 1, '1', '1'),
(17, 'DPAC', 'DIALISIS PERITONEAL AMBULATORIA CONTINUA', '2', NULL, 15, 1, '1', '1'),
(18, 'DPA', 'DIALISIS PERITONEAL AUTOMATIZADA', '3', NULL, 15, 1, '1', '1'),
(19, 'COMORBILIDAD', 'COMORBILIDAD', NULL, NULL, NULL, 0, '1', '1'),
(20, 'ENFERMEDADES ARTERIOESCLEROTICAS CARDIACAS', 'ENFERMEDADES ARTERIOESCLEROTICAS CARDIACAS', '1', NULL, 19, 1, '1', '1'),
(21, 'INSUFICIENCIA CARDIACA CONGESTIVA', 'INSUFICIENCIA CARDIACA CONGESTIVA', '2', NULL, 19, 1, '1', '1'),
(22, 'ENFERMEDAD VASCULAR PERIFERICA', 'ENFERMEDAD VASCULAR PERIFERICA', '3', NULL, 19, 1, '1', '1'),
(23, 'ACCIDENTE CEREBROVASCULAR / ACCIDENTE ISQUEMICO TRANSITORIO', 'ACCIDENTE CEREBROVASCULAR / ACCIDENTE ISQUEMICO TRANSITORIO', '4', NULL, 19, 1, '1', '1'),
(24, 'CANCER', 'CANCER', '5', NULL, 19, 1, '1', '1'),
(25, 'DIABETES', 'DIABETES', '6', NULL, 19, 1, '1', '1'),
(26, 'HIPERTENSIÓN', 'HIPERTENSIÓN', '7', NULL, 19, 1, '1', '1'),
(27, 'TUBERCULOSIS', 'TUBERCULOSIS', '8', NULL, 19, 1, '1', '1'),
(28, 'OTRA', 'OTRA: DE PRESENTAR EL PACIENTE UNA COMORBILIDAD NO INCLUIDA EN ESTA LISTA', '9', NULL, 19, 1, '1', '1'),
(29, 'MODALIDAD DE INICIO DE TRR', 'MODALIDAD DE INICIO DE TRR', NULL, NULL, NULL, 0, '1', '1'),
(30, 'HEMODIALISIS', 'HEMODIALISIS', '1', NULL, 29, 1, '1', '1'),
(31, 'DIALISIS PERITONEAL', 'DIALISIS PERITONEAL', '2', NULL, 29, 1, '1', '1'),
(32, 'TRASPLANTE RENAL', 'TRASPLANTE RENAL', '3', NULL, 29, 1, '1', '1'),
(33, 'SUBSISTEMA DE SALUD DE INICIO DE TRR', 'SUBSISTEMA DE SALUD DE INICIO DE TRR', NULL, NULL, NULL, 0, '1', '1'),
(34, 'ESSALUD', 'ESSALUD', '1', NULL, 33, 1, '1', '1'),
(35, 'MINSA', 'MINSA', '2', NULL, 33, 1, '1', '1'),
(36, 'EPS', 'EPS', '3', NULL, 33, 1, '1', '1'),
(37, 'FFAA', 'FFAA', '4', NULL, 33, 1, '1', '1'),
(38, 'FFPP', 'FFPP', '5', NULL, 33, 1, '1', '1'),
(39, 'OTROS', 'OTROS', '6', NULL, 33, 1, '1', '1'),
(40, 'TIPO DE ACCESO VASCULAR', 'TIPO DE ACCESO VASCULAR', NULL, NULL, NULL, 0, '1', '1'),
(41, 'FAV', 'FISTULA ARTERIOVENOSA', '1', NULL, 40, 1, '1', '1'),
(42, 'AUTOINJERTO', 'AUTOINJERTO', '2', NULL, 40, 1, '1', '1'),
(43, 'INJERTO', 'INJERTO PROTESICO', '3', NULL, 40, 1, '1', '1'),
(44, 'CVCP', 'CATETER VENOSO CENTRAL PERMANENTE', '4', NULL, 40, 1, '1', '1'),
(45, 'CVCT', 'CATETER VENOSO CENTRAL TEMPORAL', '5', NULL, 40, 1, '1', '1'),
(46, 'CPERITONEAL', 'CATETER PERITONEAL', '6', NULL, 40, 1, '1', '1'),
(47, 'TIPO DE DOCUMENTO', 'TIPO DE DOCUMENTO', NULL, NULL, NULL, 0, '1', '1'),
(48, 'FICHA DE EVALUACION', 'FICHA DE EVALUACION', '1', NULL, 47, 1, '1', '1'),
(49, 'VACUNACION CONTRA VHB', 'VACUNACION CONTRA VHB', NULL, NULL, NULL, 0, '1', '1'),
(50, 'COMPLETA', 'COMPLETA', '1', NULL, 49, 1, '1', '1'),
(51, 'INCOMPLETA', 'INCOMPLETA', '2', NULL, 49, 1, '1', '1'),
(52, 'EN PROCESO', 'EN PROCESO', '3', NULL, 49, 1, '1', '1'),
(53, 'NO INICIO ESQUEMA', 'NO INICIO ESQUEMA', '4', NULL, 49, 1, '1', '1'),
(54, 'TITULO DE ACHBS', 'TITULO DE ACHBS', NULL, NULL, NULL, 0, '1', '1'),
(55, '<  10 UI/L (NO PROTEGIDO)', '<  10 UI/L (NO PROTEGIDO)', '1', NULL, 54, 1, '1', '1'),
(56, '>=  10 UI/L (PROTEGIDO)', '>=  10 UI/L (PROTEGIDO)', '2', NULL, 54, 1, '1', '1'),
(57, 'DESCONOCIDO', 'DESCONOCIDO', '3', NULL, 54, 1, '1', '1'),
(58, 'CONDICION EN LA UNIDAD', 'CONDICION EN LA UNIDAD', NULL, NULL, NULL, 0, '1', '1'),
(59, 'NUEVO', 'NUEVO', '1', NULL, 58, 1, '1', '1'),
(60, 'REINGRESO', 'REINGRESO', '2', NULL, 58, 1, '1', '1'),
(61, 'CONTINUADOR', 'CONTINUADOR', '3', NULL, 58, 1, '1', '1'),
(62, 'MOTIVO DE CAMBIO DE ACCESO VASCULAR ', 'MOTIVO DE CAMBIO DE ACCESO VASCULAR ', NULL, NULL, NULL, 0, '1', '1'),
(63, 'COMPLICACION MECANICA', 'COMPLICACION MECANICA', '1', NULL, 62, 1, '1', '1'),
(64, 'COMPLICACION INFECCIOSA', 'COMPLICACION INFECCIOSA', '2', NULL, 62, 1, '1', '1'),
(65, 'PRESCRIPCION MEDICA', 'PRESCRIPCION MEDICA', '3', NULL, 62, 1, '1', '1'),
(66, 'NO APLICA', 'NO APLICA', '4', NULL, 62, 1, '1', '1'),
(67, 'TIPO DE EGRESO', 'TIPO DE EGRESO', NULL, NULL, NULL, 0, '1', '1'),
(68, 'FALLECIMIENTO', 'FALLECIMIENTO', '1', NULL, 67, 1, '1', '1'),
(69, 'HOSPITALIZACION', 'HOSPITALIZACION', '2', NULL, 67, 1, '1', '1'),
(70, 'TRANSPLANTE', 'TRANSPLANTE', '3', NULL, 67, 1, '1', '1'),
(71, 'CAMBIO DE MODALIDAD', 'CAMBIO DE MODALIDAD', '4', NULL, 67, 1, '1', '1'),
(72, 'CAMBIO DE UNIDAD', 'CAMBIO DE UNIDAD', '5', NULL, 67, 1, '1', '1'),
(73, 'OTROS', 'OTROS', '6', NULL, 67, 1, '1', '1'),
(74, 'TIPO EVENTO DP', 'TIPO EVENTO DP', NULL, NULL, NULL, 0, '1', '1'),
(75, 'EPISODIO PERITONITIS', 'EPISODIO PERITONITIS', '1', NULL, 74, 1, '1', '1'),
(76, 'EPISODIO DE INFECCION DE ORIFICIO DE SALIDA', 'EPISODIO DE INFECCION DE ORIFICIO DE SALIDA', '2', NULL, 74, 1, '1', '1'),
(77, 'AMBOS EVENTOS CONCOMITANTES', 'AMBOS EVENTOS CONCOMITANTES', '3', NULL, 74, 1, '1', '1'),
(78, 'PET', 'PET', NULL, NULL, NULL, 0, '1', '1'),
(79, 'T. RAPIDO (TRANSPORTADOR RAPIDO)', 'T. RAPIDO (TRANSPORTADOR RAPIDO)', '1', NULL, 78, 1, '1', '1'),
(80, 'T.P. RAPIDO (TRANSPORTADOR PROMEDIO RAPIDO)', 'T.P. RAPIDO (TRANSPORTADOR PROMEDIO RAPIDO)', '2', NULL, 78, 1, '1', '1'),
(81, 'T.P. LENTO (TRANSPORTADOR PROMEDIO LENTO)', 'T.P. LENTO (TRANSPORTADOR PROMEDIO LENTO)', '3', NULL, 78, 1, '1', '1'),
(82, 'T. LENTO (TRANSPORTADOR LENTO)', 'T. LENTO (TRANSPORTADOR LENTO)', '4', NULL, 78, 1, '1', '1'),
(83, 'TIEMPO DE DIALISIS', 'TIEMPO DE DIALISIS', NULL, NULL, NULL, 0, '1', '1'),
(84, '2 HORAS', '2.00', '1', NULL, 83, 1, '1', '1'),
(85, '2 HORAS CON 15 MIN', '2.25', '2', NULL, 83, 1, '1', '1'),
(86, '2 HORAS CON 30 MIN', '2.50', '3', NULL, 83, 1, '1', '1'),
(87, '2 HORAS CON 45 MIN', '2.75', '4', NULL, 83, 1, '1', '1'),
(88, '3 HORAS', '3.00', '5', NULL, 83, 1, '1', '1'),
(89, '3 HORAS CON 15 MIN', '3.25', '6', NULL, 83, 1, '1', '1'),
(90, '3 HORAS CON 30 MIN', '3.50', '7', NULL, 83, 1, '1', '1'),
(91, '3 HORAS CON 45 MIN', '3.75', '8', NULL, 83, 1, '1', '1'),
(92, '4 HORAS', '4.00', '9', NULL, 83, 1, '1', '1'),
(93, '4 HORAS CON 15 MIN', '4.25', '10', NULL, 83, 1, '1', '1'),
(94, '4 HORAS CON 30 MIN', '4.50', '11', NULL, 83, 1, '1', '1'),
(95, 'CONTROLES AGUA', 'CONTROLES AGUA', NULL, NULL, NULL, 0, '1', '1'),
(96, 'RECUENTO BACTERIANO EN AGUA TRATADA (UFC)', 'RECUENTO BACTERIANO EN AGUA TRATADA (UFC)', '1', NULL, 95, 1, '1', '1'),
(97, 'NIVEL DE ENDOTOXINAS EN AGUA TRATADA (UE)', 'NIVEL DE ENDOTOXINAS EN AGUA TRATADA (UE)', '2', NULL, 95, 1, '1', '1'),
(98, 'RECUENTO BACTERIANO EN LIQUIDO DE DIALISIS (UFC)', 'RECUENTO BACTERIANO EN LIQUIDO DE DIALISIS (UFC)', '3', NULL, 95, 1, '1', '1'),
(99, 'NIVEL DE ENDOTIXINAS EN LIQUIDO DE DIALISIS (UE)', 'NIVEL DE ENDOTIXINAS EN LIQUIDO DE DIALISIS (UE)', '4', NULL, 95, 1, '1', '1'),
(100, 'RESULTADO AGUA', 'RESULTADO AGUA', NULL, NULL, NULL, 0, '1', '1'),
(101, 'SALIDA DE LA OSMOSIS', 'SALIDA DE LA OSMOSIS', '1', NULL, 100, 1, '1', '1'),
(102, 'REATORNO DEL ANILLO DE CIRCULACION', 'REATORNO DEL ANILLO DE CIRCULACION', '2', NULL, 100, 1, '1', '1'),
(103, 'MAQUINA DE HEMODIALISIS', 'MAQUINA DE HEMODIALISIS', '3', NULL, 100, 1, '1', '1'),
(104, 'ESTADO CIVIL', 'ESTADO CIVIL', NULL, NULL, NULL, 0, '1', '1'),
(105, 'SOLTERO', 'SOLTERO', '1', NULL, 104, 1, '1', '1'),
(106, 'CASADO', 'CASADO', '2', NULL, 104, 1, '1', '1'),
(107, 'CONVIVIENTE', 'CONVIVIENTE', '3', NULL, 104, 1, '1', '1'),
(108, 'DIVORCIADO', 'DIVORCIADO', '4', NULL, 104, 1, '1', '1'),
(109, 'VIUDO', 'VIUDO', '5', NULL, 104, 1, '1', '1'),
(110, 'UBICACION DEL ACCESO VASCULAR', 'UBICACION DEL ACCESO VASCULAR', NULL, NULL, NULL, 0, '1', '1'),
(111, 'FRCD', 'RADIAL CEFALICA DERECHA', '1', 41, 110, 2, '1', '1'),
(112, 'FRCI', 'RADIAL CEFALICA IZQUIERDA', '2', 41, 110, 2, '1', '1'),
(113, 'FBD', 'BRAQUIAL DERECHA', '3', 41, 110, 2, '1', '1'),
(114, 'FBI', 'BRAQUIAL IZQUIERDA', '4', 41, 110, 2, '1', '1'),
(115, 'FFD', 'FEMORAL DERECHA', '5', 41, 110, 2, '1', '1'),
(116, 'FFI', 'FEMORAL IZQUIERDA', '6', 41, 110, 2, '1', '1'),
(117, 'IRD', 'RADIAL DERECHA', '7', 43, 110, 2, '1', '1'),
(118, 'IRI', 'RADIAL IZQUIERDA', '8', 43, 110, 2, '1', '1'),
(119, 'IHSI', 'HUMERAL DERECHA', '9', 43, 110, 2, '1', '1'),
(120, 'IHSD', 'HUMERAL IZQUIERDA', '10', 43, 110, 2, '1', '1'),
(121, 'IFI', 'FEMORAL DERECHA', '11', 43, 110, 2, '1', '1'),
(122, 'IFD', 'FEMORAL IZQUIERDA', '12', 43, 110, 2, '1', '1'),
(123, 'CTY', 'YUGULAR', '13', 45, 110, 2, '1', '1'),
(124, 'CTS', 'SUBCLAVIO', '14', 45, 110, 2, '1', '1'),
(125, 'CTF', 'FEMORAL T', '15', 45, 110, 2, '1', '1'),
(126, 'CPS', 'SUBCLAVIA', '16', 44, 110, 2, '1', '1'),
(127, 'CPF', 'FEMORAL P', '17', 44, 110, 2, '1', '1'),
(128, 'CPTL', 'TRANS LUMBAR', '18', 44, 110, 2, '1', '1'),
(129, 'CPTH', 'TRANS HEPATICO', '19', 44, 110, 2, '1', '1'),
(130, 'COMPLICACIONES DEL ACCESO VASCULAR', 'COMPLICACIONES DEL ACCESO VASCULAR', NULL, NULL, NULL, 0, '1', '1'),
(131, 'INFILTRACION VENOSA', 'INFILTRACION VENOSA', '1AV', NULL, 130, 1, '1', '1'),
(132, 'INFILTRACION ARTERIAL', 'INFILTRACION ARTERIAL', '2AV', NULL, 130, 1, '1', '1'),
(133, 'FISTULA DISFUNCIONANTE', 'FISTULA DISFUNCIONANTE', '3AV', NULL, 130, 1, '1', '1'),
(134, 'FISTULA NO FUNCIONANTE', 'FISTULA NO FUNCIONANTE', '4AV', NULL, 130, 1, '1', '1'),
(135, 'ACCESO VENOSO DE DIFICIL ABORDAJE', 'ACCESO VENOSO DE DIFICIL ABORDAJE', '5AV', NULL, 130, 1, '1', '1'),
(136, 'EDEMA DE MIEMBRO PORTADOR DE FISTULA ARTERIOVENOSA', 'EDEMA DE MIEMBRO PORTADOR DE FISTULA ARTERIOVENOSA', '6AV', NULL, 130, 1, '1', '1'),
(137, 'INFECCION LOCAL DEL ACCESO VASCULAR', 'INFECCION LOCAL DEL ACCESO VASCULAR', '7AV', NULL, 130, 1, '1', '1'),
(138, 'HEMATOMA', 'HEMATOMA', '8AV', NULL, 130, 1, '1', '1'),
(139, 'DILATACION DEL ACCESO VASCULAR (PSEUDOANEURISMA)', 'DILATACION DEL ACCESO VASCULAR (PSEUDOANEURISMA)', '9AV', NULL, 130, 1, '1', '1'),
(140, 'QB<300 ML/MIN', 'QB<300 ML/MIN', '10AV', NULL, 130, 1, '1', '1'),
(141, 'RESISTENCIA VENOSA ALTA (>150)', 'RESISTENCIA VENOSA ALTA (>150)', '11AV', NULL, 130, 1, '1', '1'),
(142, 'RESISTENCIA ARTERIAL ALTA (>-220)', 'RESISTENCIA ARTERIAL ALTA (>-220)', '12AV', NULL, 130, 1, '1', '1'),
(143, 'INVERSION DE FLUJO VENOSO', 'INVERSION DE FLUJO VENOSO', '13AV', NULL, 130, 1, '1', '1'),
(144, 'DOLOR EN LA EXTREMIDAD DISTAL DE LA ANASTOMOSIS', 'DOLOR EN LA EXTREMIDAD DISTAL DE LA ANASTOMOSIS', '14AV', NULL, 130, 1, '1', '1'),
(145, 'DEFICIENTE HIGIENE DEL ACCESO VASCULAR', 'DEFICIENTE HIGIENE DEL ACCESO VASCULAR', '15AV', NULL, 130, 1, '1', '1'),
(146, 'DISFUNCION DE UN CVC', 'DISFUNCION DE UN CVC', '1C', NULL, 130, 1, '1', '1'),
(147, 'FUNCIONAMIENTO INVERTIDO', 'FUNCIONAMIENTO INVERTIDO', '2C', NULL, 130, 1, '1', '1'),
(148, 'SANGRADO POR ORIFICIO DE INSERCION', 'SANGRADO POR ORIFICIO DE INSERCION', '3C', NULL, 130, 1, '1', '1'),
(149, 'DESPLAZAMIENTO DE CATETER VENOSO CENTRAL', 'DESPLAZAMIENTO DE CATETER VENOSO CENTRAL', '4C', NULL, 130, 1, '1', '1'),
(150, 'FISURA DE CATETER', 'FISURA DE CATETER', '5C', NULL, 130, 1, '1', '1'),
(151, 'ACODAMIENTO DE CATETER TRANSITORIO', 'ACODAMIENTO DE CATETER TRANSITORIO', '6C', NULL, 130, 1, '1', '1'),
(152, 'INFECCIONES ASOCIADAS A CATETER VENOSO CENTRAL', 'INFECCIONES ASOCIADAS A CATETER VENOSO CENTRAL', '7C', NULL, 130, 1, '1', '1'),
(153, 'CATETER VENOSO CENTRAL TRANSITORIO DE LARGA DATA', 'CATETER VENOSO CENTRAL TRANSITORIO DE LARGA DATA', '8C', NULL, 130, 1, '1', '1'),
(154, 'ETIOLOGIA', 'ETIOLOGIA', NULL, NULL, NULL, 0, '1', '1'),
(155, 'DIABETES RENAL TIPO II', 'DIABETES RENAL TIPO II', 'A1', 7, 154, 2, '1', '1'),
(156, 'DIABETES RENAL TIPO I', 'DIABETES RENAL TIPO I', 'A2', 7, 154, 2, '1', '1'),
(157, 'GLOMERULONEFRITIS GN', 'GLOMERULONEFRITIS GN', 'B1', 8, 154, 2, '1', '1'),
(158, 'GLOMERULOESCLEROSIS FOCAL, ESCLEROSANTE FOCAL', 'GLOMERULOESCLEROSIS FOCAL, ESCLEROSANTE FOCAL', 'B2', 8, 154, 2, '1', '1'),
(159, 'NEFROPATIA MEMBRANOSA', 'NEFROPATIA MEMBRANOSA', 'B3', 8, 154, 2, '1', '1'),
(160, 'GN MEMBRANOPROLIFERATIVA TIPO I, GN MEMBRANOPROLIFERATIVA DIFUSA)', 'GN MEMBRANOPROLIFERATIVA TIPO I, GN MEMBRANOPROLIFERATIVA DIFUSA)', 'B4', 8, 154, 2, '1', '1'),
(161, 'ENFERMEDAD POR DEPOSITOS DENSOS, GN MEMBRANO PROLIFERATIVA ', 'ENFERMEDAD POR DEPOSITOS DENSOS, GN MEMBRANO PROLIFERATIVA ', 'B5', 8, 154, 2, '1', '1'),
(162, 'NEFROPATIA IGA ENFERMEDAD, ENFERMEDAD DE BERGER (DEMOSTRADO POR INMUNOFLUORESCENCIA) ', 'NEFROPATIA IGA ENFERMEDAD, ENFERMEDAD DE BERGER (DEMOSTRADO POR INMUNOFLUORESCENCIA) ', 'B6', 8, 154, 2, '1', '1'),
(163, 'NEFROPATIA IGM, (DEMOSTRADO POR INMUNOFLUORESCENCIA)', 'NEFROPATIA IGM, (DEMOSTRADO POR INMUNOFLUORESCENCIA)', 'B7', 8, 154, 2, '1', '1'),
(164, 'GN RAPIDAMENTE PROGRESIVA', 'GN RAPIDAMENTE PROGRESIVA', 'B8', 8, 154, 2, '1', '1'),
(165, 'GN POST INFECCIOSAS', 'GN POST INFECCIOSAS', 'B9', 8, 154, 2, '1', '1'),
(166, 'OTRAS GN PROLIFERATIVAS', 'OTRAS GN PROLIFERATIVAS', 'B10', 8, 154, 2, '1', '1'),
(167, 'LUPUS ERITEMALOSO', 'LUPUS ERITEMALOSO', 'C1', 9, 154, 2, '1', '1'),
(168, 'SINDROME DE HENOCH-SCHONLEIN', 'SINDROME DE HENOCH-SCHONLEIN', 'C2', 9, 154, 2, '1', '1'),
(169, 'ESCLERODEMIA', 'ESCLERODEMIA', 'C3', 9, 154, 2, '1', '1'),
(170, 'SINDROME UREMICO HEMOLITICO', 'SINDROME UREMICO HEMOLITICO', 'C4', 9, 154, 2, '1', '1'),
(171, 'POLIARTERITIS', 'POLIARTERITIS', 'C5', 9, 154, 2, '1', '1'),
(172, 'GRANULOMALOSIS DE WEGENER', 'GRANULOMALOSIS DE WEGENER', 'C6', 9, 154, 2, '1', '1'),
(173, 'NEFROPATIA POR ABUSO DE HEROÍNA RELACIONADO A DROGAS', 'NEFROPATIA POR ABUSO DE HEROÍNA RELACIONADO A DROGAS', 'C7', 9, 154, 2, '1', '1'),
(174, 'OTRAS VASCULITIS Y SUS DERIVADAS', 'OTRAS VASCULITIS Y SUS DERIVADAS', 'C8', 9, 154, 2, '1', '1'),
(175, 'SINDROME DE GOODPASTURE', 'SINDROME DE GOODPASTURE', 'C9', 9, 154, 2, '1', '1'),
(176, 'OTRAS GN SECUNDARIAS', 'OTRAS GN SECUNDARIAS', 'C10', 9, 154, 2, '1', '1'),
(177, 'ABUSO DE ANALGÉSICOS', 'ABUSO DE ANALGÉSICOS', 'D1', 10, 154, 2, '1', '1'),
(178, 'NEFRITIS POR RADIACIÓN', 'NEFRITIS POR RADIACIÓN', 'D2', 10, 154, 2, '1', '1'),
(179, 'NEFROPATÍA POR PLOMO', 'NEFROPATÍA POR PLOMO', 'D3', 10, 154, 2, '1', '1'),
(180, 'NEFROPATÍA CAUSADO POR OTROS AGENTES', 'NEFROPATÍA CAUSADO POR OTROS AGENTES', 'D4', 10, 154, 2, '1', '1'),
(181, 'NEFROPATÍA POR GOTA', 'NEFROPATÍA POR GOTA', 'D5', 10, 154, 2, '1', '1'),
(182, 'NEFROLITIASIS', 'NEFROLITIASIS', 'D6', 10, 154, 2, '1', '1'),
(183, 'UROPATÍA OBSTRUCTIVA ADQUIRIDA', 'UROPATÍA OBSTRUCTIVA ADQUIRIDA', 'D7', 10, 154, 2, '1', '1'),
(184, 'PIELONEFRITIS CRÓNICA, NEFROPATIA POR REFLUJO', 'PIELONEFRITIS CRÓNICA, NEFROPATIA POR REFLUJO', 'D8', 10, 154, 2, '1', '1'),
(185, 'NEFROPATÍA INTERSTICIAL CRONICA', 'NEFROPATÍA INTERSTICIAL CRONICA', 'D9', 10, 154, 2, '1', '1'),
(186, 'NEFRITIS INTERSTICIAL AGUDA', 'NEFRITIS INTERSTICIAL AGUDA', 'D10', 10, 154, 2, '1', '1'),
(187, 'UROLITIASIS', 'UROLITIASIS', 'D11', 10, 154, 2, '1', '1'),
(188, 'OTROS DESÓRDENES DEL METABOLISMO DE CALCIO', 'OTROS DESÓRDENES DEL METABOLISMO DE CALCIO', 'D12', 10, 154, 2, '1', '1'),
(189, 'HIPERTENSIÓN NO ESPECIFICADA CON FALLA RENAL', 'HIPERTENSIÓN NO ESPECIFICADA CON FALLA RENAL', 'E1', 11, 154, 2, '1', '1'),
(190, 'ESTENOSIS DE LA ARTERIA RENAL', 'ESTENOSIS DE LA ARTERIA RENAL', 'E2', 11, 154, 2, '1', '1'),
(191, 'OCLUSIÓN DE LA ARTERIA RENAL', 'OCLUSIÓN DE LA ARTERIA RENAL', 'E3', 11, 154, 2, '1', '1'),
(192, 'EMBOLIA CAUSADA POR COLESTEROL, EMBOLIA RENAL', 'EMBOLIA CAUSADA POR COLESTEROL, EMBOLIA RENAL', 'E4', 11, 154, 2, '1', '1'),
(193, 'RIÑON POLIQUISTICO DEL ADULTO TIPO DOMINANTE', 'RIÑON POLIQUISTICO DEL ADULTO TIPO DOMINANTE', 'F1', 12, 154, 2, '1', '1'),
(194, 'RIÑON POLIQUISTICO INFANTIL RECESIVO', 'RIÑON POLIQUISTICO INFANTIL RECESIVO', 'F2', 12, 154, 2, '1', '1'),
(195, 'ENFERMEDAD QUÍSTICA MEDULAR, INCLUYE NEFRONOPTISIS', 'ENFERMEDAD QUÍSTICA MEDULAR, INCLUYE NEFRONOPTISIS', 'F3', 12, 154, 2, '1', '1'),
(196, 'ESCLEROSIS TUBULAR', 'ESCLEROSIS TUBULAR', 'F4', 12, 154, 2, '1', '1'),
(197, 'NEFRITIS HEREDITARIA, SINDROME DE ALPORT', 'NEFRITIS HEREDITARIA, SINDROME DE ALPORT', 'F5', 12, 154, 2, '1', '1'),
(198, 'CISTINOSIS', 'CISTINOSIS', 'F6', 12, 154, 2, '1', '1'),
(199, 'OXALOSIS PRIMARIA', 'OXALOSIS PRIMARIA', 'F7', 12, 154, 2, '1', '1'),
(200, 'ENFERMEDAD DE FABRY', 'ENFERMEDAD DE FABRY', 'F8', 12, 154, 2, '1', '1'),
(201, 'SÍNDROME NEFRÓTICO CONGÉNITO', 'SÍNDROME NEFRÓTICO CONGÉNITO', 'F9', 12, 154, 2, '1', '1'),
(202, 'SÍNDROME DRASH,ESCLEROSIS MESANGIAL', 'SÍNDROME DRASH,ESCLEROSIS MESANGIAL', 'F10', 12, 154, 2, '1', '1'),
(203, 'OBSTRUCCIÓN CONGÉNITA DE LA UNIÓN URETEROPÉLVICA', 'OBSTRUCCIÓN CONGÉNITA DE LA UNIÓN URETEROPÉLVICA', 'F11', 12, 154, 2, '1', '1'),
(204, 'OBSTRUCCIÓN CONGÉNITA DE LA UNIÓN URETEROVESICAL', 'OBSTRUCCIÓN CONGÉNITA DE LA UNIÓN URETEROVESICAL', 'F12', 12, 154, 2, '1', '1'),
(205, 'OTRAS UROPATÍAS OBSTRUCTIVAS CONGÉNITAS', 'OTRAS UROPATÍAS OBSTRUCTIVAS CONGÉNITAS', 'F13', 12, 154, 2, '1', '1'),
(206, 'HIPOPLASIA RENAL, DISPLASIA,OLIGONEFRONÍA', 'HIPOPLASIA RENAL, DISPLASIA,OLIGONEFRONÍA', 'F14', 12, 154, 2, '1', '1'),
(207, 'SÍNDROME DEL ABDOMEN EN CIRUELA PASA', 'SÍNDROME DEL ABDOMEN EN CIRUELA PASA', 'F15', 12, 154, 2, '1', '1'),
(208, 'OTROS (SÍNDROMES DE MALFORMACIONES CONGÉNITAS)', 'OTROS (SÍNDROMES DE MALFORMACIONES CONGÉNITAS)', 'F16', 12, 154, 2, '1', '1'),
(209, 'TUMOR RENAL MALIGNO', 'TUMOR RENAL MALIGNO', 'G1', 13, 154, 2, '1', '1'),
(210, 'TUMOR MALIGNO DEL TRACTO URINARIO', 'TUMOR MALIGNO DEL TRACTO URINARIO', 'G2', 13, 154, 2, '1', '1'),
(211, 'TUMOR RENAL BENIGNO', 'TUMOR RENAL BENIGNO', 'G3', 13, 154, 2, '1', '1'),
(212, 'TUMOR BENIGNO DEL TRACTO URINARIO', 'TUMOR BENIGNO DEL TRACTO URINARIO', 'G4', 13, 154, 2, '1', '1'),
(213, 'TUMOR RENAL NO ESPECIFICADO', 'TUMOR RENAL NO ESPECIFICADO', 'G5', 13, 154, 2, '1', '1'),
(214, 'LINFOMA DE RIÑON', 'LINFOMA DE RIÑON', 'G6', 13, 154, 2, '1', '1'),
(215, 'MIELOMA MÚLTIPLE', 'MIELOMA MÚLTIPLE', 'G7', 13, 154, 2, '1', '1'),
(216, 'OTRAS NEOPLASIAS INMUNOPROLIFERATIVAS (INCLUYE NEFROPATÍA DE CADERA LIGERA)', 'OTRAS NEOPLASIAS INMUNOPROLIFERATIVAS (INCLUYE NEFROPATÍA DE CADERA LIGERA)', 'G8', 13, 154, 2, '1', '1'),
(217, 'AMILOIDOSIS', 'AMILOIDOSIS', 'G9', 13, 154, 2, '1', '1'),
(218, 'COMPLICACIONES DE ÓRGANO TRASPLANTADO NO ESPECIFICADO', 'COMPLICACIONES DE ÓRGANO TRASPLANTADO NO ESPECIFICADO', 'G10', 13, 154, 2, '1', '1'),
(219, 'COMPLICACIONES POR TRASPLANTE DE RIÑON', 'COMPLICACIONES POR TRASPLANTE DE RIÑON', 'G11', 13, 154, 2, '1', '1'),
(220, 'COMPLICACIONES POR TRASPLANTE DE HIGADO', 'COMPLICACIONES POR TRASPLANTE DE HIGADO', 'G12', 13, 154, 2, '1', '1'),
(221, 'COMPLICACIONES POR TRASPLANTE DE CORAZÓN', 'COMPLICACIONES POR TRASPLANTE DE CORAZÓN', 'G13', 13, 154, 2, '1', '1'),
(222, 'COMPLICACIONES POR TRASPLANTE DE PULMÓN', 'COMPLICACIONES POR TRASPLANTE DE PULMÓN', 'G14', 13, 154, 2, '1', '1'),
(223, 'COMPLICACIONES POR TRASPLANTE DE MÉDULA ÓSEA', 'COMPLICACIONES POR TRASPLANTE DE MÉDULA ÓSEA', 'G15', 13, 154, 2, '1', '1'),
(224, 'COMPLICACIONES POR TRASPLANTE DE PÁNCREAS', 'COMPLICACIONES POR TRASPLANTE DE PÁNCREAS', 'G16', 13, 154, 2, '1', '1'),
(225, 'COMPLICACIONES POR TRASPLANTE DE INTESTINO', 'COMPLICACIONES POR TRASPLANTE DE INTESTINO', 'G17', 13, 154, 2, '1', '1'),
(226, 'COMPLICACIONES DE OTRO TRASPLANTADO ESPECIFICADO', 'COMPLICACIONES DE OTRO TRASPLANTADO ESPECIFICADO', 'G18', 13, 154, 2, '1', '1'),
(227, 'ENFERMEDAD DE CÉLULAS FALCIFORMES/ANEMIA', 'ENFERMEDAD DE CÉLULAS FALCIFORMES/ANEMIA', 'H1', 14, 154, 2, '1', '1'),
(228, 'RASGO DE CÉLULAS FALCIFORMES Y OTRS CÉLULAS FALCIFORMES (HBS/HB OTRAS)', 'RASGO DE CÉLULAS FALCIFORMES Y OTRS CÉLULAS FALCIFORMES (HBS/HB OTRAS)', 'H2', 14, 154, 2, '1', '1'),
(229, 'FALTA RENAL POST PARTO', 'FALTA RENAL POST PARTO', 'H3', 14, 154, 2, '1', '1'),
(230, 'NEFROPATÍA POR SIDA', 'NEFROPATÍA POR SIDA', 'H4', 14, 154, 2, '1', '1'),
(231, 'PÉRDIDA TRAUMÁTICA O QUIRÚRGICA DE RIÑON', 'PÉRDIDA TRAUMÁTICA O QUIRÚRGICA DE RIÑON', 'H5', 14, 154, 2, '1', '1'),
(232, 'SÍNDROME HEPATORENAL', 'SÍNDROME HEPATORENAL', 'H6', 14, 154, 2, '1', '1'),
(233, 'NECROSIS TUBULAR (SIN RECUPERACIÓN)', 'NECROSIS TUBULAR (SIN RECUPERACIÓN)', 'H7', 14, 154, 2, '1', '1'),
(234, 'OTROS DESÓRDENES RENALES', 'OTROS DESÓRDENES RENALES', 'H8', 14, 154, 2, '1', '1'),
(235, 'ETIOLOGÍA INCIERTA', 'ETIOLOGÍA INCIERTA', 'H9', 14, 154, 2, '1', '1'),
(236, 'OTRAS NO ESPECIFICADAS (MECIONAR)', 'OTRAS NO ESPECIFICADAS (MECIONAR)', 'H10', 14, 154, 2, '1', '1'),
(237, 'TIPO DE DOCUMENTO DE IDENTIDAD', 'TIPO DE DOCUMENTO DE IDENTIDAD', '', 0, 0, 0, '1', '1'),
(238, 'DNI', 'DNI', 'D', NULL, 237, 1, '1', '1'),
(239, 'LIBRETA ELECTORAL', 'LIBRETA ELECTORAL', 'L', NULL, 237, 1, '1', '1'),
(240, 'MENOR DE EDAD', 'MENOR DE EDAD', 'X', NULL, 237, 1, '1', '1'),
(241, 'LIBRETA MILITAR', 'LIBRETA MILITAR', 'M', NULL, 237, 1, '1', '1'),
(242, 'CARTA DE GARANTÍA', 'CARTA DE GARANTÍA', 'G', NULL, 237, 1, '1', '1'),
(243, 'PASAPORTE', 'PASAPORTE', 'P', NULL, 237, 1, '1', '1'),
(244, 'RUC', 'RUC', 'R', NULL, 237, 1, '1', '1'),
(245, 'CARNET DE EXTRANJERIA', 'CARNET DE EXTRANJERIA', 'C', NULL, 237, 1, '1', '1'),
(246, 'CARNET DE EXTRANJERIA', 'CARNET DE EXTRANJERIA', 'E', NULL, 237, 1, '1', '1'),
(247, 'EQUIVOCADO', 'EQUIVOCADO', '', 0, 237, 1, '1', '1'),
(248, 'SALAS DE ATENCIÓN', 'SALAS DE ATENCIÓN', '', 0, 0, 0, '1', '1'),
(249, 'SALA 1', 'SALA 1', 'S-1', 0, 248, 1, '1', '1'),
(250, 'SALA 2', 'SALA 2', 'S-2', 0, 248, 1, '1', '1'),
(251, 'SALA 3', 'SALA 3', 'S-3', 0, 248, 1, '1', '1'),
(252, 'SALA 4', 'SALA 4', 'S-4', 0, 248, 1, '1', '1'),
(253, 'TURNOS DE ATENCIÓN ', 'TURNOS DE ATENCIÓN ', '', 0, 0, 0, '1', '1'),
(254, 'TURNO 1', 'TURNO 1', 'T1', 0, 253, 1, '1', '1'),
(255, 'TURNO 2', 'TURNO 2', 'T2', 0, 253, 1, '1', '1'),
(256, 'TURNO 3', 'TURNO 3', 'T3', 0, 253, 1, '1', '1'),
(257, 'TURNO 4', 'TURNO 4', 'T4', 0, 253, 1, '1', '1'),
(258, 'FRECUENCIA DE SESIONES', 'FRECUENCIA DE SESIONES', '', 0, 0, 0, '1', '1'),
(259, 'LUNES-MIÉRCOLES-VIERNES', 'LUNES-MIÉRCOLES-VIERNES', 'LUN-MIER-VIER', 0, 258, 0, '1', '1'),
(260, 'MARTES-JUEVES-SÁBADO', 'MARTES-JUEVES-SÁBADO', 'MART-JUEV-SAB', 0, 258, 0, '1', '1'),
(261, 'CAUSAS DE EGRESO DE AV', 'CAUSAS DE EGRESO DE AV', NULL, NULL, NULL, 0, '1', '1'),
(262, 'TROMBOSIS', 'FAV-TROMBOSIS', '1', 61, 261, 1, '1', '1'),
(263, 'DESCANSO DE FAVI', 'FAV-DESCANSO DE FAVI', '2', 61, 261, 1, '1', '1'),
(264, 'SINDROME DE ROBO', 'FAV-SINDROME DE ROBO', '3', 61, 261, 1, '1', '1'),
(265, 'CAMBIO DE FAVI X FAVI', 'FAV-CAMBIO DE FAVI X FAVI', '4', 61, 261, 1, '1', '1'),
(266, 'DISFUNCION', 'CVC-DISFUNCION', '5', 62, 261, 1, '1', '1'),
(267, 'INFECCION', 'CVC-INFECCION', '6', 62, 261, 1, '1', '1'),
(268, 'CAMBIO POR AVD', 'CVC-CAMBIO POR AVD', '7', 62, 261, 1, '1', '1'),
(269, 'OTRAS COMPLICACIONES', 'CVC-OTRAS COMPLICACIONES', '8', 62, 261, 1, '1', '1'),
(270, 'CONDICION DE INGRESO', 'CONDICION DE INGRESO', NULL, NULL, NULL, 0, '1', '1'),
(271, 'NCFAV', 'FISTULA ARTERIOVENOSA', '1', 63, 270, 1, '1', '1'),
(272, 'NCCT', 'CATETER VENOSO CENTRAL TEMPORAL', '2', 63, 270, 1, '1', '1'),
(273, 'NCCP', 'CATETER VENOSO CENTRAL PERMANENTE', '3', 63, 270, 1, '1', '1'),
(274, 'RFH', 'FAV HABITUAL', '4', 64, 270, 1, '1', '1'),
(275, 'RFN', 'FAV NUEVA', '5', 64, 270, 1, '1', '1'),
(276, 'RCPH', 'CVCP HABITUAL', '6', 64, 270, 1, '1', '1'),
(277, 'RCTPN', 'CVCP NUEVA', '7', 64, 270, 1, '1', '1'),
(278, 'RCTH', 'CVCT HABITUAL', '8', 64, 270, 1, '1', '1'),
(279, 'RCTN', 'CVCT NUEVA', '9', 64, 270, 1, '1', '1'),
(280, 'NFAV', 'FISTULA ARTERIOVENOSA', '10', 65, 270, 1, '1', '1'),
(281, 'NCT', 'CVC TEMPORAL', '11', 65, 270, 1, '1', '1'),
(282, 'NCP', 'CVC PERMANENTE', '12', 65, 270, 1, '1', '1'),
(283, 'TRANSFERENCIA EXTERNA', 'TRANSFERENCIA EXTERNA', '9', 67, 261, 1, '1', '1'),
(284, 'HOSPÍTALIZACION', 'HOSPÍTALIZACION', '10', 67, 261, 1, '1', '1'),
(285, 'DIALISIS PERITONEAL', 'DIALISIS PERITONEAL', '11', 67, 261, 1, '1', '1'),
(286, 'FALLECIMIENTO', 'FALLECIMIENTO', '12', 67, 261, 1, '1', '1'),
(287, 'TRASPLANTE', 'TRASPLANTE', '13', 67, 261, 1, '1', '1'),
(288, 'PERDIDA DE ACREDITACION', 'PERDIDA DE ACREDITACION', '14', 67, 261, 1, '1', '1'),
(289, 'OTROS (ABANDONO DE PROGRAMA)', 'OTROS (ABANDONO DE PROGRAMA)', '15', 67, 261, 1, '1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cnsr_mov_paciente`
--

CREATE TABLE `cnsr_mov_paciente` (
  `ID_MOV` int(10) NOT NULL COMMENT 'PK de la tabla',
  `COD_PACIENTE` int(10) NOT NULL COMMENT 'FK de la tabla pacientes',
  `FECHA_I` date DEFAULT NULL COMMENT 'fecha de ingreso del paciente',
  `MOTIVO_I` varchar(100) DEFAULT NULL COMMENT 'condicion de ingreso',
  `FECHA_E` date DEFAULT NULL COMMENT 'fecha de egreso del paciente',
  `MOTIVO_E` varchar(100) DEFAULT NULL COMMENT 'motivo por el que egreso',
  `USUARIO_REG` varchar(20) DEFAULT NULL COMMENT 'usuario que registra ',
  `FECHA_REG` datetime DEFAULT NULL COMMENT 'fecha que registra ',
  `USUARIO_MOD` varchar(20) DEFAULT NULL COMMENT 'usuario que modificacion el registro',
  `FECHA_MOD` datetime DEFAULT NULL COMMENT 'fecha que se modifica el registro',
  `USUARIO_BAJA` varchar(20) DEFAULT NULL COMMENT 'usuario que elimina el registro',
  `FECHA_BAJA` datetime DEFAULT NULL COMMENT 'fecha en la que se elimina el registro',
  `ESTADO` varchar(2) DEFAULT NULL COMMENT 'estado del registro 1 = NUEVO | 2= MODIF | 0= ELIMINADO',
  `CENASICOD` varchar(3) DEFAULT NULL COMMENT 'centro asistencial al que pertenece el registro ',
  `ORICENASICOD` char(1) DEFAULT NULL COMMENT 'origen del centro asistencial al que pertenece el registro '
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cnsr_mov_paciente`
--

INSERT INTO `cnsr_mov_paciente` (`ID_MOV`, `COD_PACIENTE`, `FECHA_I`, `MOTIVO_I`, `FECHA_E`, `MOTIVO_E`, `USUARIO_REG`, `FECHA_REG`, `USUARIO_MOD`, `FECHA_MOD`, `USUARIO_BAJA`, `FECHA_BAJA`, `ESTADO`, `CENASICOD`, `ORICENASICOD`) VALUES
(1, 2116, '2018-09-18', 'PRUEBA 1', '2018-09-20', 'PRUEBA', 'ESSALUD', '2018-09-21 10:45:52', 'ESSALUD', '2018-09-21 10:46:42', NULL, NULL, '2', '401', '1'),
(2, 2065, '2018-09-16', 'PRUEBA 2', '0000-00-00', '', 'ESSALUD', '2018-09-21 10:46:21', NULL, NULL, 'ESSALUD', '2018-10-02 16:58:45', '0', '401', '1'),
(3, 1, '2018-09-19', 'PRUEBA 3', '0000-00-00', '', 'ESSALUD', '2018-09-21 10:47:46', 'ESSALUD', '2018-09-21 10:47:54', NULL, NULL, '2', '401', '1'),
(4, 1116, '2018-09-03', 'PRUEBA 4', '0000-00-00', '', 'ESSALUD', '2018-09-21 10:49:27', 'ESSALUD', '2018-09-21 10:49:35', NULL, NULL, '2', '401', '1'),
(5, 4, '2018-09-23', 'PRUEBA', '0000-00-00', '', 'ESSALUD', '2018-09-21 10:49:52', NULL, NULL, NULL, NULL, '1', '401', '1'),
(6, 823, '2018-09-25', 'PRUEBA ', '0000-00-00', '', 'ESSALUD', '2018-09-21 10:50:23', NULL, NULL, NULL, NULL, '1', '401', '1'),
(7, 1116, '2018-06-13', 'PRUEBA ANTERIOR', '2018-09-02', 'PRUEBA FINAL', 'ESSALUD', '2018-09-21 10:51:06', 'ESSALUD', '2018-09-26 09:01:18', NULL, NULL, '2', '401', '1'),
(8, 1258, '2018-09-02', 'PRUEBA 1', '0000-00-00', '', 'ESSALUD', '2018-09-21 10:52:12', 'ESSALUD', '2018-09-25 14:52:25', NULL, NULL, '2', '401', '1'),
(9, 113, '2018-09-19', 'PRUEBA', '0000-00-00', '', 'ESSALUD', '2018-09-21 10:52:23', NULL, NULL, NULL, NULL, '1', '401', '1'),
(10, 669, '2018-09-10', 'PRUEBA', '0000-00-00', '', 'ESSALUD', '2018-09-21 10:52:35', NULL, NULL, 'ESSALUD', '2018-09-26 09:01:14', '0', '401', '1'),
(11, 444, '2018-09-10', 'AHORA', '0000-00-00', '', 'ESSALUD', '2018-09-21 10:52:47', NULL, NULL, NULL, NULL, '1', '401', '1'),
(12, 256, '2018-09-19', 'AHORA', '0000-00-00', '', 'ESSALUD', '2018-09-21 10:52:59', NULL, NULL, NULL, NULL, '1', '401', '1'),
(13, 256, '2018-09-17', 'AHORA-NUEVO', '0000-00-00', '', 'ESSALUD', '2018-09-21 10:53:26', NULL, NULL, NULL, NULL, '1', '401', '1'),
(14, 2143, '2011-01-17', 'PRUEBA', '0000-00-00', '', 'ESSALUD', '2018-09-21 10:53:42', 'ESSALUD', '2018-09-21 11:13:33', NULL, NULL, '2', '401', '1'),
(15, 2116, '2018-07-18', 'AHORA', '2018-08-26', 'A', 'ESSALUD', '2018-09-21 10:54:04', NULL, NULL, 'ESSALUD', '2018-09-25 14:50:47', '0', '401', '1'),
(16, 1, '2018-09-26', 'AHORA', '2018-09-21', 'FINAL', 'ESSALUD', '2018-09-21 10:54:42', 'ESSALUD', '2018-09-21 11:27:10', NULL, NULL, '2', '401', '1'),
(17, 2133, '2018-09-26', 'A', '0000-00-00', '', 'ESSALUD', '2018-09-21 10:54:54', NULL, NULL, NULL, NULL, '1', '401', '1'),
(18, 7, '2018-09-21', 'C', '0000-00-00', '', 'ESSALUD', '2018-09-21 10:56:14', NULL, NULL, NULL, NULL, '1', '401', '1'),
(19, 1258, '2018-09-25', 'C', '0000-00-00', '', 'ESSALUD', '2018-09-21 10:58:10', NULL, NULL, NULL, NULL, '1', '401', '1'),
(20, 10, '2018-09-18', 'R', '2018-09-02', 'B', 'ESSALUD', '2018-09-21 10:59:21', 'ESSALUD', '2018-09-25 14:55:17', NULL, NULL, '2', '401', '1'),
(21, 10, '2018-09-26', 'PRUEBA', '0000-00-00', '', 'ESSALUD', '2018-09-21 11:04:31', NULL, NULL, NULL, NULL, '1', '401', '1'),
(22, 5, '2018-08-26', 'PRUEBA', '0000-00-00', '', 'ESSALUD', '2018-09-21 11:06:00', 'ESSALUD', '2018-10-02 17:08:54', NULL, NULL, '2', '401', '1'),
(23, 10, '2018-09-20', 'R', '2018-09-21', 'FALLECIDO', 'ESSALUD', '2018-09-21 11:13:12', NULL, NULL, NULL, NULL, '1', '401', '1'),
(24, 10, '2018-09-03', 'PRUEBA', '0000-00-00', '', 'ESSALUD', '2018-09-21 11:19:10', NULL, NULL, NULL, NULL, '1', '401', '1'),
(25, 823, '2018-09-29', 'PRUEBA', '0000-00-00', '', 'ESSALUD', '2018-09-21 11:19:26', NULL, NULL, 'ESSALUD', '2018-09-25 14:55:26', '0', '401', '1'),
(26, 1080, '2018-09-11', 'PRUEBA', '0000-00-00', '', 'ESSALUD', '2018-09-21 11:21:05', NULL, NULL, NULL, NULL, '1', '401', '1'),
(27, 1080, '2018-09-17', 'CDFGGDFGD', '0000-00-00', '', 'ESSALUD', '2018-09-21 11:21:43', NULL, NULL, NULL, NULL, '1', '401', '1'),
(28, 113, '2018-09-04', 'PRUEBA', '0000-00-00', '', 'ESSALUD', '2018-09-21 11:22:05', NULL, NULL, NULL, NULL, '1', '401', '1'),
(29, 2116, '2018-09-06', 'PRUEBA', '0000-00-00', '', 'ESSALUD', '2018-09-21 11:22:50', 'ESSALUD', '2018-09-25 14:50:39', NULL, NULL, '2', '401', '1'),
(30, 2145, '2018-09-19', 'PRUEBA INGRESO', '0000-00-00', '', 'ESSALUD', '2018-09-21 12:44:16', NULL, NULL, 'ESSALUD', '2018-09-25 09:26:37', '0', '401', '1'),
(31, 8, '2018-08-31', 'AHORA-NUEVO', '2018-09-14', 'FTTTTTTTTTTTTTTTT', 'ESSALUD', '2018-09-21 14:33:02', 'ESSALUD', '2018-09-25 14:55:32', NULL, NULL, '2', '401', '1'),
(32, 13, '2018-08-26', 'PRUEBA FINAL F', '2018-09-21', 'R', 'ESSALUD', '2018-09-21 14:45:45', 'ESSALUD', '2018-09-21 14:46:52', NULL, NULL, '2', '401', '1'),
(33, 823, '2018-09-27', 'PRUEBA', '0000-00-00', '', 'ESSALUD', '2018-09-26 09:01:30', NULL, NULL, NULL, NULL, '1', '401', '1'),
(34, 118, '2018-10-24', '3', '2018-10-24', '2', 'ESSALUD', '2018-10-05 15:12:42', NULL, NULL, NULL, NULL, '1', '401', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cnsr_paciente`
--

CREATE TABLE `cnsr_paciente` (
  `COD_PACIENTE` int(10) NOT NULL COMMENT 'Cod. Paciente',
  `TIPO_DOC` char(1) DEFAULT NULL COMMENT 'Tipo de documento',
  `NRO_DOC` varchar(20) DEFAULT NULL COMMENT 'Nro de documento',
  `APELLIDO_PATERNO` varchar(50) DEFAULT NULL COMMENT 'Apellido paterno',
  `APELLIDO_MATERNO` varchar(50) DEFAULT NULL COMMENT 'Apellido materno',
  `NOMBRES` varchar(150) DEFAULT NULL COMMENT 'Nombres',
  `FECHA_NAC` date DEFAULT NULL COMMENT 'Fecha de nacimiento',
  `SEXO` char(1) DEFAULT NULL COMMENT 'Sexo',
  `FECHA_REG` datetime DEFAULT NULL COMMENT 'Fecha de registro',
  `USUARIO_REG` varchar(20) DEFAULT NULL COMMENT 'Usuario que registra',
  `ORICENASICOD` char(1) DEFAULT NULL COMMENT 'Origen de centro',
  `CENASICOD` varchar(3) DEFAULT NULL COMMENT 'Centro asistencial',
  `FECHA_BAJA` datetime DEFAULT NULL COMMENT 'Fecha de baja de registro',
  `USUARIO_BAJA` varchar(20) DEFAULT NULL,
  `ESTADO` char(1) DEFAULT NULL COMMENT 'Estado del registro',
  `FECHA_MOD` datetime DEFAULT NULL COMMENT 'Fecha de modificacion',
  `USUARIO_MOD` varchar(20) DEFAULT NULL COMMENT 'Usuario que modifica',
  `ACTIVO` char(2) DEFAULT NULL COMMENT 'INDICA SI EL PACIENTE ESTA ACTIVO O INACTIVO CON RESPECTO A LAS SESIONES DE HEMODIALISIS'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cnsr_paciente`
--

INSERT INTO `cnsr_paciente` (`COD_PACIENTE`, `TIPO_DOC`, `NRO_DOC`, `APELLIDO_PATERNO`, `APELLIDO_MATERNO`, `NOMBRES`, `FECHA_NAC`, `SEXO`, `FECHA_REG`, `USUARIO_REG`, `ORICENASICOD`, `CENASICOD`, `FECHA_BAJA`, `USUARIO_BAJA`, `ESTADO`, `FECHA_MOD`, `USUARIO_MOD`, `ACTIVO`) VALUES
(1, 'L', '000000', 'VILLEGAS', 'CERVANTES', 'NIEVES  ', '1997-09-20', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', '2018-09-20 12:09:35', 'ESSALUD', '1'),
(2, 'L', '07959383', 'GOMEZ', 'CAHUANA', 'ESTHER  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, '1'),
(3, 'L', '25484625', 'GALLOZA', 'GARCIA', 'DELIA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, '1'),
(4, 'D', '15439839', 'ACEVEDO', 'YUI', 'YSRAEL ALFREDO  ', '2018-09-12', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '2', '2018-09-21 11:18:11', 'ESSALUD', '1'),
(5, 'L', '000000', 'DIAZ', 'DIAZ', 'LILIA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, '1'),
(6, '', '', 'BALCAZAR', 'CHUMPITAZ', 'ELENA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, '1'),
(7, 'P', '00000', 'MORON', 'ALMEJOR', 'YHONNY  ', '2028-12-31', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', '2018-09-21 10:57:28', 'ESSALUD', '1'),
(8, 'L', '000000', 'ABANTO', 'MENDO', 'JOSE  ', '2018-08-16', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', '2018-08-29 16:41:43', NULL, '1', '2018-08-01 00:00:00', NULL, '1'),
(9, 'L', '000000', 'HURTADO', 'ZEVALLOS', 'MANUEL  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, '1'),
(10, 'P', '00000', 'NANO', 'GALARRETA', 'JONNY  ', '2018-09-05', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '2', '2018-09-21 11:18:44', 'ESSALUD', '1'),
(11, 'D', '07803638', 'ROMAN', 'NEGRI', 'GLORIA YOLANDA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, '1'),
(12, 'L', '000000', 'MELGAREJO', 'DE RODRIGUEZ ', 'ANA GREGORIA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, '1'),
(13, 'M', '000000', 'NEVES', 'HILARIO', 'YOHANA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, '1'),
(14, 'L', '29526725', 'ARANZAENS', 'MALAGA', 'ELMER JULIO ALBE  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(15, 'L', '0000000', 'DIAZ', 'YIKA', 'MERY DEL PILAR  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(16, 'L', '08236365', 'PRADO', 'PEREZ', 'ANA  ', '2022-03-04', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(17, 'L', '000000', 'VARIAS', 'PORTILLA', 'PLACIDA ESTHER  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(18, 'L', '07888107', 'SILVA', 'RETES', 'MARGARITA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(19, 'L', '07472770', 'SANTOS', 'SALVADOR', 'AMPARO  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(20, 'D', '08317818', 'SANTIAGO', 'DUEÑAS', 'HRILDO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(21, 'L', '08284622', 'LUNA', 'JARAMILLO', 'JAVIER  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(22, 'D', '15692522', 'LOPEZ', 'ABAD', 'HECTOR  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(23, 'L', '03846759', 'ATOCHE', 'BARRIENTOS', 'VICTOR MARCOS  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(24, 'D', '09187160', 'BERROCAL', 'ARZOLA', 'GERARDINA LUCILA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(25, 'D', '07933066', 'CASANOVA', 'GALVEZ', 'LUIS ALFREDO  ', '2018-08-23', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, '1'),
(26, 'L', '07678994', 'LUCERO', 'WALDE', 'CARLOS  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(27, 'L', '31603711', 'TRINIDAD', 'VEGA', 'HUGO RICARDO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(28, 'L', '092606647', 'MILLA', 'VILLEGAS', 'RUBEN SANTOS  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(29, 'D', '09077210', 'RUBIO', 'RONDAN DE CONROY', 'ROCIO  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(30, 'L', '07109497', 'PACHECO', 'BERROSPI', 'BERTHA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(31, 'L', '07323867', 'AZABACHE', 'LEZAMA', 'LUIS  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(32, 'D', '09597149', 'BERNUY', 'CHAVEZ', 'JAQUELINE  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(33, 'L', '16008813', 'AQUINO', 'MACEDO', 'ANA MARIA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(34, 'R', '07919236', 'GARCIA', 'ARANA', 'TANIA ESPERANZA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(35, 'L', '10196244', 'LEON', 'ZAMBRANO', 'YULI MAURICIA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(36, 'D', '07260510', 'ECHEVARRIA', 'PAZ', 'JOSE FERNANDO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(37, 'L', '31922524', 'OSTOS', 'ASENCIOS', 'MARIA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(38, 'L', '07450946', 'BASILIO', 'ORE', 'LOURDEZ PRIMITIVA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(39, 'L', '15387672', 'MANCO', 'HUAPAYA', 'PEDRO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(40, 'L', '06200511', 'DIAZ', 'BALAREZO FREMIOT', 'LUIS ENRIQUE ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(41, 'L', '09121944', 'VELA', 'ALVARADO', 'CROVER  ', '2024-02-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(42, 'D', '09505023', 'CELADITA', 'PAUCAR', 'ANGELICA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(43, 'D', '25667536', 'MEDINA', 'CALDERON', 'EDWIN LALDY ', '2020-05-04', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(44, 'L', '19850253', 'DELZO', 'VILLON', 'HONORATO MAXIMO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(45, 'L', '21868624', 'GOMEZ', 'VILLALTA', 'JOSE ARTURO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(46, 'D', '15725655', 'NIZAMA', 'LAGUNA', 'CESAR  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(47, 'L', '06733417', 'TORANZO', 'PEREZ', 'HERNAN OSWALDO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(48, 'D', '09779609', 'SEVILLANO', 'SORIA', 'PATRICIA GEOVANA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(49, 'L', '21862489', 'YATACO', 'TORRES', 'JULIO CESAR ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(50, 'L', '28972820', 'FRANCO', 'CUADROS', 'TELESFORO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(113, 'D', '27075410', 'ACOSTA', 'ALIAGA', 'LUIS ANTONIO ', '1997-01-22', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', '2018-09-20 12:07:07', 'ESSALUD', '1'),
(114, 'L', '10411780', 'DIAZ', 'CHAVEZ', 'GREGORIO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(115, 'L', '00000000', 'MEZA', 'OPORTO', 'CASIMIRA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(116, 'L', '07082117', 'MENDOZA', 'PALOMINO', 'MARCELINA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(117, 'L', '33330063', 'VILLAFRANCA', 'CAYA', 'ROLANDO JAVIER ', '2024-08-08', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(118, 'L', '08314106', 'ABARCA', 'QUISPE', 'MAGDALENA  ', '2018-08-07', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', '2018-08-29 16:41:52', NULL, '0', '2018-08-01 00:00:00', NULL, '1'),
(119, 'L', '24280248', 'TICONA', 'NINA', 'NELSON RIGOBERTO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(120, 'L', '08487324', 'FUNES', 'ALBURQUERQUE', 'ANA MARIA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(121, 'L', '07391287', 'CEPEDA', 'GARCIA', 'CARLOS  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(122, 'L', '06962274', 'BAHAMONDE', 'ZUÑIGA', 'ABEL TORIBIO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(123, 'L', '32871342', 'MIYAKAWA', 'LAZARTE', 'LUZ  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(124, 'L', '000000', 'NAKAMATZU', 'MIYAHIRA', 'ALEJANDRO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(125, 'L', '00000000', 'ANCALLE', 'RAMOS', 'CLAUDIA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(126, 'L', '07764404', 'CARDENAS', 'ZAVALA', 'OSCAR EDUARDO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(127, 'C', '0000000', 'PALACIOS', 'ALDEA', 'DORALISA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(128, 'L', '0406566', 'RONCEROS', 'REYES', 'SERGIO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(129, 'D', '08380247', 'CHUMPITAZ', 'RAMOS DE MIRANDA', 'LUCRECIA MATI ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(130, 'L', '09002208', 'FLORES', 'ZAPATA', 'WILFREDO OSCAR ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(131, 'L', '03624013', 'ZAPATA', 'VASQUEZ', 'BETTY LILIANA ', '2022-07-06', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(132, 'L', '07675225', 'VIDAL', 'ASENCIO', 'GREGORIO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(133, 'L', '33944083', 'ARBILDO', 'ALVA', 'MARIA DOROTEA ', '2016-01-03', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(134, 'L', '08807436', 'NUÑEZ', 'DELGADO', 'EDMUNDO LUCAS ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(135, 'L', '18839897', 'URQUIZA', 'SOLIS', 'JUSTO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(136, 'P', '1664', 'RODRIGUEZ', 'GALLEGOS', 'JUAN  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(137, 'L', '21502047', 'TOLEDO', 'LEVANO', 'CLAUDIO MANUEL ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(138, 'L', '07407470', 'KANASHIRO', 'KANASHIRO', 'VICTORIA TOYOKO ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(139, 'L', '07771850', 'OJEDA', 'SALCEDO', 'JULIO EDUARDO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(140, 'D', '09675321', 'BALDWIN', 'OLGUIN', 'WINSTON ENRIQUE ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(141, 'D', '08759982', 'GUILLEN', 'GUILLEN DE BECERRA', 'GILDA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(142, 'L', '09694190', 'RAMOS', 'JIMENEZ', 'JULIA ISABEL ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(143, 'L', '08057814', 'GIANELLA', 'GARCIA', 'ENRIQUE  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(144, 'D', '32116377', 'REYES', 'MENDOZA', 'JOSE LUIS ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(145, 'D', '07309099', 'PALOMINO', 'GOICOCHEA', 'PEDRO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(146, 'L', '08411747', 'ESPINOZA', 'HERMITAÑO', 'TARCILA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(147, 'L', '00033409', 'PORTOCARRERO', 'RIOS', 'YOLANDA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(148, 'L', '08353489', 'BURGOS', 'YERKOVICH', 'CESAR ANDRES ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(149, 'L', '00022571', 'LOPEZ', 'PAREDES', 'JUAN  ', '2018-04-08', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(150, 'L', '15596295', 'CAÑAS', 'MEZA', 'GIL YFRE ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(151, 'L', '1494809', 'AMASIFUEN', 'AREVALO', 'MANUELA  ', '2012-04-08', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(152, 'L', '07595528', 'RAMIREZ', 'GARCIA', 'DORA LUCIA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(153, 'L', '05272180', 'ROMAN', 'CABRERA', 'GILBERTO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(154, 'D', '007586778', 'BERNALES', 'LAZO', 'LUIS MIGUEL ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(155, 'L', '08019238', 'SALINAS', 'JIMENEZ', 'MODESTO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(156, 'L', '06213567', 'REYES', 'ZUÑIGA', 'FRANCISCO  ', '2021-06-07', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(157, 'D', '21257142', 'MATOS', 'COLACHAGUA', 'DOMINGO CIRIACO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(158, 'D', '15616316', 'CAVERO', 'SANTOS', 'LILIANA MARINA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(159, 'L', '06197295', 'ALCANTARA', 'LEON', 'ELSA SOLEDAD ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(160, 'D', '07606947', 'BASURTO', 'CORVERA', 'NELLY PATRICIA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(161, 'L', '06712783', 'IBAÑEZ', 'PASCUA', 'MARIO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(162, 'D', '10184739', 'AQUINO', 'HUAMAN', 'ELEODORO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(256, 'L', '07141072', 'LAZO', 'MAYHUA', 'DIONICIA  ', '2018-09-12', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', '2018-09-20 09:42:20', 'ESSALUD', '1'),
(257, 'L', '10142535', 'MORO', 'SOMMO', 'JAIME GIL ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(258, 'L', '03301172', 'RIVAS', 'ORDINOLA', 'JESUS  ', '2021-06-04', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(259, 'D', '08616060', 'ARTEAGA', 'ALCANTARA', 'JUAN SEGUNDO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(260, 'L', '06922957', 'BARDALES', 'USHIÑAHUA', 'GERMAN  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(261, 'L', '08812253', 'MIRANDA', 'ORUE', 'PEDRO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(262, 'L', '0/818815', 'AGUILAR', 'VARGAS', 'OFELIA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', '2018-09-13 10:50:34', 'ESSALUD', '3', NULL, NULL, NULL),
(263, 'L', '07550139', 'NUÑEZ', 'CASTILLO', 'LUCILA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(264, 'D', '07291771', 'LEZCANO', 'ZURITA', 'ENRIQUE FERNANDO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(265, 'L', '09533007', 'SANCHEZ', 'PARIONA', 'PETRONILA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(266, 'L', '08879103', 'GALARZA', 'BARRIONUEVO', 'ARMANDO GREGORIO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(267, 'L', '06075737', 'GUERRA', 'RAMOS', 'FELIPE HUMBERTO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(268, 'L', '9388696', 'NAVARRO', 'BALDEON', 'SIMONA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(269, 'L', '08822150', 'GAGO', 'FERNANDEZ', 'ANDRES AVELINO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(270, 'L', '15662615', 'HERRERA', 'SOTELO', 'TEOBALDO JULIO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(271, 'L', '25596987', 'DIAZ', 'GARCIA', 'VICTOR MIGUEL ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(272, 'L', '10563255', 'CASTRO', 'ACOSTA', 'JORGE ANTONIO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(273, 'D', '06186126', 'MEDRANO', 'TORRES', 'OFELIA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(274, 'D', '09060768', 'ROMAN', 'BARBOZA', 'AGUSTIN  ', '2023-11-05', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(275, 'L', '31760790', 'ROBLES', 'RODRIGUEZ', 'ALIMPIA FLORENCIA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(276, 'D', '19236534', 'ENCIZO', 'VERA', 'MAGNA ESPERANZA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(277, 'L', '08023876', 'MATTA', 'RIVERA', 'TOMAS ALEJANDRO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(278, 'L', '25690541', 'BEJAR', 'DE', 'ROJAS LUZMILA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(279, 'L', '25589709', 'ROSSI', 'HONORES', 'JOSE GREGORIO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(280, 'L', '06069667', 'ROMAN', 'BASURTO', 'LUZ  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(444, 'L', '09406988', 'ACOSTA', 'VALVERDE', 'HILDA  ', '1989-06-06', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', '2018-09-20 12:07:53', 'ESSALUD', '1'),
(445, 'L', '20408196', 'JULCAPARI', 'VILCHEZ', 'VICENCIO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(446, 'L', '07008090', 'GONZALES', 'CABALLERO', 'AURORA PETRONILA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(447, 'L', '25525819', 'AGURTO', 'GARRIDO', 'MANUEL GILBERTO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(448, 'D', '15342510', 'BUSTAMANTE', 'NAPAN', 'ALEJANDRO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(449, 'L', '07413766', 'FALCONI', 'RAMIREZ', 'MARIA AMELIA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(450, 'D', '08929315', 'MONTES', 'BARRETO', 'TEOFILA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(451, 'L', '09781765', 'MIRAMIRA', 'PARI', 'SABINA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(452, 'L', '23881286', 'ROLDAN', 'ROLDAN', 'LUISA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(453, 'L', '05590839', 'SIFUENTES', 'FLORES', 'MELANIA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(454, 'L', '06396274', 'RIVERA', 'ARREDONDO', 'LEONOR ISABEL ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(455, 'L', '00000', 'BELTRAN', 'BALTODANO', 'ELEODORO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(456, 'D', '06784367', 'BERROSPIDE', 'SALAZAR', 'JESUS  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(457, 'L', '06575893', 'SIMON', 'CANCHO', 'ALBERTO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(458, 'D', '07000143', 'URIARTE', 'VASQUEZ DE RIMACHI', 'LUCINDA  ', '2010-02-04', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(459, 'D', '08817383', 'BRAVO', 'TORREJON DE MARTINEZ', 'LILIA LEONOR ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(460, 'D', '32784013', 'CORNEJO', 'ZAMORANO', 'WALTER ARMANDO ', '2013-12-04', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(461, 'L', '08865871', 'RAMOS', 'DAVALOS', 'GUZMAN  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(462, 'D', '07566876', 'PEREZ', 'SOL', 'SOL LADY ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(463, 'L', '24966454', 'ROSA', 'PUMA', 'JENARA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(464, 'D', '09183697', 'CANAL', 'CHAMORRO', 'LUZMILA EDELMIRA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(465, 'L', '10061644', 'MARINAS', 'SANCHEZ', 'VICTOR NAPOLEON ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(466, 'L', '08487730', 'FELICIANO', 'YANAC', 'VICTOR  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(467, 'L', '09807623', 'LLANOS', 'CARRASCAL', 'PRIMITIVA TEOFILA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(468, 'L', '28130825', 'LLEMPEN', 'MALCA', 'ESCOLASTICO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(669, 'L', '05256044', 'ACOSTA', 'ROSS', 'CARLOS  ', '2018-09-07', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', '2018-09-02 23:56:41', 'ESSALUD', '1'),
(670, 'L', '10428183', 'MANRIQUE', 'CASTRO', 'FELIX ALBERTO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(671, 'L', '07057855', 'TELLO', 'MARCHAN', 'NORMA MODESTA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(672, 'D', '08155664', 'GAHONA', 'CARRION', 'SONIA ELIZABETH ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(673, 'L', '21092195', 'HUAMAN', 'BALTAZAR', 'ENRIQUETA ALINA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(674, 'D', '40915614', 'ROJAS', 'ROJAS', 'LISSETTE  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(675, 'L', '07399928', 'CARRASCO', 'NOLE', 'LUIS SALUSTIANO ', '2017-11-06', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(676, 'D', '25819391', 'MONTERO', 'PACHECO', 'EDUARDO FRANCISCO ', '2028-04-03', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(677, 'D', '05243633', 'MORI', 'MARIN', 'BENJAMIN  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(678, 'D', '10567518', 'BARRUETA', 'VILLAR', 'JUAN ENRIQUE ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(679, 'L', '224008393', 'ZEVALLOS', 'GOMEZ', 'MERCEDES  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(680, 'D', '08578774', 'YSUHUAILAS', 'ALEJO', 'EDINSON SALVADOR ', '2020-03-07', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(681, 'D', '07098353', 'BARRIGA', 'OPORTO', 'BARTOLOME ORLANDO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(682, 'L', '25448712', 'PEREZ', 'VERGARA', 'JULIA ROSALIA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(683, 'D', '08348858', 'PEREZ', 'QUISPE', 'HILARIO ARMANDO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(684, 'L', '08486436', 'ROJAS', 'YNGA', 'JEREMIAS TEODORO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(685, 'L', '23815954', 'ESCOBAR', 'DEL CASTILLO', 'NELLY ELSA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(686, 'D', '09440254', 'PAREJA', 'VILLALOBOS', 'JUANA ROSSANA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(687, 'L', '32303928', 'CHAVEZ', 'ZUZUNAGA', 'ROBINSON ISACIO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(688, 'L', '08613906', 'GARRIDO', 'DIOSES', 'OLGA VICTORIA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(689, 'L', '21525777', 'RAVINES', 'QUISPE', 'LEONILA AURORA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(690, 'D', '08623506', 'LLAUCE', 'QUIQUIA', 'JUAN FERNANDO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(691, 'L', '06665221', 'IBARRA', 'RIVADENEYRA', 'CHRISTIAN MANUEL ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(692, 'L', '03606957', 'QUISPE', 'LLACSAHUANGA', 'RICARDO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(693, 'L', '06030612', 'KU', 'DIAZ', 'DANIEL  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(694, 'L', '09602466', 'RODRIGUEZ', 'CAUTI', 'ALBERTO FRANKLIN ', '2025-04-08', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(695, 'L', '2228392', 'ACASIETE', 'RODRIGUEZ', 'LIDIA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', '2018-08-29 16:50:01', NULL, '0', NULL, NULL, NULL),
(696, 'D', '25595312', 'CASTRO', 'GAMARRA', 'RICARDO MOISES ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(697, 'D', '07770036', 'APAZA', 'MAMANI', 'IGNACIO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(698, 'D', '07950322', 'SAMANEZ', 'URRUNAGA', 'NORA ROSA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(699, 'L', '08235837', 'KASUGA', 'KASUGA', 'JOSE  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(700, 'D', '10631363', 'RIVAS', 'MELGAR', 'CINTHIA GRACE ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(701, 'L', '33331913', 'BONILLA', 'MENDEZ', 'ANTONIO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(702, 'L', '06020354', 'ANGULO', 'RIOS', 'CARMEN MARGARITA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(703, 'L', '05282538', 'MORALES', 'GONZALES', 'ERMIL AMILCAR ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(704, 'L', '06115543', 'REYES', 'QUIROZ', 'LORENZO ORLANDO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(705, 'L', '07699316', 'QUEIROLO', 'AREVALO', 'MARIO ALBERTO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(706, 'L', '09531503', 'VALVERDE', 'MENDOZA', 'TEODOCIA VICTORIA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(707, 'D', '01147494', 'SAAVEDRA', 'RENGIFO', 'TANIA MERCEDES ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(708, 'D', '08460666', 'ZUÑIGA', 'LOAYZA', 'WALTER BUENAVENTU ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(709, 'D', '03107157', 'VALLE', 'RIOS', 'ELIDA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(710, 'L', '32763283', 'ARCE', 'CRUZADO', 'MARIA ELENA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(711, 'D', '07465842', 'LAZO', 'CASIAS', 'VICTORIA LUCIA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(712, 'L', '09083505', 'VILLAVICENCIO', 'CABRERA', 'TEODORO WENSESLAO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(823, 'L', '31000815', 'ACOSTA', 'RIOS', 'ZOILO  ', '2018-09-04', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', '2018-09-02 23:56:16', 'ESSALUD', '1'),
(1030, 'D', '43542775', 'AZARTE', 'LLANCAYA', 'CLOTILDE  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1031, 'D', '10720946', 'FABIAN', 'PEÑA', 'PABLO TEOFILO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1032, 'D', '07478744', 'QUISPE', 'ASCARZA', 'LUIS ARTURO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1033, 'L', '07242346', 'CUMPA', 'MILLONES', 'MARIA CONSUELO ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1034, 'D', '08061684', 'SORIA', 'AYULO', 'RUTH ELIZABETH ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1035, 'D', 'SN', 'MENDIVIL', 'JARAMILLO', 'FELIPA ROSA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1036, 'L', '07288250', 'AMARO', 'LOPEZ', 'ARTURO WILFREDO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1037, 'L', '06737186', 'BUSTAMANTE', 'OCHOA', 'LUCIA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1038, 'L', '0083598', 'GUSHIKEN', 'NAKAGAHUA', 'MARIA MARGARITA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1039, 'D', '10346369', 'HUAMAN', 'SAJAMI', 'JUAN ENRIQUE ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1040, 'D', '19992691', 'CARDENAS', 'PE#ALOZA', 'LEONCIO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1041, 'D', '15348967', 'YATACO', 'DAVILA', 'JOSE ORLANDO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1042, 'D', '09537692', 'CHEPPE', 'RODRIGUEZ', 'GIOVANA NATALIA ', '2026-08-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1043, 'D', '15665478', 'ZUMARAN', 'PALOMINO', 'FLORA ELCIRA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1044, 'D', '06781329', 'YLLESCAS', 'MINCHEZ', 'MIGUEL ANGEL ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1045, 'L', '08101728', 'LUCIO', 'ORELLANA', 'MYRIAM LUZ ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1046, 'D', '06155985', 'ROSAS', 'GONZALES', 'LUIS ANGEL ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1047, 'D', '07810922', 'MOSQUERA', 'DEPAZ', 'RAUL VIAMEY ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1048, 'L', '07334227', 'RODRIGUEZ', 'PASTOR', 'ELSA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1049, 'L', '10205713', 'ARIAS', 'DE DEL RIO', 'MELCHORA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1050, 'L', '07213101', 'ANDRADE', 'VARGAS', 'MARIA ROSALINA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1051, 'D', '08817288', 'SERPA', 'HERRERA', 'PIO ELEODORO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1052, 'D', '44019939', 'CABREJOS', 'CORDOVA', 'JOSE LENIN ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1053, 'D', '06709023', 'CLAROS', 'PONCE DE MELLADO', 'GLORIA MARLENE ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1054, 'L', '06711948', 'MEDINA', 'PEDROZA', 'NORMA LUCIA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1055, '*', 'S/N', 'MUCHA', 'GAMARRA', 'CLARA  ', '2014-10-03', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1056, 'L', '00000000', 'CARRILLO', 'DIAZ', 'CESAR VICENTE ', '2023-04-01', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1057, 'D', '07232967', 'VERNE', 'VELAOCHAGA', 'MARIANA HORTENCIA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1058, 'D', '07961906', 'MEJIA', 'CARBONEL', 'BENJAMIN ENRIQUE ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1059, 'L', '21787377', 'PORTILLA', 'MORON', 'DELIA ROSA ', '2020-02-05', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1060, 'D', '08860591', 'ALCANTARA', 'PALOMINO', 'JORGE LEONARDO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1061, 'D', '01094250', 'RENGIFO', 'RENGIFO', 'ASTOLFO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1062, 'D', '08569110', 'BERRIOS', 'TENORIO', 'SALOMON  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1063, 'L', '03846220', 'LOPEZ', 'SAAVEDRA', 'FELIPE  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1064, 'D', '05316418', 'RUCOBA', 'DEL CASTILLO', 'JUAN LUIS ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1065, 'D', '08748563', 'FAUSTOR', 'AVALOS', 'MARIA ELENA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1066, 'D', '06843100', 'BARRIOS', 'TAPIA', 'MARIA LUZ ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1067, 'D', '07152963', 'AGUIRRE', 'ROJAS DE PEREZ', 'GRIMALDA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', '2018-09-13 10:51:21', 'ESSALUD', '3', NULL, NULL, NULL),
(1068, 'L', '08507959', 'QUIROZ', 'CASTRO', 'RUFINA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1069, 'D', '08493839', 'RAMOS', 'BAZAN DE RIVERA', 'MARGARITA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1070, 'D', '07490177', 'TORRES', 'MEJIA', 'LIZANDRO ROBERTO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1071, 'D', '15982313', 'CAHUAS', 'RAMIREZ', 'PABLO CESAR ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1072, 'D', '06939544', 'CUEVA', 'BLANCO', 'AQUINO CROVER ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1073, 'D', '04017409', 'HINOSTROZA', 'ROQUE', 'GUZMAN  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1074, 'L', '10292917', 'BENITES', 'GUILLEN', 'ROSARIO  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1075, 'D', '07586740', 'MILLONES', 'CORNELIO', 'FAUSTINO VALERIANO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1076, 'D', '06842442', 'RETUERTO', 'ALLAUCA', 'GERONIMO VICTOR ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1077, 'D', '06958102', 'HURTADO', 'TITO DE SANCHEZ', 'ASUNTA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1078, 'D', '09021543', 'COSME', 'MUÑOZ DE EGUSQUIZA', 'EMILIA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1079, 'D', '07986402', 'GRADOS', 'URETA', 'RUFINO  ', '2010-07-03', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1080, 'L', '8549708', 'ACUÑA', 'LIÑAN', 'ELENA AURORA ', '2018-09-10', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '2', '2018-09-21 11:20:41', 'ESSALUD', '1'),
(1081, 'D', '15287244', 'ROJAS', 'OLIVARES', 'JUAN  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1082, 'L', '09514809', 'LOAYZA', 'ACOSTA', 'JULIO CESAR ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1083, 'D', '06546471', 'CARRASCO', 'ULLOA', 'MAXIMIANO ALCEDO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1084, 'D', '09171749', 'QUINTANA', 'LLACZA', 'NELLY BERTHA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1085, 'L', '3467979', 'AYALA', 'LORO', 'OLINDA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1086, 'L', '03333345', 'SANDOVAL', 'REYES', 'CRUZ MARIA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1087, 'D', '06683583', 'ARIZAGA', 'OLIVARES', 'MARGARITA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1088, 'D', '18847789', 'CORTEZ', 'DE', 'MATARA LUCILA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1089, 'D', '22410080', 'LUCAS', 'BETETA', 'FRANCISCO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1090, 'D', '19919741', 'PINEDA', 'ARIAS', 'AUGUSTO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1091, 'L', '8496850', 'CALLIRGOS', 'OCAÑA', 'GENARA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1092, 'D', '25466113', 'HERNANDEZ', 'MALCA', 'ESTANISLAO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1093, 'D', '15755618', 'OCAÑA', 'PINEDO', 'ALFONSO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1094, 'L', '15976380', 'DONAYRE', 'ARANDA', 'VICTORIA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1095, 'D', '06866187', 'FAUSTINO', 'CORNELIO', 'VICENTA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1096, 'L', '15386417', 'VELASQUEZ', 'ORE', 'TEOBALDO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1097, 'D', '15647974', 'VALLADARES', 'NICHO', 'GINA MEDALIT ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1098, 'L', '07354110', 'DEL CARPIO', 'NICOLETTI', 'DORIS ROSA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1099, 'C', '55', 'FAN', 'HON', 'YEUNG  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1116, 'L', '09721184', 'ACEVEDO', 'ORMEÑO', 'SARA CECILIA ', '1997-09-14', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '2', '2018-09-21 11:03:16', 'ESSALUD', '1'),
(1117, 'D', '07249247', 'SERRANO', 'FLORES', 'ALFREDO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1118, 'D', '10472654', 'CONCHA', 'GARCIA', 'HERNAN ANTONIO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1119, 'D', '19910221', 'AMAO', 'CHIRA DE ZORRILLA', 'ROSARIO MARIA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1120, 'L', '08478219', 'GODOS', 'SANCHEZ', 'JULIA MARIA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1121, 'D', '25601446', 'CAVERO', 'DE CARRILLO', 'VILMA CARMEN ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1122, 'L', '07718822', 'PUERTAS', 'PUERTAS', 'FERNANDO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1123, 'L', '07852450', 'GONZALES', 'GUERRERO', 'HECTOR ROBERTO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1124, 'D', '41131672', 'BERROSPI', 'ALVA', 'GUILLERMO GUSTAVO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1125, 'L', '21277866', 'NAUPARI', 'GOYAS', 'PAULINA SALOME ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1258, 'D', '70169807', 'ACEVEDO', 'PERALTA', 'BRAN DRAIN ', '2018-09-04', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', '2018-09-13 20:01:23', 'ESSALUD', '1'),
(1259, 'D', '25756196', 'MURRIETA', 'GONZALES', 'NESLANDER  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1260, 'L', '05229577', 'BARCENA', 'GARCIA', 'NICANOR ARMANDO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1261, 'D', '21420537', 'CARHUAYO', 'AYALA', 'LEONIDAS OCTAVIO ', '2021-00-02', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1262, 'D', '07784102', 'LEVANO', 'ARENAZA', 'VICTOR ERNESTO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1263, 'L', '07476918', 'MURIAS', 'LEON', 'GISELLE CONSUELO ', '2026-01-06', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1264, 'X', '8805050', 'PARRA', 'DONATO', 'PAMELA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1265, 'D', '09389636', 'CAMPOS', 'EGOAVIL DE ESPINOZA', 'MARIA TERESA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1266, 'L', '158', 'OLIVERA', 'VALENCIA', 'MELCHORA EMILIA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1267, 'D', '06589810', 'VIDAL', 'RAMOS', 'WILDER EGO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1268, 'D', '10004990', 'MUÑOZ', 'FERNANDEZ', 'LUZ  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1269, 'D', '09663001', 'HENRIQUEZ', 'IPARRAGUIRRE', 'ALVARO ALBERTO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1270, 'X', '1935', 'TORREBLANCA', 'NUÑEZ', 'RICARDO JOSE ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1271, 'D', '06599720', 'CHUNG', 'GAMONAL', 'ALEJANDRO RONALD ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1272, 'L', '40192076', 'PARRAGUEZ', 'ARBI', 'LUCY HAYDEE ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1273, 'L', '07906182', 'KANASHIRO', 'KANASHIRO', 'PEDRO MIGUEL ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1274, 'D', '10391359', 'VILLOSLADA', 'SANCHEZ', 'EDMUNDO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1275, 'D', '40046847', 'RABANAL', 'CUADROS', 'GUIDO GUILIANO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1276, 'L', '09175775', 'TAKAEZU', 'NIZATO', 'ALBERTO  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1277, 'D', '08730961', 'MACHICAO', 'VARGAS', 'VICTOR RAUL ', '2013-10-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1278, 'L', '08803055', 'INFANZON', 'QUICAÑO', 'LUZMILA DOMITILA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1279, 'D', '10019343', 'CAMPOS', 'PINARES', 'PEDRO ISAI ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1280, 'L', '07705884', 'NAKAHODO', 'HIGA', 'LORENZO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1281, 'D', '30423128', 'QUISPE', 'PEREZ', 'HUGO VICENTE ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1282, 'D', '40602601', 'VARGAS', 'BAIOCCHI', 'RAY ABDON ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1283, 'L', '08334141', 'CUNIA', 'PEÑA', 'MARGARITA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1284, 'L', '10193945', 'SIMEON', 'RODRIGUEZ', 'LAZARO FLORIANO ', '2020-07-05', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1285, 'D', '06224580', 'ARIAS', 'CASTRO', 'KETTY ELIZABETH ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1286, 'L', '10310146', 'ALBURQUEQUE', 'QUISPE', 'ALEJANDRO ALBERTO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1287, 'D', '10650638', 'CHANCAFE', 'BARRIOS', 'HUBERT SANTOS ', '2028-06-05', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1288, 'D', '41577681', 'GOMEZ', 'SERRANO', 'MIRTHA NIEVES ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1289, 'D', '06786269', 'GUTIERREZ', 'ROBLES', 'JUAN JAVIER ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1290, 'L', '07186622', 'CORDOVA', 'ACUÑA', 'JESUS FORTUNATO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1291, 'D', '07760093', 'DIAZ', 'DAVILA', 'NICOLAS  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1292, 'D', '42474629', 'MAGALLANES', 'VILLA', 'SANDRA LUCIA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1293, 'D', '32131292', 'BARRERA', 'RODRIGUEZ', 'OMAR JULIO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL);
INSERT INTO `cnsr_paciente` (`COD_PACIENTE`, `TIPO_DOC`, `NRO_DOC`, `APELLIDO_PATERNO`, `APELLIDO_MATERNO`, `NOMBRES`, `FECHA_NAC`, `SEXO`, `FECHA_REG`, `USUARIO_REG`, `ORICENASICOD`, `CENASICOD`, `FECHA_BAJA`, `USUARIO_BAJA`, `ESTADO`, `FECHA_MOD`, `USUARIO_MOD`, `ACTIVO`) VALUES
(1294, 'D', '10207161', 'COLQUE', 'FUENTES', 'JOEL RONALD ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1295, 'L', '001  10516', 'TORIBIO', 'DE ARMAS', 'MARIA ELENA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1296, 'D', '07729357', 'VASQUEZ', 'TORRES', 'LUCIANO  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1297, 'D', '06949556', 'HUAPAYA', 'ROMERO DE MAURICIO', 'ANGELICA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1298, 'D', '10602778', 'BALDEON', 'GALLO', 'KATHERINE CHRISTINE ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1299, 'L', '00850222', 'FONSECA', 'ALEGRIA', 'ORESTES  ', '2014-00-09', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1300, 'L', '25679166', 'MANRIQUE', 'VARGAS', 'LUIS BENJAMIN ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1301, 'L', '10459371', 'RAMOS', 'YLLESCAS', 'GINA MIRIAM ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1302, 'D', '08463041', 'FALCONI', 'REVILLA', 'MANUEL ROBERTO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1303, 'D', '07901624', 'CASTILLO', 'JUSTO', 'JORGE ROLANDO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1304, 'D', '08820184', 'REATEGUI', 'LUJAN', 'TERESA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1305, 'L', '0000', 'FERNANDEZ', 'HIDALGO VDA DE COCK', 'MARIA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1306, 'D', '06116528', 'DIAZ', 'PIEROLA', 'JULIA GRACIELA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1307, 'L', '10668681', 'TEMBLADERA', 'LOPEZ', 'YENY  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1308, 'D', '06839860', 'SANCHEZ', 'RODRIGUEZ', 'LEOPOLDO ZACARIAS ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1309, 'D', '09026736', 'MEJIA', 'DIAZ', 'AURELIA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1310, 'L', '15604164', 'ALOR', 'MARIN', 'EUMELIA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1311, 'D', '08262046', 'FAJARDO', 'VALDIVIESO', 'PABLO HUMBERTO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1312, 'D', '05300055', 'GUIBORD', 'LEVESQUE', 'LORENZO RODOLFO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1313, 'D', '07255137', 'CARDENAS', 'ALVAREZ', 'JOEL JAIME ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1314, 'D', '41998180', 'ANGELES', 'ANGULO', 'ARTURO  ', '2019-08-08', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1315, 'D', '07790290', 'MORENO', 'NEGLIA', 'CARLOS DUMMEL ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1316, 'D', '21823875', 'SANCHEZ', 'CUETO', 'SILVIA CECILIA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1317, 'D', '09385710', 'CASASOLA', 'BERNAOLA', 'OMAR IVAN ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1318, 'D', '25791735', 'KISHIMOTO', 'KISHIMOTO', 'TOMAS  ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1319, 'D', '09455143', 'JUAREZ', 'VILLALTA', 'MARLENE GISELLE ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1320, 'D', '08209068', 'LATORRE', 'PINO', 'GASTON FABRIZIO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1321, 'D', '09799016', 'INOCENTE', 'CHILET', 'RICHARD ALEXANDER ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1322, 'D', '25506373', 'CRIADO', 'DE', 'SALCEDO JULIA ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1323, 'L', '08464638', 'FERNANDEZ', 'MALLQUI', 'ROSA ISABEL ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1324, 'D', '09641033', 'VEGA', 'PEREZ', 'JUAN FRANCISCO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1325, 'D', '06921339', 'CARRERA', 'VDA DE SERRANO', 'FLORA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1326, 'D', '25759120', 'LLAMO', 'LUNA', 'MANUEL ANTONIO ', '2027-02-06', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1327, 'L', '15678063', 'VISITACION', 'ROBLES', 'JAVIER ROLANDO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1328, 'D', '10619837', 'RIOS', 'PANDURO', 'LENY  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1329, 'D', '25446245', 'LEUREYROS', 'PEREZ', 'MOISES JESUS ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1330, 'L', '25719973', 'GARCIA', 'MARUCO', 'EDUARDO SAMUEL ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1331, 'D', '08235916', 'FREYRE', 'CEVASCO', 'ROSARIO CLARISA ', '2016-00-01', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1332, 'L', '00083470', 'FLORES', 'MORENO', 'HERNANDO OMAR ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1333, 'D', '25623863', 'ESPINOZA', 'ALTAMIRANO', 'ALICIA  ', '0000-00-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1334, 'D', '08111493', 'VENEGAS', 'QUISPE', 'CARLOS GUSTAVO ', '0000-00-00', 'M', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(1335, 'D', '42821840', 'ZUBIATE', 'PRIETO', 'JACQUELINE MILAGROS ', '2031-04-00', 'F', '2018-04-04 00:00:00', 'CJUNCO', '1', '401', NULL, NULL, '1', NULL, NULL, NULL),
(2065, 'D', '75285727', 'DE LA CRUZ', 'ROJAS', 'EDUARDO', '1997-12-22', 'M', '2018-06-30 00:00:00', 'CJUNCO', '1', '401', '2018-07-19 00:00:00', NULL, '1', '2018-09-03 15:13:13', 'PEREZ', '1'),
(2111, 'D', '70058459', 'PEREZRTr', 'ASTORTr', 'ANGELATr', '1997-01-22', 'F', '2018-06-30 00:00:00', NULL, NULL, '001', '2018-07-19 00:00:00', NULL, '1', '2018-07-16 00:00:00', NULL, NULL),
(2112, 'L', 'fgfg', 'bub', 'b', 'bb', '2018-07-19', 'F', '2018-07-19 00:00:00', NULL, NULL, '401', '2018-07-19 00:00:00', NULL, '1', '2018-07-19 00:00:00', NULL, NULL),
(2113, 'L', '70058859', 'PEREZASTOO', 'asro', 'ANGELA THALIA', '2018-07-06', 'F', '2018-07-19 00:00:00', NULL, '1', '001', '2018-07-19 00:00:00', NULL, '1', '2018-09-03 15:13:16', 'PEREZ', '1'),
(2114, 'D', 'yuytu', 'PEREZASTOO', 'ASTOEE', 'thalia', '2018-07-10', 'F', '2018-07-19 00:00:00', NULL, '1', '001', '2018-07-19 00:00:00', NULL, '0', NULL, NULL, NULL),
(2115, 'D', '77777777777777777777', 'tyuty', 'utyuty', 'utyuyt', '2018-07-11', 'F', '2018-07-19 00:00:00', NULL, '1', '001', '2018-07-19 00:00:00', NULL, '0', '2018-07-19 00:00:00', NULL, NULL),
(2116, 'X', '70058459', 'PEREZ', 'ASTO', 'ANGELA THALIA', '2018-07-17', 'F', '2018-07-20 00:00:00', NULL, '1', '401', NULL, NULL, '1', '2018-07-20 00:00:00', NULL, '1'),
(2117, 'L', '70058459', 'PEREZASTOO', 'asro', 'EDUARDO', '2018-07-25', 'M', '2018-07-20 00:00:00', NULL, '1', '401', '2018-07-20 00:00:00', NULL, '0', '2018-07-20 00:00:00', NULL, '1'),
(2118, 'X', '6', 'J', 'J', 'J', '2018-07-13', 'F', '2018-07-20 00:00:00', NULL, '1', '002', '2018-07-20 00:00:00', NULL, '0', '2018-07-20 00:00:00', NULL, '1'),
(2119, 'D', '70058859', 'peress', 'ASTOEE', 'EDUARDO', '2018-07-07', 'M', '2018-07-20 00:00:00', NULL, '1', '002', NULL, NULL, '1', '2018-07-23 00:00:00', NULL, '1'),
(2120, 'L', '11111111111111111111', 'peress', 'asro', 'EDUARDO', '2018-07-18', 'M', '2018-07-20 00:00:00', NULL, '1', '002', '2018-07-23 00:00:00', NULL, '0', NULL, NULL, '1'),
(2121, 'L', '4444444444444444', 'PEREZASTOO', 'ASTOEE', 'EDUARDO', '2018-07-07', 'F', '2018-07-20 00:00:00', NULL, '1', '001', '2018-07-23 00:00:00', NULL, '0', '2018-07-23 00:00:00', NULL, '1'),
(2122, 'D', '33333', 'PEREZASTOO', 'ASTOEE', 'ANGELA THALIA', '2018-07-06', 'F', '2018-07-23 00:00:00', NULL, '1', '005', '2018-07-23 00:00:00', NULL, '0', NULL, NULL, '1'),
(2126, 'X', '70058859', 'peress', 'ASTOEE', 'ANGELA THALIA', '2018-08-06', 'M', '2018-08-03 00:00:00', '', '1', '005', NULL, NULL, '1', NULL, NULL, '1'),
(2130, 'D', '862625', 'PEREZASTOO', 'asro', 'ANGELA THALIA', '2018-08-02', 'M', '2018-08-08 00:00:00', '', '1', '234', NULL, NULL, '1', NULL, NULL, '1'),
(2131, 'D', '89269', 'PEREZASTOO', 'J', 'P', '2018-08-15', 'F', '2018-08-15 00:00:00', NULL, '1', '345', NULL, NULL, '1', NULL, NULL, '1'),
(2133, 'D', '70033597', 'PEREZ', 'AAAAA', 'J', '2018-08-09', 'M', '2018-08-20 00:00:00', NULL, '1', '401', NULL, NULL, '1', NULL, NULL, '1'),
(2135, 'D', '65345343', 'ABT', 'THSLIS', 'THSLIAS', '2018-08-07', 'M', '2018-08-29 16:32:17', NULL, '1', '401', '2018-08-29 16:44:24', NULL, '0', NULL, NULL, '1'),
(2141, 'D', '70032510', 'ABANTO', 'ASTO', 'ROCIO', '1969-11-13', 'F', '2018-09-03 15:13:06', 'PEREZ', '1', '001', NULL, NULL, '1', NULL, NULL, '1'),
(2142, 'L', '965747', 'ANGELA', 'ANGELA', 'ANGELA', '2018-09-11', 'F', '2018-09-11 11:51:35', 'ESSALUD', '1', '401', NULL, NULL, '2', '2018-09-11 11:52:55', 'ESSALUD', '1'),
(2143, 'D', '70033584', 'MENDOZA', 'PILCO', 'MIRIAN', '2000-07-03', 'F', '2018-09-20 09:24:27', 'ESSALUD', '1', '401', NULL, NULL, '1', NULL, NULL, '1'),
(2144, 'D', '706396', 'PEREZ', 'CHOQUE', 'CIPRIANO', '1960-09-16', 'M', '2018-09-21 11:32:30', 'ESSALUD', '1', '401', NULL, NULL, '2', '2018-09-21 12:00:56', 'ESSALUD', '1'),
(2145, 'D', '70032698', 'ACEVEDO', 'ASTO', 'ANGELA', '1986-06-11', 'F', '2018-09-21 12:02:43', 'ESSALUD', '1', '401', '2018-09-21 14:39:59', 'ESSALUD', '3', NULL, NULL, '2'),
(2146, 'D', '3432234', 'ABANTO', 'ASTO', 'ANGEL', '1992-03-03', 'M', '2018-09-21 15:04:27', 'ESSALUD', '1', '401', NULL, NULL, '1', NULL, NULL, '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cnsr_profesional`
--

CREATE TABLE `cnsr_profesional` (
  `COD_PROFESIONAL` int(10) NOT NULL COMMENT 'Cod. Profesional',
  `TIP_DOC` char(1) DEFAULT NULL COMMENT 'Tipo de documento',
  `NUM_DOC` varchar(20) DEFAULT NULL COMMENT 'Numero de documento',
  `APELLIDO_PAT` varchar(100) DEFAULT NULL COMMENT 'Apellido paterno',
  `APELLIDO_MAT` varchar(100) DEFAULT NULL COMMENT 'Apellido materno',
  `NOMBRES` varchar(150) DEFAULT NULL COMMENT 'Nombres',
  `COD_PLANILLA` varchar(20) DEFAULT NULL COMMENT 'Codigo de planilla',
  `CEEP` varchar(20) DEFAULT NULL COMMENT 'Codigo especialidad',
  `REE` varchar(20) DEFAULT NULL COMMENT 'Registro de especialidad',
  `GRUPO` varchar(20) DEFAULT NULL COMMENT 'Grupo ocupacional',
  `FECHA_REG` datetime DEFAULT NULL COMMENT 'Fecha de registro',
  `USUARIO_REG` varchar(20) DEFAULT NULL COMMENT 'Usuario que registra',
  `ORICENASICOD` char(1) DEFAULT NULL COMMENT 'Origen de centro',
  `CENASICOD` varchar(3) DEFAULT NULL COMMENT 'Centro asistencial',
  `FECHA_BAJA` datetime DEFAULT NULL COMMENT 'Fecha de baja del registro',
  `ESTADO` char(1) DEFAULT NULL COMMENT 'Estado del registro',
  `FECHA_MOD` datetime DEFAULT NULL COMMENT 'Fecha de modificacion del registro',
  `USUARIO_MOD` varchar(20) DEFAULT NULL COMMENT 'Usuario que modifica el registro',
  `USUARIO_BAJA` varchar(20) DEFAULT NULL COMMENT 'USUARIO QUE DA DE BAJA '
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cnsr_profesional`
--

INSERT INTO `cnsr_profesional` (`COD_PROFESIONAL`, `TIP_DOC`, `NUM_DOC`, `APELLIDO_PAT`, `APELLIDO_MAT`, `NOMBRES`, `COD_PLANILLA`, `CEEP`, `REE`, `GRUPO`, `FECHA_REG`, `USUARIO_REG`, `ORICENASICOD`, `CENASICOD`, `FECHA_BAJA`, `ESTADO`, `FECHA_MOD`, `USUARIO_MOD`, `USUARIO_BAJA`) VALUES
(1, 'D', '70033597', 'COLLADO', 'VARGAS', 'OMAR', '4252628666', '3452666', '6597666', 'ENFERMERA', '2018-04-04 00:00:00', 'CJUNCO', '1', '002', '2018-08-24 11:58:25', '1', '2018-08-21 12:40:03', 'CJUNCO', NULL),
(2, 'D', '7000226', 'LINARES', 'COLLADO', 'MANUEL', '12888888888', '88888888', '888888888', 'MÉDICO', '2018-04-24 00:00:00', 'CJUNCO', '1', '401', '2018-09-02 19:39:56', '1', '2018-08-22 12:27:47', 'CJUNCO', 'Array'),
(3, 'D', '07973782', 'PEREZ', 'JUAREZ', 'JUANA INES', '4230431', '1234', '1234', 'MÉDICO', '2018-04-30 00:00:00', 'CJUNCO', '1', '401', '2018-09-02 18:29:03', '1', '2018-09-19 14:11:14', 'ESSALUD', '401'),
(4, 'L', '4424527', 'PEREZ', 'ASTO', 'ANGELA THALIA', '1', '4525', '727', 'ENFERMERA', '2018-07-24 00:00:00', NULL, '1', '002', '2018-08-24 12:08:30', '1', '2018-08-21 12:21:25', NULL, 'PEREZ AS'),
(5, 'D', '895666', 'PORRAS ', 'COSSI', 'NATALY', '1', '4525', '45452', 'MÉDICO', '2018-07-24 00:00:00', NULL, '1', '401', '2018-09-02 19:48:00', '1', '2018-08-21 12:22:07', NULL, 'Array'),
(6, 'D', '5555', 'tp', 'PIPI', 'ANGELA', '12424', '4525', '45452', '444', '2018-07-24 00:00:00', NULL, '1', '002', '2018-07-24 00:00:00', '1', NULL, NULL, NULL),
(7, 'D', '78952', 'VEGA', 'SILVA ', 'CRISTIAN', '5936223', '4525', '45452', 'MÉDICO', '2018-08-08 00:00:00', NULL, '1', '401', '2018-09-02 19:36:52', '1', '2018-08-21 12:21:52', NULL, 'Array'),
(8, 'D', '89566685974', 'ASTO', 'LLANOS', 'JUANA EMERITA', '75423948', '345', '456', 'MÉDICO', '2018-09-02 18:31:45', NULL, '1', '005', '2018-09-02 19:24:03', '1', NULL, '', 'PEREZ AS'),
(9, 'D', '565454', 'VILLA', 'GOME', 'CARLOS', '35232', '256', '595', 'ENFERMERA', '2018-09-02 19:26:07', 'PEREZ ASTO', '1', '005', '2018-09-02 19:28:12', '1', '2018-09-02 19:26:20', 'PEREZ ASTO', 'PEREZ ASTO'),
(10, 'D', '58964', 'PEREZ', 'LLANOS', 'ANGELA', '3218', '878', '887', 'MÉDICO', '2018-09-02 19:31:44', 'PEREZ ASTO', '1', '005', NULL, '1', NULL, NULL, NULL),
(11, 'D', '82636', 'DIAZ', 'MARTEL', 'ANA', '534', '9', '99', 'ENFERMERA', '2018-09-02 19:32:58', 'PEREZ ASTO ANGELA TH', '1', '401', '2018-09-02 20:21:04', '1', '2018-09-21 09:35:45', 'ESSALUD', 'ESSALUD'),
(12, 'D', '488282', 'PEREZ', 'RODIRGUE', 'ANA', '792', '5115', '555', 'MÉDICO', '2018-09-02 20:15:53', 'ESSALUD', '1', '401', '2018-09-02 20:20:46', '1', '2018-09-19 14:11:23', 'ESSALUD', 'ESSALUD'),
(13, 'D', '0021563', 'OCHOA', 'BARRIOS', 'HERNAN', '56239', '254', '254', 'ENFERMERA', '2018-09-03 15:14:42', 'PEREZ', '1', '001', NULL, '1', NULL, NULL, NULL),
(14, 'M', '2456978', 'PEREZ', 'LLANOS', 'VERONICA', '673433', '22222', '25252', 'ENFERMERA', '2018-09-11 17:36:41', 'ESSALUD', '1', '401', NULL, '1', NULL, NULL, NULL),
(15, 'D', '8239', 'PEREZ', 'LLANOS', 'ANGELA', '75423948', '22222', '25252', 'MÉDICO', '2018-09-17 13:12:47', 'ESSALUD', '1', '401', NULL, '1', NULL, NULL, NULL),
(16, 'M', '454232', 'DIAZ', 'MARTELO', 'ANA', '676', '22222', '25252', 'ENFERMERA', '2018-09-17 13:13:11', 'ESSALUD', '1', '401', NULL, '1', NULL, NULL, NULL),
(17, 'D', '7854353', 'VILLA', 'GOMEZ', 'ANGELA', '75423948', '22222', '25252', 'MÉDICO', '2018-09-17 13:13:37', 'ESSALUD', '1', '401', NULL, '1', NULL, NULL, NULL),
(18, 'M', '342313', 'PEREZ', 'MARTELO', 'THSLIAS', '75423948', '22222', '25252', 'MÉDICO', '2018-09-17 13:13:55', 'ESSALUD', '1', '401', NULL, '1', NULL, NULL, NULL),
(19, 'M', '160066333', 'PEREZ', 'MARTELO', 'ANA', '534', '454', '454', 'MÉDICO', '2018-09-17 13:14:19', 'ESSALUD', '1', '401', NULL, '1', NULL, NULL, NULL),
(20, 'D', '78787', 'P', 'P', 'P', '99', 'OO', 'OO', 'ENFERMERA', '2018-09-21 09:36:01', 'ESSALUD', '1', '401', NULL, '1', NULL, NULL, NULL),
(21, 'D', '4400364000', 'GONZALES', 'GOMEZ', 'GIOVANNA', '56239', '44979', '', 'ENFERMERA', '2018-09-21 09:39:26', 'ESSALUD', '1', '401', NULL, '1', '2018-09-21 09:39:41', 'ESSALUD', NULL),
(22, 'D', '70033510', 'PEREZ', 'ASTO', 'ANGELA', '', '4525', '', 'ENFERMERA', '2018-09-21 09:42:13', 'ESSALUD', '1', '401', NULL, '1', NULL, NULL, NULL),
(23, 'D', '3423423', 'W', 'W', 'W', '', '213232', '', 'ENFERMERA', '2018-09-21 09:42:41', 'ESSALUD', '1', '401', NULL, '1', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cnsr_valfisica_fistula`
--

CREATE TABLE `cnsr_valfisica_fistula` (
  `PK_EVALUACION` int(10) NOT NULL COMMENT 'Indice de evaluacion mismo que el de evaluaciones',
  `TEST_ELEVACION` varchar(20) DEFAULT NULL COMMENT 'Test de elevacion',
  `TEST_AUM_PULSO` varchar(20) DEFAULT NULL COMMENT 'Test aumento de pulso',
  `TEST_VENAS_COLAT` varchar(20) DEFAULT NULL COMMENT 'Test venas colaterales',
  `FC_DISTANCIA` varchar(20) DEFAULT NULL COMMENT 'Factor clínico Distancia',
  `FC_CARACTERISTICA` varchar(20) DEFAULT NULL COMMENT 'Factor clínico caracteristica'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cnsr_valfisica_fistula`
--

INSERT INTO `cnsr_valfisica_fistula` (`PK_EVALUACION`, `TEST_ELEVACION`, `TEST_AUM_PULSO`, `TEST_VENAS_COLAT`, `FC_DISTANCIA`, `FC_CARACTERISTICA`) VALUES
(1, '20', '20', '20', '20', '20'),
(2, '11', '16', '500', '17', '90'),
(3, '09', '18', '205', '105', '17'),
(4, '13', '11', '08', '15', '11'),
(5, '15', '09', '10', '14', '10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_grupos`
--

CREATE TABLE `user_grupos` (
  `idGrupo` int(10) NOT NULL COMMENT 'ID CORRE GRUPO',
  `NombreGrupo` varchar(20) NOT NULL,
  `EstadoGrupo` char(2) NOT NULL,
  `EstadoReg` char(1) DEFAULT NULL COMMENT 'estado del registro'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tipo de usuarios';

--
-- Volcado de datos para la tabla `user_grupos`
--

INSERT INTO `user_grupos` (`idGrupo`, `NombreGrupo`, `EstadoGrupo`, `EstadoReg`) VALUES
(1, 'SUPER-ADMINISTRADOR', '1', '1'),
(2, 'ADMINISTRADOR', '1', '1'),
(3, 'NEFRÓLOGO', '1', '1'),
(4, 'SUPERVISOR', '1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(10) NOT NULL COMMENT 'ID CORRE usuario',
  `Usuario` varchar(8) NOT NULL COMMENT 'nombre usuario &#039;angelapa&#039;',
  `NivelUsuario` int(10) NOT NULL COMMENT 'Nivel de usuario 01:SUPER-ADMIN | 2: ADMIN | 3: NEFROLOGO | 4: SUPERVISOR',
  `Nombre` varchar(60) NOT NULL COMMENT 'Nombre completo de usuario',
  `Clave` varchar(40) DEFAULT NULL COMMENT 'Clave de acceso al sistema',
  `Estado` char(1) DEFAULT NULL COMMENT 'Identificador del estado del usuario',
  `FechaAlta` datetime DEFAULT NULL COMMENT 'Fecha de alta de usuario',
  `UltimoAcceso` datetime DEFAULT NULL,
  `EstadoReg` char(1) DEFAULT NULL,
  `FechaBaja` datetime DEFAULT NULL COMMENT 'Fecha que se dio de baja al usuario.',
  `UsrBaja` varchar(8) DEFAULT NULL,
  `TmpSesion` tinyint(4) DEFAULT '120' COMMENT 'Tiempo Sesion (default 5) min',
  `FechaRegistro` datetime DEFAULT NULL COMMENT 'Fecha de registro de usuario en la base de datos',
  `UsrRegistro` varchar(8) DEFAULT NULL COMMENT 'Usuario de Registro',
  `FechaModifica` datetime DEFAULT NULL COMMENT 'Fecha de ultima modificacion',
  `UsrModifica` varchar(8) DEFAULT NULL COMMENT 'Usuario de ultima modif.',
  `Oricenasicod` char(1) DEFAULT NULL COMMENT 'Origen de centro',
  `Cenasicod` varchar(3) NOT NULL COMMENT 'Codigo de centro'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Usuarios del sistema' PACK_KEYS=0;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `Usuario`, `NivelUsuario`, `Nombre`, `Clave`, `Estado`, `FechaAlta`, `UltimoAcceso`, `EstadoReg`, `FechaBaja`, `UsrBaja`, `TmpSesion`, `FechaRegistro`, `UsrRegistro`, `FechaModifica`, `UsrModifica`, `Oricenasicod`, `Cenasicod`) VALUES
(1, 'ESSALUD', 1, 'PEREZ ASTO, ANGELA THALIA', '81dc9bdb52d04dc20036dbd8313ed055', '1', '2018-09-02 22:14:52', '2018-09-03 09:38:32', '1', NULL, NULL, 120, '2018-09-02 22:14:52', 'ADMIN1', '2018-09-03 13:07:45', 'ESSALUD', '1', '401'),
(2, 'ALEXM', 1, 'MANCILLA FLORES, ALEXANDER', '81dc9bdb52d04dc20036dbd8313ed055', '1', '2018-09-03 13:04:07', NULL, '1', NULL, NULL, 120, '2018-09-03 13:04:07', 'ESSALUD', NULL, NULL, '1', '401'),
(3, 'ADMIN', 2, 'CASTILLO COVEÑAS, KHATERINE', '81dc9bdb52d04dc20036dbd8313ed055', '1', '2018-09-03 11:21:54', NULL, '1', NULL, NULL, 120, '2018-09-03 11:21:54', 'ESSALUD', '2018-09-03 13:07:50', 'ESSALUD', '1', '401'),
(4, 'NEFRO', 3, 'GARCIA LUNA, MARCO', '81dc9bdb52d04dc20036dbd8313ed055', '1', '2018-09-03 11:22:59', NULL, '1', NULL, NULL, 120, '2018-09-03 11:22:59', 'ESSALUD', '2018-09-03 13:10:02', 'ESSALUD', '1', '401'),
(5, 'SUPER', 4, 'VENEGAS COLLADO, KEVIN', '81dc9bdb52d04dc20036dbd8313ed055', '1', '2018-09-03 11:24:10', NULL, '1', NULL, NULL, 120, '2018-09-03 11:24:10', 'ESSALUD', NULL, NULL, '1', '401'),
(6, 'CAMPOSB', 1, 'CAMPOS VIQUE MARIA BELEN ', '81dc9bdb52d04dc20036dbd8313ed055', '1', '2018-09-03 13:15:09', NULL, '1', NULL, NULL, 120, '2018-09-03 13:15:09', 'ESSALUD', NULL, NULL, '1', '005'),
(7, 'ASTO', 1, 'ASTO LLANOS.LUZ', '81dc9bdb52d04dc20036dbd8313ed055', '1', '2018-09-03 13:17:06', NULL, '1', NULL, NULL, 120, '2018-09-03 13:17:06', 'ESSALUD', NULL, NULL, '1', '002'),
(8, 'PEREZ', 1, 'PEREZ APARICIO MANUEL', '81dc9bdb52d04dc20036dbd8313ed055', '1', '2018-09-03 13:23:02', NULL, '1', NULL, NULL, 120, '2018-09-03 13:23:02', 'ESSALUD', '2018-09-03 13:26:04', 'ESSALUD', '1', '001');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cmcas10`
--
ALTER TABLE `cmcas10`
  ADD PRIMARY KEY (`CENASICOD`,`ORICENASICOD`) USING BTREE;

--
-- Indices de la tabla `cnsr_datos_av`
--
ALTER TABLE `cnsr_datos_av`
  ADD PRIMARY KEY (`PK_ACCESO_V`),
  ADD KEY `COD_PACIENTE` (`COD_PACIENTE`),
  ADD KEY `COD_CENCLI` (`COD_CENCLI`);

--
-- Indices de la tabla `cnsr_estructura`
--
ALTER TABLE `cnsr_estructura`
  ADD PRIMARY KEY (`COD_CENCLI`);

--
-- Indices de la tabla `cnsr_evaluaciones_av`
--
ALTER TABLE `cnsr_evaluaciones_av`
  ADD PRIMARY KEY (`PK_EVALUACION`),
  ADD KEY `PK_ACCESO_V` (`PK_ACCESO_V`),
  ADD KEY `COD_PROFESIONAL` (`COD_PROFESIONAL`);

--
-- Indices de la tabla `cnsr_maestro`
--
ALTER TABLE `cnsr_maestro`
  ADD PRIMARY KEY (`COD_TIPO`);

--
-- Indices de la tabla `cnsr_mov_paciente`
--
ALTER TABLE `cnsr_mov_paciente`
  ADD PRIMARY KEY (`ID_MOV`),
  ADD KEY `fk_cod_paciente` (`COD_PACIENTE`);

--
-- Indices de la tabla `cnsr_paciente`
--
ALTER TABLE `cnsr_paciente`
  ADD PRIMARY KEY (`COD_PACIENTE`);

--
-- Indices de la tabla `cnsr_profesional`
--
ALTER TABLE `cnsr_profesional`
  ADD PRIMARY KEY (`COD_PROFESIONAL`),
  ADD KEY `CENASICOD` (`CENASICOD`),
  ADD KEY `USUARIO_BAJA` (`USUARIO_BAJA`);

--
-- Indices de la tabla `cnsr_valfisica_fistula`
--
ALTER TABLE `cnsr_valfisica_fistula`
  ADD PRIMARY KEY (`PK_EVALUACION`);

--
-- Indices de la tabla `user_grupos`
--
ALTER TABLE `user_grupos`
  ADD PRIMARY KEY (`idGrupo`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`),
  ADD KEY `fk_idgrupo` (`NivelUsuario`),
  ADD KEY `Cenasicod` (`Cenasicod`),
  ADD KEY `Oricenasicod` (`Oricenasicod`),
  ADD KEY `Usuario` (`Usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cnsr_datos_av`
--
ALTER TABLE `cnsr_datos_av`
  MODIFY `PK_ACCESO_V` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `cnsr_evaluaciones_av`
--
ALTER TABLE `cnsr_evaluaciones_av`
  MODIFY `PK_EVALUACION` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Indice de evaluacion', AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT de la tabla `cnsr_maestro`
--
ALTER TABLE `cnsr_maestro`
  MODIFY `COD_TIPO` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Indice', AUTO_INCREMENT=290;

--
-- AUTO_INCREMENT de la tabla `cnsr_mov_paciente`
--
ALTER TABLE `cnsr_mov_paciente`
  MODIFY `ID_MOV` int(10) NOT NULL AUTO_INCREMENT COMMENT 'PK de la tabla', AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `cnsr_paciente`
--
ALTER TABLE `cnsr_paciente`
  MODIFY `COD_PACIENTE` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Cod. Paciente', AUTO_INCREMENT=2147;

--
-- AUTO_INCREMENT de la tabla `cnsr_profesional`
--
ALTER TABLE `cnsr_profesional`
  MODIFY `COD_PROFESIONAL` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Cod. Profesional', AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `user_grupos`
--
ALTER TABLE `user_grupos`
  MODIFY `idGrupo` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID CORRE GRUPO', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID CORRE usuario', AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cnsr_datos_av`
--
ALTER TABLE `cnsr_datos_av`
  ADD CONSTRAINT `COD_CENCLI` FOREIGN KEY (`COD_CENCLI`) REFERENCES `cnsr_estructura` (`COD_CENCLI`),
  ADD CONSTRAINT `COD_PACIENTE` FOREIGN KEY (`COD_PACIENTE`) REFERENCES `cnsr_paciente` (`COD_PACIENTE`);

--
-- Filtros para la tabla `cnsr_evaluaciones_av`
--
ALTER TABLE `cnsr_evaluaciones_av`
  ADD CONSTRAINT `COD_PROFESIONAL` FOREIGN KEY (`COD_PROFESIONAL`) REFERENCES `cnsr_profesional` (`COD_PROFESIONAL`),
  ADD CONSTRAINT `PK_ACCESO_V` FOREIGN KEY (`PK_ACCESO_V`) REFERENCES `cnsr_datos_av` (`PK_ACCESO_V`);

--
-- Filtros para la tabla `cnsr_mov_paciente`
--
ALTER TABLE `cnsr_mov_paciente`
  ADD CONSTRAINT `fk_cod_paciente` FOREIGN KEY (`COD_PACIENTE`) REFERENCES `cnsr_paciente` (`COD_PACIENTE`);

--
-- Filtros para la tabla `cnsr_valfisica_fistula`
--
ALTER TABLE `cnsr_valfisica_fistula`
  ADD CONSTRAINT `PK_EVALUACION` FOREIGN KEY (`PK_EVALUACION`) REFERENCES `cnsr_evaluaciones_av` (`PK_EVALUACION`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_nivel_usuario` FOREIGN KEY (`NivelUsuario`) REFERENCES `user_grupos` (`idGrupo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
